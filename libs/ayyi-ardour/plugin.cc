/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <cerrno>
#include <cstdlib>
#include <signal.h>
#include <unistd.h>

#include <gtk/gtk.h>
#include <pbd/xml++.h>
#include <pbd/pthread_utils.h>
#include <sigc++/bind.h>

#include <ardour/ardour.h>

#include <ardour/tempo.h>
#include <ardour/audio_track.h>
#include <ardour/session_playlist.h>
#include <ardour/diskstream.h>
#include <ardour/playlist.h>
#include <ardour/audioplaylist.h>
#include <ardour/audioregion.h>
#include <ardour/region.h>
#include <ardour/region_factory.h>
#include <ardour/audiofilesource.h>
#include <ardour/session.h>
#include <ardour/bundle.h>

#include <ardour/plugin.h>
#include <ardour/plugin_manager.h>
#include <ardour/plugin_insert.h>
#include <ardour/ladspa_plugin.h>
#ifdef VST_SUPPORT
#include <ardour/vst_plugin.h>
#endif

#include <ayyi/ayyi_time.h>

namespace Ayi {
	template<class T> class MixerTrack;
}
extern int debug;
extern int debug_general;
extern int debug_plugins;

#include "ayyi/utils.h"
#include "ayyi/interface.h" //testing
#include "plugin.h"

#define dbg(A, ...) dbgprintf("AyyiPlugin", __func__, (char*)A, ##__VA_ARGS__)
#define warn(A, ...) warnprintf2("AyyiPlugin", __func__, (char*)A, ##__VA_ARGS__)
#define err(A, ...) errprintf2("AyyiPlugin", __func__, (char*)A, ##__VA_ARGS__)

namespace Ayi {

using namespace Ayi;
using namespace std;
using namespace ARDOUR;
using boost::shared_ptr;

template<class T, class P>
Ayi::ShmContainer<struct _ayyi_plugin_nfo>* AyyiPlugin<T, P>::plugins;

/**
 *
 *  AyyiPlugin represents a PluginInfo, not a plugin instantiation.
 *
 */
template<class T, class P>
AyyiPlugin<T, P>::AyyiPlugin(T* parent_plugin, ShmSegDefn* segment)
	: Shared<P>(segment), controls(segment)
{
	if(debug_plugins>1) dbg("%s", parent_plugin->name.c_str());

	parent_plugin->ad1_plugin = this;

	this->container = AyyiPlugin::plugins;

	P* plugin = AyyiPlugin::plugins->add_item_with_idx();
	//_ayyi_plugin_nfo* item = (_ayyi_base_item*)plugin;
	//set_shm(item);
	{
		this->shm = plugin;

		this->slot = this->shm->shm_idx % BLOCK_SIZE;
		this->s1   = this->shm->shm_idx / BLOCK_SIZE;
	}

	snprintf(plugin->name, 255, "%s", parent_plugin->name.c_str());
	strncpy(plugin->category, parent_plugin->category.c_str(), 63);
	plugin->n_inputs  = parent_plugin->n_inputs.get(ARDOUR::DataType::AUDIO);
	plugin->n_outputs = parent_plugin->n_outputs.get(ARDOUR::DataType::AUDIO);
	plugin->latency   = 0; //FIXME is a plugin NOT plugininfo thing. Fill this field following 1st instantiation.

#if 0
	//TODO is this still needed?
	struct block* block = this->container->block[this->s1];
	block->slot[this->slot] = plugin;                 //register the plugin in the index.
	if (debug_plugins>1) dbg("block=%x last=%i", (char*)block-(char*)this->base, block->last);
#endif

	if (debug_plugins) dbg("initialising controls... plugin=%p controls=%p", plugin, &plugin->controls);
	controls.init(&plugin->controls, "Controls");
}
template AyyiPlugin<ARDOUR::PluginInfo, struct _ayyi_plugin_nfo>::AyyiPlugin(ARDOUR::PluginInfo* parent_plugin, ShmSegDefn* segment);


template<class T, class P>
void
AyyiPlugin<T, P>::set_controls(boost::shared_ptr<ARDOUR::Plugin> plugin)
{
	//once a plugin is loaded, we can put a list of its controls into shm.

	if(controls.slot_is_used(0)) return; //controls are already loaded.

	set<Evoral::Parameter> params = plugin->automatable();
	for(set<Evoral::Parameter>::iterator p = params.begin(); p != params.end(); p++){
		if(debug_plugins) dbg("type=%i id=%i", (*p).type(), (*p).id());
		struct _ayyi_control* control = controls.add_item();
		if(control){
			//dbg("control=%p", control);
			//strncpy(control->name, (*p).to_string().c_str(), 31);
			strncpy(control->name, parameter_to_string((*p).type(), (*p).id()).c_str(), 31);
			//control->type = (*p).type();
		}
	}
}
template void AyyiPlugin<ARDOUR::PluginInfo, struct _ayyi_plugin_nfo>::set_controls(boost::shared_ptr<ARDOUR::Plugin> plugin);


template<class T, class P>
AyyiPlugin<T, P>*
AyyiPlugin<T, P>::find_plugin(ARDOUR::PluginInfoPtr info)
{
	AyyiPlugin<ARDOUR::PluginInfo, Ayi::plugin_shared>* plugin = NULL;

	ARDOUR::PluginInfoList& pluginlist = PluginManager::instance().windows_vst_plugin_info();
	for (ARDOUR::PluginInfoList::iterator x = pluginlist.begin(); x != pluginlist.end(); x++) {
		//dbg(0, "  %s", (*x)->name.c_str());
		if ((*x)->unique_id == info->unique_id) return (*x)->ad1_plugin;
	}

	warn("plugin object not found. info='%s'\n", info->name.c_str());
	return plugin;
}
template AyyiPlugin<ARDOUR::PluginInfo, Ayi::_ayyi_plugin_nfo>* AyyiPlugin<ARDOUR::PluginInfo, Ayi::_ayyi_plugin_nfo>::find_plugin(ARDOUR::PluginInfoPtr);


template<class T, class P>
AyyiPlugin<T, P>*
AyyiPlugin<T, P>::find_plugin(boost::shared_ptr<Plugin> plugin)
{
	PluginInfoPtr info = plugin->get_info();
	return info->ad1_plugin;
}
template AyyiPlugin<ARDOUR::PluginInfo, Ayi::_ayyi_plugin_nfo>* AyyiPlugin<ARDOUR::PluginInfo, Ayi::_ayyi_plugin_nfo>::find_plugin(boost::shared_ptr<Plugin>);


#include <pbd/compose.h>
template<class T, class P>
std::string
AyyiPlugin<T, P>::parameter_to_string(uint32_t _type, uint32_t _id)
{
	//temporary fn duplicating the one removed from ARDOUR::Parameter

    if (_type == GainAutomation) {
        return "gain";
    } else if (_type == PanAzimuthAutomation) {
        return string_compose("pan-%1", _id);
    } else if (_type == SoloAutomation) {
        return "solo";
    } else if (_type == MuteAutomation) {
        return "mute";
    } else if (_type == FadeInAutomation) {
        return "fadein";
    } else if (_type == FadeOutAutomation) {
        return "fadeout";
    } else if (_type == EnvelopeAutomation) {
        return "envelope";
    } else if (_type == PluginAutomation) {
        return string_compose("parameter-%1", _id);
#if 0
    } else if (_type == MidiCCAutomation) {
        return string_compose("midicc-%1-%2", _channel, _id);
#endif
    } else {
        assert(false);
        PBD::warning << "Uninitialized Parameter to_string() called." << endmsg;
        return "";
    }
}


} //end namespace
