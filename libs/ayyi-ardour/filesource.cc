/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <cerrno>
#include <cstdlib>
#include <signal.h>
#include <unistd.h>

#include <glibmm/miscutils.h>
#include <glibmm/fileutils.h>
#include <pbd/pthread_utils.h>
extern "C" {
#include "tlsf/src/tlsf.h"
}

#include <ardour/ardour.h>

#include <ardour/tempo.h>
#include <ardour/audio_track.h>
#include <ardour/midi_track.h>
#include <ardour/session_playlist.h>
#include <ardour/diskstream.h>
#include <ardour/playlist.h>
#include <ardour/audioplaylist.h>
#include <ardour/audioregion.h>
#include <ardour/midi_region.h>
#include <ardour/region.h>
#include <ardour/region_factory.h>
#include <ardour/audiofilesource.h>
#include <ardour/silentfilesource.h>
#include <ardour/session.h>
#include <ardour/session_directory.h>
#include <ardour/bundle.h>
#include <ardour/plugin.h>

#include <sigc++/bind.h>

#include <ayyi/ayyi_time.h>
#include <ayyi/ayyi_types.h>
#include "ayyi/list.h"
#include "ayyi/utils.h"
#include "ayyi/inc/song.h"

extern int debug_shm;
extern int debug_source;
extern int debug;

#include "ardourd/dbus_object.h"
#include "ayyi-ardour/mixer_track.h"
#include "ayyi-ardour/filesource.h"

#define dbg(N, A, ...) { if (N <= debug_source) dbgprintf("AyyiFilesource", __func__, (char*)A, ##__VA_ARGS__); }
#define err(A, ...) errprintf2("AyyiFilesource", __func__, (char*)A, ##__VA_ARGS__)

namespace Ayi {

using namespace Ayi;
using namespace std;
using namespace ARDOUR;
using boost::shared_ptr;

gnash::Shm* AyyiFilesource::shm;
Ayi::ShmContainer<struct Ayi::_filesource_shared>* AyyiFilesource::filesources;


/*
 * initialise the shm-shared part of the fileSource class.
 *
 * currently, the object is not announced() until after construction of the peak file is finished.
 *
 * warning! ardour session does not set the length() property!
 *
 */
AyyiFilesource::AyyiFilesource(AudioFileSource* parent)
	: Shared<_filesource_shared>(AyyiFilesource::shm)
{
	object_type = AYYI_OBJECT_FILE;
	container = AyyiFilesource::filesources;

	PBD::ID parent_id = parent->id();

	if(debug_source) p(0, "%sAyyiFilesource::AyyiFilesource%s", bold, white);
	/*

	testing for 'valid' files
	-------------------------

	is the file in any regions?
		:-( looking in the region list is not reliable, as the regionlist hasnt been made at startup.
		boost::shared_ptr<AudioRegion> region = region_find_by_source_id(parent_id.get_id());
		if(region) printf("  yes\n");

	'internal' files - these do not have the 'origin' property. We expect not to be instantiated for these types.

	these files have channel count 0 - can that be used as a test?

	*/

	// warning: get_filesize() will return 0 when the file is in the process of being imported.
	std::string base_name = std::string(Glib::path_get_basename(parent->name()));
	if (get_filesize(parent->name()) < 512 && (base_name.substr(base_name.length() - 6) == "-1.wav"/* || base_name.substr(base_name.length() - 6) == "-2.wav"*/)) {
		filesource = NULL;
		dbg(debug_source, "%s <Audio> use_count=%i length=%i - ignoring...", parent->name().c_str(), parent->used(), get_filesize(parent->name()));
		return;
	}
	dbg(1, "%s id=%Li use_count=%i", parent->name().c_str(), parent_id.get_id(), parent->used());

	if(filesources->lookup_by_id(parent_id.get_id()) != 0){ err("dupe id! %s\n", parent->name().c_str()); throw string("shared source already exists."); }

	_filesource_shared* shared = filesource = filesources->add_item_with_idx();
	set_shm((_ayyi_base_item*)shared);
	if(!shared){ err("out of memory?"); return; }
#if 0
	if(debug_source) core->song.osong->report();
#endif

	snprintf(filesource->name, AYYI_FILENAME_MAX - 1, "%s", parent->name().c_str());
	shared->id        = parent_id.get_id();
	shared->object    = (void*)parent;
	shared->length    = parent->length(0);

	dbg(1, "origin=%s", parent->origin().c_str());
	gchar* basename = g_path_get_basename(parent->origin().c_str());
	g_strlcpy(shared->original_name, basename, AYYI_NAME_MAX);
	g_free(basename);

	parent->PeaksReady.connect_same_thread(signal_connections, boost::bind(&AyyiFilesource::peak_ready, this));
	parent->DropReferences.connect_same_thread(signal_connections, boost::bind(&AyyiFilesource::going_away, this));
}


AyyiFilesource::AyyiFilesource()
	: Shared<_filesource_shared>(AyyiFilesource::shm)
{
	//Make an empty object. Only for testing.

#if 0
	char* shm_address = (char*)core->song.shm + sizeof(core->song.shm);
	if(!shm_address){ err("shm not initialised!\n"); return; }
#endif

	object_type = AYYI_OBJECT_FILE;
	container = AyyiFilesource::filesources;

	_filesource_shared* shared = filesource = filesources->add_item_with_idx();
	slot = filesource->shm_idx;

#if 0
	if(debug_source) core->song.osong->report();
#endif

	g_strlcpy(shared->name, "test file", AYYI_FILENAME_MAX);
	shared->id      = 123456789;
	shared->object  = (void*)0x8000000;
	shared->length  = 44100;

	is_announced = true; // ok to send signals now.
}


AyyiFilesource::~AyyiFilesource()
{
	p(1, "%sAyyiFilesource::~AyyiFilesource()...%s", blue, white);
	dbg(1, "filesource=%p", filesource);

	if(filesource){
		filesources->remove_item(get_index());

		//g_return_if_fail(filesource);
		memset(filesource, 0, sizeof(*filesource));
	}
	//string name(filesource->name);
	//dbg(1, "'%s' done. last=%i", filesource->name, core->song.shm->filesources.block[0]->last);
}


void
AyyiFilesource::going_away()
{
	if(0) dbg(0, "...");
}


ARDOUR::AudioFileSource*
AyyiFilesource::get_filesource(ObjIdx pod_index)
{
	filesource_shared* shared = filesources->get_item(pod_index);
	if (!shared) { dbg(0, "failed to get AyyiRegion. region_num=%u", pod_index); return NULL; }

	ARDOUR::AudioFileSource* file = (ARDOUR::AudioFileSource*)shared->object;
	if (!file) { dbg(0, "failed to get region object. region_num=%u", pod_index); return NULL; }
	//dbg(0, "pod_index=%u", pod_index);
	return file;
}


boost::shared_ptr<Source>
AyyiFilesource::get_filesource_boost(ObjIdx slot_num)
{
	boost::shared_ptr<Source> ret;

	ARDOUR::AudioFileSource* src = get_filesource(slot_num);
	if(src){
		dbg(1, "name=%s", src->name().c_str());
		PBD::ID id = src->id();
		dbg(1, "id=%Lu", id.get_id());
		//boost::shared_ptr<Source> a = src->shared_from_this(); TODO why doesnt this work?
		return Session::instance->source_by_id(src->id());
	}
	return ret;
}


int64_t
AyyiFilesource::get_filesize(std::string name)
{
	char path[256];

	if(name[0] != '/'){
		std::string audio_dir = Session::instance->session_directory().sound_path();
		snprintf(path, 255, "%s/%s", audio_dir.c_str(), name.c_str());
	}
	else g_strlcpy(path, name.c_str(), 256);

	return filesize(path);
}


bool
AyyiFilesource::kill(int slot_num)
{
	//there is a slight danger that the killed slot be reused, then cleared by the filesource destructor when it finally runs.

	boost::shared_ptr<ARDOUR::Source> source = get_filesource_boost(slot_num);
	if(source){
		ARDOUR::AudioFileSource* audio_source = dynamic_cast<AudioFileSource*>(source.get());
		//AyyiFilesource* sh = audio_source->shared_obj;
		audio_source->shared_obj = NULL;
	}else{
		//this is what we normally get...

		dbg(0, "ARDOUR::Source not found. slot_num=%i - if deleted, why destructor not run?", slot_num);
#if 0
		core->print_files();
#endif
	}

	filesource_shared* shm = AyyiFilesource::filesources->get_item(slot_num);
	if(debug_source) printf("  deleting... (%s)\n", shm->name);
	//delete sh; //segfault!
	//...as deleting the object segfaults, we just clear remove it from the shm container:
	filesources->remove_item(slot_num);

	return true;
}


void
AyyiFilesource::peak_ready()
{
	dbg(1, "...");
	emit_changed();
}


} //end namespace Ayi
