/*
    Copyright (C) 2006-2012 Tim Orford 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id: playlist.h 933 2006-09-28 17:23:52Z paul $
*/

#include <sys/stat.h>
#include <string>
#include <set>
#include <map>
#include <list>
#include <boost/shared_ptr.hpp>
#include <glib.h>
#include <sigc++/signal.h>

#include <pbd/undo.h>
#include <pbd/stateful.h> 
#include <pbd/statefuldestructible.h> 

#include <ardour/ardour.h>
#include <ardour/location.h>
#include <ardour/playlist.h>
#include <ardour/audioplaylist.h>

#include "ayyi-ardour/shm.h"

using namespace std;
namespace Ayi {
    typedef struct _ayyi_shm_playlist playlist_shared;
}

namespace ARDOUR  {

class Session;
class Region;
class AudioRegion;
class Source;
class Track;

class AyiPlaylist : public AudioPlaylist, public Ayi::Shared<struct Ayi::_ayyi_shm_playlist>
{
  public:
	Ayi::playlist_shared* shared_; //shm POD 

	AyiPlaylist (Session&, const XMLNode&, bool hidden = false);
	AyiPlaylist (Session&, string name, bool hidden = false);
	AyiPlaylist (boost::shared_ptr<const AudioPlaylist>, string name, bool hidden = false);
	AyiPlaylist (boost::shared_ptr<const AudioPlaylist> other, ARDOUR::framecnt_t start, ARDOUR::framecnt_t cnt, string name, bool hidden = false);
	~AyiPlaylist();

	void                                     add_pod();
	void                                     remove_pod();
	void                                     remove_region(boost::shared_ptr<Region> region);

	void                                     name_changed();

	/*const*/ boost::shared_ptr<AudioRegion> find_region_by_id      (uint64_t);
	bool                                     has_region_starting_at (framepos_t const p) const;
	boost::shared_ptr<ARDOUR::Track>         find_track             ();
	void                                     set_track(boost::shared_ptr<ARDOUR::Track>);

	static gnash::Shm*                       shm;
	static Ayi::ShmContainer<struct Ayi::_ayyi_shm_playlist>* playlists;
	static bool                              kill                   (int slot);
	static AudioRegion*                      find_region            (uint64_t id);
	static boost::weak_ptr<ARDOUR::Playlist> lookup_by_trackname    (string);

  protected:
};

class AyiAudioPlaylist : /*public AudioPlaylist,*/ public AyiPlaylist
{
  public:
	AyiAudioPlaylist (Session&, const XMLNode&, bool hidden = false);
};

} /* namespace ARDOUR */


