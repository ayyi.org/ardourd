/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2006-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <set>
#include <fstream>
#include <algorithm>
#include <unistd.h>
#include <cerrno>
#include <string>
#include <climits>

#include <sigc++/bind.h>
#include <ardour/ardour.h>
#include <ardour/audioregion.h>
#include <ardour/midi_track.h>
#include <ardour/audiofilesource.h>
#include <ardour/plugin_manager.h>

#include <ayyi/ayyi_time.h>
namespace Ayi {
	template<class T> class MixerTrack;
}
#include <ayyi/ayyi_types.h>
#include "ayyi/utils.h"
#include "ayyi-ardour/route.h"
#include "ayyi-ardour/mixer_track.h"
#include "ayyi-ardour/plugin.h"
#include "ayyi-ardour/control.h"

extern int debug;

using namespace std;
using namespace ARDOUR;
using namespace PBD;
using namespace Ayi;

#define dbg(N, A, ...) { if (N <= debug) dbgprintf("AyiControl", __func__, (char*)A, ##__VA_ARGS__); }
#define err(A, ...) errprintf2("AyiControl", __func__, (char*)A, ##__VA_ARGS__)

namespace Ayi {

AyiControl::AyiControl(ARDOUR::AutomationControl* parent)
	: Shared<struct _ayyi_control>(AyyidRoute::shm.mixer)
{
	// if the ARDOUR::AutomationControl is not for a plugin, an AyiControl object will exist,
	// but must not be accessed as it is not initialised.
	if(parent->name() == "gaincontrol") return;
	if(parent->name() == "trimcontrol") return;
	if(parent->name() == "solo") return;
	if(parent->name() == "mute") return;
	if(parent->name() == "solo-iso") return;
	if(parent->name() == "solo-safe") return;
	//if(parent->name() == "pan-azimuth") return;
	if(parent->name() == "pan-elevation") return;
	if(parent->name() == "pan-width") return;
	if(parent->name() == "pan-frontback") return;
	if(parent->name() == "pan-lfe") return;
	if(parent->name() == "recenable") return;

	p(0, "%sAyiControl::AyiControl%s: control.name=%s", bold, white, parent->name().c_str());

#if 0
	gnash::MixerShm* seg = core->mixer.omixer;

	int plugin_idx = 0; //FIXME
	int b = plugin_idx / BLOCK_SIZE;

	if(!core->mixer.omixer->plugins.count_items()){
	}
	_ayyi_plugin_nfo* plugin2 = core->mixer.omixer->plugins.get_item(plugin_idx);
	if(!plugin2){
		//this is normal if plugins are not enabled.
		dbg(1, "cannot get plugin_info. aborting...");
		return;
	}

	struct _container* plugin_container = &core->mixer.shm->plugins;
	void** plugin_table = plugin_container->block[b]->slot; //TODO use api instead.
	_ayyi_plugin_nfo* plugin = (_ayyi_plugin_nfo*)plugin_table[plugin_idx];

	//plugins->print();
	container = &ayyi_plugin->controls;
	block* block = container->block[b];
	if(!block){
		dbg(1, "controls container not initialised?");
#if 0 // block_init was removed  - it should no longer be needed.
		block = core->block_init(container, core->song.osong);
#endif
	}
	slot = this->next_available_slot();

	if((shm_pod = (struct _ayyi_control*)seg->malloc_tlsf(sizeof(struct _ayyi_control), container))){
		strcpy(shm_pod->name, "unnamed");

		object_type = AYYI_OBJECT_AUTO;

		block->slot[slot] = shm_pod;           //register the control in the index.
	}
#else
														// TODO we want to use Container::next instead.
														//      for that we need the plugin object
														//      but we dont have it and may not be able to look it up yet.
														//      * perhaps we need to delay shm initialisation *

					ARDOUR::PluginInfoList list = PluginManager::instance().windows_vst_plugin_info();
					if(list.size()){
						boost::shared_ptr<ARDOUR::PluginInfo> info = list.front(); // FIXME use correct plugin
						AyyiPlugin<ARDOUR::PluginInfo, Ayi::plugin_shared>* ayyi_plugin = AyyiPlugin<ARDOUR::PluginInfo, Ayi::plugin_shared>::find_plugin(info);
	//Ayi::ShmContainer<Ayi::_ayyi_plugin_nfo>* plugins = core->mixer.omixer->plugins;
	//AyyiPlugin* plugin = plugins->;
	container = &ayyi_plugin->controls;
	shm_pod = container->add_item();
					}
#endif
}


#if 0
int
AyiControl::next_available_slot()
{
	/*
		-returns the index of the next free POD slot.

		-this "new" version is for containerised objects, and updates the "last" field in the parent block.

		-DEPRECATED! use ShmContainer::next_available_slot() instead.
	*/

	void** pod_table = (void**)container->block[0]->slot;

	if(!pod_table){ err("pod_table not set! object_type=%i\n", object_type); return -1; }

	for(int slot=0; slot<BLOCK_SIZE; slot++){
		if(!pod_table[slot]){

			struct block* block = container->block[s1];
			block->last = MAX(block->last, slot);
			//printf("shared<P>::%s(): slot=%i s1=%i last=%i\n", __func__, slot, s1, block->last);

			return slot;
		}
	}

	errprintf((char*)"shared<P>::%s(): no free slots!\n", __func__);
	return -1;
}
#endif

} //end namespace

