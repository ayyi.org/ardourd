/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2019 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __shared_route_h_
#define __shared_route_h_
#include <ardour/track.h>
#include <ardour/audio_track.h>

namespace Ayi {
class ShmSegDefn;
template<class T> class MixerTrack;
}
#include <ayyi/inc/song.h>
#include "ayyi-ardour/shared.h"
#include "ayyi-ardour/shm.h"

namespace Ayi {

class AyyidRoute : public Shared<_route_shared>
{
  public:
	                      AyyidRoute              (ARDOUR::Route* parent);
	                     ~AyyidRoute              ();
	struct _route_shared* route;
	MixerTrack<ARDOUR::Route>* mixer_track;
	void                  going_away              ();
	void                  set_outputs             (char* s);
	void                  input_changed           (ARDOUR::IOChange change, void* src);
	void                  output_changed          (ARDOUR::IOChange change, void* src);
	void                  ports_created           (ARDOUR::ChanCount);
	void                  modify_output           (unsigned t);
	void                  mute_changed            ();
	//void                  solo_changed          (bool self_solo_change, void*, boost::weak_ptr<ARDOUR::Route>);
	void                  solo_changed            (bool);
	void                  arm_changed             ();
	void                  name_changed            ();
	void                  playlist_changed        (boost::weak_ptr<ARDOUR::Track>);
	void                  on_ready                ();
	void                  set_plugin_automation   ();
	void                  vol_changed             ();
	void                  vol_automation_changed  ();
	void                  pan_automation_changed  ();
	void                  automation_changed      ();
	void                  automation_slot_init    (uint32_t plugin_slot);
	void                  add_automation_type     (uint32_t plugin_slot, uint32_t control_idx);

	static boost::shared_ptr<Evoral::ControlList> processor_get_automationlist(boost::shared_ptr<ARDOUR::Processor>, guint control_idx);

	AyyiList*             input_routing;
	AyyiList*             output_routing;

	void                  print_routing           ();

	static ARDOUR::AudioTrack* get_track          (ObjIdx);
	static void           route_del               (ARDOUR::Route*);
	       void           route_del2              (ARDOUR::Route*);
	static struct _Shm {
	    gnash::Shm*       mixer;
	}                     shm;
	static Ayi::ShmContainer<struct _route_shared>* routes;

  protected:
	void                  set_flag                (int flag, bool val);
	PBD::ScopedConnection panner_connection;

	void                  set_vol_automation      ();
	void                  set_pan_automation      ();
};

} //namespace

#endif // __shared_route_h_
