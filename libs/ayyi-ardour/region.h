/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __ayyi_region_h__
#define __ayyi_region_h__

#include "ardour/region.h"
#include "ardour/audioregion.h"
#include "ayyi/inc/song.h"
#include "shared.h"
#include "shm.h"

namespace Ayi {

class AyyiRegion : public Shared<struct _region_shared>
{
  public:
	                       AyyiRegion            ();
	                      ~AyyiRegion            ();
	void                   state_changed         (const PBD::PropertyChange& what_changed, boost::weak_ptr<ARDOUR::Region>);
	void                   on_removed2           (boost::weak_ptr<ARDOUR::Region>);
	bool                   on_removed4           ();
	void                   playlist_changed      ();
	void                   playlist_name_changed ();
	void                   track_name_changed    (char*);

	void                   ensure_source         ();

	struct _region_base_shared* shm_pod;

	static gnash::Shm*     shm;
	static Ayi::ShmContainer<struct _region_shared>* regions;

  protected:
	PBD::ScopedConnectionList playlist_connections;
};

class AyyiAudioRegion : public AyyiRegion
{
  public:
	                       AyyiAudioRegion       (ARDOUR::AudioRegion*);
	                      ~AyyiAudioRegion       ();
	bool                   del                   ();

	static bool            kill                  (ObjIdx);
	static ARDOUR::AudioRegion* get_region       (ObjIdx);
};

};

#endif //__ayyi_region_h__
