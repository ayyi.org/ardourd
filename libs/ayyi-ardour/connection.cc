/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2025 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include <sys/types.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <cerrno>
#include <cstdlib>
#include <signal.h>
#include <unistd.h>

#include <pbd/xml++.h>
#include <pbd/pthread_utils.h>

#include <ardour/ardour.h>
#include <ardour/session.h>
#include <ardour/audio_track.h>
#include <ardour/midi_track.h>
#include <ardour/bundle.h>
#include <ardour/auto_bundle.h>
#include <ardour/plugin.h>
#include <ardour/panner.h>
#include <ardour/send.h>

#include <sigc++/bind.h>

#include <ayyi/ayyi_time.h>
#include <ayyi/utils.h>
#include <ayyi/list.h>
#include <ayyi/ayyi_types.h>
#include <ayyi/inc/song.h>

#include "ayyi-ardour/mixer_track.h"
#include "ayyi-ardour/connection.h"

extern int debug_bundle;
#define dbg(N, A, ...) { if (N <= debug_bundle) dbgprintf("AyyiConnection", __func__, (char*)A, ##__VA_ARGS__); }
#define warn(A, ...) warnprintf2("AyyiConnection", __func__, (char*)A, ##__VA_ARGS__)
#define err(A, ...) errprintf2("AyyiConnection", __func__, (char*)A, ##__VA_ARGS__)

namespace Ayi {
using namespace ARDOUR;


ShmSegDefn* AyyiConnection::base;
Ayi::ShmContainer<struct _connection_shared>* AyyiConnection::connections;
std::vector<AyyiConnection*> AyyiConnection::connections_without_bundles;


AyyiConnection::AyyiConnection(ARDOUR::Bundle* parent)
	: Shared<_connection_shared>(AyyiConnection::base)
{
	/*
		connections have a name, and list of ports.
		Eg a typical stereo output would look like: "out 1+2" with ports "net_pcm:playback_1" and "net_pcm:playback_2"
		Sometimes, when the ports are Ardour ports, the ports are not listed.
		The name is currently not supposed to be a complete description of the ports, just a label. This may change later.
	*/

	dbg(2, "bundle=%s", parent->name().c_str());

#if 0
	char* shm_address = (char*)core->song.shm + sizeof(_song_shm);
	if(!shm_address){ err("shm not initialised!\n"); return; }
#endif

	object_type = AYYI_OBJECT_ROUTE; //note Ayyi routes are not the same as Ardour routes.
	container = AyyiConnection::connections;

	if(!(connection = container->add_item_with_idx())) throw;
	set_shm((_ayyi_base_item*)connection);
	dbg(2, "connection=%p", connection);

	parent->shared = this;

	connection->object = (void*)&(*parent);
	connection->nports = parent->nchannels().n_total();
	connection->io     = parent->ports_are_inputs();

	parent->Changed.connect_same_thread(signal_connections, boost::bind(&AyyiConnection::on_change, this, _1));

	dbg(2, "connection=%s", connection->name);
}


AyyiConnection::AyyiConnection(string jack_port, bool io)
	: Shared<_connection_shared>(AyyiConnection::base)
{
	/*
		experimental!
		Not all jack ports appear to have ARDOUR::bundles instantiated.
		So here we create an AyyiConnection representing a raw jack port.

		param io: 0=output, 1=input
	*/
	dbg(2, "AyyiConnection::AyyiConnection (jack_port)");

	this->object_type = AYYI_OBJECT_ROUTE; //note Ayyi routes are not the same as Ardour routes.
	container = AyyiConnection::connections;

	connection_shared* shared = this->connection = container->add_item_with_idx();
	this->slot = this->connection->shm_idx % BLOCK_SIZE;

	int divider = jack_port.find(":");
	string nameL = jack_port.substr (divider ? divider + 1 : 0);
	divider = nameL.find("/");
	string name = nameL.substr (0, divider ? divider : 100);

	snprintf(shared->name, AYYI_NAME_MAX - 1, "%s{%s}", name.c_str(), nameL.c_str());
	shared->nports   = 1;
	shared->io       = io;

	//device (jack client) name:
	strncpy(shared->device, jack_port.c_str(), 32);
	char* end = strstr(shared->device, ":");
	if(end) *end = '\0';
}


AyyiConnection::AyyiConnection(string jack_port1, string jack_port2, bool io)
	: Shared<_connection_shared>(AyyiConnection::base)
{
	/*
		stereo! TODO if this works out, merge with constructor above - use list as arg?

		param io: 0=output, 1=input
	*/

	dbg(0, "AyyiConnection::AyyiConnection (stereo jack_port)");

	this->object_type = AYYI_OBJECT_ROUTE; //note Ayyi routes are not the same as Ardour routes.
	container = AyyiConnection::connections;

	connection_shared* shared = this->connection = container->add_item_with_idx();
	this->slot = this->connection->shm_idx % BLOCK_SIZE;

	int divider = jack_port1.find(":");
	string nameL = jack_port1.substr (divider ? divider + 1 : 0);
	string nameR = jack_port2.substr (divider ? divider + 1 : 0);
	divider = nameL.find("/");
	string name = nameL.substr (0, divider ? divider : 100);

	snprintf(shared->name, AYYI_NAME_MAX - 1, "%s{%s}{%s}", name.c_str(), nameL.c_str(), nameR.c_str());
	shared->nports = 2;
	shared->io     = io;

	//device (jack client) name:
	strncpy(shared->device, jack_port1.c_str(), 32);
	char* end = strstr(shared->device, ":");
	if(end) *end = '\0';
}


AyyiConnection::~AyyiConnection()
{
	if(debug_bundle) p(0, "%sAyyiConnection::~AyyiConnection()...%s", blue, white);

	container->remove_item(get_index());

	memset(connection, 0, sizeof(*connection));
}


void
AyyiConnection::__set_name()
{
	dbg(1, "connection=%p", connection);
	ARDOUR::Bundle* parent = (ARDOUR::Bundle*)connection->object;
	g_return_if_fail(parent);

	// Get the list of ports (actually just a list of portnames, not actual ports):
	char a[256] = "";
	char name[256] = "";
	for (unsigned p=0;p<parent->nchannels().n_total();p++) {
		const Bundle::PortList& ports = parent->channel_ports(p);
		for (std::vector<std::string>::const_iterator i = ports.begin(); i != ports.end(); i++) {

			//device (jack client) name:
			strncpy(connection->device, (*i).c_str(), 32);
			char* end = strstr(connection->device, ":");
			if (end) *end = '\0';
			dbg(2, "port: name=%s device=%s", (*i).c_str(), connection->device);
			char* ltc = strstr((char*)(*i).c_str(), ":LTC");
			if (ltc) {
				shm->flags |= deleted;
			}

			// copy the port name (ignore everything before the ":")
			char* m = strstr((char*)(*i).c_str(), ":");
			snprintf(a, 255, "%s{%s}", name, m ? m + 1 : (*i).c_str());
			strcpy(name, a);
		}
	}

	_set_name((char*)parent->name().c_str());
}


void
AyyiConnection::on_change(ARDOUR::Bundle::Change change)
{
	if(debug_bundle) dbgprintf("--> AyyiConnection", __func__, (char*)"...");

	ARDOUR::Bundle* parent = (ARDOUR::Bundle*)connection->object;
	g_return_if_fail(parent);

	//if(change & ARDOUR::Bundle::NameChanged){
		__set_name();
	//}

	if((change & ARDOUR::Bundle::PortsChanged) || (change & ARDOUR::Bundle::ConfigurationChanged)){
		dbg(1, "ports changed. n_ports=%i", connection->nports);
		connection->nports = parent->nchannels().n_total();

		if(parent->nchannels().n_total()){
			if(parent->channel_type(0) == DataType::MIDI){
				connection->flags = Connection::Midi;
			}
		}
	}
}


//temporarily static
string
AyyiConnection::get_port_name(connection_shared* shared)
{
	char name[64];
	strcpy(name, shared->name);
	char* start = g_strstr_len(name, 64, "{") + 1;
	char* end = g_strstr_len(name, 64, "}");
	if(end < start) return 0;
	char name2[64];
	strncpy(name2, start, end - start);
	name2[end - start] = '\0';
	return string(name2);
}


std::list<std::string>
AyyiConnection::get_port_names(connection_shared* shared)
{
	std::list<string> port_names;

	if(!strlen(shared->name)) return port_names;
	char str[AYYI_NAME_MAX];

	strcpy(str, shared->name);

	char* start = str;
	char name[64];
	while(start && (start = g_strstr_len(start, 64, "{"))){
		char* end = g_strstr_len(start, 64, "}");
		if(!end) break;
		if(end > start){
			strncpy(name, start + 1, end - start - 1);
			name[end - start - 1] = '\0';
			string a = name;
			port_names.push_back(a);
		}
		start = end + 1;
	}
	return port_names;
}


AyyiConnection*
AyyiConnection::find_connection(const char* s)
{
	// currently this fn is mono only! needs merging with find_connection_stereo

	// @param s   connection string of form: "jack_client:port_name"

	AyyiConnection* connection = 0;

	//if the string contains the jack_client name, remove it.
	char* ss = strstr((char*)s, ":");
	if(ss) ss++; else ss = (char*)s;

	dbg(1, "%s", s);
	connection_shared* shared = 0;
	while((shared = connections->next(shared))){
		if(!shared->shm_idx) continue; //ignore slot0 null entry
		ARDOUR::Bundle* bundle = (ARDOUR::Bundle*)shared->object;
		if(!bundle){
			//experimental! now some connections do not have bundles. Iterate over the Ayyi container instead of BundleList.

			if(!strcmp(shared->name, ss)){
				for(std::vector<AyyiConnection*>::iterator i = connections_without_bundles.begin(); i != connections_without_bundles.end(); i++){
					if((*i)->connection == shared) return (*i);
				}
				return (AyyiConnection*)shared->object;
			}
			continue;
		}
		//dbg(0, "shared=%p %s", shared, shared->name);

#if 0
		boost::shared_ptr<Bundle> sp;
		//temporary check for valid bundle:
		bool bundle_valid = false;
		boost::shared_ptr<ARDOUR::BundleList> bundles = session->bundles();
		//for(BundleList::const_iterator i = bundles->begin(); i != bundles->end(); i++){
		for(ARDOUR::BundleList::iterator i = bundles->begin(); i != bundles->end(); i++){
			if((*i).get() == c){/* dbg(0, "valid");*/ sp = (*i); bundle_valid = true; break; }
		}
		if(!bundle_valid){ warn("bundle not valid\n"); continue; }
#endif

		for(uint32_t port=0; port < bundle->nchannels().n_total(); port++){
			vector<string> portlist = bundle->channel_ports(port);
			for(vector<string>::iterator x = portlist.begin(); x != portlist.end(); x++){
				//dbg(0, "    %s==%s", s, (*x).c_str());
				if(!strcmp(s, (*x).c_str())) return bundle->shared;
			}
		}

	}
	warn("string not found: %s (%s)\n", s, ss);
	print_connections();
	return connection;
}


AyyiConnection*
AyyiConnection::find_connection_stereo(const char* port_left, const char* port_right)
{
	// find a bundle that has both of the given named ports - TODO will merge with above

	// @param l   connection string of form: "jack_client:port_name"
	// @param r   connection string of form: "jack_client:port_name"

	connection_shared* shared = 0;
	while((shared = AyyiConnection::connections->next(shared))){
		if(!shared->shm_idx) continue; //ignore slot0 null entry
		ARDOUR::Bundle* bundle = (ARDOUR::Bundle*)shared->object;
		if(bundle){
			//dbg(0, "  bundle=%s", bundle->shared->connection->name);
			bool found[2] = {false, false};
			for(uint32_t port=0; port < bundle->nchannels().n_total(); port++){
				vector<string> portlist = bundle->channel_ports(port);
				for(vector<string>::iterator x = portlist.begin(); x != portlist.end(); x++){
					//dbg(0, "    %s==%s", port_left, (*x).c_str());
					if(!strcmp(port ? port_right : port_left, (*x).c_str())){
						//l = g_list_append(l, bundle);
						found[port] = true;
						break;
					}
				}
			}
			if(found[0] && found[1]){
				dbg(0, "found: %s", bundle->shared->connection->name);
				return bundle->shared;
			}
		}
	}

	return (AyyiConnection*)0;
}


void
AyyiConnection::print_connections()
{
	p  (0, "----------------------------------------");
	dbg(0, "Bundles:");
	p  (0, "  bundle        nports");

	boost::shared_ptr<BundleList> bundles = Session::instance->bundles();
	for(BundleList::const_iterator x = bundles->begin(); x != bundles->end(); x++){
		print_connection((*x));
	}

#ifdef SHOW_PORTS
	const char** ports = engine->get_ports("", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput);
	if(ports){
		for(int i=0;ports[i];i++){
			p(0, "port[%i]=%s", i, ports[i]);
		}
	}
#endif

	p(0, " ayyi connections:");
	connection_shared* shared = 0;
	while((shared = connections->next(shared))){
		p(0, "  %2i %s", shared->shm_idx, shared->name);
	}
	p(0, "----------------------------------------");
}


void
AyyiConnection::print_connection(boost::shared_ptr<ARDOUR::Bundle> c/*, std::vector<boost::shared_ptr<Bundle> >* bundles*/)
{
	string ports;

	for(uint32_t port=0; port < c->nchannels().n_total(); port++){
		vector<string> portlist = c->channel_ports(port);
		for(vector<string>::iterator x = portlist.begin(); x != portlist.end(); x++){
			ports += *x + " ";
		}
	}

	printf("  %-18s %i  %s ref=%li\n", c->name().c_str(), c->nchannels().n_total(), ports.c_str(), c.use_count());
}


}
