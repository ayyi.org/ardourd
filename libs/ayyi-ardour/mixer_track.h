/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2019 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __mixer_track_h__
#define __mixer_track_h__
#include <ardour/plugin_insert.h>
#include "ayyi-ardour/shm.h"

namespace Ayi {

template<class T>

class MixerTrack : public Ayi::Shared<_ayyi_channel>
{
  public:
	                         MixerTrack           (T* parent_track, ShmSegDefn* segment);
	                        ~MixerTrack           ();

	_ayyi_channel*           track;

	static bool              kill                 (int s1, int s2);
	void                     activate_plugin_slot (uint32_t plugin_slot, int plugin_idx);
	void                     add_send             (boost::shared_ptr<ARDOUR::Processor>);
	bool                     remove_send          (int);
	boost::shared_ptr<ARDOUR::Send> get_send      (int);
	bool                     set_insert           (int, boost::shared_ptr<ARDOUR::PluginInsert>);
#ifdef VST_SUPPORT
	bool                     open_plugin_window   (int plugin_slot, boost::shared_ptr<ARDOUR::PluginInsert>);
#endif

	ShmContainer<_shm_event> vol_auto;
	ShmContainer<_shm_event> pan_auto;
	AyyiList*                plugin0_automation;

	static double            pan_map_in          (double);
	static double            pan_map_out         (double);

	static Ayi::ShmContainer<struct _ayyi_channel>* tracks;

  private:
	void                     automation_changed  ();
	void                     gain_changed        ();
	void                     panner_changed      ();
	void                     panner_shell_changed();
	void                     processors_changed  (ARDOUR::RouteProcessorChange);

	boost::weak_ptr<ARDOUR::Processor> inserts[AYYI_PLUGINS_PER_CHANNEL];

	int                      _processor_count;

	void                     _print_processor(boost::weak_ptr<ARDOUR::Processor>);
};

} //end namespace

#endif
