/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <set>
#include <fstream>
#include <algorithm>
#include <unistd.h>
#include <cerrno>
#include <string>
#include <climits>

#include <sigc++/bind.h>
#include <ardour/ardour.h>
#include <ardour/audioregion.h>
#include <ardour/audio_track.h>
#include <ardour/audiofilesource.h>
#include <ardour/send.h>
#include <ardour/amp.h>
#include <ardour/auto_bundle.h>

#include <ayyi/ayyi_time.h>
#include <ayyi/ayyi_types.h>
#include <ayyi/utils.h>
#include "ayyi-ardour/route.h"
#include "ayyi-ardour/mixer_track.h"
#include "ayyi-ardour/connection.h"
#include "ayyi-ardour/send.h"

using namespace std;
using namespace ARDOUR;
using namespace PBD;
using namespace Ayi;

extern int debug;
#define dbg(N, A, ...){ if(N <= debug) dbgprintf("AyiSend", __func__, (char*)A, ##__VA_ARGS__); }
#define warn(A, ...) warnprintf2("AyiSend", __func__, (char*)A, ##__VA_ARGS__)
#define err(A, ...) errprintf2("AyiSend", __func__, (char*)A, ##__VA_ARGS__)

namespace Ayi {

template<class T> class MixerTrack;


AyiSend::AyiSend(ARDOUR::Send* parent)
	: Shared<struct _ayyi_aux>(AyyidRoute::shm.mixer),
	  send(parent)
{
	//aux sends are fairly unique in that they currently dont reside in Ayi::ShmContainer. There is a fixed number that are children of the mixer channel.
	//This could end up messy. Maybe it should be a list instead.

	//loaded sends are created in Route::add_processor_from_xml(). They always have the name "send".

	dbg(0, "...");
	object_type = AYYI_OBJECT_AUX;

	if(!(shm_pod = (ayyi_aux*)AyyidRoute::shm.mixer->malloc_tlsf(sizeof(struct _ayyi_aux), NULL))) return;

	shm_pod->idx   = 0;   //as there is no container, there is no idx.
	shm_pod->level = 0.0;
	shm_pod->pan   = 0.0;
	shm_pod->flags = 0;
	shm_pod->bus_num = 0;

	//if this is a new send, the outputs are unlikely to have been made yet.
	boost::shared_ptr<IO> out = parent->output();
	boost::shared_ptr<Bundle> bundle = out->bundle();
	if(bundle){
		AyyiConnection* sc = new Ayi::AyyiConnection(bundle.get());
		dbg(0, "connection_idx=%i", sc->get_index());

		if(bundle->nchannels().n_total()){
			connection_shared* c = bundle->shared->connection;
			if(c){
				shm_pod->bus_num = c->shm_idx;
			}
			else warn("failed to get Ayyi::AyyiConnection.\n");
		}
		//else dbg(0, "bundle has no channels (send has %i channels)\n", io->n_outputs().n_total());
	}
	else dbg(0, "send has no outputs?");

	//dbg(0, "bus_num=%i", shm_pod->bus_num);

	out->changed.connect_same_thread(signal_connections, boost::bind(&AyiSend::output_changed, this, _1, _2));
}


AyiSend::~AyiSend()
{
	dbg(0, "TODO do we need to free() the bus connection object? is it being used by aux's on other channels?");
	AyyidRoute::shm.mixer->free_tlsf(shm_pod);
}


void
AyiSend::created()
{
	dbg(0, "...");

	if(send->amp()){
		send->amp()->gain_control()->Changed.connect_same_thread (signal_connections, boost::bind(&AyiSend::level_changed, this));
	}
	else dbg(0, "no amp!!");
}


void
AyiSend::level_changed()
{
	dbg(0, "...");
	shm_pod->level = send->amp()->gain_control()->get_value();
}


void
AyiSend::output_changed(IOChange change, void *src)
{
	//the main task is to set the bus_num. To do this we need to get the ayyi idx for the Bundle attached to the Send IO.

	//this gets called multiple times per route.
	//Eg, for stereo channels it will be called twice, the first time, only one port will be set.

	/*
	enum IOChange {
		NoChange = 0,
		ConfigurationChanged = 0x1,
		ConnectionsChanged = 0x2
	};
	*/

	dbg (0, "...");

	if(1/*change & ConnectionsChanged*/){
		boost::shared_ptr<IO> out = send->output();

		//sends are not in the session->bundlelist (bug or feature?) so this method is currently of no use...    ..??
		boost::shared_ptr<Bundle> bundle = out->bundle();
		if(bundle){
			//if(boost::dynamic_pointer_cast<UserBundle>(bundle)) dbg(0, "*** UserBundle!");
			if(!bundle->nchannels().n_total()) warn("bundle has no channels (io has %i channels)\n", out->n_ports().n_total());
			if(!bundle->shared){ warn("shm_pod not set.\n"); /*return;*/ }

			if(bundle->shared){
				connection_shared* c = bundle->shared->connection;
				if(c){
					shm_pod->bus_num = c->shm_idx;
					dbg(0, "bus=%i '%s'", shm_pod->bus_num, c->name);
				}
				else warn("failed to get Ayyi::AyyiConnection.\n");
			}
#if 0
			const ChanCount& channel_count = io->n_outputs();
			dbg(0, "channel_count=%i %i", channel_count.n_total(), bundle->nchannels());
			for (uint32_t i = 0; i < bundle->nchannels(); ++i) {
				PortList const & pl = bundle->channel_ports(i);
				if(pl.size() != 1) dbg(0, "!! portlist has: %i ports", pl.size());
			}
#endif
		}
		else err("failed to get ARDOUR::Bundle.\n");

#if 0
		//examine individual ports:

		int n_out;
		if((n_out = io->n_outputs().get(ARDOUR::DataType::AUDIO))){
			dbg(0, "n_out=%i", n_out);
			for(int p=0;p<n_out;p++){
				Port* out = io->output(p);
				if(out){
					dbg (0, "  port=%s", out->name().c_str());

					//we have a string - now get the shm index for it (without using session::BundleList).
					//AyyiConnection* c = NULL;
					struct _connection_shared* c = NULL;
					while((c = core->shm.osong->connections.next(c))){
						if(!c->shm_idx) continue; //ignore slot0 null entry
					}
					if(c){
						dbg(0, "have ayyi_connection! idx=%i", c->shm_idx);
					}
					else warn("failed to get connection object\n");
				}
			}
		}
		else dbg(0, "send has no outputs.");
#endif

		//ad_dbus_emit_property(core->dbus, AYYI_OBJECT_TRACK, route->shm_idx, AYYI_OUTPUT, NULL);
	}

#if 0
	if(change & ConfigurationChanged){
		dbg (0, "ConfigurationChanged!");
	}
#endif
}


} //end namespace

