/*
    Copyright (C) 2008-2009 Tim Orford 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 3.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef __ardourd_control_h__
#define __ardourd_control_h__

#include <string>
#include <set>
#include <map>
#include <list>
#include <boost/shared_ptr.hpp>
#include <sys/stat.h>
#include <glib.h>
#include <sigc++/signal.h>
#ifdef VST_SUPPORT
#include "fst/fst.h"
#endif

namespace ARDOUR {
	class AutomationControl;
};

#include <ayyi/ayyi_time.h>
#include <ayyi/interface.h>
#include <ayyi/ayyi++.h>
#include "shared.h"

namespace Ayi {

class AyiControl : public Shared<struct _ayyi_control>
{
  public:
	ayyi_control*      shm_pod;

	                   AyiControl(ARDOUR::AutomationControl*);

  protected:
	int                next_available_slot(); // temporary
};

}; // end namespace


#endif // __ardourd_control_h__
