/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2006-2019 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <cerrno>
#include <cstdlib>
#include <signal.h>
#include <unistd.h>

#include <pbd/pthread_utils.h>
#include <ardour/ardour.h>
#include <ardour/audio_track.h>
#include <ardour/midi_track.h>
#include <ardour/bundle.h>
#include <ardour/auto_bundle.h>
#include <ardour/plugin.h>
#include <ardour/panner.h>
#include "ardour/pannable.h"
#include <ardour/send.h>
#include <ardour/region.h>
#include <ardour/port.h>
#include <ardour/chan_count.h>
#include <ardour/meter.h>
#include <ardour/amp.h>
#include <ardour/delivery.h>
#include <ardour/audioengine.h>
#include <ardour/plugin_insert.h>

#include <ayyi/ayyi_time.h>
#include <ayyi/utils.h>
#include <ayyi/list.h>
#include <ayyi/ayyi_types.h>
#include "ayyi-ardour/connection.h"
#include "ayyi-ardour/playlist.h"
#include "ayyi-ardour/mixer_track.h"
#include "ayyi-ardour/midi_region.h"
#include "ayyi-ardour/midi_track.h"
#include "ayyi-ardour/plugin.h"
#include "ayyi-ardour/route.h"
#include "ardourd/dbus_object.h"

#define dbg(N, A, ...){ if(N <= debug_route) dbgprintf("AyyidRoute", __func__, (char*)A, ##__VA_ARGS__); }
#define warn(A, ...) warnprintf2("AyyidRoute", __func__, (char*)A, ##__VA_ARGS__)
#define err(A, ...) errprintf2("AyyidRoute", __func__, (char*)A, ##__VA_ARGS__)

extern int debug_route;
extern int debug_automation;

#include "ayyi/container-impl.cc"
namespace Ayi {

using namespace ARDOUR;

AyyidRoute::_Shm AyyidRoute::shm;
ShmContainer<struct _route_shared>* AyyidRoute::routes;


AyyidRoute::AyyidRoute(ARDOUR::Route* parent)
	: Shared<_route_shared>(AyiPlaylist::shm),
	  input_routing(0),
	  output_routing(0)
{
	g_return_if_fail(AyiPlaylist::shm);
	ShmSegDefn* mixer_seg = (ShmSegDefn*)AyyidRoute::shm.mixer;
	g_return_if_fail(mixer_seg);

	bool is_master = (parent->is_master() || parent->name() == "master");

#if 0
	container = &((SongShm*)base2)->routes;
#else
	container = AyyidRoute::routes;
#endif

	if(parent->is_master()){
		//sometimes is_master is set and sometimes it isnt.
		dbg(0, "track is master.");
	}
	route_shared* shared = (route_shared*)set_shm((_ayyi_base_item*)(route = container->add_item_with_idx()));

	object_type = AYYI_OBJECT_AUDIO_TRACK;

	//outputs are added later, eg when engine starts.
	boost::shared_ptr<Bundle> input  = parent->input()->bundle();
	boost::shared_ptr<Bundle> output = parent->output()->bundle();

	snprintf(shared->name, AYYI_NAME_MAX - 1, "%s", parent->name().c_str());
	shared->colour         = -1;
	shared->input_routing  = 0;
	shared->output_routing = 0;
	shared->object         = (void*)parent;
	shared->id             = ((PBD::ID)parent->id()).get_id(); // this id is provisional. it will be set again later.

	set_flag(master, is_master);
	set_flag(solod, parent->soloed());
	set_flag(muted, parent->muted());
	set_flag(armed, parent->record_enabled());

	input_routing  = new AyyiList(base2, &shared->input_routing);
	output_routing = new AyyiList(base2, &shared->output_routing);

	p(1, "%sAyyidRoute::%s():%s slot=%i", bold, __func__, white, slot);

	parent->output()->changed.connect_same_thread(signal_connections, boost::bind(&AyyidRoute::output_changed, this, _1, _2));
	parent->input()->changed.connect_same_thread(signal_connections, boost::bind(&AyyidRoute::input_changed, this, _1, _2));

	//check this following upstream change:
	parent->output()->PortCountChanged.connect_same_thread(signal_connections, boost::bind(&AyyidRoute::ports_created, this, _1));

	boost::weak_ptr<Route> wpr ();
	parent->mute_changed.connect_same_thread (signal_connections, boost::bind(&AyyidRoute::mute_changed, this));
	//parent->solo_changed.connect_same_thread (signal_connections, boost::bind(&AyyidRoute::solo_changed, this, _1, _2, wpr));
	parent->solo_changed.connect_same_thread (signal_connections, boost::bind(&AyyidRoute::solo_changed, this, _1));
	//for name:
	parent->PropertyChanged.connect_same_thread (signal_connections, boost::bind(&AyyidRoute::name_changed, this));
	parent->DropReferences.connect_same_thread  (signal_connections, boost::bind(&AyyidRoute::going_away, this));
	parent->DropReferences.connect_same_thread  (signal_connections, boost::bind(&AyyidRoute::route_del2, this, (Route*)parent));

	parent->ready.connect_same_thread (signal_connections, boost::bind(&AyyidRoute::on_ready, this));

	boost::shared_ptr<Track> track = boost::dynamic_pointer_cast<Track>(parent->shared_from_this());
	if(track){
		track->PlaylistChanged.connect_same_thread (signal_connections, boost::bind(&AyyidRoute::playlist_changed, this, boost::weak_ptr<Track> (track)));
	}

	boost::shared_ptr<ARDOUR::AutomationList> vol_automation_list = parent->gain_control()->alist();
	vol_automation_list->StateChanged.connect_same_thread (signal_connections, boost::bind(&AyyidRoute::vol_automation_changed, this));
	parent->gain_control()->Changed.connect_same_thread (signal_connections, boost::bind(&AyyidRoute::vol_changed, this));

	this->mixer_track = new MixerTrack<ARDOUR::Route> (parent, mixer_seg);
	this->mixer_track->vol_auto.init(&mixer_track->track->automation[VOL], "vol automation", false);
	this->mixer_track->pan_auto.init(&mixer_track->track->automation[PAN], "pan automation", false);
	set_vol_automation();
	set_pan_automation();

	for(int i=0; i<AYYI_PLUGINS_PER_CHANNEL; i++){ //TODO loop over the processorlist instead
		boost::shared_ptr<Processor> processor = parent->nth_processor(i);
		if(processor.get()){
			boost::shared_ptr<PluginInsert> insert;
			if(insert = boost::dynamic_pointer_cast<PluginInsert>(processor)){
			   dbg(0, "plugin=%s", processor->name().c_str());
				AyyiPlugin<ARDOUR::PluginInfo, Ayi::plugin_shared>* plugin = AyyiPlugin<ARDOUR::PluginInfo, Ayi::_ayyi_plugin_nfo>::find_plugin(insert->plugin()->get_info());
				if(plugin){
					mixer_track->activate_plugin_slot(i, plugin->get_index());

					dbg(0, "updating plugin controls info...");
					plugin->set_controls(insert->plugin());
				}
			}
			else if(boost::dynamic_pointer_cast<Send>(processor)){
				//warn("processor is a send!\n");
			}
			else if(boost::dynamic_pointer_cast<PeakMeter>(processor)){
			}
			else if(boost::dynamic_pointer_cast<Amp>(processor)){
			}
			else if(boost::dynamic_pointer_cast<Delivery>(processor)){
			}
			else err("processor list item is unknown type. name=%s\n", processor->name().c_str());
		}
	}

	announce();
	dbg(1, "done.");
}


AyyidRoute::~AyyidRoute()
{
	dbg(1, "...");
//not sure its safe to remove this
	if(!route) return; //probably a hidden track.
	p(0, "%sAyyidRoute::~AyyidRoute%s name='%s'", blue, white, route->name);

	if(route){
		AyyidRoute::routes->remove_item(get_index());
		memset(route, 0, sizeof(*route));
	}

	if(output_routing) delete output_routing;

	//removed associated data in the mixer segment:
	delete (MixerTrack<ARDOUR::Route>*)mixer_track;

	announce_delete();
}


void
AyyidRoute::going_away()
{
	dbg(1, "...");
}


void
AyyidRoute::set_outputs(char* s)
{
	dbg(1, "name=%s", s);
	dbg(0, "need to update output_routing list? %s", s);
	//snprintf(route->output_name, 128, "%s", s);
}


void
AyyidRoute::input_changed(IOChange change, void *src)
{
	dbg(1, "---> %s", route->name);

	if(true/*change & ConnectionsChanged*/){
		AudioTrack* track = (AudioTrack*)route->object;
		if(!track){ err("cannot get track.\n"); return; }

		input_routing->clear();
		BundleList input_connections = track->input()->bundles_connected ();
		for(BundleList::iterator x = input_connections.begin(); x != input_connections.end(); x++){
			if((*x)->shared){
				AyyiConnection* c = (*x)->shared;
				input_routing->prepend_unique(c->get_index(), "");
			}
			else warn("failed to get Ayyi::AyyiConnection.\n");
		}

		string inputs;
		int n_in;
		if((n_in = track->n_inputs().get(ARDOUR::DataType::AUDIO))){
			for(int p=0;p<n_in;p++){
				boost::shared_ptr<ARDOUR::Port> in = track->input()->nth(p);
				if(in){
					vector<string> port_names;
					int n_connections = in->get_connections(port_names);
					if(n_connections){
						int n = 0;
						for (vector<string>::iterator p = port_names.begin(); p != port_names.end(); ++p, ++n) {
							inputs = inputs + '{' + *p + '}';
						}
					}
				}
			}
		}
		dbg(1, "slot=%i inputs=%s", slot, inputs.c_str());
		strncpy(route->input_name, inputs.c_str(), 127);
	}
}


void
AyyidRoute::output_changed(IOChange change, void *src)
{
	//this gets called multiple times per route.
	//Eg, for stereo channels it will be called twice, the first time, only one port will be set.

	//for some reason, when it is called by libardour, it is too early and the correct objects are not set up. If called later, it does work.
	//see AD::onload_delayed().

	/*
	enum IOChange {
		NoChange = 0,
		ConfigurationChanged = 0x1,
		ConnectionsChanged = 0x2
	};
	*/

	dbg (1, "(%s) ...", route->name);
	if(!change.type){ dbg(1, "no IOChange"); }
	else dbg(1, "ConfigurationChanged=%i ConnectionsChanged=%i IOChange=%i", change.type & IOChange::ConfigurationChanged, (change.type & IOChange::ConnectionsChanged / IOChange::ConnectionsChanged), change.type);
	bool shm_changed = false;

	if(1/*change & ConnectionsChanged*/){
		dbg (1, "slot=%i output_changed!", slot);

		AudioTrack* track = (AudioTrack*)route->object;
		if(!track){ err("cannot get track.\n"); return; }

		dbg(1, "%s: n_inputs=%i n_outputs=%i", track->name().c_str(), track->n_outputs().n_total(), track->n_inputs().n_total());
		mixer_track->track->n_in = track->n_inputs().n_total();
		mixer_track->track->n_out = track->n_outputs().n_total();

		//update shm and notify clients:

		// ....actually, it seems that it is set after AD::set_output() is called 
		//     TODO find the correct signal to attach to.
		{
    		boost::shared_ptr<Bundle> bundle = track->output()->bundle();
			if(bundle){
				//if this is a new track the bundle ports may not yet be created.
				if(!bundle->nchannels().n_total()) warn("route output bundle has no channels (AudioTrack has %i channels) - new object? bundle=%s", track->n_outputs().n_total(), bundle->name().c_str());
				else dbg(2, "route output bundle has channels! (%i) bundle=%s", bundle->nchannels().n_total(), bundle->name().c_str());

		shm_changed = true;
				//each channel seems to have only 1 port.
				for(uint32_t i=0;i<bundle->nchannels().n_total();i++){
					Bundle::PortList const & port_names = bundle->channel_ports(i);
					for (uint32_t j = 0; j < port_names.size(); ++j) {
						dbg(1, "  ch=%s port=%s", bundle->channel_name(i).c_str(), port_names[j].c_str());

						boost::shared_ptr<ARDOUR::Port> port = AudioEngine::instance()->get_port_by_name (port_names[j]);
						if(port){
							std::vector<std::string> connections;
							int n = port->get_connections (connections);
							if(!n){
								dbg(1, "      not connected");
								if(output_routing && output_routing->size()){
									output_routing->clear();
									shm_changed = true;
								}
							}
							for(int j=0;j<n;j++){
								dbg(1, "      --> %s", connections[j].c_str());
							}
						}
						else dbg(0, "        no port");
					}
				}

				BundleList output_connections = track->output()->bundles_connected ();
				if(output_connections.size()){
					dbg(1, "bundles_connected.size=%i", output_connections.size());
				}else{
					dbg(1, "route output has no bundles connected");
				}
				for(BundleList::iterator x = output_connections.begin(); x != output_connections.end(); x++){
					if((*x)->shared){
						AyyiConnection* c = (*x)->shared;
						output_routing->prepend_unique(c->get_index(), "");
						shm_changed = true;
					}
					else warn("failed to get Ayyi::AyyiConnection.\n");
				}
			}
			else err("failed to get ARDOUR::Bundle.\n");
		}
#if 0
		//test: see what we are connected to:
		std::vector<boost::shared_ptr<Bundle> > connected_to = track->bundles_connected_to_outputs ();
		if(!connected_to.size()) dbg(0, "track output is not connected.");
		for (std::vector<boost::shared_ptr<Bundle> >::iterator i = connected_to.begin(); i != connected_to.end(); i++) {
			dbg("  connectedto: %s", (*i)->name().c_str());
		}
#endif

		dbg(1, "___");

		//-do we want to use Bundles, or individual ports? Ideally Bundles are better. see above. Though results seem to be identical.
		// I think it may be the case that external jack connections do not have bundles.

		string outputs;
		int n_out;
		if((n_out = track->n_outputs().get(ARDOUR::DataType::AUDIO))){

			//note: this path is probably now only useful for external jack connections

			for(int p=0;p<n_out;p++){
				boost::shared_ptr<ARDOUR::Port> out = track->output()->nth(p);
				if(out){
					dbg (1, "port=%s", out->name().c_str());
					vector<string> port_names;
					int n_connections = out->get_connections(port_names);
					if(n_connections){
						int n = 0;
						if(port_names.size() == 2){
							dbg(0, "STEREO");
							AyyiConnection* c = AyyiConnection::find_connection_stereo(port_names[0].c_str(), port_names[1].c_str());
							if(c){
								if(!output_routing->find(c->get_index())){
									output_routing->prepend(c->get_index(), "");
									shm_changed = true;
								}
							}
						}else{
							for (vector<string>::iterator p = port_names.begin(); p != port_names.end(); ++p, ++n) {
								dbg (1, "  n=%i", n);
								outputs = outputs + '{' + *p + '}';

								//we have a string - now get the shm index for it.
								AyyiConnection* c = AyyiConnection::find_connection((*p).c_str());
								if(c){
									Ayi::_connection_shared* cs = c->connection;
									dbg(1, "found: %i: %s", cs->shm_idx, cs->name);
									if(output_routing->find(c->get_index())){
										dbg(1, "output is already in the routing list.");
									}else{
										dbg(1, "output is _not_ in the routing list. Adding: %i: %s%s%s ...", c->get_index(), bold, cs ? cs->name : 0, white);
										output_routing->prepend(c->get_index(), "");
										shm_changed = true;
									}

									//TODO items appear to never be removed from the list.
								}
								else warn("failed to get connection object\n");
							}
						}
						dbg (2, "port connection count: %i (expected 1)", n);
					} else dbg(1, "slot=%i port=%i get_connections() returned NULL.", slot, p);
				}
			}
		}
		dbg(1, "slot=%i outputs=%s", slot, outputs.c_str());
		if(debug_route) print_routing();
	}

	if(change.type & IOChange::ConfigurationChanged){
		dbg (1, "ConfigurationChanged!");
	}

	if(shm_changed) emit_changed(AYYI_OUTPUT);
	else dbg(0, "shm unchanged");
}


void
AyyidRoute::ports_created(ChanCount n_chans)
{
	// panners are not added when the route is created.
	// ports_created() is fired more than once so we have to be careful not to bind multiple times.

	dbg(2, "...");

	boost::shared_ptr<ARDOUR::Panner> panner = ((ARDOUR::Route*)route->object)->panner();
	if(panner){
		if(panner->out().n_total()){
			boost::shared_ptr<ARDOUR::AutomationList> pan_automation_list = panner->pannable()->pan_azimuth_control->alist();
			panner_connection.disconnect();
			pan_automation_list->StateChanged.connect_same_thread (panner_connection, boost::bind(&AyyidRoute::pan_automation_changed, this));
		}
	}
}


void
AyyidRoute::modify_output(unsigned t)
{
	dbg(1, "...");
}


void
AyyidRoute::mute_changed()
{
	dbg(1, "...");

	if(route){
		ARDOUR::Route* obj = (ARDOUR::Route*)route->object;
		if(obj) set_flag(muted, obj->muted());

		emit_changed(AYYI_MUTE);
	}
}


void
AyyidRoute::solo_changed(bool self_solo_change)
{
	dbg(1, "...");

	if(route){
		ARDOUR::Route* parent = (ARDOUR::Route*)route->object;
		if(parent){
			set_flag(solod, parent->soloed());
		}

		emit_changed(AYYI_SOLO);
	}
}


void
AyyidRoute::arm_changed()
{
	dbg(1, "...");

	if(route){
		ARDOUR::Route* parent = (ARDOUR::Route*)route->object;
		if(parent){
			if((route->flags & armed) != parent->record_enabled()){
				set_flag(armed, parent->record_enabled());
				ad_dbus_emit_property(dbus, object_type, route->shm_idx, AYYI_ARM, NULL);
				//emit_changed(AYYI_ARM);
			}
		}
	}
}


void
AyyidRoute::name_changed()
{
	if(route){
		ARDOUR::Route* obj = (ARDOUR::Route*)route->object;
		if(obj){
			_set_name((char*)obj->name().c_str());
			//mixer_track->_set_name((char*)obj->name().c_str());
							snprintf(mixer_track->track->name, AYYI_NAME_MAX - 1, "%s", (char*)obj->name().c_str()); //TODO _set_name is private
			dbg(0, "new_name=%s%s%s", bold, obj->name().c_str(), white);
			if(obj->name() == "master") set_flag(master, true); //hacky
		}else warn("track name not updated!");

		//update ALL regions! (region track is identified by playlist name.)
		_region_shared* region = NULL;
		while((region = AyyiRegion::regions->next(region))){
			if(region->flags & deleted) continue;

			ARDOUR::Region* r = (ARDOUR::Region*)region->server_object;
			if(r){
				if(r->shared_obj) r->shared_obj->track_name_changed((char*)obj->name().c_str());
			}
			else warn("no ARDOUR::region!\n");
		}

		emit_changed(AYYI_NAME);
	}
}


void
AyyidRoute::playlist_changed (boost::weak_ptr<Track> wp)
{
	dbg(1, "...");

	boost::shared_ptr<Track> track = wp.lock ();
	if(track) {
		boost::shared_ptr<AyiPlaylist> playlist;
		if ((playlist = boost::dynamic_pointer_cast<AyiPlaylist>(track->playlist()))) {
			playlist->set_track(track);
		}
		/*
		boost::shared_ptr<AyiMidiPlaylist> midi_playlist;
		else if ((midi_playlist = boost::dynamic_pointer_cast<AyiMidiPlaylist>(track->playlist()))) {
			midi_playlist->set_track(track);
		}
		*/
	}
}


/*
 *   ARDOUR::Route.ready() signal is an Ayyi addition. It is triggered when Route.set_state() has finished.
 */
void
AyyidRoute::on_ready()
{
	ARDOUR::Route* parent = (ARDOUR::Route*)route->object;
	route->id = ((PBD::ID)parent->id()).get_id();

	boost::shared_ptr<Track> track = boost::dynamic_pointer_cast<Track>(parent->shared_from_this());
	track->rec_enable_control()->Changed.connect_same_thread (signal_connections, boost::bind(&AyyidRoute::arm_changed, this));
}


void
AyyidRoute::set_vol_automation()
{
	dbg(0, "...");

	mixer_track->vol_auto.clear();

	ARDOUR::Route* obj = (ARDOUR::Route*)route->object;
	if(obj){
		dbg(1, "have obj...");
		boost::shared_ptr<ARDOUR::AutomationList> automation = obj->gain_control()->alist();
		if(automation->size()){
			double old_when = 0;
			if(debug_automation) dbg(0, "VOL: size=%i", automation->size());
			for(ARDOUR::AutomationList::iterator i = automation->begin(); i != automation->end(); i++){
				shm_event* event = this->mixer_track->vol_auto.add_item();
				event->when  = (*i)->when;
				event->value = (*i)->value;
				if(debug_automation) dbg(0, "  VOL: %10.2f %4.2f", event->when, event->value);

				if(event->when < old_when) err("VOL: automation list not sorted.\n");
				old_when = event->when;
			}
		}
		//mixer_track->vol_auto.print();
		//mixer_track->vol_auto.verify();

	}
}


void
AyyidRoute::set_pan_automation()
{
	ARDOUR::Route* obj = (ARDOUR::Route*)route->object;
	if(obj){
		dbg(1, "checking panner...");
		if(obj->panner() && obj->panner()->out().n_total()){
			mixer_track->pan_auto.clear();

			boost::shared_ptr<ARDOUR::Panner> panner = obj->panner();
			boost::shared_ptr<ARDOUR::AutomationList> automation = panner->pannable()->pan_azimuth_control->alist();
			if(automation->size()){
				//double old_when = 0;
				for(ARDOUR::AutomationList::iterator i = automation->begin(); i != automation->end(); i++){
					shm_event* event = mixer_track->pan_auto.add_item();
					event->when  = (*i)->when;
					event->value = (*i)->value;
					if(debug_automation) dbg(0, "PAN: %10.2f %4.2f", (*i)->when, (*i)->value);

					//if(event->when < old_when) errprintf("AyyidRoute::%s(): PAN: automation list not sorted.\n", __func__);
					//old_when = event->when;
				}
			}

#if 0
			{
				ARDOUR::BaseStereoPanner* base_stero_panner = dynamic_cast<ARDOUR::BaseStereoPanner*>(&obj->panner());
				dbg("is BaseStereoPanner: %p", base_stero_panner);
				ARDOUR::Panner* base_panner = dynamic_cast<ARDOUR::Panner*>(&obj->panner());
				dbg("is Panner: %p", base_panner);
			}
#endif
#if 0
			if(this->mixer_track->pan_auto.count_items()){
				//printf("AyyidRoute::%s(): showing PAN... size=%i\n", __func__, this->pan_auto.count_items());
				//AD::auto_list_print(automation);
			}
#endif
		} else dbg(1, "no panner");
		dbg(1, "panner done.");
	}
}


void
AyyidRoute::set_plugin_automation()
{
}


void
AyyidRoute::vol_changed()
{
	//there is an issue here with different gain_control methods returning different values depending on whether the list value is used or not.
	//-depends on automation state. see: AutomationControl::get_value()

#if 0 // update here is not necesary as it is also done on a timer
	dbg(0, "...");
	ARDOUR::Route* obj = (ARDOUR::Route*)route->object;
	if(obj){
		mixer_track->track->level = obj->amp()->gain_control()->get_double();
		dbg(0, "%s: level=%.3f list_val=%.3f", obj->name().c_str(), mixer_track->track->level, obj->amp()->gain_control()->get_value());
	}
#endif
}


void
AyyidRoute::vol_automation_changed()
{
	dbg(0, "...");
	ARDOUR::Route* obj = (ARDOUR::Route*)route->object;
	if(obj){
		boost::shared_ptr<ARDOUR::AutomationList> automation = obj->gain_control()->alist();
		dbg(1, "size=%i", automation->size());
		set_vol_automation();
	}
}


void
AyyidRoute::pan_automation_changed()
{
	dbg(0, "...");
	ARDOUR::Route* obj = (ARDOUR::Route*)route->object;
	if(obj){
		boost::shared_ptr<ARDOUR::AutomationList> automation = obj->pan_azimuth_control()->alist();
		dbg(1, "size=%i", automation->size());
		set_pan_automation();
	}
}


void
AyyidRoute::automation_changed()
{
	dbg(0, "...");
	ARDOUR::Route* obj = (ARDOUR::Route*)route->object;
	if(obj){
		//we dont yet have a way of knowing which control has changed.
	}
}


void
AyyidRoute::automation_slot_init(uint32_t plugin_slot)
{
	dbg(0, "making automation list...");
	mixer_track->plugin0_automation = new AyyiList(AyyiRegion::shm, &mixer_track->track->automation_list);
	dbg(0, "automation_list=%p.", mixer_track->track->automation_list);
}


typedef boost::shared_ptr<Plugin> ArdourPlugin;

void
AyyidRoute::add_automation_type(uint32_t plugin_slot, uint32_t control_idx)
{
	//add an automationlist container. Should only be done if the automatable has events.

	if (!mixer_track->plugin0_automation) automation_slot_init(plugin_slot);

	ARDOUR::Route* aroute = (ARDOUR::Route*)route->object;
	boost::shared_ptr<Processor> processor = aroute->nth_processor(plugin_slot);
	ARDOUR::Automatable::Controls& insert_controls = processor->controls();
	guint i = 0;
	const char* name = NULL;
	uint32_t id = 0;
	bool found = false;
	Evoral::Parameter param(0);
	for (ARDOUR::Automatable::Controls::iterator p = insert_controls.begin(); p != insert_controls.end(); p++) {
		if (i == control_idx) {
			param = (*p).first;
			//name = param.to_string().c_str(); //FIXME to_string removed from libardour
			id = param.id();
			found = true;
			dbg(0, "adding automation control: '%s'", name);
			break;
		}
		i++;
	}
	if (!found) warn("param not found!\n");

	int plugin_idx = 0;
	boost::shared_ptr<PluginInsert> insert;
	if (insert = boost::dynamic_pointer_cast<PluginInsert>(processor)) {
		ArdourPlugin aplugin = insert->plugin(0); //take 1st plugin, but inserts can have more than 1 plugin (!)
		PluginInfoPtr p_info = aplugin->get_info();
		dbg(0, "got p_info. name=%s", p_info->name.c_str());
		Ayi::AyyiPlugin<ARDOUR::PluginInfo, Ayi::plugin_shared>* shared = p_info->ad1_plugin;
		if (shared) {
			plugin_idx = shared->get_index();
			dbg(0, "plugin_idx=%i", plugin_idx);
		}
		else err("plugin ayyi_track not set!\n");
	}
	int autolist_id = id | plugin_idx << 16;
	dbg(0, "autolist_id=0x%x", autolist_id);
	const char* param_name = "FIXME param_name";
	//if (mixer_track->plugin0_automation->prepend(autolist_id, param.to_string().c_str())) {
	if (mixer_track->plugin0_automation->prepend(autolist_id, param_name)) {
		//the new list item is first in the list so we can access it directly:
		//(*plugin0_automation->list)->id = id;
		dbg (0, "type=%i name=%s", (*mixer_track->plugin0_automation->list)->id, (*mixer_track->plugin0_automation->list)->name);

		boost::shared_ptr<Evoral::ControlList> list = processor_get_automationlist(processor, id/*(*plugin0_automation->list)->id*/);
		if (list) dbg(0, "connecting to StateChanged signal...");
#warning FIXME processor_get_automationlist
		dbg(0, "FIXME");
#if 0
		if (list) list->StateChanged.connect (sigc::mem_fun(*this, &AyyidRoute::automation_changed));
		else dbg("*** couldnt get ardour automation list. control_idx=%i", id);
#endif
	}
}


void
AyyidRoute::set_flag(int flag, bool val)
{
	//dbg("flag=%i val=%i", flag, val);
	//int old_flags = route->flags;
	if(val){
		route->flags |= flag;
	} else {
		route->flags &= ~flag;
	}
	//dbg("flags: %i --> %i", old_flags, route->flags);
}


void
AyyidRoute::print_routing()
{
	_ayyi_list* l = *output_routing->list;
	if(!l) dbg(0, "%10s: no routing.", route->name);
	for(;l;l=l->next){
		Ayi::_connection_shared* c = AyyiConnection::connections->get_item(l->id);
		dbg(0, "%10s: out=%i '%s'", route->name, l->id, c ? c->name : NULL);
	}
}


void
AyyidRoute::route_del(Route* route)
{
	dbg(0, "...");

	if(route->ayyi_track == NULL){ printf("*** shared object already removed?\n"); return; }

	if(dynamic_cast<AudioTrack*>(route) != 0){
		dbg(0, "is audio!");
		Ayi::AyyidRoute* shared_obj = route->ayyi_track;
		delete shared_obj;
	}
	else if(dynamic_cast<MidiTrack*>(route) != 0) {
		dbg(0, "is midi!");
		delete ((Ayi::AyyidMidiTrack*)(route->ayyi_track));
	}
	else{
		dbg(0, "**** unknown type!");
	}

	route->ayyi_track = NULL; 
}


void
AyyidRoute::route_del2(Route* route)
{
	dbg(0, "...");

	if(route->ayyi_track == NULL){ printf("*** shared object already removed?\n"); return; }

	if(dynamic_cast<AudioTrack*>(route) != 0){
		dbg(0, "is audio!");
		Ayi::AyyidRoute* shared_obj = route->ayyi_track;
		delete shared_obj;
	}
	else if(dynamic_cast<MidiTrack*>(route) != 0) {
		dbg(0, "is midi!");
		delete ((Ayi::AyyidMidiTrack*)(route->ayyi_track));
	}
	else{
		dbg(0, "**** unknown type!");
	}

	route->ayyi_track = NULL; 
}


ARDOUR::AudioTrack*
AyyidRoute::get_track(ObjIdx slot)
{
	route_shared* shared = AyyidRoute::routes->get_item(slot);
	if (!shared) { dbg(0, "failed to get AyyidRoute. route_num=%u", slot); return NULL; }
	ARDOUR::AudioTrack* route = (ARDOUR::AudioTrack*)shared->object;
	if (!shared->object) { err("failed to get route object. route_num=%u\n", slot); return NULL; }

	return route;
}


boost::shared_ptr<Evoral::ControlList>
AyyidRoute::processor_get_automationlist(boost::shared_ptr<Processor> p, guint control_idx)
{
	boost::shared_ptr<Evoral::ControlList> list;

	boost::shared_ptr<PluginInsert> insert;
	if (insert = boost::dynamic_pointer_cast<PluginInsert>(p)) { //TODO i think this line is unnecesary
		ARDOUR::Automatable::Controls& insert_controls = insert->controls();
		int i = 0;
		for (ARDOUR::Automatable::Controls::iterator p = insert_controls.begin(); p != insert_controls.end(); p++) {
			//Evoral::Parameter param = (*p).first;
			//boost::shared_ptr<AutomationControl> ctl = (*p).second;
			boost::shared_ptr<Evoral::Control> ctl = (*p).second;
			//TODO this might not work with more than one plugin slot.
			if (i == (int)control_idx) {
				return ctl->list();
			}
			i++;
		}
	}
	return list;
}


template _route_shared* ShmContainer<_route_shared>::get_item(int);
template bool ShmContainer<_route_shared>::slot_is_used(int);
}
