/*
    Copyright (C) 2006-2016 Tim Orford 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 3.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id: playlist.h 933 2006-09-28 17:23:52Z paul $
*/

#include <string>
#include <set>
#include <map>
#include <list>
#include <boost/shared_ptr.hpp>

#include <sys/stat.h>

#include <glib.h>

#include <sigc++/signal.h>

#include <pbd/undo.h>
#include <pbd/stateful.h> 
#include <pbd/statefuldestructible.h> 

#include <ardour/ardour.h>
#include <ardour/location.h>
#include <ardour/playlist.h>
#include <ardour/midi_playlist.h>

namespace ARDOUR  {

class Session;
class Region;
class AudioRegion;
class Source;

class AyiMidiPlaylist : public MidiPlaylist, public Ayi::Shared<struct Ayi::_ayyi_shm_playlist>
{
  public:
	Ayi::playlist_shared* shared_; //shm POD 

	AyiMidiPlaylist (Session&, const XMLNode&, bool hidden = false);
	AyiMidiPlaylist (Session&, string name, bool hidden = false);
	AyiMidiPlaylist (boost::shared_ptr<const MidiPlaylist>, string name, bool hidden = false);
	AyiMidiPlaylist (boost::shared_ptr<const MidiPlaylist> other, ARDOUR::framecnt_t start, ARDOUR::framecnt_t cnt, string name, bool hidden = false);
	~AyiMidiPlaylist();

	void                                     add_pod();
	void                                     remove_pod();
	/*const*/ boost::shared_ptr<AudioRegion> get_boost_region_from_pod(uint32_t pod_index);
	void                                     remove_region(boost::shared_ptr<Region> region);

	void                                     set_track(boost::shared_ptr<ARDOUR::Track>);

	void                                     name_changed();

	static void                              remove_unused();

  protected:
};

} /* namespace ARDOUR */


