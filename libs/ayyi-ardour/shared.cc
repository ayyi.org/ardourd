/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <cerrno>
#include <cstdlib>
#include <signal.h>
#include <unistd.h>

#include <pbd/pthread_utils.h>
extern "C" {
#include "tlsf/src/tlsf.h"
}

#include <ardour/ardour.h>
#include <ardour/tempo.h>
#include <ardour/audio_track.h>
#include <ardour/midi_track.h>
#include <ardour/session_playlist.h>
#include <ardour/diskstream.h>
#include <ardour/playlist.h>
#include <ardour/audioplaylist.h>
#include <ardour/audioregion.h>
#include <ardour/midi_region.h>
#include <ardour/region.h>
#include <ardour/region_factory.h>
#include <ardour/audiofilesource.h>
#include <ardour/silentfilesource.h>
#include <ardour/session.h>
#include <ardour/session_directory.h>
#include <ardour/bundle.h>
#include <ardour/plugin.h>

#include <sigc++/bind.h>

#include <ayyi/ayyi_time.h>
#include <ayyi/ayyi_types.h>
#include "ayyi/list.h"
#include "ayyi/utils.h"
#include "ayyi/inc/song.h"
#include "ardourd/dbus_object.h"

extern int debug_shm;
extern int debug_source;
extern int debug_region;
extern int debug_playlist;
extern int debug;
namespace Ayi {
	template<class T> class MixerTrack;
}

#define dbg(N, A, ...){ if(N <= debug) dbgprintf("Shared", __func__, (char*)A, ##__VA_ARGS__); }
#define warn(A, ...) warnprintf2("Shared", __func__, (char*)A, ##__VA_ARGS__)
#define err(A, ...) errprintf2("Shared", __func__, (char*)A, ##__VA_ARGS__)
#define P_GERR if(error){ err("%s\n", error->message); g_error_free(error); error = NULL; }

namespace Ayi {

using namespace Ayi;
using namespace std;
using namespace ARDOUR;
using boost::shared_ptr;

ArdourDbus* DBusClient::dbus;


template<class P>
Shared<P>::Shared(ShmSegDefn* _base)
	: base2(_base)
	, s1(0)
	, is_announced(false)
{
}
template Shared<_ayyi_channel>::Shared(ShmSegDefn*);
template Shared<_ayyi_plugin_nfo>::Shared(ShmSegDefn*);
template Shared<_ayyi_control>::Shared(ShmSegDefn*);
template Shared<_ayyi_aux>::Shared(ShmSegDefn*);
template Shared<_ayyi_shm_playlist>::Shared(ShmSegDefn*);
template Shared<_shm_midi_note>::Shared(ShmSegDefn*);
template Shared<_midi_region_shared>::Shared(ShmSegDefn*);
template Shared<_connection_shared>::Shared(ShmSegDefn*);
template Shared<_region_shared>::Shared(ShmSegDefn*);
template Shared<_route_shared>::Shared(ShmSegDefn*);
template Shared<_filesource_shared>::Shared(ShmSegDefn*);


#if 0
template<class P>
int
shared<P>::next_available_slot()
{
	/*
		-returns the index of the next free POD slot.

		-this "new" version is for containerised objects, and updates the "last" field in the parent block.

		-DEPRECATED! use ShmContainer::next_available_slot() instead.
	*/

	void** pod_table = (void**)container->block[0]->slot;

	if(!pod_table){ err("pod_table not set! object_type=%i\n", object_type); return -1; }

	for(int slot=0; slot<BLOCK_SIZE; slot++){
		if(!pod_table[slot]){

			struct block* block = container->block[s1];
			block->last = MAX(block->last, slot);
			//printf("shared<P>::%s(): slot=%i s1=%i last=%i\n", __func__, slot, s1, block->last);

			return slot;
		}
	}

	errprintf((char*)"shared<P>::%s(): no free slots!\n", __func__);
	return -1;
}
template int shared<_ayyi_plugin_nfo>::next_available_slot();
template int shared<_shm_midi_note>::next_available_slot();
template int shared<_ayyi_control>::next_available_slot();
template int shared<_route_shared>::next_available_slot();
#endif


template<class P>
void
Shared<P>::decrement_last(struct block* block)
{
	//updates the "last" pointer following object deletion

	if(block->last < 0) err("last out of range: %i\n", block->last);

    int old_last = block->last;
	dbg(1, "old_last=%i slot=%i", old_last, slot);
	if(slot == block->last) block->last = slot - 1;
	//decrement until we find a non-empty slot.
	while(!block->slot[block->last]){
		if(block->last == -1) break; //all slots are empty.
		block->last--;
		if(block->last <  -1) err("last=%i\n", block->last);
	}

	dbg(1, "last=%i--->%i", old_last, block->last);
}
template void Shared<_ayyi_channel>::decrement_last(struct block* block);
template void Shared<_route_shared>::decrement_last(struct block* block);


template<class P>
void
Shared<P>::announce()
{
	if(is_announced)
		return;
	is_announced = true;

	if(!AyyiServer::enable_announcements)
		return;
	msg_out_obj_added(object_type, get_index());
}
template void Shared<_region_shared>::announce();
template void Shared<_filesource_shared>::announce();
template void Shared<_route_shared>::announce();
template void Shared<_ayyi_shm_playlist>::announce();
template void Shared<_ayyi_channel>::announce();


template<class P>
void
Shared<P>::announce_delete()
{
	if(AyyiServer::enable_announcements) msg_out_obj_deleted(object_type, get_index());
}
template void Shared<_region_shared>::announce_delete();
template void Shared<_route_shared>::announce_delete();
template void Shared<_ayyi_shm_playlist>::announce_delete();


template<class P>
void
Shared<P>::emit_changed(int prop_type)
{
	if(!AyyiServer::enable_announcements) return;

	if(is_announced)
		ad_dbus_emit_property(dbus, object_type, get_index(), prop_type, NULL);
	else
		dbg(1, "cannot emit signal: object not yet announced. %s:%i", AyyiServer::get_object_string((ObjType)object_type)->c_str(), get_index());
}
template void Shared<_filesource_shared>::emit_changed(int);
template void Shared<_region_shared>::emit_changed(int);
template void Shared<_route_shared>::emit_changed(int);
template void Shared<_ayyi_shm_playlist>::emit_changed(int);
template void Shared<_ayyi_channel>::emit_changed(int);


template<class P>
void
Shared<P>::unbind()
{
	signal_connections.drop_connections();
}
template void Shared<_route_shared>::unbind();


template<class P>
_ayyi_base_item*
Shared<P>::set_shm(_ayyi_base_item* item)
{
	shm = (P*)item;

	if(item){
		slot = item->shm_idx % BLOCK_SIZE;
		s1   = item->shm_idx / BLOCK_SIZE;
	}else{
		slot = -1;
		s1   = 0;
	}

	return item;
}
template _ayyi_base_item* Shared<_route_shared>::set_shm(_ayyi_base_item*);
template _ayyi_base_item* Shared<_region_shared>::set_shm(_ayyi_base_item*);
template _ayyi_base_item* Shared<_filesource_shared>::set_shm(_ayyi_base_item*);
template _ayyi_base_item* Shared<_ayyi_plugin_nfo>::set_shm(_ayyi_base_item*);
template _ayyi_base_item* Shared<_ayyi_channel>::set_shm(_ayyi_base_item*);
template _ayyi_base_item* Shared<_connection_shared>::set_shm(_ayyi_base_item*);
template _ayyi_base_item* Shared<_ayyi_shm_playlist>::set_shm(_ayyi_base_item*);


template<class P>
void
Shared<P>::print()
{
	dbg(0, "...");
	dbg(0, "type=%i slot_num=%i container=%p", object_type, get_index(), container);
}


template<class P>
bool
Shared<P>::verify()
{
	if(slot < 0 || slot >= BLOCK_SIZE){ return false; }
	if(s1 < 0 || s1 >= CONTAINER_SIZE){ return false; }
	if(!object_type || object_type > AYYI_OBJECT_AUTO){ return false; }
	return true;
}
template bool Shared<_filesource_shared>::verify();


template<class P>
void
Shared<P>::_set_flag(int flag, bool val)
{
	/*
	if(val){
		route->flags |= flag;
	} else {
		route->flags &= ~flag;
	}
	*/
}


template<class P>
void
Shared<P>::_set_name(char* name)
{
	g_strlcpy(shm->name, name, AYYI_NAME_MAX);
}
template void Shared<_route_shared>::_set_name(char*);
template void Shared<_region_shared>::_set_name(char*);
template void Shared<_ayyi_shm_playlist>::_set_name(char*);
template void Shared<_connection_shared>::_set_name(char*);


int
DBusClient::msg_out_obj_added(ObjType object_type, uint32_t pod_idx)
{
	//broadcast to other clients that an object has been added.

	dbg(1, "object_type=%s pod_idx=%i", AyyiServer::get_object_string((ObjType)object_type)->c_str(), pod_idx);

	if(object_type == AYYI_OBJECT_UNSUPPORTED) return 0;

#ifdef USE_DBUS
	GError* error = NULL;
	ad_dbus_emit_objnew(dbus, object_type, pod_idx, &error);
	if(error){
		err("%s\n", error->message);
		g_free(error);
	}
#endif

	return 0;
}


int
DBusClient::msg_out_obj_deleted(ObjType object_type, uint32_t pod_idx)
{
	//broadcast to other clients that an object has been removed.

	dbg(0, "...");
	if(!object_type) warn("object_type not set!");

#ifdef USE_DBUS
	GError* error = NULL;
	ad_dbus_emit_deleted(dbus, object_type, pod_idx, &error);
	P_GERR;
#endif

	return 0;
}


} //end namespace Ayi
