/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <unistd.h>
#include <cerrno>
#include <string>
#include <climits>

#include <sigc++/bind.h>

#include <pbd/failed_constructor.h>
#include <pbd/stl_delete.h>
#include <pbd/xml++.h>

#include <ardour/playlist.h>
#include <ardour/session.h>
#include <ardour/session_playlist.h>
#include <ardour/region.h>
#include <ardour/audioregion.h>
#include <ardour/region_factory.h>
#include <ardour/track.h>

#include "i18n.h"

#ifdef VST_SUPPORT
  #include "fst.h"
#endif

#include "libs/ayyi/utils.h"
#include "ayyi/inc/song.h"

extern int debug_playlist;
#define PLAYLIST_COLOUR "\x1b[38;5;177m"
#define dbg(N, A, ...) { if (N <= debug_playlist){p_(PLAYLIST_COLOUR); dbgprintf("AyiMidiPlaylist", __func__, (char*)A, ##__VA_ARGS__); p_(white); }}
#define warn(A, ...) warnprintf2("AyiMidiPlaylist", __func__, (char*)A, ##__VA_ARGS__)
#define err(A, ...) errprintf2("AyiMidiPlaylist", __func__, (char*)A, ##__VA_ARGS__)

namespace Ayi {
	template<class T> class MixerTrack;
}
#include "ayyi-ardour/mixer_track.h"
#include "ayyi-ardour/route.h"
#include "ayyi-ardour/region.h"
#include "ayyi-ardour/playlist.h"
#include "ayyi-ardour/midi_playlist.h"

using namespace std;
using namespace ARDOUR;
using namespace PBD;
using namespace Ayi;

AyiMidiPlaylist::AyiMidiPlaylist (Session& session, string nom, bool hide)
	: MidiPlaylist (session, nom, hide),
	  Shared<_ayyi_shm_playlist>(AyiPlaylist::shm)
{
	dbg(1, "(nom, hide)...");

	add_pod();
}


AyiMidiPlaylist::AyiMidiPlaylist (Session& session, const XMLNode& node, bool hide)
	: MidiPlaylist (session, node, hide),
	  Shared<_ayyi_shm_playlist>(AyiPlaylist::shm)
{
	dbg(1, "xml...");

	add_pod();
}


AyiMidiPlaylist::AyiMidiPlaylist (boost::shared_ptr<const MidiPlaylist> old, string namestr, bool hidden)
	: MidiPlaylist (old, namestr, hidden),
	  Shared<_ayyi_shm_playlist>(AyiPlaylist::shm)
{
	dbg(1, "(other)... ***** not handled!");
	
	add_pod();
}


AyiMidiPlaylist::AyiMidiPlaylist (boost::shared_ptr<const MidiPlaylist> other, framecnt_t start, framecnt_t cnt, string str, bool hide)
	: MidiPlaylist (other, start, cnt, str, hide),
	  Shared<_ayyi_shm_playlist>(AyiPlaylist::shm)
{
	dbg(1, "(other2)...");

	add_pod();
}


AyiMidiPlaylist::~AyiMidiPlaylist ()
{
	p(0, "%sAyyiMidiPlaylist::~AyyiMidiPlaylist()...%s", blue, white);

	remove_pod();

	announce_delete();
}


void
AyiMidiPlaylist::add_pod()
{
	//initialise properties related to shm data, and allocate one playlist block in shm.

	dbg(1, "%s", name().c_str());

	if(name().find("auditioner") != string::npos) return;

	ShmContainer<struct _ayyi_shm_playlist>* ocontainer = AyiPlaylist::playlists;

	if(!(shared_ = ocontainer->add_item_with_idx())) throw;
	set_shm((_ayyi_base_item*)shared_);

	_set_name((char*)name().c_str());
	shared_->object = (void*)this;
	shared_->track = -1;
	shared_->flags = Ayi::Playlist::Midi;

	//for name:
	static PBD::ScopedConnectionList ____connections; // FIXME added for compilation, but almost certainly wont work!
	this->PropertyChanged.connect_same_thread (____connections, boost::bind(&AyiMidiPlaylist::name_changed, this));
}


void
AyiMidiPlaylist::remove_pod()
{
	ShmContainer<_ayyi_shm_playlist>* container = AyiPlaylist::playlists;
	container->remove_item(get_index());
	set_shm(NULL);
}


//const
boost::shared_ptr<AudioRegion>
AyiMidiPlaylist::get_boost_region_from_pod(uint32_t pod_index)
{
	//look through this playlist's regions for one with the given pod index.

	dbg(1, "...");
	dbg(1, "playlistname=%s", name().c_str());
	boost::shared_ptr<AudioRegion> r;

	_region_shared* region_pod = AyyiRegion::regions->get_item(pod_index);
	if(!region_pod){ err("shared not set."); return r; }
	dbg(1, "shared=%p", shared_);

	for(RegionList::const_iterator x = regions.begin(); x != regions.end(); ++x){
		boost::shared_ptr<AudioRegion> audioregion = boost::dynamic_pointer_cast<AudioRegion> (*x);
		PBD::ID id       = audioregion->id();
		uint64_t test_id = id.get_id();
		uint64_t this_id = region_pod->id;
		dbg(1, "   test_id=%Lu %Lu", test_id, this_id);
		if(test_id == this_id) return audioregion;
	}

	return r;
}


void
AyiMidiPlaylist::remove_region(boost::shared_ptr<Region> region)
{
	cout << "AyiMidiPlaylist::remove_region()..." << endl;

	//we could connect to the signal, but overrideing the parent fn directly seems easier...
	//warning!! parent fn is not virtual.
	//sigc::signal<void,boost::shared_ptr<Region> > RegionRemoved;
	//this->RegionRemoved.connect(sigc::mem_fun(*this, &AyiMidiPlaylist::region_removed));

	Playlist::remove_region(region);

	struct _region_base_shared* shared_region = region->shared_obj->shm_pod;
	shared_region->playlist = -1;
}


void
AyiMidiPlaylist::name_changed()
{
	dbg(1, "...");
	//TODO i think that when a track name changes, the playlist name changes too. Parts need to be updated.
}


void
AyiMidiPlaylist::set_track(boost::shared_ptr<ARDOUR::Track> track)
{
	Ayi::AyyidRoute* ayyi_track = track->ayyi_track;
	if(ayyi_track) shared_->track = ayyi_track->get_index();
	else warn("no track");
	dbg(0, "playlist=%i '%s' track=%i", get_index(), shared_->name, shared_->track);

	//check this track is not used by another playlist
	struct _ayyi_shm_playlist* shared = NULL;
	while((shared = AyiPlaylist::playlists->next(shared))){
		if(shared->shm_idx == get_index()) continue;
		if(shared->track == shared_->track && (shared->flags & Ayi::Playlist::Midi) == (shared_->flags & Ayi::Playlist::Midi)){
			err("track already used by another playlist. track=%i", shared_->track);
		}
	}

	//core->print_playlists();
}


void
AyiMidiPlaylist::remove_unused()
{
#if 0
	_ayyi_shm_playlist* playlist = NULL;
	while((playlist = core->song.osong->playlists.next(playlist))){
		ARDOUR::Playlist* _pl = (ARDOUR::Playlist*)playlist->object;
		AyiMidiPlaylist* pl = dynamic_cast<AyiMidiPlaylist*> (_pl);
		dbg(0, "       playlist track=", pl->shm->track);
	}
#endif
	struct _ayyi_shm_playlist* shared = NULL;
	while((shared = AyiPlaylist::playlists->next(shared))){
		if(shared->track < 0){
			dbg(0, "found unused playlist. removing...");
			//Playlist* _pl = (Playlist*) shared->object;
			boost::shared_ptr<ARDOUR::Playlist> pl = ((Playlist*)shared->object)->shared_from_this();
			Session::instance->remove_playlist(boost::weak_ptr<ARDOUR::Playlist>(pl));
		}
	}
}


