/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <cerrno>
#include <cstdlib>
#include <signal.h>
#include <unistd.h>
#include <sigc++/bind.h>

#include <ardour/ardour.h>
#include <ardour/region.h>
#include <ardour/region_factory.h>
#include <ardour/midi_source.h>
#include <ardour/midi_model.h>
#include <ardour/bundle.h>
#include <ardour/session.h>

#include "ayyi/utils.h"
#include "ardourd/dbus_object.h"

#include "ayyi-ardour/midi_region.h"

#include "ayyi/container-impl.cc"

extern int debug_source;
extern int debug_region;
extern int debug;
extern int debug_general;

#define dbg(N, A, ...){ if(N <= debug) dbgprintf("AyyiMidiNote", __func__, (char*)A, ##__VA_ARGS__); }
#define warn(A, ...) warnprintf2("AyyiMidiNote", __func__, (char*)A, ##__VA_ARGS__)
#define err(A, ...) errprintf2("AyyiMidiNote", __func__, (char*)A, ##__VA_ARGS__)
#define region_dbg(L, A, ...) { if(L <= debug_region) dbgprintf("AyyiMidiRegion", __func__, (char*)A, ##__VA_ARGS__); }

namespace Ayi {

using namespace Ayi;
using namespace std;
using namespace ARDOUR;
using namespace Evoral;
using boost::shared_ptr;

extern ARDOUR::Session* session;


#if 0
AyyiMidiNote::AyyiMidiNote()
	: Shared<_shm_midi_note>(static_cast<ShmSegDefn*>(AyyiRegion::shm))
{
#warning AyyiMidiNote: hardcoded region 0
	boost::shared_ptr<ARDOUR::MidiRegion> r = AyyiMidiRegion::get(0);
	AyyiMidiRegion* mr = (AyyiMidiRegion*)((midi_region_shared*)r->shared_obj)->server_object;
	container = &mr->events;
#if 0
	Ayi::ShmContainer<struct _midi_region_shared>* midi_regions = AyyiMidiRegion::midi_regions;

	block* block = container->block[0];
	if(!container->block[0]) dbg(0, "*** error! events container not initialised!");

	shm_midi_note* note = (struct _shm_midi_note*)malloc_ex(sizeof(struct _shm_midi_note), ((ShmHeader*)AyyiRegion::shm->address)->tlsf_pool);

	dbg(0, "container=%x block=%p slot=%p", (char*)this->container-(char*)this->base, block, block->slot);
	this->slot          = this->next_available_slot();
	this->object_type   = AYYI_OBJECT_MIDI_NOTE;

	block->slot[slot] = note;
#else
	shm_midi_note* note = container->add_item();
#endif

	//----------------------------------

	static int            note_num = 48;
	static jack_nframes_t start    = 0;
	static jack_nframes_t length   = 44100 / 8;

	note->shm_idx = slot;
	note->note    = note_num;
	note->start   = start;
	note->length  = length;

	note_num += 7;
	start    += 44100 / 8;

	//dbg(0, "pod_index=%x slot=%i last=%i", (char*)slot-(char*)this->base, slot, block->last);
}
#endif


Ayi::ShmContainer<struct Ayi::_midi_region_shared>* AyyiMidiRegion::midi_regions;


AyyiMidiRegion::AyyiMidiRegion(MidiRegion* parent_region)
	: events((ShmSegDefn*)AyyiRegion::shm)
{
	//initialise the shm-shared part of the region class.
	region_dbg(0, "...");

	if(!AyyiRegion::shm){ dbg(0, "shm not initialised!"); return; }

	//struct _song_shm* song_shm = (_song_shm*)AyyiRegion::shm->address;

	// TODO remove cast - class is parameterised incorrectly
	container = (Ayi::ShmContainer<Ayi::_region_shared>*)AyyiMidiRegion::midi_regions;

	region_dbg(1, "name='%s'", parent_region->name().c_str());
	midi_region_shared* shared = AyyiMidiRegion::midi_regions->add_item_with_idx();
	set_shm((_ayyi_base_item*)shared);
	shm_pod = (_region_base_shared*)shared;

	//parent_region->shared = (struct _region_shared*)shared;

	PBD::ID parent_id       = parent_region->id();
	shared->id              = parent_id.get_id();
	shared->server_object   = (void*)parent_region;
	shared->position        = parent_region->position();
	shared->length          = parent_region->length();
	shared->flags           = 0;
	/*
	shared->level           = parent_region->scale_amplitude();
	*/
	shared->playlist = -1;      // at this point the region has no playlist - it is set later.

	object_type = AYYI_OBJECT_MIDI_PART;

	region_dbg(0, "shared=%p slot=%i pod=%p name=%s position=%i", this, slot, shm_pod, shared->name, shared->position);

	boost::shared_ptr<ARDOUR::MidiSource> src = parent_region->midi_source();
	if (src.get()) {
		Glib::Threads::Mutex::Lock lock (src->mutex());
		src->load_model(lock);
		boost::shared_ptr<ARDOUR::MidiModel> model = src->model();
		model->ContentsChanged.connect_same_thread(signal_connections, boost::bind(&AyyiMidiRegion::contents_changed, this));
	}

	events.init(&shared->events, "midi events");

	update_shm_contents();
	region_dbg(1, "events.count=%i", events.count_items());

	parent_region->PropertyChanged.connect_same_thread(signal_connections, boost::bind(&AyyiRegion::state_changed, this, _1, boost::weak_ptr<Region> ()));

	if(events.count_items()) events.print();
}


AyyiMidiRegion::~AyyiMidiRegion()
{
	region_dbg(0, "...");
	AyyiMidiRegion::midi_regions->remove_item_delayed(get_index());
}


void
AyyiMidiRegion::modify_events(std::list<struct _shm_midi_note*>* modified_notes)
{
	//as of 200801, libardour doesnt have a move function though its on the todo list. Gtkardour deletes the note and creates a new one.
	//     update: try NoteDiffCommand::change (const NotePtr note, Property prop, TimeType new_time)

	MidiRegion* region = (MidiRegion*)shm_pod->server_object;
	boost::shared_ptr<ARDOUR::MidiSource> src = region->midi_source();
	if (src.get()) {
		boost::shared_ptr<ARDOUR::MidiModel> model = src->model();

		for (std::list<struct _shm_midi_note*>::iterator i = modified_notes->begin(); i != modified_notes->end(); ++i) {
			struct _shm_midi_note* modified_note = (*i);

			// find this note in the model.
			// for now we can just find the nth event in the model, but we need to find a better way of doing it.
			// model->notes() is a std::multiset - see evoral/Sequence

			region_dbg(1, "modifying note... idx=%i", modified_note->shm_idx);
			/*
			{
				boost::shared_ptr<Evoral::Note<Evoral::Beats> > note;
				for (MidiModel::Notes::const_iterator j = model->notes().begin(); j != model->notes().end(); j++) {
					note = (*j);
					region_dbg(0, "    note=%i start=%f len=%.2f", note->note(), note->time(), note->length());
				}
			}
			*/
			boost::shared_ptr<Evoral::Note<Evoral::Beats> > model_note;
			guint n = 0;
			for (MidiModel::Notes::const_iterator j = model->notes().begin(); j != model->notes().end(); j++) {
				if (n == modified_note->shm_idx) {
					model_note = (*j);
					break;
				}
				n++;
			}
			if (!model_note.get()) { err("model_note not found.\n"); return; }

			//struct _shm_midi_note* note = events.get_item(modified_note->shm_idx);

			BeatsFramesConverter converter (Session::instance->tempo_map(), 0);

			uint8_t chan = 0;
			Evoral::Beats time = Evoral::Beats(converter.from(modified_note->start));
			Evoral::Beats len = Evoral::Beats(converter.from(modified_note->length));
#if 0
			region_dbg(1, "new_start=%.2f", time);
			region_dbg(0, "start: %.4f --> %.4f", model_note->time(), time);
			region_dbg(0, "velocity: %i --> %i", model_note->velocity(), modified_note->velocity);
			region_dbg(0, "new_length=%.2f", len);
#endif
			const boost::shared_ptr<Evoral::Note<Evoral::Beats> > copy(new Note<Evoral::Beats>(chan, time, len, modified_note->note, modified_note->velocity)); 

			ARDOUR::MidiModel::NoteDiffCommand* command = model->new_note_diff_command("modify notes");
			command->remove(model_note);
			command->add(copy);

			model->apply_command(*session, command); // this xfers ownership of the command - no need to free.
		}
	}
}


void
AyyiMidiRegion::contents_changed()
{
	region_dbg(1, "...");
	update_shm_contents();
	ad_dbus_emit_property(AyyiMidiRegion::dbus, AYYI_OBJECT_MIDI_PART, get_index(), 0, NULL);
	//this->events.print();
}


bool
AyyiMidiRegion::del()
{
	g_return_val_if_fail(shm_pod, false);
	ARDOUR::Region* r = (ARDOUR::Region*)shm_pod->server_object;
	if(r){
		delete (AyyiMidiRegion*)r->shared_obj;
		r->shared_obj = NULL;
		signal_connections.drop_connections();
	}
	return true;
}


template<>
void
ShmContainer<midi_region_shared>::set_container()
{
	container = &((SongHeader*)AyyiRegion::shm->address)->midi_regions;
	container->obj_type = AYYI_OBJECT_MIDI_PART;
}


void
AyyiMidiRegion::update_shm_contents()
{
dbg(0, "...");
	events.erase_tlsf();
//events.erase_tlsf();

	MidiRegion* region = (MidiRegion*)shm_pod->server_object;
	boost::shared_ptr<ARDOUR::MidiSource> src = region->midi_source();
	if (src.get()) {
		region_dbg(0, "source_position=%Lu", src->timeline_position());
		/* load_model no longer has an implementation for base ARDOUR::MidiSource
		if(src->load_model != 0){
			src->load_model();
		}
		*/
		boost::shared_ptr<ARDOUR::MidiModel> model = src->model();
		MidiModel::Notes& notes (model->notes());
		region_dbg(0, "notes.len=%i %i", notes.size(), model->n_notes());
		int n = 0;
		for (MidiModel::Notes::const_iterator i = notes.begin(); i != notes.end(); i++) {
			region_dbg(0, "  note: %i %i time=%.2f len=%.2f", (*i)->note(), (*i)->velocity(), (*i)->time(), (*i)->length());

			struct _shm_midi_note* shm_note = this->events.add_item();
			if(!shm_note) return;
			copy_note_contents(shm_note, (*i), n);
			region_dbg(1, "idx=%i", shm_note->shm_idx);
			n++;
		}
	}
	//core->print_regions();
}


void
AyyiMidiRegion::copy_note_contents(struct _shm_midi_note* shm_note, boost::shared_ptr<Evoral::Note<Evoral::Beats> > note, uint16_t idx)
{
	BeatsFramesConverter converter (Session::instance->tempo_map(), 0);

	*shm_note = {
		.shm_idx  = idx,
		.note     = note->note(),
		.velocity = note->velocity(),
		.start    = converter.to(note->time()),
		.length   = converter.to(note->length())
	};

	dbg(1, "idx=%i vel=%i len=%.2f %i", idx, note->velocity(), note->length(), converter.to(note->length()));
}


void
AyyiMidiRegion::print()
{
	printf("        notes (%i):\n", events.count_items());
	//ShmContainer<struct _shm_midi_note> events;
	_shm_midi_note* note = NULL;
	while((note = events.next(note))){
		printf("            note=%i vel=%i start=%i length=%i\n", note->note, note->velocity, note->start, note->length);
	}
}


//temporary! for debugging
void
AyyiMidiRegion::playlist_changed()
{
	//warning: called directly by libardour. no signal is emitted when the playlist is set.

	/*
	dbg(0, "...");

	if(!shm_pod) return;
	if(is_deleted) return;

	ARDOUR::Region* r = (ARDOUR::Region*)shm_pod->server_object;
	if(r->playlist().get() == 0) return;

	g_strlcpy(shm_pod->playlist_name, r->playlist()->name().c_str(), AYYI_NAME_MAX);
	dbg("region playlist updated: %s.", shm_pod->playlist_name);
	dbg("len=%i", AYYI_NAME_MAX);
	events.verify();
	events.print();

	//now that we have a playlist, we can announce the region:
	announce();
	playlist_connections.drop_connections();
	r->playlist()->PropertyChanged.connect_same_thread(playlist_connections, boost::bind(&Ayi::AyyiRegion::playlist_name_changed, this));
	*/
}


boost::shared_ptr<MidiRegion>
AyyiMidiRegion::get(ObjIdx pod_index)
{
	boost::shared_ptr<MidiRegion> r;

	if(!session){ err("session not loaded!\n"); return r; }

	struct _region_shared* shared = (struct _region_shared*)AyyiMidiRegion::midi_regions->get_item(pod_index);
	if(!shared)     { dbg (1, "failed to get AyyiRegion. region_num=%u", pod_index); return r; }
	if(!shared->id) { dbg (1, "failed to get region id. region_num=%u", pod_index); return r; }
	uint64_t id = shared->id;

	const RegionFactory::RegionMap& region_map (RegionFactory::all_regions());
	for (RegionFactory::RegionMap::const_iterator i = region_map.begin(); i != region_map.end(); ++i) {
		PBD::ID test_id = i->first;
		if(test_id.get_id() == id){
			dbg (1, "found! region_num=%u name=%s", pod_index, shared->name);
			return boost::dynamic_pointer_cast<MidiRegion> (i->second);
		}
	}

	return r;
}


template struct _midi_region_shared* ShmContainer<struct _midi_region_shared>::get_item(int);
}
