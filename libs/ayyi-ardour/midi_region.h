/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __ayyi_midi_region_h__
#define __ayyi_midi_region_h__

#include <evoral/Sequence.hpp>
#include <ardour/midi_region.h>
#include "ayyi-ardour/region.h"
#include "ayyi-ardour/shm.h"

namespace Ayi {

class AyyiMidiNote : public Shared<struct _shm_midi_note>
{
  public:
	                       AyyiMidiNote();
	struct _shm_midi_note* shm_pod;
};


class AyyiMidiRegion : public AyyiRegion
{
  public:
	                       AyyiMidiRegion(ARDOUR::MidiRegion* parent_region);
	                      ~AyyiMidiRegion();
	void                   modify_events(std::list<struct _shm_midi_note*>*);
	void                   contents_changed();
	bool                   del();

	ShmContainer<struct _shm_midi_note> events;

	void                   print();

	void                   playlist_changed(); //temp!

	static boost::shared_ptr<ARDOUR::MidiRegion> get (ObjIdx);

	static Ayi::ShmContainer<struct Ayi::_midi_region_shared>* midi_regions;

  private:
	void                   update_shm_contents();
	void                   copy_note_contents(_shm_midi_note*, boost::shared_ptr<Evoral::Note<Evoral::Beats> >, uint16_t idx);
};

}

#endif
