/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://wwww.ayyi.org         |
 | copyright (C) 2006-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include <sys/types.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <cerrno>
#include <cstdlib>
#include <signal.h>
#include <unistd.h>

#include <gtk/gtk.h>
#include <pbd/pthread_utils.h>
extern "C" {
#include "tlsf/src/tlsf.h"
}

#include <ardour/ardour.h>

#include <ardour/tempo.h>
#include <ardour/audio_track.h>
#include <ardour/midi_track.h>
#include <ardour/session_playlist.h>
#include <ardour/diskstream.h>
#include <ardour/playlist.h>
#include <ardour/audioplaylist.h>
#include <ardour/audioregion.h>
#include <ardour/midi_region.h>
#include <ardour/region.h>
#include <ardour/region_factory.h>
#include <ardour/audiofilesource.h>
#include <ardour/silentfilesource.h>
#include <ardour/session.h>
#include <ardour/session_directory.h>
#include <ardour/bundle.h>
#include <ardour/plugin.h>

#include <sigc++/bind.h>

#include <ayyi/ayyi_time.h>
#include <ayyi/ayyi_types.h>

extern int debug_shm;
extern int debug_source;
extern int debug_connection;
extern int debug_route;
extern int debug_region;
extern int debug_playlist;
extern int debug;
namespace Ayi {
	template<class T> class MixerTrack;
}
#include "ardourd/dbus_object.h"
#include "ayyi/list.h"
#include "ayyi/utils.h"
#include "ayyi-ardour/mixer_track.h"
#include "ayyi-ardour/playlist.h"
#include "ayyi-ardour/midi_playlist.h"
#include "ayyi-ardour/midi_track.h"

#define dbg(A, ...) dbgprintf("shared", __func__, (char*)A, ##__VA_ARGS__)
#define track_err(A, ...) errprintf2("shared_track", __func__, (char*)A, ##__VA_ARGS__)
#define filesource_dbg(A, ...) dbgprintf("shared_filesource", __func__, (char*)A, ##__VA_ARGS__)

namespace Ayi {

using namespace Ayi;
using namespace std;
using namespace ARDOUR;
using boost::shared_ptr;

gnash::Shm* AyyidMidiTrack::shm;
Ayi::ShmContainer<struct Ayi::_midi_track_shared>* AyyidMidiTrack::midi_tracks;


AyyidMidiTrack::AyyidMidiTrack(ARDOUR::MidiTrack* parent)
	: Shared<_route_shared>(AyyidMidiTrack::shm)
{
	if(!parent){ track_err("no parent"); throw; }
	dbg("...");

	midi_track_shared* shared = route = AyyidMidiTrack::midi_tracks->add_item_with_idx();
	set_shm((_ayyi_base_item*)shared);

	shared->id             = ((PBD::ID)parent->id()).get_id();
	shared->object         = parent;
	shared->colour         = -1;
	shared->flags          = 0;
	_set_name((char*)parent->name().c_str());

	object_type = AYYI_OBJECT_MIDI_TRACK;

	parent->DropReferences.connect_same_thread  (connections, boost::bind(&AyyidMidiTrack::del, this, (Route*)parent));
	parent->PropertyChanged.connect_same_thread (connections, boost::bind(&AyyidMidiTrack::changed, this));
	parent->ready.connect_same_thread (connections, boost::bind(&AyyidMidiTrack::on_ready, this));

	boost::shared_ptr<Track> track = boost::dynamic_pointer_cast<Track>(parent->shared_from_this());
	if(track){
		track->PlaylistChanged.connect_same_thread (signal_connections, boost::bind(&AyyidMidiTrack::playlist_changed, this, boost::weak_ptr<Track> (track)));
	}

	announce();
}


AyyidMidiTrack::~AyyidMidiTrack()
{
	dbg("...");
	if(!route) return; //probably a hidden track.
	dbg("name=%s", route->name);

	memset(route, 0, sizeof(*route));

	AyyidMidiTrack::midi_tracks->remove_item(get_index());

	announce_delete();
}


void
AyyidMidiTrack::del(Route* route)
{
	dbg("...");

	if(route->ayyi_track == NULL){ printf("*** shared object already removed?\n"); return; }

	delete ((Ayi::AyyidMidiTrack*)(route->ayyi_track));

	route->ayyi_track = NULL; 
}


void
AyyidMidiTrack::changed()
{
	ARDOUR::Route* parent = (ARDOUR::Route*)route->object;
	_set_name((char*)parent->name().c_str());
}


void
AyyidMidiTrack::on_ready()
{
	ARDOUR::Route* parent = (ARDOUR::Route*)route->object;
	route->id = ((PBD::ID)parent->id()).get_id();
}


void
AyyidMidiTrack::playlist_changed (boost::weak_ptr<Track> wp)
{
	dbg("...");

	boost::shared_ptr<Track> track = wp.lock ();
	if(track) {
		boost::shared_ptr<AyiMidiPlaylist> midi_playlist;
		if ((midi_playlist = boost::dynamic_pointer_cast<AyiMidiPlaylist>(track->playlist()))) {
			midi_playlist->set_track(track);
		}
	}
}


} //end namespace Ayi
