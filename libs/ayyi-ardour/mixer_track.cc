/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2019 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <cerrno>
#include <cstdlib>
#include <signal.h>
#include <unistd.h>

#include <gtkmm.h>
#include <pbd/xml++.h>
#include <pbd/pthread_utils.h>

#include <ardour/ardour.h>

#include <ardour/tempo.h>
#include <ardour/session_playlist.h>
#include <ardour/diskstream.h>
#include <ardour/playlist.h>
#include <ardour/audioplaylist.h>
#include <ardour/audioregion.h>
#include <ardour/region.h>
#include <ardour/region_factory.h>
#include <ardour/audiofilesource.h>
#include <ardour/session.h>
#include <ardour/bundle.h>
#include <ardour/plugin.h>
#include <ardour/plugin_insert.h>
#include <ardour/panner.h>
#include <ardour/pannable.h>
#include <ardour/panner_shell.h>
#include <ardour/amp.h>
#include <ardour/send.h>

#include <sigc++/bind.h>

#include <ayyi/ayyi_types.h>
#include <ayyi/ayyi_time.h>
#include <ayyi/list.h>
#include "ayyi/utils.h"
#include "ayyi/inc/mixer.h"
#include "ayyi-ardour/route.h"
#include "ayyi-ardour/plugin.h"
#ifdef USE_DBUS
  #include "ardourd/dbus_object.h"
#endif
#define DAE_NOT_LIB

#ifdef VST_SUPPORT
  #include "fst/fst.h"
#endif

#include "mixer_track.h"

extern int debug_general;
extern int debug_source;
extern int debug_connection;
extern int debug_route;
extern int debug;

#define dbg(N, A, ...){ if(N <= debug_route) dbgprintf("MixerTrack", __func__, (char*)A, ##__VA_ARGS__); }
#define warn(A, ...) warnprintf2("MixerTrack", __func__, (char*)A, ##__VA_ARGS__)
#define err(A, ...) errprintf2("MixerTrack", __func__, (char*)A, ##__VA_ARGS__)

#include "ardourd/plugin_ui_simple.h"

namespace Ayi {
template<class T> class MixerTrack;

using namespace Ayi;
using namespace std;
using namespace ARDOUR;
using boost::shared_ptr;


template<class T>
Ayi::ShmContainer<struct _ayyi_channel>* MixerTrack<T>::tracks;
template Ayi::ShmContainer<struct _ayyi_channel>* MixerTrack<ARDOUR::Route>::tracks;


template<class T>
MixerTrack<T>::MixerTrack(T* parent_track, ShmSegDefn* segment)
	: Shared<_ayyi_channel>(AyyidRoute::shm.mixer),
	  vol_auto((ShmSegDefn*)AyyidRoute::shm.mixer),
	  pan_auto((ShmSegDefn*)AyyidRoute::shm.mixer),
	  plugin0_automation(0)
{
	dbg(1, "...");

	parent_track->ayyi_mixer_track = this;

	object_type = AYYI_OBJECT_CHAN; //TODO check

	container = MixerTrack<T>::tracks;

	if(!(track = container->add_item_with_idx())) throw;
	set_shm((_ayyi_base_item*)track);

	g_strlcpy(track->name, parent_track->name().c_str(), AYYI_NAME_MAX);
	track->object        = parent_track;
	track->n_in          = 1;
	track->n_out         = 1;
#if 0
	track->level         = parent_track->amp()->gain_control()->get_value();
#else
	track->level = -100;
#endif
	track->pan = 0.0;
	for(int i=0;i<AYYI_PLUGINS_PER_CHANNEL;i++) track->plugin[i].active = false;
	for(int i=0;i<AYYI_AUX_PER_CHANNEL;i++) track->aux[i] = 0;
	track->automation_list = 0;

	dbg(1, "slot=%i track_address=%p", slot, track);

	parent_track->gain_control()->alist()->StateChanged.connect_same_thread(signal_connections, boost::bind(&MixerTrack<T>::automation_changed, this));
	parent_track->gain_control()->Changed.connect_same_thread(signal_connections, boost::bind(&MixerTrack<T>::gain_changed, this));
	parent_track->processors_changed.connect_same_thread(signal_connections, boost::bind(&MixerTrack<T>::processors_changed, this, _1));

	//the panner is likely not created yet so we connect to the panner_shell change and connect the signal there when the panner is available.
	parent_track->panner_shell()->Changed.connect_same_thread(signal_connections, boost::bind(&MixerTrack<T>::panner_shell_changed, this));

#if 0
	list<boost::shared_ptr<Processor> > processors = parent_track->get_processors();
	for(list<boost::shared_ptr<Processor> >::iterator i = processors.begin(); i != processors.end(); i++){
		boost::shared_ptr<Send> send;
		if((send = boost::dynamic_pointer_cast<Send>(*i))){
			add_send(*i);
		}
	}
#else
	for(int n=0;n<AYYI_AUX_PER_CHANNEL;n++){
		boost::shared_ptr<Processor> send = parent_track->nth_send(n);
		if(send) add_send(send); else break;
	}
#endif

	announce();
}
template MixerTrack<ARDOUR::Route>::MixerTrack(ARDOUR::Route*, ShmSegDefn* segment);


template<class T>
MixerTrack<T>::~MixerTrack()
{
	//clear the shm slot and update pointers

	dbg(1, "...");
#if 0
	if (this->slot >= BLOCK_SIZE) { err("bad slot number! slot=%i\n", this->slot); return; }
	dbg(1, "base=%p", this->base);
	struct block* block = ((struct _shm_seg_mixer*)this->base)->tracks.block[0];
	if (!block) { err("slot=%i block=NULL\n", this->slot); return; }
	block->slot[this->slot] = NULL;
	this->decrement_last(block);
#else
	container->remove_item(get_index());
#endif

	((T*)track->object)->ayyi_mixer_track = 0;

	delete plugin0_automation;
}
template MixerTrack<ARDOUR::Route>::~MixerTrack();


template<class T>
bool
MixerTrack<T>::kill(int s1, int s2)
{
	//call this before rudely killing an object so that the parent can be cleaned up.

	//parent could either be updated here or in shared_track... Currently its updated in shared_track, so this function doesnt need to be called.

	dbg(1, "...");

	T* track = AyyidRoute::get_track(s2);
	if(track){
		// !!! we seem to get here even if the object has been destroyed....?

		dbg(1, "   marking parent NULL...");
		track->ayyi_track = NULL;
		return true;
	} else err("couldnt get shared track. s2=%i\n", s2); 

	return false;
}
template bool MixerTrack<ARDOUR::Route>::kill(int s1, int s2);


template<class T>
void
MixerTrack<T>::panner_changed()
{
	dbg(1, "...");
	T* parent = (T*)track->object;
	track->pan = parent->panner()->position();
}


template<class T>
void
MixerTrack<T>::panner_shell_changed()
{
	dbg(1, "...");
	T* parent = (T*)track->object;
	boost::shared_ptr<ARDOUR::Panner> __panner = parent->panner();
	if(__panner){
		dbg(1, "n_out=%i", __panner->out().n_total());
		if (__panner->out().n_total()){
			if(!track->has_pan){
				parent->pannable()->pan_azimuth_control->Changed.connect_same_thread(signal_connections, boost::bind(&MixerTrack<T>::panner_changed, this));
				dbg(1, "panner connected. size=%i position=%.2f trackwid=%i-->%i", __panner->out().n_total(), track->pan, parent->n_outputs().n_total(), track->pan, parent->n_inputs().n_total());
			}
			track->has_pan = true;
			track->pan = __panner->position();
		}
	}
}


template<class T>
void
MixerTrack<T>::automation_changed()
{
	dbg(0, "...");
}


template<class T>
void
MixerTrack<T>::gain_changed()
{
	dbg(0, "...");

	track->level = ((T*)track->object)->amp()->gain_control()->get_value();

	emit_changed(AYYI_LEVEL);
}


template<class T>
void
MixerTrack<T>::_print_processor(boost::weak_ptr<Processor> processor)
{
	PluginInsert* ins = dynamic_cast<PluginInsert*>(processor.lock().get());
	if(ins){
		dbg(0, "  plugin=%s", processor.lock()->name().c_str());
		_processor_count++;
#if 0
		boost::shared_ptr<Plugin> plugin = ins->plugin();
		if(0) core->print_plugin_parameters(plugin);
#endif
	}
}


template<class T>
void
MixerTrack<T>::processors_changed(RouteProcessorChange c)
{
	T* parent = (T*)track->object;

	_processor_count = 0;
	parent->foreach_processor(sigc::mem_fun(*this, &MixerTrack<ARDOUR::Route>::_print_processor));
	dbg(1, "plugin_count=%i", _processor_count);

	boost::shared_ptr<Processor> p;
	for(int n=0;(p = parent->nth_plugin(n));n++){
		PluginInsert* ins = dynamic_cast<PluginInsert*>(p.get());
		if(ins){
			AyyiPlugin<ARDOUR::PluginInfo, Ayi::plugin_shared>* shared_plugin = AyyiPlugin<ARDOUR::PluginInfo, Ayi::plugin_shared>::find_plugin(ins->plugin());
			dbg(1, "updating shm: plugin_slot=%i plugin_idx=%i", n, shared_plugin->get_index());
			activate_plugin_slot(n, shared_plugin->get_index());

			shared_plugin->set_controls(ins->plugin());
		}
	}
}


template<class T>
void
MixerTrack<T>::activate_plugin_slot(uint32_t plugin_slot, int plugin_idx)
{
	dbg(1, "...");
	track->plugin[plugin_slot].idx = plugin_idx;
	track->plugin[plugin_slot].active = true;
}
template void MixerTrack<ARDOUR::Route>::activate_plugin_slot(uint32_t, int);


template<class T>
void
MixerTrack<T>::add_send(boost::shared_ptr<ARDOUR::Processor> processor)
{
	//add a reference to an existing send in the first available slot in the track->aux array.

	if(!track){ warn("track is not set!"); return; }

	boost::shared_ptr<ARDOUR::Send> send;
	if(!(send = boost::dynamic_pointer_cast<ARDOUR::Send>(processor))){ warn("expected Send"); return; }

	int slot = -1;
	for(int i=0;i<AYYI_AUX_PER_CHANNEL;i++){
		if(!track->aux[i]){ slot = i; break; }
	}
	dbg(1, "slot=%i", slot);
	if(slot == -1){ warn("no aux send slots available for this channel."); return; }

	track->aux[slot] = send->shm_pod;
}
template void MixerTrack<ARDOUR::Route>::add_send(boost::shared_ptr<ARDOUR::Processor>);


template<class T>
bool
MixerTrack<T>::remove_send(int aux_idx)
{
	dbg(0, "...");
	//TODO check that: the ARDOUR::Send is freed, the shm object is freed.
	boost::shared_ptr<Send> send = get_send(aux_idx);
	if(send){
		T* route = (T*)track->object;
		route->remove_processor(send);
		track->aux[aux_idx] = NULL;
		return true;
	}
	return false;
}
template bool MixerTrack<ARDOUR::Route>::remove_send(int aux_idx);


template<class T>
boost::shared_ptr<Send>
MixerTrack<T>::get_send(int aux_idx)
{
	_ayyi_aux* aux = track->aux[aux_idx];
	if(!aux) return boost::shared_ptr<Send>();

	T* route = (T*)track->object;
	list<boost::shared_ptr<Processor> > processors = route->get_processors();
	for(list<boost::shared_ptr<Processor> >::iterator i = processors.begin(); i != processors.end(); i++){
		boost::shared_ptr<Send> send;
		if((send = boost::dynamic_pointer_cast<Send>(*i))){
			if(send->shm_pod == aux)
				return send;
		}
	}

	return boost::shared_ptr<Send>();
}
template boost::shared_ptr<Send> MixerTrack<ARDOUR::Route>::get_send(int);


template<class T>
bool
MixerTrack<T>::set_insert(int plugin_slot, boost::shared_ptr<ARDOUR::PluginInsert> insert)
{
#ifdef VST_SUPPORT
	T* parent = (T*)track->object;
	if(!insert){
		//find the processor so we can remove it
		dbg(0, "removing slot %i...", plugin_slot);

//TODO use this
//boost::shared_ptr<Processor> nth_plugin (uint32_t n);
		//g_return_val_if_fail(!inserts[plugin_slot].expired(), false);
		if(!inserts[plugin_slot].expired()){                                //this path doesnt work - use the one below
			Processor* target = inserts[plugin_slot].lock().get();
			for(guint i=0;i<parent->max_processor_streams().n_audio();i++){
				boost::shared_ptr<Processor> p = parent->nth_processor (i);
				PluginInsert* ins = dynamic_cast<PluginInsert*>(p.get());
				if(ins){
					dbg(0, "  name%i=%s", i, p->name().c_str());
					if(ins == target){
						dbg(0, "found. removing processor...");
						g_return_val_if_fail(!parent->remove_processor(p, NULL), false);
						dbg(0, "remove processor done.");
						track->plugin[plugin_slot] = 0;
						track->insert_active[plugin_slot] = false;
					}
				}
			}
		} else {
			dbg(0, "old plugin expired! %i", plugin_slot);
			//try another method instead...

			boost::shared_ptr<Processor> target = parent->nth_plugin (plugin_slot);
			if(target){
				PluginInsert* ins = dynamic_cast<PluginInsert*>(target.get());
				if(ins){
					dbg(0, "  name=%s", target->name().c_str());
					dbg(0, "  removing processor...");
					g_return_val_if_fail(!parent->remove_processor(target, NULL), false);
					dbg(0, "remove processor done.");
					track->plugin[plugin_slot] = 0;
					track->insert_active[plugin_slot] = false;
				}
			}
		}
	
	}else{
		if (parent->add_processor (insert, PreFader)) {
			warn("Route::add_processor failed!\n");
			return false;
		}

		if(!insert->active()) insert->activate(); //think is usually active...

		dbg(0, "plugin activated. opening window...");
		/* //refactored into fn below
		//open a plugin window - fst init creates a thread for this.
		Gtk::Window* parent_window = NULL;
		PluginUIWindow* plugin_ui = new PluginUIWindow (parent_window, insert);
		//plugin_windows.push_back(plugin_ui);
		dbg(0, "window created. setting ui to Insert...");
		insert->set_ui (plugin_ui);
		dbg(0, "window created.");

		if (plugin_ui->is_visible()) {
			plugin_ui->get_window()->raise();
		} else {
			plugin_ui->show_all();
		}
		*/
		open_plugin_window(plugin_slot, insert);

	 	inserts[plugin_slot] = boost::weak_ptr<ARDOUR::Processor>(insert);
		g_return_val_if_fail(!inserts[plugin_slot].expired(), false); //tmp!

		//insert.drop_references.connect();
	}
#endif
	return true;
}
template bool MixerTrack<ARDOUR::Route>::set_insert(int, boost::shared_ptr<ARDOUR::PluginInsert>);


#ifdef VST_SUPPORT
template<class T>
bool
MixerTrack<T>::open_plugin_window(int plugin_slot, boost::shared_ptr<ARDOUR::PluginInsert> insert)
{
	//open a plugin window - fst init creates a thread for this.
	Gtk::Window* parent_window = NULL;
	PluginUIWindow* plugin_ui = new PluginUIWindow (parent_window, insert);
	//plugin_windows.push_back(plugin_ui);
	dbg(0, "window created. setting ui to Insert...");
	insert->set_ui (plugin_ui);
	dbg(0, "window created.");

	if (plugin_ui->is_visible()) {
		plugin_ui->get_window()->raise();
	} else {
		plugin_ui->show_all();
	}
	return true;
}
template bool MixerTrack<ARDOUR::Route>::open_plugin_window(int, boost::shared_ptr<ARDOUR::PluginInsert>);
#endif


template<class T>
double
MixerTrack<T>::pan_map_in(double in)
{
	//map from:
	// -1.0 to +1.0, to
	//  0.0 to  1.0
	return (in + 1.0) * 0.5;
}
template double MixerTrack<ARDOUR::Route>::pan_map_in(double);


template<class T>
double
MixerTrack<T>::pan_map_out(double out)
{
	return (out * 2) - 1.0;
}
template double MixerTrack<ARDOUR::Route>::pan_map_out(double);


} //end namespace
