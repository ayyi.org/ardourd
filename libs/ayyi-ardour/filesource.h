/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __ayyi_filesource_h_
#define __ayyi_filesource_h_
#include "ayyi-ardour/shared.h"
#include "ayyi-ardour/shm.h"

#include <ardour/types.h>
namespace ARDOUR {
	class AudioFileSource;
}

namespace Ayi {

class AyyiFilesource : public Shared<struct _filesource_shared>
{
  public:
	                      AyyiFilesource(ARDOUR::AudioFileSource* parent);
	                      AyyiFilesource();
	                     ~AyyiFilesource();
	void                  going_away();
	void                  peak_ready();

	static ARDOUR::AudioFileSource*          get_filesource        (ObjIdx);
	static boost::shared_ptr<ARDOUR::Source> get_filesource_boost  (ObjIdx);
	static int64_t        get_filesize(std::string name);
	static bool           kill(int);

	struct _filesource_shared* filesource;

	static gnash::Shm*    shm;
	static Ayi::ShmContainer<struct Ayi::_filesource_shared>* filesources;
  private:
};

};     // namespace Ayi

#endif // __ayyi_filesource_h_
