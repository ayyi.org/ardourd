// 
//   Copyright (C) 2005, 2006 Free Software Foundation, Inc.
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

#ifndef __SHM_H__
#define __SHM_H__

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string>

#include <sys/types.h>
#if !defined(HAVE_WINSOCK_H) && !defined(__riscos__)
# include <sys/ipc.h>
# include <sys/shm.h>
#elif !defined(__riscos__)
# include <windows.h>
# include <process.h>
# include <fcntl.h>
# include <io.h>
#endif

#include <ayyi/ayyi++.h>
#include <ayyi/shm_seg.h>
//#include "plugin.h"
#include "../libs/ayyi/container.h"
namespace Ayi {
	template<class T> class ShmContainer;
	class _shm_seg_mixer;
}

namespace gnash {
	class fn_call;

#ifndef MAP_INHERIT
const int MAP_INHERIT = 0;
#endif
#ifndef MAP_HASSEMAPHORE
const int MAP_HASSEMAPHORE = 0;
#endif

#define MIXER_SEGMENT_SIZE 256

class Shm : public Ayi::ShmSegDefn 
{
  public:

	Shm();
	~Shm();

	// Initialize the shared memory segment
#if 0
	bool attach(char const *filespec, bool nuke);
#endif
    
	// Resize the allocated memory segment
	bool resize(int bytes);
	bool resize();

	// Allocate a memory from the shared memory segment
	void* brk(int bytes);

	// Close the memory segment. This removes it from the system.
	bool closeMem();

	Shm*                 cloneSelf (void);

	int                  get_fd()       { return _shmfd; };
	// Accessors for testing
	void*                get_addr()     { return address; };
	char*                getName()      { return _filespec; };
	int                  getAllocated() { return _alloced; };
	bool                 exists();
	void                 print();
	void                 clear();

	enum {
		SEG_TYPE_NOT_SET,
		SEG_TYPE_SONG,
		SEG_TYPE_MIXER
	};
protected:

};

// Custom memory allocator for the shared memory segment
/*
template<typename _Tp>
class ShmAlloc
{
private:
    Shm *mmptr;
    Shm mem;
public:
    typedef size_t     size_type;
    typedef ptrdiff_t  difference_type;
    typedef _Tp*       pointer;
    typedef const _Tp* const_pointer;
    typedef _Tp&       reference;
    typedef const _Tp& const_reference;
    typedef _Tp        value_type;
    
    template<typename _Tp1>
    struct rebind
    { typedef ShmAlloc<_Tp1> other; };
    
    ShmAlloc() throw() { }
    
    ShmAlloc(const ShmAlloc& other) throw() 
        : mem(other.mem)
        { }
    
    template<typename _Tp1>
    ShmAlloc(const ShmAlloc<_Tp1>& other) throw() 
        : mem(other.mem)
        { }
    
    ~ShmAlloc() throw() { }
    
    pointer
    address(reference __x) const        { return &__x; }
    
    const_pointer
    address(const_reference __x) const { return &__x; }
    
    // Allocate memory
    _Tp*
    allocate(size_type n, const void* p = 0) {
        _Tp* ret = 0;
        if (n) {
            ret = (_Tp*)mmptr->brk(n * sizeof(_Tp));
            if (ret == 0)
                throw std::bad_alloc();
        }
        return ret;
    }
    
    // Deallocate memory
    void
    deallocate(pointer __p, size_type __n) {
        //mmptr->free(__p);
    }
    
    void construct(pointer __p, const _Tp& __val) {
        new(__p) _Tp(__val);
    }
    void destroy(pointer __p)   { __p->~_Tp(); }
};
*/

/*
class shm_as_object : public as_object
{
public:
    Shm obj;
};
*/

//#ifdef ENABLE_TESTING 
void shm_getname(const fn_call& fn);
void shm_getsize(const fn_call& fn);
void shm_getallocated(const fn_call& fn);
void shm_exists(const fn_call& fn);
//#endif

} // end of gnash namespace

// end of __SHM_H__
#endif


