/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __ad_plugin_h__
#define __ad_plugin_h__

namespace ARDOUR {
	class Plugin;
}

#include <ayyi/interface.h>
#include <ayyi/ayyi++.h>
#include "ayyi-ardour/shared.h"
namespace Ayi {

template<class T, class P>
class AyyiPlugin : public Shared<P>
{
  public:
	                         AyyiPlugin   (T* parent_plugin, ShmSegDefn* segment);
	void                     set_controls (boost::shared_ptr<ARDOUR::Plugin>);

	ShmContainer<struct _ayyi_control> controls;

	static Ayi::ShmContainer<struct _ayyi_plugin_nfo>* plugins;

	static AyyiPlugin<T, P>* find_plugin         (ARDOUR::PluginInfoPtr);
	static AyyiPlugin<T, P>* find_plugin         (boost::shared_ptr<ARDOUR::Plugin>);
	static std::string       parameter_to_string (uint32_t type, uint32_t id);
};

} //end namespace

#endif //__ad_plugin_h__
