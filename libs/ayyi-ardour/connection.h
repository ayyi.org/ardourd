/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __shared_connection_h_
#define __shared_connection_h_

#include "ardour/bundle.h"
#include "shared.h"

using namespace std;

namespace Ayi {

class AyyiConnection : public Shared<struct _connection_shared>
{
  public:
	static ShmSegDefn*    base; // try to use base in parent class?
	//static gnash::Shm*    shm;
	static Ayi::ShmContainer<struct _connection_shared>* connections;


	                       AyyiConnection   (ARDOUR::Bundle*);
	                       AyyiConnection   (std::string jack_port, bool io);
	                       AyyiConnection   (std::string jack_port1, std::string jack_port2, bool io);
	                      ~AyyiConnection   ();
	void                   on_change        (enum ARDOUR::Bundle::Change);
	static string          get_port_name    (_connection_shared*);
	static list<string>    get_port_names   (_connection_shared*);
	static AyyiConnection* find_connection  (const char*);
	static AyyiConnection* find_connection_stereo(const char*, const char*);
	static void            print_connections();
	static void            print_connection (boost::shared_ptr<ARDOUR::Bundle>/*, std::vector<boost::shared_ptr<ARDOUR::Bundle> >* bundles*/);

	struct _connection_shared* connection;
	static std::vector<AyyiConnection*> connections_without_bundles;

  private:
	void                __set_name         ();
	PBD::ScopedConnectionList signal_connections;
};

}
#endif //__shared_connection_h_
