/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __shared_h_
#define __shared_h_

#include <ayyi/ayyi_time.h>
#include <ayyi/interface.h>
#include <ayyi/container.h>

typedef struct _ArdourDbus ArdourDbus;

namespace Ayi {
class AyyiList;


class DBusClient
{
  public:
	static ArdourDbus* dbus;

	static int         msg_out_obj_added   (ObjType, uint32_t pod_idx);
	static int         msg_out_obj_deleted (ObjType, uint32_t pod_idx);
};


template<class P>
class Shared : public DBusClient
{
  public:
	                   Shared              (ShmSegDefn*);

	ObjType            object_type;
	ShmSegDefn*        base2;
	ShmContainer<P>*   container;

	void               decrement_last      (struct block*);
	void               announce            ();
	void               announce_delete     ();
	void               emit_changed        (int prop_type = AYYI_NO_PROP);
	void               unbind              ();
	int                get_index           () { return BLOCK_SIZE * s1 + slot; };
	void               print               ();
	bool               verify              ();

  protected:
	int                slot;                  //block-level index into the slot array. Value must be (>=0 && <BLOCK_SIZE)
	int                s1;                    //container index pointing to the blocks.

	bool               is_announced;          //no signals are sent until this is set.

	void               _set_name          (char*);

	_ayyi_base_item*   set_shm            (_ayyi_base_item*);
	void               _set_flag          (int flag, bool val);

	//_ayyi_base_item*   shm;
	P*                 shm;

	PBD::ScopedConnectionList signal_connections;
};

}; //end namespace Ayi

#endif //__shared_h_
