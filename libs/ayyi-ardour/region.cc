/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <cerrno>
#include <cstdlib>
#include <signal.h>
#include <unistd.h>

#include <glibmm/main.h>
extern "C" {
#include "tlsf/src/tlsf.h"
}

#include <ardour/ardour.h>
#include <ardour/tempo.h>
#include <ardour/audio_track.h>
#include <ardour/midi_track.h>
#include <ardour/session_playlist.h>
#include <ardour/diskstream.h>
#include <ardour/playlist.h>
#include <ardour/audioplaylist.h>
#include <ardour/audioregion.h>
#include <ardour/midi_region.h>
#include <ardour/region.h>
#include <ardour/region_factory.h>
#include <ardour/audiofilesource.h>
#include <ardour/silentfilesource.h>
#include <ardour/session.h>
#include <ardour/session_directory.h>
#include <ardour/bundle.h>
#include <ardour/plugin.h>

#include <ayyi/ayyi_time.h>
#include <ayyi/ayyi_types.h>
#include "ayyi/list.h"
#include "ayyi/utils.h"

extern int debug_shm;
extern int debug_source;
extern int debug_connection;
extern int debug_route;
extern int debug_region;
extern int debug_playlist;
extern int debug;
#include "ardourd/dbus_object.h"
#include "ayyi-ardour/playlist.h"
								#include "ardourd/midi.h" //temp?
#include "ayyi-ardour/midi_playlist.h"
#include "ayyi-ardour/filesource.h"
#include "ayyi-ardour/region.h"

#include "ayyi/container-impl.cc"

#define dbg(N, A, ...){ if(N <= debug_region) dbgprintf("AyyidRegion", __func__, (char*)A, ##__VA_ARGS__); }
#define warn(A, ...) warnprintf2("AyyiRegion", __func__, (char*)A, ##__VA_ARGS__)
#define err(A, ...) errprintf2("AyyiRegion", __func__, (char*)A, ##__VA_ARGS__)
#define audio_region_dbg(A, ...) dbgprintf("AyyiAudioRegion", __func__, (char*)A, ##__VA_ARGS__)

#define is_deleted (bool)(shm_pod->flags & deleted)
#define shm_pod_audio_ ((struct _region_shared*)shm_pod)


namespace Ayi {

using namespace Ayi;
using namespace std;
using namespace ARDOUR;
using boost::shared_ptr;

gnash::Shm* AyyiRegion::shm;
Ayi::ShmContainer<struct _region_shared>* AyyiRegion::regions;


AyyiRegion::AyyiRegion()
	: Shared<_region_shared>((ShmSegDefn*)AyyiRegion::shm)
{
	//because this runs *before* the derived classes, we cannot write to shm :-(

	dbg(2, "...");
}


AyyiAudioRegion::AyyiAudioRegion(AudioRegion* parent_region)
{
	//initialise the shm-shared part of the region class.

	g_return_if_fail(AyyiRegion::shm);
	if(debug_region) audio_region_dbg("...");

	container = AyyiRegion::regions;

	if(debug_region) audio_region_dbg("name='%s'", parent_region->name().c_str());
	region_shared* shared = container->add_item_with_idx();
	if(!shared) throw;
	set_shm((_ayyi_base_item*)shared);

	PBD::ID source_id       = parent_region->source()->id();

	//snprintf(shared->name, 63, "%s", parent_region->name().c_str());
	shared->id              = ((PBD::ID)parent_region->id()).get_id();
	shared->server_object   = (void*)parent_region;
	shared->flags           = 0;
	shared->position        = parent_region->position();
	shared->length          = parent_region->length();
	shared->start           = parent_region->start();
	shared->source0         = source_id.get_id();
	shared->fade_in_length  = (uint32_t)parent_region->fade_in()->back()->when;    // double ??
	shared->fade_out_length = (uint32_t)parent_region->fade_out()->back()->when;
	shared->level           = parent_region->scale_amplitude();
	//shared->playlist_name[0] = '\0';      // Currrently the region has no playlist. This is set later in track_name_changed().
	shared->playlist        = -1;      // Currrently the region has no playlist. This is set later in track_name_changed().

	shm_pod = (struct _region_base_shared*)shared;

	object_type = AYYI_OBJECT_AUDIO_PART;

	dbg(0, "slot=%i.%i name=%s position=%i", s1, slot, parent_region->name().c_str(), shared->position);

	ensure_source();

	parent_region->PropertyChanged.connect_same_thread(signal_connections, boost::bind(&AyyiRegion::state_changed, this, _1, boost::weak_ptr<Region> ()));

	//announce(); //we cant announce yet, as the playlist (track) is not defined. It is announced in playlist_name_changed().
}


AyyiRegion::~AyyiRegion()
{
	p(0, "%sAyyiRegion::~AyyiRegion()...%s", blue, white);

	announce_delete();
}


AyyiAudioRegion::~AyyiAudioRegion()
{
	p(0, "%sAyyiAudioRegion::~AyyiAudioRegion()...%s", blue, white);

	g_return_if_fail(shm_pod);

	struct _region_shared* shm_pod_audio = shm_pod_audio_;
	shm_pod_audio->start    =  0;   //unnecesary. done below
	shm_pod_audio->playlist = -1;   //unnecesary. done below

	container->remove_item_delayed(shm_pod->shm_idx);
}


void
AyyiRegion::state_changed(const PBD::PropertyChange& what_changed, boost::weak_ptr<ARDOUR::Region> weak_region)
{
	//if(debug_region) dbg("%i", what_changed);

	g_return_if_fail(shm_pod);

	if(shm_pod->flags & deleted) return;

	ARDOUR::Region* parent = (ARDOUR::Region*)shm_pod_audio_->server_object;

	//these changes are defined in ardour/ardour.h - numbers are assigned in new_change()
#if 0
								if(object_type == AYYI_OBJECT_MIDI_PART){
									dbg("** is midi");
									AyyiMidiRegion* mr = (AyyiMidiRegion*)this;
									mr->events.verify();
								}
								else dbg("<< is NOT midi");
#endif
	bool is_audio = (object_type == AYYI_OBJECT_AUDIO_PART);
	int changed = AYYI_NO_PROP;

	if (what_changed.contains (Properties::name)) {
		dbg (1, "name changed!");
		_set_name((char*)parent->name().c_str());
	}
	if (what_changed.contains(Properties::start)) {
		if(debug_region > 1) cout << "AyyiRegion::state_changed(): start=" << parent->start() << endl;
		if(is_audio){ //TODO
			shm_pod_audio_->start = parent->start();
		}
		changed |= AYYI_STIME;
	}
	if (what_changed.contains(Properties::position)) {
		dbg (2, "position changed! position=%u", parent->position());
		shm_pod_audio_->position = parent->position();
		shm_pod_audio_->length = parent->length(); //testing for trim ops
		//what_changed = (ARDOUR::Change)(what_changed & !ARDOUR::PositionChanged);
		changed |= AYYI_STIME;
	}
	if (what_changed.contains(Properties::length)) {
		//this doesnt get called for trim operations ?!
		dbg (1, "length changed! length=%i", shm_pod_audio_->length);
		shm_pod_audio_->length = parent->length();
		//what_changed = (ARDOUR::Change)(what_changed & !ARDOUR::LengthChanged);

		if(is_audio){
			//check the part is not too long (for split ops)
			boost::shared_ptr<Source> source = parent->source();
			if(shm_pod_audio_->start + shm_pod->length > source->length(0) + 1){
				dbg(0, "too long !! start=%i", shm_pod_audio_->start);
			}
			else dbg(2, "length ok. start=%i", shm_pod_audio_->start);
		}
		changed |= AYYI_LENGTH;
	}
	if (what_changed.contains(Properties::scale_amplitude)) {
		dbg (1, "pb_level changed!");
		if(is_audio){ //TODO
			shm_pod_audio_->level = ((ARDOUR::AudioRegion*)parent)->scale_amplitude();
		}
		changed |= AYYI_PB_LEVEL;
	}
	if (what_changed.contains(Properties::layer)) {
		dbg (1, "layer changed!");
	}

/* id is not a property??
	if (what_changed.contains(Properties::ID)) {
		if(debug_region) dbg ("id changed!");
	}
*/
	//TODO properly check if id has changed
	shm_pod_audio_->id = ((PBD::ID)parent->id()).get_id();

	/*
	if(debug_region > 1) dbg("remaining changes: %i", what_changed);
	else if(what_changed && debug_region) cout << "AyyiRegion::state_changed(): remaining changes: " << what_changed << endl;
	*/

	emit_changed(changed);
}


#if 0
static gboolean
check_is_deleted_timeout(gpointer data)
{
	AyyiAudioRegion* region = (AyyiAudioRegion*)data;
	struct _region_base_shared* shm_pod = region->shm_pod;
	dbg(0, "idx=%i deleted=%i", shm_pod->shm_idx, is_deleted);

	if(!is_deleted){
		dbg(0, "setting Deleted flag...");
		((_region_shared*)shm_pod)->flags |= deleted;
		region->announce_delete();

		//g_timeout_add(1000, on_audio_region_deleted_timeout, pod);
	}
	return FALSE; //stop
}
#endif


#if 0
void
AyyiRegion::on_removed(boost::weak_ptr<ARDOUR::Region> weak_region)
{
	//if this is attached to session->RegionRemoved, it will be called for *each* existing region.
	//-it is called just _before_ the objects are deleted.

	if (weak_region.expired()) { err("expired!"); return; }

	//boost::shared_ptr<Region> region = weak_region.lock();
	boost::shared_ptr<Region> region (weak_region.lock());
	if(!region) return;

	//we get here because the RegionRemoved signal is emitted. However, _sometimes_ the ARDOUR region is not actually deleted.

	//as the region is not actually deleted (because of use_count issues?), we set the deleted flag:
	AudioRegion* ar = dynamic_cast<AudioRegion*>(region.get());
	if (ar) {
		if(shm_pod){
			dbg("  %s", region->name().c_str());
#if 0 //is all this safe?
			dbg("@@@ region=%s use_count=%i pod=%p flags=%i %s", region->name().c_str(), region.use_count(), shm_pod, ((_region_shared*)shm_pod)->flags, shm_pod->name);
#endif
			PBD::ID id = ar->id();
			dbg("  got id");
			dbg("  shm_id=%Li", shm_pod->id);
			if (id.get_id() == shm_pod->id) {
				dbg("found. setting timeout...");
				g_timeout_add(1000, check_is_deleted_timeout, this);
			}
		}
	}
	//else err("FIXME doesnt work for midi regions.\n");
}
#endif


void
AyyiRegion::on_removed2(boost::weak_ptr<ARDOUR::Region> weak_region)
{
	dbg(0, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<");
}


bool
AyyiRegion::on_removed4()
{
	dbg(0, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<");
	return false;
}


void
AyyiRegion::playlist_changed()
{
	//warning: called directly by libardour. no signal is emitted when the playlist is set.

	dbg(0, "...");

	if(!shm_pod) return;
	if(is_deleted){ dbg(1, "region is deleted"); return; }

	ARDOUR::Region* r = (ARDOUR::Region*)shm_pod->server_object;
	if(r->playlist().get() == 0) return;

	int playlist_index = -1;
	char* playlist_name = 0;
	if(object_type == AYYI_OBJECT_MIDI_PART){
#if 0
		AyyiMidiRegion* mr = (AyyiMidiRegion*)this;
		//mr->events.verify();
		mr->events.print();
#endif
		AyiMidiPlaylist* ap = dynamic_cast<AyiMidiPlaylist*>(r->playlist().get());
		playlist_index = ap->get_index();
		playlist_name = ap->shared_->name;
	}else{
		AyiPlaylist* ap = dynamic_cast<AyiPlaylist*>(r->playlist().get());
		playlist_index = ap->get_index();
		playlist_name = ap->shared_->name;
	}
	if(playlist_index > -1){
		shm_pod->playlist = playlist_index;
		dbg(0, "region playlist updated: %i '%s'.", shm_pod->playlist, playlist_name);

		//now that we have a playlist, we can announce the region:
dbg(0, "announcing... id=%Lu", shm_pod->id);
		announce();
		playlist_connections.drop_connections();
		r->playlist()->PropertyChanged.connect_same_thread(playlist_connections, boost::bind(&Ayi::AyyiRegion::playlist_name_changed, this));
	} else warn("cannot get index of region playlist: %s", r->playlist()->name().c_str());

	//core->print_playlists();
}


void
AyyiRegion::playlist_name_changed()
{
	if(!shm_pod) return;
	if(is_deleted) return;
	dbg(0, "...");

	if(!shm_pod) return;

	ARDOUR::Region* r = (ARDOUR::Region*)shm_pod->server_object;
	if(!r || (r->playlist().get() == 0)) return;

//#warning check the ayyi_playlist is updated
#if 0
									AyiPlaylist* ap = dynamic_cast<AyiPlaylist*>(r->playlist().get());
									if(ap->get_index() != shm_pod->playlist) warn("playlist index mismatcplaylist index mismatchh");
	g_strlcpy(shm_pod->playlist_name, r->playlist()->name().c_str(), AYYI_NAME_MAX);
#endif
	dbg(0, "region playlist NOT updated.");
}


void
AyyiRegion::track_name_changed(char* track_name)
{
	//TODO we would like to get the name from the ARDOUR::Playlist but it is not yet updated so we temporarily pass it as an argument.

	g_return_if_fail(shm_pod);
	ARDOUR::Region* r = (ARDOUR::Region*)shm_pod->server_object;
	if(r->playlist().get()){
#if 0
#if 1
		g_strlcpy(shm_pod->playlist_name, r->playlist()->name().c_str(), AYYI_NAME_MAX);
#else
		//disabled as we are getting incorrect name
		g_return_if_fail(track_name && strlen(track_name));
		g_strlcpy(shm_pod->playlist_name, track_name, AYYI_NAME_MAX);
#endif
#endif
		dbg(0, "region playlist NOT updated.");
	}
}


bool
AyyiAudioRegion::kill(ObjIdx slot)
{
	//call this before rudely killing an object so that the parent can be cleaned up.
	//note: the shm is not cleared here - that is done in the destructor.
	dbg(0, "...");

	/* we cant use boost pointer cos it needs the session to be loaded.
	boost::shared_ptr<ARDOUR::AudioRegion> boost = core->get_region_boost(s2);

	if(boost){
		printf("%s():   marking NULL... use_count=%li\n", __func__, boost.use_count());
		boost->shared_obj = NULL;
		boost->shared     = NULL;
		return true;
	} else errprintf("%s(): couldnt get region.\n", __func__); 
	*/


	ARDOUR::AudioRegion* region = AyyiAudioRegion::get_region(slot);
	if(region){
		// !!! we seem to get here even if the object has been destroyed....?

		//if(region->shared_obj) delete region->shared_obj;

		dbg(0, "   marking NULL...");
		region->shared_obj = NULL;
		return true;
	} else {
		err("couldnt get region.\n"); 
		//core->print_regions(); <--- cant print regions when session not loaded.
	}

	return false;
}


bool
AyyiAudioRegion::del()
{
	g_return_val_if_fail(shm_pod, false);
	ARDOUR::Region* r = (ARDOUR::Region*)shm_pod->server_object;
	if(r){
		delete (AyyiAudioRegion*)r->shared_obj; // same as 'delete this'?
		r->shared_obj = NULL;
		signal_connections.drop_connections();
	}
	return true;
}


void
AyyiRegion::ensure_source()
{
	//make sure the filesources for this region are in shm.

	ARDOUR::AudioRegion* r = (ARDOUR::AudioRegion*)shm_pod_audio_->server_object;
	if(!r){ err ("failed to get ARDOUR::AudioRegion...\n"); return; }
	for(unsigned c=0; c<r->n_channels(); c++){
		PBD::ID id = r->source(c)->id();
		dbg(1, "channel %i of %i: id=%Lu", c + 1, r->n_channels(), id.get_id());
		boost::shared_ptr<Source> source = Session::instance->source_by_id(id);
		ARDOUR::AudioFileSource* audio_source = dynamic_cast<AudioFileSource*>(source.get());
		if(!dynamic_cast<AudioFileSource*>(source.get())){ err ("cast failed! source not audio?"); }

		//**** we are getting references to sources that were deleted???? but ARDOUR object looks ok, though shared obj is screwed.
		//these pointers look ok...
		//printf("%p %p shared=%p\n", dynamic_cast<AudioFileSource*>(source.get()), audio_source, audio_source->shared_obj);

		//if(id==0){ err("failed to get source_id...\n"); return; }
		//uint64_t id_num = id.get_id();
		//if(audio_source->shared) printf("  already in shm. source_id=%Lu\n", id_num);
		AyyiFilesource* obj = audio_source->shared_obj;
		if(!obj || !obj->verify()){
			dbg(0, "filesource not in shm. Adding...");
			try {
				AyyiFilesource* shared_fs = new AyyiFilesource(audio_source);
				if(shared_fs && shared_fs->filesource) shared_fs->announce(); //eventually this will probably be in the constructor, but currently files are not announced generally.
				obj = shared_fs;
			} catch(...) {}
		}

		dbg(1, "AudioFileSource name='%s'", audio_source->name().c_str());
		dbg(1, "AudioFileSource shm_idx=%i", obj ? obj->get_index() : -1);

		if(obj){
			//obj->print();
			if(!obj->filesource){
				err("bad shared object! shm filesource not set. slot=%i\n", obj->get_index());
				/*
				core->print_files();
				return;
				*/
			}
			/*
			if((void*)audio_source->shared_obj->filesource < (void*)4096){ err("bad shared filesource pointer!! %p\n", audio_source->shared_obj->filesource); return; }
			*/

			/*
			if(!audio_source->shared_obj->filesource->name){ err("filesource name!!\n"); return; }
			dbg("source ok? filesource=%p", audio_source->shared_obj->filesource);
			//dbg("source ok? source_name=%s", audio_source->shared_obj->filesource->name);
			//cout << audio_source->shared_obj->filesource->name << endl;
			*/
		}
	}
}


ARDOUR::AudioRegion*
AyyiAudioRegion::get_region(ObjIdx pod_index)
{
	struct _region_shared* shared = AyyiRegion::regions->get_item(pod_index);

	if (!shared) { dbg(0, "failed to get AyyiRegion. region_num=%u", pod_index); return NULL; }
	ARDOUR::AudioRegion* region = (ARDOUR::AudioRegion*)shared->server_object;
	if (!region) { dbg(0, "failed to get region object. region_num=%u", pod_index); return NULL; }
	dbg(0, "pod_index=%u", pod_index);
	return region;
}


template struct _region_shared* ShmContainer<struct _region_shared>::get_item(int);
template bool ShmContainer<struct _region_shared>::slot_is_used(int slot_num);
} //end namespace Ayi
