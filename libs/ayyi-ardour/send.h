/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __ayyi_send__
#define __ayyi_send__

#include <string>
#include <set>
#include <map>
#include <list>
#include <boost/shared_ptr.hpp>
#include <sys/stat.h>
#include <glib.h>
#include <sigc++/signal.h>

namespace ARDOUR {
	class Send;
}

#include <ayyi/ayyi_time.h>
#include <ayyi/interface.h>
#include <ayyi/ayyi++.h>
#include "shared.h"
namespace Ayi {

class AyiSend : public Shared<struct _ayyi_aux>
{
  public:
	ayyi_aux*          shm_pod;
	ARDOUR::Send*      send;

	                   AyiSend(ARDOUR::Send*);
	                  ~AyiSend();
	void               created();

  protected:
	void               level_changed();
	void               output_changed(ARDOUR::IOChange, void* src);
};

}; // end namespace


#endif //__ayyi_send__
