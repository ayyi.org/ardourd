/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __ayyi_midi_track_h_
#define __ayyi_midi_track_h_

#include <ardour/types.h>
namespace ARDOUR {
	class MidiTrack;
}

#include <ayyi/ayyi_time.h>
#include <ayyi/inc/song.h>
#include <ayyi/container.h>
#include "ayyi-ardour/shm.h"

namespace Ayi {
using namespace Ayi;
class ShmSegDefn;
template<class T> class Shm_container;
template<class T> class MixerTrack;
class AyyiList;

class AyyidMidiTrack : public Shared<struct _route_shared>
{
  public:
	                      AyyidMidiTrack (ARDOUR::MidiTrack* parent);
	                     ~AyyidMidiTrack ();
	void                  del            (ARDOUR::Route*);
	void                  changed        ();
	void                  on_ready       ();
	_midi_track_shared*   route;

	static gnash::Shm*    shm;
	static Ayi::ShmContainer<struct Ayi::_midi_track_shared>* midi_tracks;
  private:
	PBD::ScopedConnectionList connections;
	void                  playlist_changed(boost::weak_ptr<ARDOUR::Track>);
};

}; //end namespace Ayi
#endif //__ayyi_midi_track_h_
