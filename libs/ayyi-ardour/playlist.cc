/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2006-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <set>
#include <fstream>
#include <algorithm>
#include <unistd.h>
#include <cerrno>
#include <string>
#include <climits>

#include <sigc++/bind.h>

#include <pbd/failed_constructor.h>
#include <pbd/stl_delete.h>
#include <pbd/xml++.h>

#include <ardour/playlist.h>
#include <ardour/session.h>
#include <ardour/session_playlist.h>
#include <ardour/region.h>
#include <ardour/audioregion.h>
#include <ardour/region_factory.h>
#include <ardour/track.h>

#include "i18n.h"

#define PLAYLIST_COLOUR "\x1b[38;5;177m"
#define dbg(N, A, ...) { if (N <= debug_playlist){p_(PLAYLIST_COLOUR); dbgprintf("AyiPlaylist", __func__, (char*)A, ##__VA_ARGS__); p_(white); }}
#define warn(A, ...) warnprintf2("AyiPlaylist", __func__, (char*)A, ##__VA_ARGS__)
#define err(A, ...) errprintf2("AyiPlaylist", __func__, (char*)A, ##__VA_ARGS__)

extern int   debug_playlist;

#include "ayyi/utils.h"
#include "ayyi-ardour/midi_region.h"
#include "ayyi-ardour/mixer_track.h"
#include "ayyi-ardour/route.h"
#include "ayyi-ardour/playlist.h"

using namespace std;
using namespace ARDOUR;
using namespace PBD;
using namespace Ayi;

gnash::Shm* AyiPlaylist::shm;
Ayi::ShmContainer<struct Ayi::_ayyi_shm_playlist>* AyiPlaylist::playlists;


AyiPlaylist::AyiPlaylist (Session& session, string nom, bool hide)
	: AudioPlaylist (session, nom, hide),
	  Shared<_ayyi_shm_playlist>(AyiPlaylist::shm)
{
	dbg(1, "(nom, hide)...");

	add_pod();
}

AyiPlaylist::AyiPlaylist (Session& session, const XMLNode& node, bool hide)
	: AudioPlaylist (session, node, hide),
	  Shared<_ayyi_shm_playlist>(AyiPlaylist::shm)
{
	p(1, "AyiPlaylist::AyiPlaylist: (xml)%s", white);

	add_pod();
}

AyiPlaylist::AyiPlaylist (boost::shared_ptr<const AudioPlaylist> old, string namestr, bool hidden)
	: AudioPlaylist (old, namestr, hidden),
	  Shared<_ayyi_shm_playlist>(AyiPlaylist::shm)
{
	dbg(1, "(other)... ***** not handled!");
	
	add_pod();
	//new Ayi::shared_playlist(this);
}

AyiPlaylist::AyiPlaylist (boost::shared_ptr<const AudioPlaylist> other, framecnt_t start, framecnt_t cnt, string str, bool hide)
	: AudioPlaylist (other, start, cnt, str, hide),
	  Shared<_ayyi_shm_playlist>(AyiPlaylist::shm)
{
	dbg(1, "(other2)...");

	add_pod();
}


AyiPlaylist::~AyiPlaylist ()
{
	p(0, "%sAyyiPlaylist::~AyyiPlaylist()...%s", blue, white);

//	if(shared_) memset(shared_, 0, sizeof(*shared_));

	remove_pod();

	announce_delete();
}


void
AyiPlaylist::add_pod()
{
	//initialise properties related to shm data, and allocate one playlist block in shm.

	dbg(1, "%s", name().c_str());

	if(name().find("auditioner") != string::npos){
		dbg(1, "auditioner! ignoring...");
		return;
	}

	ShmContainer<_ayyi_shm_playlist>* ocontainer = AyiPlaylist::playlists;
	object_type = AYYI_OBJECT_LIST; // TODO probably wrong

	if(!(shared_ = ocontainer->add_item_with_idx())) throw;
	set_shm((_ayyi_base_item*)shared_);

	_set_name((char*)name().c_str());
	shared_->object = (void*)this;
	shared_->track = -1;
	shared_->flags = 0;

	//for name:
	this->PropertyChanged.connect_same_thread (signal_connections, boost::bind(&AyiPlaylist::name_changed, this));

	announce();
}


void
AyiPlaylist::remove_pod()
{
	if(shared_){
		int idx = get_index();
		if(idx > -1){
			ShmContainer<_ayyi_shm_playlist>* container = AyiPlaylist::playlists;
			dbg(0, "idx=%i", get_index());
			container->remove_item(get_index());
		}
		set_shm(NULL);
	}
}


void
AyiPlaylist::remove_region(boost::shared_ptr<Region> region)
{
	dbg(1, "...");

	//we could connect to the signal, but overrideing the parent fn directly seems easier...
	//warning!! parent fn is not virtual.
	//sigc::signal<void,boost::shared_ptr<Region> > RegionRemoved;
	//this->RegionRemoved.connect(sigc::mem_fun(*this, &AyiPlaylist::region_removed));

	Playlist::remove_region(region);

	struct _region_base_shared* shared_region = region->shared_obj->shm_pod;
	shared_region->playlist = -1;

	//its not clear how the ARDOUR::Region reference counting is supposed to work.
	//-should removing it from the playlist free the region?
	dbg(0, "warning: deleted region! could it be used in another playlist?");
	boost::shared_ptr<AudioRegion> audio_region = boost::dynamic_pointer_cast<AudioRegion>(region);
	if(audio_region){
		((AyyiAudioRegion*)region->shared_obj)->del();
	}else{
		((AyyiMidiRegion*)region->shared_obj)->del();
	}
}


void
AyiPlaylist::name_changed()
{
	//when a track name changes, the playlist name changes too. Parts need to be updated.

	dbg(0, PLAYLIST_COLOUR "newname=%s%s", name().c_str(), white);

	g_strlcpy(shared_->name, name().c_str(), AYYI_NAME_MAX);

	emit_changed(AYYI_NAME);
}


//const
boost::shared_ptr<AudioRegion>
AyiPlaylist::find_region_by_id(uint64_t id)
{
	//look through this playlist's regions for one with the given pod index.

	dbg(1, "playlistname=%s", name().c_str());
	boost::shared_ptr<AudioRegion> r;

	//_region_shared* region_pod = core->song.osong->regions.get_item(pod_index);
	//if(!region_pod){ err("shared not set."); return r; }

	for(RegionList::const_iterator x = regions.begin(); x != regions.end(); ++x){
		boost::shared_ptr<AudioRegion> audioregion = boost::dynamic_pointer_cast<AudioRegion> (*x);
		//PBD::ID id       = audioregion->id();
		uint64_t test_id = ((PBD::ID)audioregion->id()).get_id();
		dbg(0, "   test_id=%Lu %Lu\n", test_id, id);
		if(test_id == id) return audioregion;
	}

	return r;
}


bool
AyiPlaylist::has_region_starting_at (framepos_t const p) const
{
	Playlist::RegionReadLock (const_cast<AyiPlaylist *> (this));
	
	RegionList::const_iterator i = regions.begin ();
	while (i != regions.end() && (*i)->position() != p) {
		++i;
	}
						if (i != regions.end()) {
							boost::shared_ptr<AudioRegion> audioregion = boost::dynamic_pointer_cast<AudioRegion> (*i);
							dbg(0, "duplicate region=%s", audioregion->name().c_str());
						}

	return (i != regions.end());
}


//not being used.
boost::shared_ptr<ARDOUR::Track>
AyiPlaylist::find_track()
{
	PBD::ID target_id = id();

	boost::shared_ptr<ARDOUR::RouteList> routelist = session().get_routes();
	for(ARDOUR::RouteList::iterator i = routelist->begin(); i != routelist->end(); ++i){
		//dbg(0, "   testing: route_num=%u id=%Lu test_id=%Lu %s", pod_index, id, test_id.get_id(), (*i)->name().c_str());
		boost::shared_ptr<ARDOUR::Track> track = boost::dynamic_pointer_cast<Track> ((*i));
		if(track){
			boost::shared_ptr<Playlist> playlist = track->playlist();
			if(playlist){
				PBD::ID playlist_id = playlist->id();
				dbg(0, "  id=%Li", playlist_id.get_id());
				if(playlist_id.get_id() == target_id.get_id()){
					dbg(0, "found");
					return track;
				}
			}
		}
		/*
		if(test_id.get_id() == id){
			if(debug_route) dbg(0, "route_num=%u name=%s", slot, pod->name);
			return boost::dynamic_pointer_cast<Track> ((*i));
		}
		*/
	}

	dbg(0, "not found");
	boost::shared_ptr<ARDOUR::Track> track;
	return track;
}


void
AyiPlaylist::set_track(boost::shared_ptr<ARDOUR::Track> track)
{
	Ayi::AyyidRoute* ayyi_track = track->ayyi_track;
	if(ayyi_track) shared_->track = ayyi_track->get_index();
	else warn("no track");
	dbg(0, "playlist=%i '%s' track=%i", get_index(), shared_->name, shared_->track);

	//check this track is not used by another playlist
	struct _ayyi_shm_playlist* shared = NULL;
	while((shared = playlists->next(shared))){
		if(shared->shm_idx == get_index()) continue;
		if(shared->track == shared_->track){
			err("track already used by another playlist. track=%i", shared_->track);
		}
	}

	//core->print_playlists();
}


bool
AyiPlaylist::kill(int slot)
{
	//call this before rudely killing an object so that the parent can be cleaned up.
	//note: the shm is not cleared here - that is done in the destructor.
	dbg(1, "...");

#if 0
	ARDOUR::Playlist* playlist = core->get_playlist(slot);
	if(playlist){
		// !!! we seem to get here even if the object has been destroyed....?

		//if(region->shared_obj) delete region->shared_obj;

		dbg("   marking NULL...");
		region->shared_obj = NULL;
		return true;
	} else {
		err("couldnt get region.\n"); 
	}
#endif

	return false;
}


#ifdef UNUSED
AudioRegion*
AyiPlaylist::find_region(uint64_t id)
{
	/*
	-get a playlist region object for the node with the given guid.

	*/

	dbg(0, "looking up id %Lu", id);

	/*
	AudioRegionList::iterator x = session->audio_regions.begin();

	cout << "AD::playlist_region_find(): iterating over audioregion list..." << endl;
	//for (AudioRegionList::iterator x = session->audio_regions.begin(); x != session->audio_regions.end(); ++x) {
	for ( ; x != session->audio_regions.end(); ++x) {

		AudioRegion* region = (*x).second;
		cout << "AD::playlist_region_find(): in loop: id=" << region->id() << endl;

		//if(region->id() == id) return region;
	}

	dbg(0, "failed to get playlist");
	*/

	vector<boost::shared_ptr<ARDOUR::Playlist> > playlists;
	Session::instance->playlists->get(playlists);

	for (vector<boost::shared_ptr<ARDOUR::Playlist> >::iterator i = playlists.begin(); i != playlists.end(); ++i) {
		if (!(*i)->hidden()) {
			p(0, "**********************%s", (*i)->name().c_str());
			//(obj->*func) (*i);
		}
	}

	return NULL;
}
#endif


#if 0 // cannot be used because remove_playlist is private           -------- is not public
bool
AyiPlaylist::del(ObjIdx pod_idx)
{
	playlist_shared* playlist = AyiPlaylist::playlists->get_item(pod_idx);
	ARDOUR::Playlist* _pl = (ARDOUR::Playlist*)playlist->object;
	if(_pl){
		boost::shared_ptr<ARDOUR::Playlist> pl = _pl->shared_from_this();

		dbg(0, "removing playlist...");
		Session::instance->remove_playlist (boost::weak_ptr<ARDOUR::Playlist>(pl));
	}
	return true;
}
#endif


boost::weak_ptr<ARDOUR::Playlist>
AyiPlaylist::lookup_by_trackname(string track_name) //<--- should be reference?
{
	//is now silent when playlist not found. caller must report errors.

	//** use ARDOUR::Track.playlist() in preference to this

	boost::weak_ptr<ARDOUR::Playlist> empty;
	g_return_val_if_fail(track_name.size(), empty);

	for(int i=0;i<10;i++){
		std::stringstream s;
		s << i;
		string playlist_name = i ? track_name + "." + s.str() : track_name; //try without appended number first. 
		boost::shared_ptr<ARDOUR::Playlist> playlist = Session::instance->playlists->by_name(playlist_name);
		if(playlist){
			dbg(0, "found playlist. %s --> '%s'", track_name.c_str(), playlist->name().c_str());
			return boost::weak_ptr<ARDOUR::Playlist>(playlist);
		}
	}
	dbg(0, "playlist not found: %s", track_name.c_str());
	return empty;
}


