/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __shm_seg_h__
#define __shm_seg_h__

#include <list>

#define SHM_BLOCK_SIZE 4096

namespace Ayi {

const int MAX_SHM_NAME_SIZE = 48;

typedef struct _container AyyiContainer;

#if 0
template<class P>
class shm_seg
{
  public:
	                 shm_seg(P* dummy);
	char             name[64];
	int              size;
	void*            address;
	int              seg_id;
	void*            next[1]; //top of allocation.

	P*               shm__malloc(size_t size, AyyiContainer*);
};
#endif

//this class isnt templatized so that the app can have a segment list.
class ShmSegDefn
{
  public:
	                 ShmSegDefn();
	void*            get_addr()  { return address; };
	char             name[64];
	int              size;
	int              _content_type;
	void*            address;
	int              seg_id;

	bool             attach_simple(char const *filespec, int size);

	void*            malloc(size_t size, AyyiContainer*);
	void*            malloc_tlsf(size_t size, AyyiContainer*);
	void             free_tlsf(void* ptr);
	void             reset(char* name);
	void             report();
	const char*      get_sig();

	bool             ptr_is_valid(char*);

    size_t           get_size() { return _size; };
    size_t           unallocated_space();

	struct _chunk {
		void*        start;
		size_t       size;
	}                chunk;
	std::list<struct _chunk> free_list;
  protected:
	void*            next[1];   //top of allocation.
	void*            sp;        //testing - same as above.
	size_t           _size;     //bytes
	int              table_size;

	int         _debug;

    long        _alloced;
    char        _filespec[MAX_SHM_NAME_SIZE];
#ifndef HAVE_WINSOCK_H
    key_t       _shmkey;
#else
    long        _shmkey;
    HANDLE      _shmhandle;
#endif
    int         _shmfd;
};

} //end Ayi

#endif //__shm_seg_h__
