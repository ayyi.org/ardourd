/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://ayyi.org               |
 | copyright (C) 2004-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#pragma once

#include <stdint.h>
#include <ayyi/ayyi_typedefs.h>
#include <ayyi/ayyi_types.h>
#include <ayyi/interface.h>

#ifdef __cplusplus
namespace Ayi {
using namespace Ayi;
#endif //__cplusplus

struct _song_shm
{
	char         service_name[16];
	char         service_type[16];
	int          version;
	void*        owner_shm;         // clients need this so they can calculate address offsets.
	int          num_pages;

	void*        tlsf_pool;

	char         path[256];
	char         snapshot[256];
	char         peaks_dir[128];
	uint32_t     sample_rate;
	Ayi::SongPos start;
	Ayi::SongPos end;
	Ayi::SongPos locators[MAX_LOC];
	double       bpm;
	uint32_t     transport_frame;
	float        transport_speed;
	int          play_loop;         // boolean
	int          rec_enabled;       // boolean

	struct _container regions;
	struct _container midi_regions;
	struct _container filesources;
	struct _container connections;
	struct _container routes;
	struct _container midi_tracks;
	struct _container playlists;
	struct block ports;
	struct _container vst_plugin_info;
};
typedef struct _song_shm SongHeader;

struct _region_base_shared {
	int            shm_idx; // slot_num | (block_num / BLOCK_SIZE)
	char           name[AYYI_NAME_MAX];
	uint64_t       id;
	void*          server_object;
	int            flags;
	int            playlist;
	nframes_t      position;
	nframes_t      length;
};

struct _region_shared {
	int            shm_idx;
	char           name[AYYI_NAME_MAX];
	uint64_t       id;
	void*          server_object;
	int            flags;
	int            playlist;
	nframes_t      position;
	nframes_t      length;
	// end base.
	nframes_t      start;
	uint64_t       source0;
	char           channels;
	uint32_t       fade_in_length;
	uint32_t       fade_out_length;
	float          level;
};
typedef struct _region_shared region_shared;

struct _midi_region_shared {
	int            shm_idx;
	char           name[AYYI_NAME_MAX];
	uint64_t       id;
	void*          server_object;
	int            flags;
	int            playlist;
	nframes_t      position;
	nframes_t      length;
	// end base.
	struct _container events;
};

struct _filesource_shared {
	int            shm_idx;
	char           name[AYYI_FILENAME_MAX];
	char           original_name[AYYI_NAME_MAX];
	uint64_t       id;
	void*          object;
	uint32_t       length;
};

struct _connection_shared {
	int            shm_idx;
	char           name[AYYI_NAME_MAX];
	uint64_t       id;
	void*          object;
	int            flags;
	char           device[32];
	uint32_t       nports;
	char           io;   //bool: 0=output 1=input
};

struct _route_shared {
	int            shm_idx;
	char           name[AYYI_NAME_MAX];
	uint64_t       id;
	void*          object;
	int/*ChanFlags*/ flags;
	int32_t        colour;
	char           input_name[128];
	int            input_idx;

	struct _ayyi_list* input_routing;
	struct _ayyi_list* output_routing;

	float          visible_peak_power[2];
};
typedef struct _route_shared route_shared;

struct _midi_track_shared {
	int            shm_idx;
	char           name[AYYI_NAME_MAX];
	uint64_t       id;
	void*          object;
	int            flags;
	int32_t        colour;
};
typedef struct _midi_track_shared midi_track_shared;

struct _ayyi_shm_playlist {
	int            shm_idx;
	char           name[AYYI_NAME_MAX];
	uint64_t       id;
	void*          object;
	int            flags;
	int            track;
};

#ifdef __cplusplus
}      //namspace Ayi
#endif //__cplusplus
