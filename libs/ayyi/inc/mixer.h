/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __ayyi_mixer_interface_h__
#define __ayyi_mixer_interface_h__

#include <stdint.h>
#include <ayyi/ayyi_typedefs.h>
#include <ayyi/ayyi_types.h>
#include <ayyi/interface.h>

#ifdef __cplusplus
namespace Ayi {
using namespace Ayi;
#endif

struct _shm_seg_mixer {
	char          service_name[16];
	char          service_type[16];
	int           version;
	void*         owner_shm;         //clients need this so they can calculate address offsets.
	int           num_pages;
	void*         tlsf_pool;
	//end standard shm header

	AyyiChannel       master;
	struct _container tracks;
	struct _container plugins;
};
typedef struct _shm_seg_mixer Shm_seg_mixer;

#ifdef __cplusplus
}      // namspace Ayi
#endif // __cplusplus

#endif
