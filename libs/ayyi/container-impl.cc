#include <sys/types.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <cerrno>
#include <cstdlib>
#include <string.h>
#include <stdint.h>
#include <signal.h>
#include <unistd.h>
#include <glibmm/main.h>
extern "C" {
#include "tlsf/src/tlsf.h"
}

#include <ayyi/ayyi_types.h>
#include "ayyi.h"
#include "ayyi++.h"
#include "utils.h"
#include "container.h"

namespace Ayi {

template<class T>
T*
ShmContainer<T>::get_item(int slot_num)
{
	if(!slot_is_used(slot_num)) return NULL;

	block* block = container->block[slot_num / BLOCK_SIZE];
	T* item = (T*)block->slot[slot_num % BLOCK_SIZE];
	return item;
}


template<class T>
bool
ShmContainer<T>::slot_is_used(int slot_num)
{
	// once all the other types have containers, we can move this to class Shared

	int s1 = slot_num / BLOCK_SIZE;
	int s2 = slot_num % BLOCK_SIZE;

	if(s1 < 0 || s1 >= BLOCK_SIZE) return false;
	if(s2 < 0 || s2 >= BLOCK_SIZE) return false;

	if(s1 > container->last) return false;

	block* block = container->block[s1];
	if(s2 > block->last) return false;

	if(!block->slot[s2]) return false; // valid slot number, but slot is unused.

	return true;
}

}
