/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __ayyi_h__
#define __ayyi_h__
#include <vector>

#include <ayyi/shm_seg.h>
#include <ayyi/ayyi_types.h>

namespace Ayi {

typedef struct _container AyyiContainer;
typedef struct _filesource_shared filesource_shared;
typedef struct _connection_shared connection_shared;
typedef struct _shm_midi_note shm_midi_note;
typedef struct _midi_region_shared midi_region_shared;
typedef struct _ayyi_plugin_nfo plugin_shared;
typedef struct _ayyi_control ayyi_control;
typedef struct _ayyi_aux ayyi_aux;

void                     test_set_shm_callback(int (*recv_shm_notify)(short int, int, char*, int, int, int));

class AyyiServer
{
  public:
	                     AyyiServer();
	                    ~AyyiServer();

	int                  session_num; //server should support multiple sessions. This is normally 1.

	//pointers to individual shm segments have now moved to derived class.

	static ShmSegDefn    shm_segs[10];       //change to std::vector<*> ? what type?
	//shm_seg<class P>     shm_segs_p[10];
	void                 shm_segment_add  (char* name, int size);
	void                 shm_segment_drop (char* name);
	void                 shm_segment_reset(char* name);
#ifndef USE_DBUS
	void                 shm_segment_set  (char* name, int seg_id);
	void                 shm_connect      ();
	void                 shm_disconnect   ();
	ShmSegDefn*          shm_segment_find_by_name(char* name);
#endif
	bool                 shm_is_complete  ();
	int                  shm_count_active_segments();
	bool                 shm_connect_sent;

	int                  shm_debug;

	int                  got_messaging;
	static int           got_shm;

	static void          set_shm_callback (int (Ayi::AyyiServer::*recv_shm_notify)(short int, int, char*, int, int, int));
	static void          shm_set_callback1(int (*recv_shm_notify)(short int, int, char*, int, int, int));
	int                  recv_shm_notify  (short int _notify_level, int _seg_id, char *_name, int _pages, int _rw, int _status);
	void                 shm_set_debug    (int level);

	bool                 start_messaging  ();

	static std::string*  get_object_string(Ayi::ObjType);
	static const char*   get_prop_type_string(uint32_t type);

	static bool          enable_announcements;

  protected:
	void                 shm_cleanup      ();
};

}; // namespace Ayi

#endif //__ayyi_h__
