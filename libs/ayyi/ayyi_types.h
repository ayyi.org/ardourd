#ifndef __ayyi_types_h__
#define __ayyi_types_h__
#include <stdint.h>

#ifdef __cplusplus
namespace Ayi {
#endif //__cplusplus

typedef enum {
	AYYI_AUDIO = 1,
	AYYI_MIDI
} MediaType;

//op types
enum {
	AYYI_NEW = 1,
	AYYI_DEL,
	AYYI_GET,
	AYYI_SET,
	AYYI_UNDO
};

typedef enum {
	AYYI_OBJECT_EMPTY = 0,
	AYYI_OBJECT_TRACK,
	AYYI_OBJECT_AUDIO_TRACK,
	AYYI_OBJECT_MIDI_TRACK,
	AYYI_OBJECT_CHAN,
	AYYI_OBJECT_AUX,
	AYYI_OBJECT_PART,
	AYYI_OBJECT_AUDIO_PART,
	AYYI_OBJECT_MIDI_PART,
	AYYI_OBJECT_EVENT,
	AYYI_OBJECT_RAW,
	AYYI_OBJECT_STRING,
	AYYI_OBJECT_ROUTE,
	AYYI_OBJECT_FILE,
	AYYI_OBJECT_LIST,
	AYYI_OBJECT_MIDI_NOTE,
	AYYI_OBJECT_SONG,
	AYYI_OBJECT_TRANSPORT,
	AYYI_OBJECT_LOCATORS,
	AYYI_OBJECT_AUTO,
	AYYI_OBJECT_PROGRESS,
	AYYI_OBJECT_METRONOME,
	AYYI_OBJECT_UNSUPPORTED,
	AYYI_OBJECT_ALL
#ifdef __cplusplus
} ObjType;
#else
} AyyiObjType;
#endif

//properties
enum {
	AYYI_NO_PROP         = 0,
	AYYI_NAME            = 1 <<  0,
	AYYI_STIME           = 1 <<  1,
	AYYI_LENGTH          = 1 <<  2,
	AYYI_LEVEL           = 1 <<  3,
	AYYI_PAN             = 1 <<  4,
	AYYI_PAN_ENABLE      = 1 <<  5,
	AYYI_DELAY           = 1 <<  6,
	AYYI_HEIGHT          = 1 <<  7,
	AYYI_COLOUR          = 1 <<  8,
	AYYI_END             = 1 <<  9,
	AYYI_TRACK           = 1 << 10,
	AYYI_MUTE            = 1 << 11,
	AYYI_ARM             = 1 << 12,
	AYYI_SOLO            = 1 << 13,
	AYYI_SDEF            = 1 << 14,
	AYYI_INSET           = 1 << 15,
	AYYI_FADEIN          = 1 << 16,
	AYYI_FADEOUT         = 1 << 17,
	AYYI_INPUT           = 1 << 18,
	AYYI_OUTPUT          = 1 << 19,
	AYYI_PREPOST         = 1 << 20,
	AYYI_SPLIT           = 1 << 21,

	AYYI_PLUGIN_SEL      = 1 << 22,
	AYYI_PLUGIN_BYPASS   = 1 << 23,

	AYYI_PB_LEVEL        = 1 << 24,
	AYYI_PB_PAN,
	AYYI_PB_DELAY,

	AYYI_TRANSPORT_PLAY,
	AYYI_TRANSPORT_STOP,
	AYYI_TRANSPORT_REW,
	AYYI_TRANSPORT_FF,
	AYYI_TRANSPORT_REC,
	AYYI_TRANSPORT_LOCATE,
	AYYI_TRANSPORT_CYCLE,
	AYYI_TRANSPORT_LOCATOR,

	AYYI_AUTO_PT,

	AYYI_ADD_POINT,
	AYYI_DEL_POINT,

	AYYI_TEMPO,
	AYYI_HISTORY,

	AYYI_LOAD_SONG,
	AYYI_SAVE,
	AYYI_NEW_SONG
};

enum {
	VOL = 0,
	PAN,
	AUTO_MAX
};

enum // channel/track/aux flags
{
	muted  = 1 << 0,
	solod  = 1 << 1,
	armed  = 1 << 2,
	master = 1 << 3,
	deleted= 1 << 4,

}/* ChanFlags*/;

namespace Connection
{
	enum Flags
	{
		Midi = 1 << 0,
	};
}

namespace Playlist
{
	enum Flags
	{
		Midi = 1 << 0,
	};
}

#ifdef __cplusplus
}      // namspace Ayi
#endif // __cplusplus
#endif // __ayyi_types_h__
