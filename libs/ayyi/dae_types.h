#define SM_OBJECT_TRACK DAE_OBJECT_TRACK
#define SM_OBJECT_PART  DAE_OBJECT_PART
#define SM_OBJECT_FILE  DAE_OBJECT_FILE
#define SM_OBJECT_ROUTE DAE_OBJECT_ROUTE

#define SM_NO_PROP
#define SM_NAME
#define SM_STIME
#define SM_LENGTH
#define SM_TRACK
#define SM_MUTE         SMUTE
#define SM_MUTE         PROP_MUTE
#define SM_ARM
#define SM_SOLO
#define SM_SDEF
#define SM_TRIM_LEFT    PROP_TRIM_LEFT
#define SM_SPLIT        PROP_SPLIT

#define SM_PB_DELAY_MU  PB_DELAY_MU
#define SM_PB_LEVEL     PB_LEVEL
#define SM_PB_PAN       PB_PAN
