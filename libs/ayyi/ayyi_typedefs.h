#include "ayyi/ayyi_types.h"

#ifdef __cplusplus
namespace Ayi {
#endif //__cplusplus

typedef uint32_t                    nframes_t;

typedef uint64_t                    AyyiId;
typedef int32_t                     ObjIdx;

#ifdef __cplusplus
}      //namspace Ayi
#endif //__cplusplus
