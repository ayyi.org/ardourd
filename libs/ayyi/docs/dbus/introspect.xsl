<?xml version="1.0" encoding="ISO-8859-15"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:str="http://xsltsl.org/string" xmlns:doc="http://www.freedesktop.org/standards/dbus/1.0/introdoc">

  <!-- 
       Copyright (C) 2005 Lennart Poettering.
       Modifications (C) 2006 Matthew Johnson.
       Further modifications and ugly hacks (C) 2007 Lars Luthman
       
       Licensed under the Academic Free License version 2.1

       This program is free software; you can redistribute it and/or modify
       it under the terms of the GNU General Public License as published by
       the Free Software Foundation; either version 2 of the License, or
       (at your option) any later version.

       This program is distributed in the hope that it will be useful,
       but WITHOUT ANY WARRANTY; without even the implied warranty of
       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
       GNU General Public License for more details.

       You should have received a copy of the GNU General Public License
       along with this program; if not, write to the Free Software
       Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
       -->

  <!--
       This licence doesn't make any sense. Is it academic, or GPL?
       Matt
       -->

  <!-- $Id$ -->

  <xsl:import href="http://xsltsl.sourceforge.net/modules/stdlib.xsl"/>
  <!--xsl:import href="xsltsl-1.2.1/stdlib.xsl"/-->

  <xsl:output method="xml" version="1.0" encoding="iso-8859-15" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" indent="yes" />

  <xsl:key name="types-dict" match="type" use="dbus"/>
  <xsl:variable name="type-index" select="document('types.xml')/types"/>

  <xsl:template match="/">
    <html>
      <head>
        <title>D-Bus API documentation</title>
        <style type="text/css">
          body { color: black; background-color: white; font-family: sans } 
          //h1,h3 { background-color: #CCF; border: 1px solid #999; }
          th,h3 { font-size: 125%; }
          tr.sub th { text-align: left; font-size: 1em; padding: 5px; background-color: #CCF; }
          tr.spacer td { height: 20 px; }
          tr.member td { font-size: 1em; padding: 5px; background-color: #E6E6FF; vertical-align: top; }
          td ul { list-style-type: none; padding: 0px; margin: 0px;  }
          .keyword { font-style: normal }
          .type { font-weight: bold }
          .symbol { font-style: italic }
          .interface, .package { margin: 10px; padding: 10px; }
          //table, td { border: 1px solid #999; }
          table { border-collapse: separate; border-spacing: 2px; width: 90%; }
          td { padding: 10px; }
          a { text-decoration: none }
        </style>
      </head>
      <body>
        
        <xsl:for-each select="node">
          <xsl:copy-of select="p"/>
        </xsl:for-each>
        
        <table class="package">
          <tr class="sub"><th>Interfaces</th></tr>
          <xsl:for-each select="node/interface">
          <xsl:variable name="iface_name" select="@name"/>

            <tr class="member">
              <td><a href="#{@name}"><xsl:value-of select="@name"/></a></td>
            </tr>
            <tr><td>
            <xsl:if test="method">
              <span class="type">Methods:</span><xsl:text> </xsl:text>
              <xsl:for-each select="method">
                <span class="symbol"><a href="#{$iface_name}.{@name}"><xsl:value-of select="@name"/></a></span>
                <xsl:text>, </xsl:text>
              </xsl:for-each>
              <br/>
            </xsl:if>
            <xsl:if test="signal">
              <span class="type">Signals:</span><xsl:text> </xsl:text>
              <xsl:for-each select="signal">
                <span class="symbol"><a href="#{$iface_name}.{@name}"><xsl:value-of select="@name"/></a></span>
                <xsl:text>, </xsl:text>
              </xsl:for-each>
              <br/>
            </xsl:if>
          </td></tr>
          </xsl:for-each>
        </table>
   
<xsl:for-each select="node/interface">
  <div class="interface">
    <h2 id="{@name}">
      <span class="keyword">interface</span><xsl:text> </xsl:text>
      <span class="symbol"><xsl:value-of select="@name"/></span>
    </h2>   

    <p>
      <xsl:value-of select="doc:description" />
    </p>
    
    <ul>
      <xsl:apply-templates select="annotation"/> 
    </ul>

    <xsl:variable name="iface_name" select="@name"/>

    <xsl:if test="property">
        <h3>Properties</h3>
        <table class="package">
        <xsl:for-each select="property">
          <tr>
            <td id="{$iface_name}.{@name}">
              <span class="keyword">property</span><xsl:text
              disable-output-escaping='yes'>&amp;</xsl:text>nbsp;<span
              class="symbol"><xsl:value-of select="@name"/></span>
            </td>
            <td><span class="type"><xsl:value-of select="@type"/></span></td>
            <td><span class="keyword"><xsl:value-of select="@access"/></span></td></tr>
            <tr><td colspan="3">
            <xsl:value-of select="doc:description" />
          </td>
        </tr>
        <xsl:apply-templates select="annotation"/> 
      </xsl:for-each>
    </table>
  </xsl:if>

  <xsl:if test="method">
    <!--    <table class="package">
      <tr><th colspan="3">Methods</th></tr>-->
      <h3>Methods</h3>
      <table class="package">
        <tr class="sub"><th>Name</th><th>In</th><th>Out</th></tr>
        <tr class="spacer"><td></td></tr>
      <xsl:for-each select="method">
        <tr class="member">
          <td id="{$iface_name}.{@name}"><span class="keyword">method</span><xsl:text disable-output-escaping='yes'>&amp;</xsl:text>nbsp;<span class="symbol"><xsl:value-of select="@name"/></span></td>
          <td>
            <ul>
              <xsl:for-each select="arg[@direction='in']">
                <li><span class="type"><xsl:call-template name="print-type"><xsl:with-param name="type" select="@type" /></xsl:call-template></span><xsl:text disable-output-escaping='yes'>&amp;</xsl:text>nbsp;<span class="symbol"><xsl:value-of select="@name"/></span> </li>
              </xsl:for-each>
            </ul>
          </td>
          <td>
            <ul>
              <xsl:for-each select="arg[@direction='out']">
                <li><span class="type"><xsl:call-template name="print-type"><xsl:with-param name="type" select="@type" /></xsl:call-template></span><xsl:text disable-output-escaping='yes'>&amp;</xsl:text>nbsp;<span class="symbol"><xsl:value-of select="@name"/></span> </li>
              </xsl:for-each>
            </ul>
          </td></tr>
          <tr><td colspan="3">
            <xsl:value-of select="doc:description" />
          </td></tr>
          <tr class="spacer"><td></td></tr>
        <xsl:apply-templates select="annotation"/> 
      </xsl:for-each>
    </table>
  </xsl:if>

  <xsl:if test="signal">
    <!--<table class="package">
      <tr><th colspan="2">Signals</th></tr>-->
      <h3>Signals</h3>
      <table class="package">
      <tr class="sub"><th>Name</th><th>Out</th></tr>
      <tr class="spacer"><td></td></tr>
      <xsl:for-each select="signal">
        <tr class="member">
          <td id="{$iface_name}.{@name}"><span class="keyword">signal</span><xsl:text disable-output-escaping='yes'>&amp;</xsl:text>nbsp;<span class="symbol"><xsl:value-of select="@name"/></span></td>
          <td>
            <ul>
              <xsl:for-each select="arg">
                <li><span class="type"><xsl:call-template name="print-type"><xsl:with-param name="type" select="@type" /></xsl:call-template></span><xsl:text disable-output-escaping='yes'>&amp;</xsl:text>nbsp;<span class="symbol"><xsl:value-of select="@name"/></span> </li>
              </xsl:for-each>
            </ul>
          </td></tr>
          <tr><td colspan="2">
            <xsl:value-of select="doc:description" />
          </td></tr>
          <tr class="spacer"><td></td></tr>
        <xsl:apply-templates select="annotation"/> 
      </xsl:for-each>
    </table>
  </xsl:if>

</div>
</xsl:for-each>
</body>
</html>
</xsl:template>

<xsl:template match="interface/annotation"> 
<li>
  <span class="keyword">annotation</span>
  <xsl:text disable-output-escaping='yes'>&amp;</xsl:text>nbsp;
  <code><xsl:value-of select="@name"/></code><xsl:text> = </xsl:text>
  <code><xsl:value-of select="@value"/></code>
</li>
</xsl:template>

<xsl:template match="method/annotation|property/annotation"> 
<tr><td colspan="4">
<span class="keyword">annotation</span>
<xsl:text disable-output-escaping='yes'>&amp;</xsl:text>nbsp;
<code><xsl:value-of select="@name"/></code><xsl:text> = </xsl:text>
<code><xsl:value-of select="@value"/></code>
</td></tr>
</xsl:template>

<xsl:template match="signal/annotation"> 
<tr><td colspan="3">
<span class="keyword">annotation</span>
<xsl:text disable-output-escaping='yes'>&amp;</xsl:text>nbsp;
<code><xsl:value-of select="@name"/></code><xsl:text> = </xsl:text>
<code><xsl:value-of select="@value"/></code>
</td></tr>
</xsl:template>

<xsl:template match="types"> 
<xsl:param name="typeid"/>
<xsl:value-of select="key('types-dict', $typeid)/human" />
</xsl:template>

<xsl:template name="print-type"> 
<xsl:param name="type"/>
<xsl:if test="$type != ''">
  <xsl:apply-templates select="$type-index"><xsl:with-param name="typeid" select="substring($type,1,1)"/></xsl:apply-templates>
  <xsl:text> </xsl:text>
  <xsl:call-template name="print-type"><xsl:with-param name="type" select="substring($type,2)"/></xsl:call-template>
</xsl:if>
</xsl:template>

</xsl:stylesheet>
