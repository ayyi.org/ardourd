/****************************************************************************
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
****************************************************************************/

#ifndef __ayyi_time_h__
#define __ayyi_time_h__

#ifdef __cplusplus
namespace Ayi {
#endif

#define EVENT_SUB_RANGE 3840
#define EVENT_MU_RANGE 11050
#define SM_SUBS_PER_BEAT 3840
#define SM_MU_PER_SUB 11025 //diff to above !!??

// 8 bytes wide
struct SongPos
	{
	int beat;
	unsigned short int sub;
	unsigned short int mu;
	};

#ifdef __cplusplus
} //namespace Ayi
#endif
#endif //__ayyi_time_h__
