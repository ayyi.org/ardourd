/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include <dirent.h>
#include <fcntl.h>
#include <cstdio>
#include <string.h>
#include <glib.h>
#include <iostream>
#include <list>

#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/msg.h>
#include <sys/shm.h>

#ifdef VST_SUPPORT
  #include "fst.h"
#endif

extern "C" {
#include "tlsf/src/tlsf.h"
}

#include <ayyi/ayyi_time.h>
#include <ayyi/ayyi_types.h>
#include <ayyi/ayyi++.h>
#include <ayyi/shm_seg.h>
#include "utils.h"
#include <ayyi/interface.h>
#include <ayyi/inc/song.h> //temp - create a base type for casting

#define dbg(N, A, ...) { if (N <= _debug) dbgprintf("Ayyi::shm_seg", __func__, (char*)A, ##__VA_ARGS__); }
#define err(A, ...) errprintf2("ayyi::shm_seg", __func__, (char*)A, ##__VA_ARGS__)
#define warn(A, ...) warnprintf("ayyi::shm_seg", __func__, (char*)A, ##__VA_ARGS__)

extern int debug_shm;
extern char fail[];

namespace Ayi {
//extern SongHeader* shm_index;

#if 0
template<class P>
shm_seg<P>::shm_seg(P* dummy)
{
}


template<class P>
P*
shm_seg<P>::shm__malloc(size_t malloc_size, AyyiContainer* container)
{
    P* ret;
	if(!address){
		err("shm not initialised!\n");
		return NULL;
	}

	//templateize!
	void* next_;
	if(!strcmp(name, "amixer")){
		if(debug) printf("%s(): using mixer segment...\n", __func__);
		next_ = next[0];
		next[0] = (char*)next[0] + malloc_size;
	}else if(!strcmp(name, "song")){
		next_ = next[0];
		next[0] = (char*)next[0] + malloc_size;
	}else err("type error.\n"); 

	if ( !next_ ) {
		err("segment stack pointer not initialised\n");
	}

	ret = next_;
	unsigned ceiling = (unsigned)address + size * SHM_BLOCK_SIZE;
	unsigned total_used = (unsigned)next_ - (unsigned)address;

	int percentage = 100 * total_used / ( size * SHM_BLOCK_SIZE - (unsigned)address );

	if(debug_shm) printf("shm_seg<>::%s(): segment_base=%p next=%p used=%u %i%%\n", __func__, address, next_, total_used, percentage);
	if ( (unsigned)next_ > ceiling ){
		printf("shm_seg<>::%s(): block full!? trying to use a new block...\n", __func__);

		//block_init(container);    //FIXME put this back
		return NULL;
	}

	return(ret);
}
#endif


ShmSegDefn::ShmSegDefn() : _debug(0), _alloced(0), _shmkey(0), _shmfd(0)
{
	if(debug_shm) _debug = debug_shm;
}


char nodes_dir[32] = "/tmp/ayyi/nodes/";
char tmp_dir[32] = "/tmp/ayyi/";

bool
ShmSegDefn::attach_simple(char const *filespec, int num_pages)
{
	dbg(1, "filespec=%s", filespec);
	int pagesize = sysconf(_SC_PAGESIZE);
	long size = num_pages * pagesize;
	_size = num_pages * pagesize;

	if (g_mkdir_with_parents (nodes_dir, 0775)) {
		err("cannot create tmp dir '%s', %s\n", nodes_dir, strerror(errno));
		return false;
	}

	char node_name[256];
	sprintf(node_name, "%s%s-%d", nodes_dir, filespec, (int)getpid());
if(!g_file_test(node_name, G_FILE_TEST_EXISTS)) dbg(1, "**** creating new node... (%s)", node_name)
else dbg(1, "node already exists (%s)", node_name);
	if(!g_file_test(node_name, G_FILE_TEST_EXISTS))
		creat(node_name, O_RDWR);

	if((_shmkey = ftok(node_name, 'D')) == -1){
		err("ftok: node_name=%s %s\n", node_name, fail);
		return false;
	}

	if((_shmfd = shmget(_shmkey, size, IPC_CREAT|0660)) == -1){ // 0600: r/w for user and group (see man 2 stat)
		err("shmget %s\n", fail);
		return false;
	}
	dbg(1, "_shmfd=%i", _shmfd);

	if((address = (char*)shmat(_shmfd, 0, 0)) == (char*)-1){
		if (_debug>-1) {
			if(errno == EACCES){
				err("shm attach: permission denied (%s)\n%s\n", node_name, print_fail());
			}else{
				dbg(1, "error=%i", errno);
			}
		}
		return false;
	}

	strcpy(name, "song"); //FIXME hack so that malloc can cast correctly. It acts as a seg "type" field. 

	//set the allocation pointer to just above the shm_header.
	//int index_size = (_content_type == SEG_TYPE_SONG) ? sizeof(Ayi::SongHeader) : ((_content_type == SEG_TYPE_MIXER) ? sizeof(Ayi::_shm_seg_mixer) : 0);
	next[0] = sp = (char*)address + table_size;

	return true;
}


void*
ShmSegDefn::malloc(size_t malloc_size, AyyiContainer* container)
{
	//FIXME we need a way to remove the casts to a fixed number of preset types. Templates?
	//      -we can use a "type" field, but the types really belong to the derived classes, not here.
	//      -or cast, eg, _shm_seg_mixer to a lower form  <------- yes testing this. Ok now?

	dbg(2, "...");

	if (!address || !this->size) {
		err("shm not initialised! address=%p size=%i\n", address, this->size);
		return NULL;
	}

	if (malloc_size > unallocated_space()) { err("not enough space to satisfy request!"); return NULL; }

	//void* next_;
	//next_ = next[0];
	void* ret = sp;
	next[0] = (unsigned char*)next[0] + malloc_size;
	sp = (unsigned char*)sp + malloc_size;

	if (!sp) err("segment stack pointer not initialised\n");

	uintptr_t ceiling = (uintptr_t)address + this->size * SHM_BLOCK_SIZE;

	uintptr_t total_used = (uintptr_t)sp - (uintptr_t)address;

	int percentage = 100 * total_used / this->get_size();

	dbg(2, "address=%p requested=%u(kB) available=%u(kB) next=%x ceiling=%x used=%u(kB) %i%%", address, malloc_size/1024, unallocated_space()/1024, (char*)sp-(char*)address, (char*)ceiling-(char*)address, total_used/1024, percentage);
	if ((uintptr_t)sp > ceiling) {
		err("segment full!? FIXME make new segment...sp=%p ceiling=%x", sp, ceiling);

		return NULL;
	}

    return ret;
}


void*
ShmSegDefn::malloc_tlsf(size_t malloc_size, AyyiContainer* container)
{
	//the idea is that the tlsf pool will take up most of the shm segment.
	//When the pool becomes full, we need to create a new segment.

	if(!address || !this->size){
		err("shm not initialised! address=%p size=%i\n", address, this->size);
		return NULL;
	}

	void* pool = ((ShmHeader*)address)->tlsf_pool;
	if(!pool){ err("pool not set!\n"); return NULL; }
	dbg(2, "address=%p pool=%p malloc_size=%u", address, pool, malloc_size);

	//printf("ShmSegDefn::%s(): calling malloc... size=%i\n", __func__, malloc_size);
	void* ret = malloc_ex(malloc_size, ((SongHeader*)address)->tlsf_pool);
	if(!ret){
		err("tlsf malloc failed. request_size=%i\n", malloc_size);
		report();
		return NULL;
	}

	size_t tot_used = get_used_size(((SongHeader*)address)->tlsf_pool);
	dbg(2, "used=%u", tot_used);

	return ret;
}


void
ShmSegDefn::free_tlsf(void* ptr)
{
	free_ex(ptr, ((SongHeader*)address)->tlsf_pool);
}


void
ShmSegDefn::reset(char* name)
{
	//clean up following a shm create error.

	AyyiServer::got_shm = 0;

	for(int i=0;i<10;i++){
		if(!AyyiServer::shm_segs[i].size) continue;
		if(strcmp(AyyiServer::shm_segs[i].name, name)) continue;
		AyyiServer::shm_segs[i].address = NULL;
		return;
	}
	err("couldnt find segment. name=%s\n", name);
}


#define TLSF_POOL ((SongHeader*)address)->tlsf_pool
void
ShmSegDefn::report()
{
	uintptr_t total_used = (uintptr_t)sp - (uintptr_t)address;
	int percentage = (100 * total_used) / this->get_size();
	size_t available = unallocated_space();
	dbg(0, "address=%p next=%x size=0x%x used=0x%x %i%% available=%u(kB) used_in_pool=%u(kB)", address, sp, this->get_size(), total_used, percentage, available/1024, get_used_size(((ShmHeader*)address)->tlsf_pool)/1024);
	dbg(0, "tlsf: used=%i/%i", (int)get_used_size(TLSF_POOL), (int)get_max_size(TLSF_POOL));
}


const char*
ShmSegDefn::get_sig()
{
	return ((ShmHeader*)address)->service_name;
}


bool
ShmSegDefn::ptr_is_valid(char* ptr)
{
	bool ok = (ptr > (char*)address) && (ptr < (char*)address + _size);
	if (!ok) err("bad shm address: %p.\n", ptr); 
	return ok;
}


size_t
ShmSegDefn::unallocated_space()
{
	//this returns the unallocated space in the *segment*, not the space available in the *pool*.
	//-the amount of space remaining in the pool is unknown.

	unsigned used = ((char*)sp - (char*)address);
	g_return_val_if_fail(this->get_size() >= used, 0);

	return this->get_size() - used;
}


} //end Ayi
