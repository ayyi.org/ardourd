/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <cerrno>
#include <cstdlib>
#include <string.h>
#include <stdint.h>
#include <signal.h>
#include <unistd.h>
#include <glibmm/main.h>
extern "C" {
#include "tlsf/src/tlsf.h"
}

#include <ayyi/ayyi_types.h>
#include "ayyi.h"
#include "ayyi++.h"
#include "utils.h"
#include "container.h"

#define dbg(N, A, ...){ if (N <= _debug) dbgprintf((char*)"ShmContainer", __func__, (char*)A, ##__VA_ARGS__); }
#define warn(A, ...) warnprintf2("ShmContainer", __func__, (char*)A, ##__VA_ARGS__)
#define err(A, ...) errprintf2("ShmContainer", __func__, (char*)A, ##__VA_ARGS__)

using namespace std;

namespace Ayi {

template<class T>
ShmContainer<T>::ShmContainer(ShmSegDefn* shm_seg)
	: _debug(0), container(0), seg(shm_seg)
{
	if(!shm_seg) throw;
}
template ShmContainer<_region_shared>::ShmContainer(ShmSegDefn*);
template ShmContainer<_filesource_shared>::ShmContainer(ShmSegDefn*);
template ShmContainer<_connection_shared>::ShmContainer(ShmSegDefn*);
template ShmContainer<_route_shared>::ShmContainer(ShmSegDefn*);
template ShmContainer<_ayyi_plugin_nfo>::ShmContainer(ShmSegDefn*);
template ShmContainer<_shm_event>::ShmContainer(ShmSegDefn*);
template ShmContainer<_shm_midi_note>::ShmContainer(ShmSegDefn*);
template ShmContainer<_midi_track_shared>::ShmContainer(ShmSegDefn*);
template ShmContainer<_midi_region_shared>::ShmContainer(ShmSegDefn*);
template ShmContainer<_ayyi_control>::ShmContainer(ShmSegDefn*);
template ShmContainer<_ayyi_shm_playlist>::ShmContainer(ShmSegDefn*);
template ShmContainer<_ayyi_channel>::ShmContainer(ShmSegDefn*);
#ifdef VST_SUPPORT
#warning compiling vst shm container
template ShmContainer<_shm_vst_info>::ShmContainer(ShmSegDefn*);
#endif

//note: specialisation for set_container() is done in application files.


template<class T>
void
ShmContainer<T>::init(const char* signature, bool allocate_block)
{
	dbg(0, "%s", signature);

	set_container();
	if(!container) throw; //this can only result from a compilation error.

	for(int i=0; i<CONTAINER_SIZE; i++) container->block[i] = NULL;
	container->last = -1;
	container->full =  0;

	strncpy(container->signature, signature, 15);

	if(allocate_block) block_new();
}
template void ShmContainer<_region_shared>::init(const char*, bool);
template void ShmContainer<_filesource_shared>::init(const char*, bool);
template void ShmContainer<_connection_shared>::init(const char*, bool);
template void ShmContainer<_route_shared>::init(const char*, bool);
template void ShmContainer<_ayyi_plugin_nfo>::init(const char*, bool);
template void ShmContainer<_midi_track_shared>::init(const char*, bool);
template void ShmContainer<_midi_region_shared>::init(const char*, bool);
template void ShmContainer<_ayyi_shm_playlist>::init(const char*, bool);
template void ShmContainer<_ayyi_channel>::init(const char*, bool);
#ifdef VST_SUPPORT
template void ShmContainer<_shm_vst_info>::init(const char*, bool);
#endif


template<class T>
void
ShmContainer<T>::init(struct _container* _container, const char* signature, bool allocate_block)
{
	//for containers that live inside other containers.

	dbg(2, "container=%p", _container);

	container = _container;

	if(!container){ err("container!!\n"); return; }

	for(int i=0; i<CONTAINER_SIZE; i++) container->block[i] = NULL;
	container->last = -1;
	container->full =  0;

	g_strlcpy(container->signature, signature, 16);

	if(allocate_block) block_new();
}
template void ShmContainer<_shm_event>::init(_container*, const char*, bool);
template void ShmContainer<_shm_midi_note>::init(_container*, const char*, bool);
template void ShmContainer<_ayyi_control>::init(_container*, const char*, bool);


template<class T>
void
ShmContainer<T>::clear()
{
	for(int i=0; i<CONTAINER_SIZE; i++){
		if(container->block[i]) block_free(i);
		container->block[i] = NULL;
	}
	container->last = -1;
	container->full =  0;
}
template void ShmContainer<_shm_event>::clear();


template<class T>
struct block*
ShmContainer<T>::block_new()
{
	//allocate and initialise shm for a new block and make it current

	//TODO should an allocated but empty block be pointed to by container.last ?
	//     201101: we seem to be heading for allocating blocks on demand. there should not be empty blocks...?

	dbg(1, "... ");

	if(!container){ err("container NULL!\n"); return NULL; }

	//find next free block:
	int b;
	bool found = false;
	for(b=0;b<CONTAINER_SIZE;b++){
		if(!container->block[b]){ found = true; break; }
	}
	if(!found){ err("cant find empty block. out of memory?\n"); return NULL; }

	container->last = max(container->last, b); //see TODO above.
	if(0) dbg(0, "new_block=%i", b);

	struct block* block = container->block[b] = (struct block*)seg->malloc_tlsf(sizeof(*block), NULL); // Pass null to stop recursion feedback.
	if(!block){ err("malloc failed. block NULL!!\n"); return NULL; }

	for(int i=0; i<BLOCK_SIZE; i++) block->slot[i] = NULL;
	block->last = -1;
	block->full = false;

	container->last = b;
	return container->block[b];
}
template struct block* ShmContainer<struct _region_shared>::block_new();
template struct block* ShmContainer<struct _filesource_shared>::block_new();
template struct block* ShmContainer<struct _connection_shared>::block_new();
template struct block* ShmContainer<struct _route_shared>::block_new();
template struct block* ShmContainer<struct _ayyi_plugin_nfo>::block_new();
template struct block* ShmContainer<struct _midi_track_shared>::block_new();
template struct block* ShmContainer<struct _midi_region_shared>::block_new();
template struct block* ShmContainer<struct _ayyi_shm_playlist>::block_new();
template struct block* ShmContainer<struct _track_shared>::block_new();
#ifdef VST_SUPPORT
template struct block* ShmContainer<struct _shm_vst_info>::block_new();
#endif


template<class T>
void
ShmContainer<T>::block_free(int b)
{
	if(container->block[b]) seg->free_tlsf(container->block[b]);
	container->block[b] = NULL;
}
template void ShmContainer<struct _filesource_shared>::block_free(int);


bool
block_is_full(block* block, int s)
{
	bool full = false;
	if(block->last >= BLOCK_SIZE - 1){
		full = true;
		for(int i=s;i<BLOCK_SIZE;i++){
			if(!block->slot[i]) full = false;
		}
	}
	return full;
}


template<class T>
T*
ShmContainer<T>::add_item_with_idx()
{
	T* item = 0;
	int slot_num = next_available_slot();
	if(slot_num >= 0){
		int b = slot_num / BLOCK_SIZE;
		int s = slot_num % BLOCK_SIZE;
		dbg(1, "%i.%i", b, s);

		container->block[b]->slot[s] = seg->malloc_tlsf(sizeof(T), container);
		if((item = (T*)container->block[b]->slot[s])){
			//core->shm.song_local->block_new(&route->automation);
			item->shm_idx = slot_num;

			if(block_is_full(container->block[b], s)){
				dbg(0, "block full!");
				container->block[b]->full = true;
			}
		}
	}
	return item;
}
template _region_shared*     ShmContainer<_region_shared>::add_item_with_idx();
template _route_shared*      ShmContainer<_route_shared>::add_item_with_idx();
template _midi_track_shared* ShmContainer<_midi_track_shared>::add_item_with_idx();
template _ayyi_channel*      ShmContainer<_ayyi_channel>::add_item_with_idx();
template _ayyi_shm_playlist* ShmContainer<_ayyi_shm_playlist>::add_item_with_idx();
template _connection_shared* ShmContainer<_connection_shared>::add_item_with_idx();
template _filesource_shared* ShmContainer<_filesource_shared>::add_item_with_idx();
template _ayyi_plugin_nfo*   ShmContainer<_ayyi_plugin_nfo>::add_item_with_idx();
template _midi_region_shared* ShmContainer<_midi_region_shared>::add_item_with_idx();


template<class T>
T*
ShmContainer<T>::add_item()
{
	T* item = 0;
	if(!seg->ptr_is_valid((char*)container)) return item;
	int slot_num = next_available_slot();
	dbg(1, "slot_num=%i container=%p", slot_num, container);
	if(slot_num >= 0){
		int b = slot_num / BLOCK_SIZE;
		container->block[b]->slot[slot_num] = seg->malloc_tlsf(sizeof(T), container);
		item = (T*)container->block[b]->slot[slot_num];
	}
	return item;
}
template _shm_midi_note*   ShmContainer<_shm_midi_note>::add_item();
template _ayyi_control*    ShmContainer<_ayyi_control>::add_item();
template _ayyi_shm_playlist* ShmContainer<_ayyi_shm_playlist>::add_item();
template _shm_event*       ShmContainer<_shm_event>::add_item();


template<class T>
void
ShmContainer<T>::remove_item(int slot_num)
{
	//TODO perhaps zero the slot memory.

	if(slot_num < 0 || slot_num > BLOCK_SIZE * CONTAINER_SIZE){ err("slot_num out of range: %i\n", slot_num); return; }

	dbg(1, "%s slot_num=%i", container->signature, slot_num);

	int b = slot_num / BLOCK_SIZE;
	if(b > container->last) { err("block out of range: %i > %i", b, container->last); return; }
	block* block = container->block[b];
	void** slot_ptr = &block->slot[slot_num % BLOCK_SIZE];

	if(!slot_ptr) { err ("not allocated: %i.%i", b, slot_num % BLOCK_SIZE); return; }

	if(*slot_ptr){
		free_ex(*slot_ptr, ((SongHeader*)seg->address)->tlsf_pool);
		*slot_ptr = NULL;

		decrement_last(slot_num);
		block->full = false;
	}
	else warn("slot already empty?");
}
template void ShmContainer<_route_shared>::remove_item(int);
template void ShmContainer<_midi_track_shared>::remove_item(int);
template void ShmContainer<_region_shared>::remove_item(int);
template void ShmContainer<_midi_region_shared>::remove_item(int);
template void ShmContainer<_connection_shared>::remove_item(int);
template void ShmContainer<_filesource_shared>::remove_item(int);
template void ShmContainer<_ayyi_channel>::remove_item(int);
template void ShmContainer<_ayyi_shm_playlist>::remove_item(int);


template<class T>
void
ShmContainer<T>::remove_item_delayed(int slot_num)
{
	dbg(0, "...");
	T* item = get_item(slot_num);
	g_return_if_fail(item);
	item->flags |= deleted;
	uint64_t id = item->id;

	memset(item, 0, sizeof(T)); //this might be a bad idea. perhaps do it later in the timeout
	item->shm_idx = slot_num;
	item->id = id;
	item->flags |= deleted;

	Glib::signal_timeout().connect(sigc::bind(sigc::mem_fun(*this, &ShmContainer<T>::remove_item_delayed_on_timeout), slot_num), 2000); 
}
template void ShmContainer<_region_shared>::remove_item_delayed(int);
template void ShmContainer<_midi_region_shared>::remove_item_delayed(int);


template<class T>
bool
ShmContainer<T>::remove_item_delayed_on_timeout(int slot_num)
{
	dbg(0, "idx=%i", slot_num);

	T* item = get_item(slot_num);
	if((bool)item){
		if(!(item->flags & deleted)) warn("deleted flag not set. flags=%x\n", item->flags);
		remove_item(slot_num);
	}

	return false;
}


template<class T>
int
ShmContainer<T>::next_available_slot()
{
	// -returns the index of the next free POD slot.

	g_return_val_if_fail(container, -1);

	struct block* block = NULL;
	int b = 0;
	//find first block with space:
	for(;b<CONTAINER_SIZE;b++){
		block = container->block[b];
		if(!block){
			dbg(2, "%s: unallocated block. adding block... b=%i", container->signature, b);
			block = block_new();
			dbg(2, "block=%p", block);
			break;
		}
		if(!block->full) break;
	}
	void** table = block->slot;
	if(!table) return -1; //malloc probably failed.
	if(_debug) dbg(0, "found block: %i last=%i", b, container->last);

	for(int slot=0; slot<BLOCK_SIZE; slot++){
		if(!table[slot]){
			block->last = MAX(block->last, slot);
			return b * BLOCK_SIZE + slot;
		}
	}

	err("no free slots!\n");
	return -1;
}
template int ShmContainer<struct _region_shared>::next_available_slot();
template int ShmContainer<struct _filesource_shared>::next_available_slot();
template int ShmContainer<struct _connection_shared>::next_available_slot();
template int ShmContainer<struct _route_shared>::next_available_slot();
template int ShmContainer<struct _ayyi_plugin_nfo>::next_available_slot();
template int ShmContainer<struct _shm_midi_note>::next_available_slot();
template int ShmContainer<struct _midi_region_shared>::next_available_slot();
template int ShmContainer<struct _ayyi_control>::next_available_slot();


template<class T>
T*
ShmContainer<T>::next(T* prev)
{
	if(!container){ err("object not initialised!\n"); return (T*)NULL; }

	if(container->last < 0) return (T*)NULL; //no items.

	if(!prev){
		//return first object:
		int b0 = -1;
		for(int b=0; b<=container->last; b++){
			if(container->block[b]){
				if(container->block[b]->slot){
					b0 = b;
					break;
				}
				else err("block not initialised?\n");
			}
		}
		if(b0 < 0){ err("no initialised blocks found.\n"); return (T*)NULL; }

		for(int b=b0; b<=container->last; b++){
			for(int i=0; i<=container->block[b]->last; i++){
				T* t = (T*)container->block[b]->slot[i];
				if(t) return (T*)t;
			}
		}
		dbg(0, "first: nothing found");
		return (T*)NULL;
	}

	if(!idx_is_valid(prev->shm_idx)) return (T*)NULL;

	int slot_num  = prev->shm_idx % BLOCK_SIZE;
	int block_num = prev->shm_idx / BLOCK_SIZE;

	bool first_block = true;
	for(int b=block_num; b<=container->last; b++){
		block* block = container->block[b];
		void** table = block->slot;

		int i = first_block ? slot_num+1 : 0;
		int size = MIN(BLOCK_SIZE, block->last+1);
		for(; i<size; i++){
			if(!table[i]) continue;
			if(i == slot_num){ err("!!same\n"); return (T*)NULL; }
			T* item = (T*)table[i];
			return item;
		}

		first_block = false;
	}
	return (T*)NULL;
}
template _route_shared*       ShmContainer<struct _route_shared>::next(struct _route_shared*);
template _filesource_shared*  ShmContainer<struct _filesource_shared>::next(struct _filesource_shared*);
template _connection_shared*  ShmContainer<struct _connection_shared>::next(struct _connection_shared*);
template _region_shared*      ShmContainer<_region_shared>::next     (_region_shared*);
template _midi_region_shared* ShmContainer<_midi_region_shared>::next(_midi_region_shared*);
template _ayyi_shm_playlist*  ShmContainer<_ayyi_shm_playlist>::next (_ayyi_shm_playlist*);
template _ayyi_channel*       ShmContainer<_ayyi_channel>::next      (_ayyi_channel*);
template _shm_midi_note*      ShmContainer<_shm_midi_note>::next     (_shm_midi_note*);


template<class T>
T*
ShmContainer<T>::lookup_by_id(uint64_t id)
{
	T* t = 0;
	while((t = next(t))){
		if(t->id == id) return t;
	}
	return (T*)0;
}
template struct _filesource_shared* ShmContainer<_filesource_shared>::lookup_by_id(uint64_t);


template<class T>
int
ShmContainer<T>::count_items()
{
	if(!container){ dbg(0, "container=%p", container); return 0; }

	int count = 0;
	if(_debug) dbg(0, "lastblock=%i", container->last);
    for(int i=0; i<=container->last+1; i++){
		block* block = container->block[i];
		if(block){
			for(int j=0;j<=block->last+1;j++){
				if(block->slot[j]) count++;
			}
		}
	}
	return count;
}
template int ShmContainer<_shm_event>::count_items();
template int ShmContainer<_shm_midi_note>::count_items();
template int ShmContainer<_route_shared>::count_items();
template int ShmContainer<_filesource_shared>::count_items();
template int ShmContainer<_ayyi_channel>::count_items();
template int ShmContainer<_region_shared>::count_items();
template int ShmContainer<_ayyi_shm_playlist>::count_items();
template int ShmContainer<_ayyi_plugin_nfo>::count_items();


template<class T>
bool
ShmContainer<T>::idx_is_valid(int idx)
{
	int last_block = container->last;
	if(last_block < 0) return false;
	int max_idx = last_block * BLOCK_SIZE + container->block[last_block]->last;
	return (idx >= 0 && idx <= max_idx);
}
template bool ShmContainer<_route_shared>::idx_is_valid(int);
template bool ShmContainer<_midi_track_shared>::idx_is_valid(int);
template bool ShmContainer<_filesource_shared>::idx_is_valid(int);


template<class T>
bool
ShmContainer<T>::item_is_valid(T* item)
{
	//check that the given item exists in the container.

	if((uintptr_t)item<1024) return false;

	block* block = container->block[0];

	for(int r=0; r<=block->last; r++){
		T* pod = (T*)block->slot[r];
		if(!pod) continue;

		if(item == pod) return true;
	}
	return false;
}
template bool ShmContainer<_route_shared>::item_is_valid(_route_shared*);


#if 0
template<class T>
void
ShmContainer<T>::erase()
{
	// this currently orphans memory. Use tlsf instead

	dbg (1, "...");

	if(!container){ err("container!!\n"); return; }

	for(int i=0; i<CONTAINER_SIZE; i++){
		if(container->block[i]){
			for(int j=0;j<BLOCK_SIZE;j++){
				container->block[i]->slot[j] = NULL;
			}
			container->block[i]->last = -1;
			container->block[i]->full = false;
			container->block[i] = NULL;
		}
	}
	container->last = -1;
	container->full =  0;
}
#endif


template<class T>
void
ShmContainer<T>::erase_tlsf()
{
	dbg (0, "...");

	if(!container){ err("container!!\n"); return; }

	for(int b=0; b<CONTAINER_SIZE; b++){
		if(container->block[b]){
			for(int s=0;s<BLOCK_SIZE;s++){
				if(container->block[b]->slot[s]) seg->free_tlsf(container->block[b]->slot[s]);
				container->block[b]->slot[s] = NULL;
			}
			container->block[b]->last = -1;
			container->block[b]->full = false;
			container->block[b] = NULL;
		}
	}
	container->last = -1;
	container->full =  0;
}
template void ShmContainer<struct _shm_midi_note>::erase_tlsf();


template<class T>
void
ShmContainer<T>::print()
{
	if(!container){ err("object not initialised!\n"); return; }
	if(container->last < 0){ dbg(0, "container is empty: %s", container->signature); return; }
	if(!container->block[0]){ err("container has no blocks.\n"); return; }
	if(!container->block[0]->slot[0]){ dbg(0, "first slot is empty."); }
	dbg(0, "sig='%s' shm_segment='%s'", container->signature, seg->get_sig());
	dbg(0, "container=%p slot0=%p last_block=%i", container, container->block[0]->slot[0], container->last);
	for(int b=0; b<=container->last; b++){
		block* block = container->block[b];
		//void** table = block->slot;
		dbg(0, "block %02i: last_slot=%i", b, block->last);
		for(int i=0; i<=block->last && i<10; i++){
			dbg(0, "  slot %i: %p", i, block->slot[i]);
		}
	}
}
template void ShmContainer<_route_shared>::print();
template void ShmContainer<_ayyi_channel>::print();
template void ShmContainer<_region_shared>::print();
template void ShmContainer<_shm_event>::print();
template void ShmContainer<_shm_midi_note>::print();
template void ShmContainer<_ayyi_plugin_nfo>::print();
template void ShmContainer<_filesource_shared>::print();


template<class T>
void
ShmContainer<T>::verify()
{
	dbg(1, "...");
	// The slot pointed at by 'last' should always be used.
	for(int b=0; b<=container->last; b++){
		block* block = container->block[b];
		if(block->last > -1){
			if(!block->slot[block->last]) dbg(0, "ERROR 'last' slot empty! block=%i last=%i", b, block->last);
		}
	}
}
template void ShmContainer<_shm_event>::verify();
//template void ShmContainer<_route_shared>::verify();
//template void ShmContainer<_region_shared>::verify();
//template void ShmContainer<_ayyi_channel>::verify();
//template void ShmContainer<_shm_midi_note>::verify();


template<class T>
void
ShmContainer<T>::decrement_last(int slot_num)
{
	dbg(2, "slot_num=%i", slot_num);
	int b = slot_num / BLOCK_SIZE;
	g_return_if_fail(b <= container->last);

	block* last_block = container->block[container->last];
	g_return_if_fail(last_block);
	int i_last = BLOCK_SIZE * container->last + last_block->last;
	dbg(2, "i_last=%i", i_last);

	if(i_last > slot_num) return;

	int i = i_last;
	block* block = container->block[i / BLOCK_SIZE];
	int n = 0;
	while(i > -1){
		int b = i / BLOCK_SIZE;
		block = container->block[i / BLOCK_SIZE];
		void* slot = block->slot[i % BLOCK_SIZE];
		if(slot) dbg(2, "found non-empty slot: %i (%i.%i)", i, b, i % BLOCK_SIZE);
		if(slot) break; //found non-empty block: stop.
		if(i % BLOCK_SIZE == 0){
			dbg(2, "block is now empty. freeing block %i...", b);
			block_free(b);
		}
		i--;
		//if(i < 0){ dbg(0, "reached end"); break; }
		n++;
	}
	if(i < 0){
		container->last = -1;
		dbg(2, "container now empty");
		return;
	}
	container->last = i / BLOCK_SIZE;
	block->last = i % BLOCK_SIZE;
	dbg(2, "n=%i new_last=%i (%i.%i)", n, i, container->last, block->last);
}


} //end namespace

#include "container-impl.cc"
namespace Ayi {
template struct _filesource_shared* ShmContainer<struct _filesource_shared>::get_item(int);
template struct _midi_track_shared* ShmContainer<struct _midi_track_shared>::get_item(int);
template struct _shm_midi_note* ShmContainer<struct _shm_midi_note>::get_item(int);
template struct _ayyi_channel* ShmContainer<struct _ayyi_channel>::get_item(int);
template struct _connection_shared* ShmContainer<_connection_shared>::get_item(int);
template struct _ayyi_plugin_nfo* ShmContainer<_ayyi_plugin_nfo>::get_item(int);
template struct _ayyi_shm_playlist* ShmContainer<_ayyi_shm_playlist>::get_item(int);

template bool ShmContainer<struct _ayyi_control>::slot_is_used(int slot_num);

} //end namespace

