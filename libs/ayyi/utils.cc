
/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2006-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#define __ayyi_utils_cc__

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <glib.h>

#include <ayyi/ayyi_time.h>
#include <ayyi/utils.h>

#define LOG_DIR "/tmp"
bool prefer_stdout = false;

extern char ok[];
extern char fail[];

void
song_pos_set(Ayi::SongPos* pos, int beat, uint16_t sub, uint16_t mu)
{
  pos->beat = beat;
  pos->sub  = sub;
  pos->mu   = mu;
}


int64_t
beats2mu(double beats)
{
  //note beats is float.

  return (int64_t)beats * (SM_MU_PER_SUB * SM_SUBS_PER_BEAT); //this is the definition of a mu.
}


void
mu2pos(uint64_t mu, Ayi::SongPos *pos)
{
	//ASSERT_POINTER(pos, "pos");

	uint32_t beats = mu / (SM_MU_PER_SUB * SM_SUBS_PER_BEAT );

	uint32_t remainder  = mu % (SM_MU_PER_SUB * SM_SUBS_PER_BEAT );
	uint32_t subs = remainder / SM_MU_PER_SUB;
	remainder = remainder % SM_MU_PER_SUB;

	pos->beat = beats;
	pos->sub  = subs;
	pos->mu   = remainder;
}


/*
uint32_t
posbuffer2samples(char* buffer)
{
	struct ::_song_pos pos;
	memcpy(&pos, buffer, 8);
	return DAE::core->pos2samples(&pos);
}
*/

extern "C" {

static void
_output(const char* str, gboolean newline)
{
	FILE* logfile = prefer_stdout ? NULL : fopen (LOG_DIR "/ardourd.log", "a"); 
	FILE* stream = logfile ? logfile : stderr;
	fprintf(stream, "%s", str);

	if(newline){
		fprintf(stream, "\n");
	}else{
		fflush(stream);
	}

	if(logfile) fclose (logfile);
}


void
p(int level, const char* format, ...)
{
	va_list argp;
	va_start(argp, format);
	//if (level <= debug) {
		gchar* s = g_strdup_vprintf(format, argp);
		_output(s, true);
		g_free(s);
	//}
	va_end(argp);
}


void
p_(const char* format, ...)
{
	va_list argp;
	va_start(argp, format);
		gchar* s = g_strdup_vprintf(format, argp);
		_output(s, false);
		g_free(s);
	va_end(argp);
}


void
dbgprintf(const char* classname, const char* func, char* format, ...)
{
	va_list args;

	va_start(args, format);
#if 0
	printf("%s::%s(): ", classname, func);
	vprintf(format, argp);
	printf("\n");
#else
	gchar* s = g_strdup_vprintf(format, args);
	gchar* t = g_strdup_printf("%s::%s(): %s", classname, func, s);
	_output(t, true);
	g_free(s);
	g_free(t);
#endif
	va_end(args);
}


void
errprintf(char *format, ...)
{
	//fn prints an error string, then passes arguments on to vprintf.

	va_list args; //points to each unnamed arg in turn

	va_start(args, format); //make ap (arg pointer) point to 1st unnamed arg

#if 0
	printf("%s ", smerr);
	vprintf(format, argp);
#else
	gchar* s = g_strdup_vprintf(format, args);
	gchar* t = g_strdup_printf("%s %s", smerr, s);
	_output(t, true);
	g_free(s);
	g_free(t);
#endif

	va_end(args); //clean up
}


void
errprintf2(const char* classname, const char* func, char *format, ...)
{
	va_list args; //points to each unnamed arg in turn

	va_start(args, format); //make ap (arg pointer) point to 1st unnamed arg
#if 0
	printf("%s %s::%s(): ", smerr, classname, func);
	vprintf(format, argp);
#else
	gchar* s = g_strdup_vprintf(format, args);
	gchar* t = g_strdup_printf("%s %s::%s(): %s", smerr, classname, func, s);
	_output(t, true);
	g_free(s);
	g_free(t);
#endif

	va_end(args); //clean up
}


void
warnprintf(char *format, ...)
{
  //fn prints a warning string, then passes arguments on to vprintf.


	va_list args;           //points to each unnamed arg in turn
	va_start(args, format); //make ap (arg pointer) point to 1st unnamed arg
#if 0
	printf("%s ", smwarn);
	vprintf(format, args);
#else
	gchar* s = g_strdup_vprintf(format, args);
	gchar* t = g_strdup_printf("%s %s", smwarn, s);
	_output(t, true);
	g_free(s);
	g_free(t);
#endif
	va_end(args);
}


void
warnprintf2(const char* classname, const char* func, char *format, ...)
{
	//fn prints an error string, then passes arguments on to vprintf.

	va_list args;
	va_start(args, format);
#if 0
	static char warn[32] = "\x1b[1;33mwarning:\x1b[0;39m";
	printf("%s %s::%s(): ", warn, classname, func);
	vprintf(format, args);
#else
	gchar* s = g_strdup_vprintf(format, args);
	gchar* t = g_strdup_printf("%s %s::%s(): %s", smwarn, classname, func, s);
	_output(t, true);
	g_free(s);
	g_free(t);
#endif

	va_end(args);
}


#if 0
void
printf_ok(char *format, ...)
{
  va_list argp; //points to each unnamed arg in turn

  va_start(argp, format); //make ap (arg pointer) point to 1st unnamed arg
  vprintf(format, argp);
  va_end(argp);           //clean up

  printf("                      [ %sok%s ]", green, white);
}
#endif


int
get_terminal_width()
{
	struct winsize ws;

	if (ioctl(1, TIOCGWINSZ, (void *)&ws) != 0) printf("ioctl failed\n");

	//printf("terminal width = %d\n", ws.ws_col);

	return ws.ws_col;
}


char*
print_ok()
{
	static char goto_rhs[32];
	sprintf(goto_rhs, "\x1b[A\x1b[%iC%s", MIN(get_terminal_width() - 12, 100), ok); //go up one line, then goto rhs
	return goto_rhs;
}


char*
print_fail()
{
	static char goto_rhs[32];
	sprintf(goto_rhs, "\x1b[A\x1b[%iC%s", MIN(get_terminal_width() - 12, 100), fail);
	return goto_rhs;
}


int64_t
filesize(const char *s)
{
	//printf("%s(): '%s'\n", __func__, s);
    struct stat st;

    //if (!stat_utf8(s, &st)) return 0;
    if (stat(s, &st)) return 0;
    return (int64_t)st.st_size;
}

} //end extern C.

