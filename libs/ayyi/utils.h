#ifndef __ayyi_utils_h__
#define __ayyi_utils_h__
#include <stdint.h>
#include "ayyi/ayyi_time.h"

#define UNDERLINE p(1, "-----------------------------------------------------\n")

#ifdef __ayyi_utils_cc__
char bold  [16] = "\x1b[1;39m"; // 1 = bright
char white [16] = "\x1b[0;39m";
char green [16] = "\x1b[1;32m";
char red   [16] = "\x1b[1;31m";
char blue  [16] = "\x1b[1;34m";
char yellow[16] = "\x1b[1;33m";
char smerr [32];
char smwarn[32];
#else
extern char bold  [16];
extern char white [];
extern char green [];
extern char red   [];
extern char blue  [];
extern char yellow[];
extern char smerr [32];
extern char smwarn[32];
#endif

#ifdef __cplusplus
void     song_pos_set           (Ayi::SongPos*, int beat, uint16_t sub, uint16_t mu);
int64_t  beats2mu               (double beats);
void     mu2pos                 (uint64_t mu, Ayi::SongPos*);
#endif

#ifdef __cplusplus
extern "C" {
#endif
	void     p                  (int level, const char* format, ...);
	void     p_                 (const char* format, ...); // no newline
	void     dbgprintf          (const char* classname, const char* func, char* format, ...);
	void     errprintf          (char* format, ...);
	void     errprintf2         (const char* classname, const char* func, char* format, ...);
	void     warnprintf         (char* format, ...);
	void     warnprintf2        (const char* classname, const char* func, char* format, ...);

	int      get_terminal_width ();
	char*    print_ok           ();
	char*    print_fail         ();

	int64_t  filesize           (const char*);
#ifdef __cplusplus
} //extern C
#endif

#endif //__ayyi_utils_h__
