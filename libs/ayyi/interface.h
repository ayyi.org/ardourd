#ifndef __ayyi_interface_h__
#define __ayyi_interface_h__

#include "stdint.h"
#include <ayyi/ayyi_typedefs.h>
#include <ayyi/ayyi_types.h>

#ifdef __cplusplus
namespace Ayi {
using namespace Ayi;
#endif //__cplusplus

#ifdef VST_SUPPORT
  #include "fst/fst.h"
#endif

#define AYYI_SHM_VERSION 7

#define CONTAINER_SIZE 128
#define BLOCK_SIZE 128
#define MAX_LOC 10
#define AYYI_FILENAME_MAX 256
#define AYYI_NAME_MAX 64
#define AYYI_PLUGINS_PER_CHANNEL 3
#define AYYI_AUX_PER_CHANNEL 3

struct _container {
	struct block* block[CONTAINER_SIZE];
	int           last;
	int           full;
	char          signature[16];
	ObjType       obj_type;
};

struct block
{
	void*       slot[BLOCK_SIZE];
	int         last;
	int         full;
	void*       next;
};

struct _ayyi_list
{
	struct _container* data;
	struct _ayyi_list* next;
	int                id;
	char               name[32];
};

typedef struct _shm_virtual
{
	char        service_name[16];
	char        service_type[16];
	int         version;
	void*       owner_shm;
	int         num_pages;
	void*       tlsf_pool;
} ShmHeader;

enum {
	ARDOUR_TYPE_MASK  = 0xf0000000,
	ARDOUR_TYPE_TRACK = 0x10000000,
	ARDOUR_TYPE_PART  = 0x20000000,
	ARDOUR_TYPE_FILE  = 0x30000000,
	ARDOUR_TYPE_SONG  = 0x40000000,
};

struct _ayyi_base_item {
	int            shm_idx;
	char           name[AYYI_NAME_MAX];
	AyyiId         id;
	void*          server_object;
	int            flags;
};

struct _ayyi_channel {
	int            shm_idx;
	char           name[AYYI_NAME_MAX];
	uint64_t       id;
	void*          object;
	int            flags;
	char           n_in;
	char           n_out;
	double         level;
	int            has_pan;
	float          pan;
	float          visible_peak_power[2];
	struct {
		ObjIdx     idx;
		char       active;
		char       bypassed;
	}              plugin          [AYYI_PLUGINS_PER_CHANNEL];
	struct _ayyi_aux* aux          [AYYI_AUX_PER_CHANNEL];
	struct _container automation[2];    //this will move into the list below
	struct _ayyi_list* automation_list;
};
typedef struct _ayyi_channel AyyiChannel;

struct _ayyi_plugin_nfo {
	int            shm_idx;
	char           name[AYYI_FILENAME_MAX];
	char           category[64];
	uint32_t       n_inputs;
	uint32_t       n_outputs;
	uint32_t       latency;
	struct _container controls;
};

struct _ayyi_control {
	char           name[32];
};

#ifdef VST_SUPPORT
struct _shm_vst_info {
	FSTInfo        fst_info;
};
typedef struct _shm_vst_info shm_vst_info;
#endif

struct _shm_event {
	double when;
	double value;
};
typedef struct _shm_event shm_event;

struct _shm_midi_note {
	uint32_t       shm_idx; // uint32_t must match type specified in setNoteList
	uint8_t        note;
	uint8_t        velocity;
	nframes_t      start;
	nframes_t      length;
};

struct _ayyi_aux {
	int           idx;
	float         level;
	float         pan;
	//ChanFlags     flags;
	int           flags;
	int           bus_num;
};

#ifdef __cplusplus
}      //namspace Ayi
#endif //__cplusplus
#endif //__ayyi_interface_h__
