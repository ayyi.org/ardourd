/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __ayyi_list_h__
#define __ayyi_list_h__

namespace Ayi {


class AyyiList
{
  public:
	                    AyyiList       (ShmSegDefn*, struct _ayyi_list**);
	                   ~AyyiList       ();
	void                clear          ();
	struct _ayyi_list*  prepend        (int id, const char* name);
	struct _ayyi_list*  prepend_unique (int id, const char* name);
	struct _ayyi_list*  find           (AyyiContainer*);
	struct _ayyi_list*  find           (uint32_t id);
	int                 size           ();
	void                print          ();

	struct _ayyi_list** list;

  protected:
	ShmSegDefn*         seg;

	struct _ayyi_list*  allocate();
};


} // end Ayi

#endif
