/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

// class provides access to the shm container structures.

// one instantiation each for: regions, filesources, playlists, etc.

#ifndef __ayyi_container_h__
#define __ayyi_container_h__

#include "ayyi_typedefs.h"

namespace Ayi {

class ShmSegDefn;

template<class T>
class ShmContainer
{
  public:
	       ShmContainer       (ShmSegDefn*);

	void   init               (const char* signature, bool allocate_block = false);
	void   init               (struct _container*, const char* signature, bool allocate_block = false);
	void   clear              ();
	T*     add_item           ();
	T*     add_item_with_idx  ();
	void   remove_item        (int);
	void   remove_item_delayed(int);
	bool   slot_is_used       (int);
	T*     next               (T*);
	T*     get_item           (int);
	T*     lookup_by_id       (uint64_t);
	int    count_items        ();
	bool   idx_is_valid       (int);
	bool   item_is_valid      (T*);
	void   erase_tlsf         ();

	void   print              ();
	void   verify             ();

	int    next_available_slot(); //TODO make this private (users should use add_item instead)

	int    _debug;

  protected:
	struct _container*        container;
	ShmSegDefn*               seg;

	block* block_new          ();
	void   block_free         (int);
	void   set_container      ();
	void   decrement_last     (int);

	bool   remove_item_delayed_on_timeout(int);
};


};

#endif //__ayyi_container_h__
