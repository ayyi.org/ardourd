/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <cerrno>
#include <cstdlib>
#include <cstdio>
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <glib.h>
#include <iostream>
#include <list>

#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/msg.h>
#include <sys/shm.h>

#ifdef VST_SUPPORT
  #include "fst.h"
#endif

extern "C" {
#include "tlsf/src/tlsf.h"
}

extern int debug;
extern int debug_shm;

#include <ayyi/ayyi_time.h>
#include <ayyi/shm_seg.h>
#include "utils.h"

#define dbg(N, A, ...) { if (N <= debug) dbgprintf("Ayyi", __func__, (char*)A, ##__VA_ARGS__); }
#define err(A, ...) errprintf2("ayyi", __func__, (char*)A, ##__VA_ARGS__)
#define warn(A, ...) warnprintf("ayyi", __func__, (char*)A, ##__VA_ARGS__)

#include "ayyi++.h"

#include <ayyi/inc/song.h>
#include <ayyi/inc/mixer.h>
namespace Ayi {
SongHeader* shm_index = 0;

using namespace std;
using namespace Ayi;

int        AyyiServer::got_shm = 0;
ShmSegDefn AyyiServer::shm_segs[10];
bool       AyyiServer::enable_announcements = false; // prevent asynchronous object creation during startup.


AyyiServer::AyyiServer()
	: session_num(1)
{
	dbg (1, "...");

	shm_debug        = 0;
	got_messaging    = 0;
	got_shm          = 0;
	shm_connect_sent = false;

	shm_cleanup();

	for(int i=0;i<10;i++){
		shm_segs[i].name[0] = 0;
		shm_segs[i].size    = 0;
		shm_segs[i].address = NULL;
	}
	shm_segment_add((char*)"global", 1);
	//shm_segment_add("core", 512);
}


AyyiServer::~AyyiServer()
{
}


void
AyyiServer::shm_segment_add(char* name, int size)
{
	//add a segment name to the list of segments we want to use.

	for(int i=0;i<10;i++){
		if(shm_segs[i].size) continue;
		shm_segs[i].size = size;
		strcpy(shm_segs[i].name, name);
		if(shm_debug) for(int j=0;j<10;j++) if(shm_segs[j].size) printf("%s(): %i: name=%s size=%i\n", __func__, j, shm_segs[j].name, shm_segs[j].size);
		return;
	}
	err("full!\n");
}


#ifndef USE_DBUS
void
AyyiServer::shm_segment_drop(char* name)
{
	//struct _shm_seg_defn* segment;
	shm_seg_defn* segment;
	if(!(segment = shm_segment_find_by_name(name))){ err("cannot find name %s\n", name); return; }

	//FIXME
}
#endif


void
AyyiServer::shm_segment_reset(char* name)
{
	//clean up following a shm create error.

	got_shm = 0;

	for(int i=0;i<10;i++){
		if(!shm_segs[i].size) continue;
		if(strcmp(shm_segs[i].name, name)) continue;
		shm_segs[i].address = NULL;
		return;
	}
	err("couldnt find segment. name=%s\n", name);
}


#ifndef USE_DBUS
void
AyyiServer::shm_segment_set(char* name, int seg_id)
{
	//fill segment properties following successful shm_create.

	shm_seg_defn* segment;
	if(!(segment = shm_segment_find_by_name(name))){ err("cannot find name '%s'\n", name); return; }

	void* seg_pointer;
	if(!(seg_pointer = dae_shm_get_ptr(seg_id))){ err("shm create failed. Cannot get segment pointer. seg=%s\n", name); return; }

	segment->seg_id  = seg_id;
	segment->address = seg_pointer;

	shm_is_complete();
}


void
AyyiServer::shm_connect()
{
	//request shm segments be made.

	if(shm_connect_sent) return;

	for(int i=0;i<10;i++){
		if(!shm_segs[i].size) continue;
		dae_shm_connect_export(shm_segs[i].name, shm_segs[i].size, DAE_SHM_R);
	}
	shm_connect_sent = true;
}


void
AyyiServer::shm_disconnect()
{
	//disconnect all shm segments. They will be destroyed if no longer used.

	for(int i=0;i<10;i++){
		if(!shm_segs[i].size) continue;
		printf("%s(): %s...\n", __func__, shm_segs[i].name);
		if(dae_shm_disconnect(shm_segs[i].name, DAE_SHM_EXPORT)) printf("%s(): *** failed to disconnect shm segment: %s\n", __func__, shm_segs[i].name);
		sleep(2); //does this help prevent the machine from momentarily freezing?
	}
}


shm_seg_defn*
AyyiServer::shm_segment_find_by_name(char* name)
{
	shm_seg_defn* ret = NULL;

	for(int i=0;i<10;i++){
		if(!shm_segs[i].size) continue;
		//shm_segs[i].size = size;
		if(!strcmp(shm_segs[i].name, name)) return &shm_segs[i];
	}
	printf("%s(): **** not found: %s\n", __func__, name);
	return ret;
}
#endif


bool
AyyiServer::shm_is_complete()
{
	//checks if all segments have been created, and updates got_shm.

	//this is only used for imported segments. Locally created segments are assumed to be complete.

	for(int i=0;i<10;i++){
		if(!shm_segs[i].size) continue;
		if(!shm_segs[i].seg_id){
			return (got_shm = false);
		}
	}

	//(debug) check the address is set:
	for(int i=0;i<10;i++) if(shm_segs[i].size) if(!shm_segs[i].address) printf("%s(): *** address not set!\n", __func__);

	return (got_shm = true);
}


int
AyyiServer::shm_count_active_segments()
{
	int count = 0;
	for(int i=0;i<10;i++){
		if(!shm_segs[i].size || !shm_segs[i].address) continue;
		count++;
	}
	return count;
}


/*
 *  Remove orphaned shm segments.
 */
void
AyyiServer::shm_cleanup ()
{
	struct shmid_ds shmseg;
	struct shm_info shm_info;

	int maxid = shmctl (0, SHM_INFO, (struct shmid_ds *) (void *) &shm_info);
	if (maxid < 0) {
		printf ("kernel not configured for shared memory?\n");
		return;
	}

	int count = 0;
	for (int id = 0; id <= maxid; id++) {
		int shmid = shmctl (id, SHM_STAT, &shmseg);
		if (shmid < 0) continue;
		if (shmseg.shm_nattch) continue; //segment is still being used.

		dbg(1, "%12i %12lu %li", shmid, (unsigned long)shmseg.shm_segsz, (long)shmseg.shm_nattch);

		int r;
		if(!(r = shmctl(shmid, IPC_RMID, &shmseg))){
			count++;
		} else {
			err("shmctl error! cannot delete dead shm segment.\n");
		}
	}

	if(count) dbg(1, "%i dead shm segments removed.", count);
}


void
AyyiServer::set_shm_callback(int (Ayi::AyyiServer::*recv_shm_notify)(short int, int, char*, int, int, int))
{
	//cant compile - is there a missing 'this'?

 	//note: a given member function pointer can only point to functions that are members of the class it was declared with. It cannot be applied to an object of a different class even if it has member functions with the same signature

	dbg (1, "setting callback...");
	//dae_shm.shm_notify_callback = (int (*)(short int, int, char*, int, int, int))recv_shm_notify;
}


#ifndef USE_DBUS
void
AyyiServer::shm_set_callback1(int (*app_recv_shm_notify)(short int, int, char*, int, int, int))
{
	dae_shm.shm_notify_callback = app_recv_shm_notify;
}
#endif


int
AyyiServer::recv_shm_notify(short int _notify_level, int _seg_id, char *_name, int _pages, int _rw, int _status)
{
	return 0;
}


#ifndef USE_DBUS
void
AyyiServer::shm_set_debug(int level)
{
	dae_shm_set_debug(level);
}
#endif


#if 0 //moved to Container
struct block*
AyyiServer::block_init(AyyiContainer* container, shm_seg_defn* seg)
{
	//allocate and initialise shm for a new block and make it current
	//TODO this really belongs in AyyiContainer

	if(!container){ err("bad arg! no container!\n"); return NULL; }

	//cout << "AyyiServer::block_init()..." << endl;
	//cout << "AyyiServer::block_init(): " << " container=" << container << endl;

	//find next free block:
	int b;
	bool found = false;
	for(b=0;b<CONTAINER_SIZE;b++){
		if(!container->block[b]){ found = true; break; }
	}
	if(!found){ err("cant find empty block. out of memory?\n"); return NULL; }

	container->last = max(container->last, b);
	if(shm_debug) printf("AyyiServer::%s(): new_block=%i\n", __func__, b);

	//struct block* block = container->block[b] = (struct block*)shm_malloc(sizeof(*block), NULL); //hmmm, recursive. Pass null to stop feedback...?
	struct block* block = container->block[b] = (struct block*)seg->malloc_tlsf(sizeof(*block), container);
	if(!block){ err("block NULL!!\n"); return NULL; }

	for(int i=0; i<BLOCK_SIZE; i++) block->slot[i] = NULL;
	block->last = -1;
	block->full = false;

	container->last = b;
	return container->block[b];
}
#endif


#ifndef USE_DBUS
bool
AyyiServer::start_messaging()
{
	int use_postq = 0; //what ????

	dae_start_messaging(use_postq);
	got_messaging = 1;

	printf("AyyiServer::%s(): got_messaging=1\n", __func__);

	return true;
}
#endif


std::string*
AyyiServer::get_object_string(ObjType object_type)
{
	static std::string str[AYYI_OBJECT_ALL];
#define CASE(x) case AYYI_OBJECT_##x: { str[AYYI_OBJECT_##x] = "AYYI_OBJECT_"#x; break; }
	switch(object_type){
		CASE(EMPTY);
		CASE(TRACK);
		CASE(AUDIO_TRACK);
		CASE(CHAN);
		CASE(STRING);
		CASE(MIDI_TRACK);
		CASE(AUX);
		CASE(PART);
		CASE(AUDIO_PART);
		CASE(MIDI_PART);
		CASE(EVENT);
		CASE(RAW);
		CASE(ROUTE);
		CASE(FILE);
		CASE(LIST);
		CASE(MIDI_NOTE);
		CASE(SONG);
		CASE(TRANSPORT);
		CASE(LOCATORS);
		CASE(AUTO);
		CASE(METRONOME);
		CASE(PROGRESS);
		CASE(UNSUPPORTED);
		CASE(ALL);
		break;
	}
#undef CASE
	return &str[object_type];
}


#if 0
static inline uint32_t ilog2(const uint32_t x) {
  uint32_t y;
  asm ( "\tbsr %1, %0\n"
      : "=r"(y)
      : "r" (x)
  );
  return y;
}
#endif

uint32_t
get_powof2(uint32_t n)
{
	//given a bitmap with more than one value set, this will return the bitmap with only one value set (the highest)

	n--;
	n |= n >> 1;
	n |= n >> 2;
	n |= n >> 4;
	n |= n >> 8;
	n |= n >> 16;
	n++;
	n = n >> 1;

	return n;
}


const char*
AyyiServer::get_prop_type_string(uint32_t prop_type)
{
	uint32_t type = get_powof2(prop_type);

	const char* str;
#define CASE(x) case AYYI_##x: { str = "AYYI_"#x; break; }
	switch(type){
		CASE(NO_PROP);
		CASE(NAME);
		CASE(STIME);
		CASE(LENGTH);
		CASE(LEVEL);
		CASE(PAN);
		CASE(PAN_ENABLE);
		CASE(HEIGHT);
		CASE(COLOUR);
		CASE(END);
		CASE(TRACK);
		CASE(MUTE);
		CASE(ARM);
		CASE(SOLO);
		CASE(SDEF);
		CASE(INSET);
		CASE(FADEIN);
		CASE(FADEOUT);
		CASE(INPUT);
		CASE(OUTPUT);
		CASE(PREPOST);
		CASE(SPLIT);
		CASE(PB_LEVEL);
		CASE(PB_PAN);
		CASE(PB_DELAY);
		CASE(PLUGIN_SEL);
		CASE(PLUGIN_BYPASS);
		CASE(TRANSPORT_PLAY);
		CASE(TRANSPORT_STOP);
		CASE(TRANSPORT_REW);
		CASE(TRANSPORT_FF);
		default:
			str = "UNKNOWN";
	}
#undef CASE
	return str;

	/*
	static const char* types[] = {
	                        "AYYI_TRANSPORT_REC", "AYYI_TRANSPORT_LOCATE", "AYYI_TRANSPORT_CYCLE", "AYYI_TRANSPORT_LOCATOR",
	                        "AYYI_AUTO_PT",
	                        "AYYI_ADD_POINT", "AYYI_DEL_POINT",
	                        "AYYI_TEMPO", "AYYI_HISTORY",
	                        "AYYI_LOAD_SONG", "AYYI_SAVE", "AYYI_NEW_SONG"
	                       };
	if(G_N_ELEMENTS(types) != AYYI_NEW_SONG + 1) err("!!\n");
	return types[prop_type];
	*/
}

} //end namespace Ayi
