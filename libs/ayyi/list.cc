/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://ayyi.org              |
 | copyright (C) 2004-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include <string.h>
#include <iostream>
#include <list>

#ifdef VST_SUPPORT
  #include "fst.h"
#endif

#include <ayyi/ayyi_time.h>
#include <ayyi/utils.h>
#include <ayyi/ayyi++.h>
#include <ayyi/interface.h>
#include <ayyi/container.h>
#include <ayyi/list.h>

extern int debug;

namespace Ayi {

#define dbg(A, ...) dbgprintf("AyyiList", __func__, (char*)A, ##__VA_ARGS__);
#define warn(A, ...) warnprintf2("AyyiList", __func__, (char*)A, ##__VA_ARGS__)
#define err(A, ...) errprintf2("AyyiList", __func__, (char*)A, ##__VA_ARGS__)


AyyiList::AyyiList(ShmSegDefn* shm_seg, struct _ayyi_list** listp)
	: list(listp), seg(shm_seg)
{
	if(!listp) err("listp is null.");
}


AyyiList::~AyyiList()
{
	for(struct _ayyi_list* l = *list; l; l=l->next){
		seg->free_tlsf(l->data);
	}
}


void
AyyiList::clear()
{
	for(struct _ayyi_list* l = *list; l; l=l->next){
		seg->free_tlsf(l->data);
	}
	*list = 0;
}


struct _ayyi_list*
AyyiList::prepend(int id, const char* name)
{
	struct _ayyi_list* new_list = 0;

	AyyiContainer* container = (AyyiContainer*)seg->malloc_tlsf(sizeof(AyyiContainer), NULL);
	if(!container) return new_list;
	//TODO store this somewhere
	ShmContainer<shm_event> ocontainer(seg);
	ocontainer.init(container, "name");

	if(!(new_list = allocate())) return new_list;
	if(debug) dbg("adding: id=0x%x", id);

	new_list->id = id;
	new_list->data = container;
	new_list->next = *list;
	strncpy(new_list->name, name, 31);
	new_list->name[31] = '\0';

	*list = new_list;
	return new_list;
}


struct _ayyi_list*
AyyiList::prepend_unique(int id, const char* name)
{
	//add to the list only if the item isn't already present.

	if(find(id)) return *list;
	return prepend(id, name);
}


struct _ayyi_list*
AyyiList::find(AyyiContainer* container)
{
	struct _ayyi_list* ret = NULL;
	return ret;
}


struct _ayyi_list*
AyyiList::find(uint32_t id)
{
	struct _ayyi_list* ret = NULL;

	if(!list){ err("list has no list!"); return ret; }
	//dbg("id=%i list=%p", id, list);

	for(struct _ayyi_list* l = *list; l; l=l->next){
		//dbg("  id=0x%x", l->id);
		if((l->id & 0xffff) == (int)id) return l;
	}

	if(debug) dbg("id not found in list. id=%i", id);
	return ret;
}


int
AyyiList::size()
{
	int len = 0;
	for(struct _ayyi_list* l = *list; l; l=l->next){
		len++;
	}
	return len;
}

void
AyyiList::print()
{
	if(!list) return;

	if(debug) dbg("showing list: '%s':", (*list)->name);
	for(struct _ayyi_list* l = *list; l; l=l->next){
		dbg("  %p", l->data);
	}
}


struct _ayyi_list*
AyyiList::allocate()
{
	return (struct _ayyi_list*)seg->malloc_tlsf(sizeof(struct _ayyi_list), NULL);
}


} // end Ayi

