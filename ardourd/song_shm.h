/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __song_shm_h__
#define __song_shm_h__

#include "ayyi-ardour/shm.h"

#define SONG_SEGMENT_SIZE 4096

namespace Ayi {

class SongShm : public gnash::Shm
{
  public:
	                                                   SongShm             ();
	Ayi::SongHeader*                                   get_table           () { return (Ayi::SongHeader*)address; };
	void                                               set_transport_frame (uint32_t);

	Ayi::ShmContainer<struct Ayi::_region_shared>      regions;
	Ayi::ShmContainer<struct Ayi::_filesource_shared>  filesources;
	Ayi::ShmContainer<struct Ayi::_connection_shared>  connections;
	Ayi::ShmContainer<struct Ayi::_route_shared>       routes;
	Ayi::ShmContainer<struct Ayi::_midi_track_shared>  midi_tracks;
	Ayi::ShmContainer<struct Ayi::_midi_region_shared> midi_regions;
	Ayi::ShmContainer<struct Ayi::_ayyi_shm_playlist>  playlists;
#ifdef VST_SUPPORT
	Ayi::ShmContainer<struct Ayi::_shm_vst_info>       vst_info;
#endif
};

}

#endif
