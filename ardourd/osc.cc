#include <sys/types.h>
#include "stdint.h"
#include <ardour/ardour.h>
#include <ardour/audioengine.h>
#include <ardour/audiofilesource.h>
#include <ardour/audioregion.h>
#include <ardour/audio_track.h>
#include "lo/lo.h"

#include "app.h"
#include <ayyi/ayyi_types.h>
#include "ayyi/utils.h"
#include "osc.h"

namespace Ayi {
	extern Ayi::AD* core;
}
extern int debug;
extern int debug_general;

#define dbg(A, ...) dbgprintf("AD_OSC", __func__, (char*)A, ##__VA_ARGS__)
#define warn(A, ...) warnprintf2("AD_OSC", __func__, (char*)A, ##__VA_ARGS__)
#define err(A, ...) errprintf2("AD_OSC", __func__, (char*)A, ##__VA_ARGS__)

using namespace Ayi;


bool
AD_OSC::ayyi_ping()
{
#ifdef HAVE_LIBLO
	dbg ("ping! sending ack...");
	lo_send(t, "/ayyi/ack", "");
#endif
	return true;
}


bool
AD_OSC::ayyi_set_prop_int(int obj_type, int prop, int obj_idx, int val)
{
	//TODO this fn is exactly the same as ad_dbus_set_prop_int() but without returns or error reporting - should we add another level of abstraction?

	switch(obj_type){
		case AYYI_OBJECT_PART:
			{
				dbg("OBJECT_PART");

				if(!Ayi::core->song.region_id_ok(obj_idx)) return FALSE;

				switch (prop){
					case AYYI_STIME:
						Ayi::core->song.region_set_start((uint32_t)obj_idx, (uint32_t)val, AYYI_AUDIO);
						//*ok = TRUE;
						break;
					case AYYI_LENGTH:
						//length must be in samples. For mu, use ad_dbus_set_prop_int64().
						Ayi::core->song.region_set_length((uint32_t)obj_idx, AYYI_AUDIO, (uint32_t)val);
						//*ok = TRUE;
						break;
					case AYYI_TRACK:
						Ayi::core->song.audio_region_set_track((uint32_t)obj_idx, (uint32_t)val);
						//*ok = TRUE;
						break;
					case AYYI_INSET:
						warn("TRIM_LEFT is normally a 64 bit function.\n");
						Ayi::core->song.region_trim_left(obj_idx, (uint32_t)val, AYYI_AUDIO);
						//*ok = TRUE;
						break;
					case AYYI_SPLIT:
						warn("split is a 64 bit function.\n");
						break;
					case AYYI_PB_LEVEL:
						warn("pb_level is a float function. Ignoring...\n");
						break;
					default:
						dbg("OBJECT_PART: unexpected property type. %i", prop);
						//g_set_error(error, AD_DBUS_ERROR, 23420, "OBJECT_PART: unexpected property type: %i", prop);
						break;
				}
			}
			break;
		default:
			dbg("unhandled object_type.");
			break;
	}
	return true;
}


bool
AD_OSC::ayyi_get_history()
{
	dbg ("get_history!");
	return true;
}


