/*
    Copyright (C) 1999-2002 Paul Davis 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id: ardour_ui.h 878 2006-08-31 13:23:43Z paul $
*/

#ifndef __ardour_gui_h__
#define __ardour_gui_h__

/* need _BSD_SOURCE to get timersub macros */

#ifdef _BSD_SOURCE
#include <sys/time.h>
#else
#define _BSD_SOURCE
#include <sys/time.h>
#undef _BSD_SOURCE
#endif

#include <list>

#include <cmath>

#include <pbd/xml++.h>
#include <gtkmm/box.h>
#include <gtkmm/frame.h>
#include <gtkmm/label.h>
#include <gtkmm/table.h>
#include <gtkmm/fixed.h>
#include <gtkmm/drawingarea.h>
#include <gtkmm/eventbox.h>
#include <gtkmm/menu.h>
#include <gtkmm/menuitem.h>
#include <gtkmm/button.h>
#include <gtkmm/togglebutton.h>
#include <gtkmm/treeview.h>
#include <gtkmm/menubar.h>
#include <gtkmm/adjustment.h>
#include <ardour/ardour.h>
#include <ardour/session.h>

class AudioClock;
class PublicEditor;
class Keyboard;
class MeterBridge;
class OptionEditor;
class Mixer_UI;
class ConnectionEditor;
class RouteParams_UI;
class SoundFileBrowser;
class About;
class AddRouteDialog;
class NewSessionDialog;
class LocationUI;
class ColorManager;

namespace Gtkmm2ext {
	class TearOff;
}

namespace ARDOUR {
	class AudioEngine;
	class Route;
	class Port;
	class IO;
	class ControlProtocolInfo;
}

/*
namespace ALSA {
	class MultiChannelDevice;
}

#define FRAME_NAME "BaseFrame"
*/

#endif
