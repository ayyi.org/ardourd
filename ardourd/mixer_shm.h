/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __mixer_shm_h__
#define __mixer_shm_h__

#include "ayyi-ardour/shm.h"

namespace Ayi {

class MixerShm : public gnash::Shm
{
  public:
	MixerShm ();

	struct Ayi::_shm_seg_mixer *get_addr()    { return (struct Ayi::_shm_seg_mixer*)address; };

	//Ayi::ShmContainer<Ayi::_ayyi_channel>     tracks;
	Ayi::ShmContainer<Ayi::_ayyi_plugin_nfo>  plugins;
};

}

#endif
