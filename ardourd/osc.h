
#include <ardour/types.h>
#include "surfaces/control_protocol/control_protocol/basic_ui.h"

class AD_OSC : public BasicUI
{
  public:
	lo_address  t;       // use to send messages.

	bool        ayyi_ping               ();
	bool        ayyi_set_prop_int       (int type, int prop, int obj_idx, int val);
	bool        ayyi_get_history        ();
};

