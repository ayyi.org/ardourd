/*
    Copyright (C) 2006-2020 Tim Orford
    Copyright (C) 2001-2006 Paul Davis
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id: main.cc 873 2006-08-30 20:48:16Z nickm $
*/

#include <pbd/error.h>
#include <pbd/textreceiver.h>
#include <pbd/failed_constructor.h>
#include <pbd/pthread_utils.h>
#include <ardour/types.h>
#include <ardour/ardour.h>
#include <ardour/session_utils.h>
#include <ardour/audioengine.h>
#include <ardour/engine_state_controller.h>
#include <ardour/audiofilesource.h>
#include <ardour/audioregion.h>
#include <ardour/filesystem_paths.h>
#include <ardour/template_utils.h>
#include <ardour/debug.h>

#ifdef VST_SUPPORT
  #include <ardour/plugin.h>
  #include <ardour/plugin_manager.h>
  #include <ardour/plugin_insert.h>
  #include <ardour/ladspa_plugin.h>
  #include <ardour/vst_plugin.h>
#endif

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtkmm.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"

#define ENABLE_DBUS
#ifdef ENABLE_DBUS
  #define DBUS_API_SUBJECT_TO_CHANGE
  #include "dbus_object.h"
#endif

#include "version.h"
#include "opts.h"

#include <errno.h>
#include "ayyi/utils.h"
extern "C" {
  #ifdef VST_SUPPORT
    #include "fst.h"
    #define USE_GTK
  #else
    #undef USE_GTK
  #endif
  int debug = 1;
}
#define dbg(N, A, ...) { if (N <= debug) dbgprintf("", __func__, (char*)A, ##__VA_ARGS__); }

int debug_general   = 0;
int debug_shm       = 0;
int debug_region    = 0;
int debug_route     = 0;
int debug_io        = 0;
int debug_playlist  = 0;
int debug_plugins   = 0;
int debug_transport = 0;
int debug_automation= 0;
int debug_bundle    = 0;

using namespace Gtk;
using namespace GTK_ARDOUR;
using namespace ARDOUR;
using namespace PBD;
using namespace sigc;

static PBD::ScopedConnectionList engine_connections;

static int
do_audio_midi_setup (uint32_t desired_sample_rate)
{
	boost::shared_ptr<AudioBackend> backend = AudioEngine::instance()->current_backend();
	assert(backend);
	dbg(1, "backend=%s", backend->name().c_str());
	if(backend->name() == "ALSA"){
#if 0
		if(!EngineStateController::instance()){
			dbg(0, "!cannot get engine controller");
			return -1;
		}

		std::vector<AudioBackend::DeviceStatus> devices;
		dbg(0, "enginecontroller=%p ------------------------- **", EngineStateController::instance());
		EngineStateController::instance()->enumerate_devices(devices);
#else

		std::vector<AudioBackend::DeviceStatus> devices = backend->enumerate_devices ();

		/* this only gets FullDuplex devices

		#include "libs/ardouralsautil/ardouralsautil/devicelist.h"
		#include "libs/backends/alsa/alsa_audiobackend.h"

		std::map<std::string, std::string> devices2;
		get_alsa_audio_device_names (devices2, FullDuplex);
		for (std::map<std::string, std::string>::const_iterator i = devices2.begin (); i != devices2.end(); ++i) {
			dbg(0, " * device=%s", i->first.c_str());
		}
		*/
#endif
		p(0, "Available devices (%i):", devices.size());

		for(std::vector<AudioBackend::DeviceStatus>::iterator iter = devices.begin(); iter != devices.end(); ++iter){
			p(0, "  %s", (*iter).name.c_str());
			/*
			 *  primitive device blacklist pending better determination of which device to use
			 */
			if((*iter).name.find("HDA") != std::string::npos) {
				dbg(0, "ignoring HDA device");
			}else{
				p(0, "  current device: %s", backend->device_name().c_str());
				if(!backend->device_name().size()) backend->set_device_name(iter->name);
			}
		}
	}

	return AudioEngine::instance()->start();
}

class LogReceiver : public Receiver
{
  protected:
	void receive (Transmitter::Channel chn, const char* str);
};

void
LogReceiver::receive (Transmitter::Channel chn, const char* str)
{
	const char* prefix = "";

	switch (chn) {
		case Transmitter::Error:
			prefix = "\x1b[1;31m" "ERROR:" "\x1b[0;39m";
			break;
		case Transmitter::Info:
			/* ignore */
			return;
		case Transmitter::Warning:
			prefix = "[WARNING]:";
			break;
		case Transmitter::Fatal:
			prefix = "[FATAL]:";
			break;
		case Transmitter::Throw:
			/* this isn't supposed to happen */
			abort ();
	}

	/* note: iostreams are already thread-safe: no external
	   lock required.
	*/

	p(0, "%s %s", prefix, str);

	if (chn == Transmitter::Fatal) {
		::exit (9);
	}
}

LogReceiver receiver;

//----------------------------------------------

#include "app.h"
namespace Ayi {
	extern Ayi::AD* core;
}

#include "plugin.h"

#include "i18n.h"

//----------------------------------------------

static void shutdown (int status);

#if 0
static void
sigterm(int sig)
{
	(void)sig;
	printf("\n**GOT SIGTERM**\n");
}
#endif

#if 0
int
catch_signals (void)
{
	//cout << "catch_signals()..." << endl;

	struct sigaction action;
	pthread_t signal_thread_id;
	sigset_t signals;

//	if (setpgid (0,0)) {
	if (setsid ()) {
		warning << string_compose (_("cannot become new process group leader (%1)"), 
				    strerror (errno))
			<< endmsg;
	}

	sigemptyset (&signals);
	sigaddset(&signals, SIGHUP);
	sigaddset(&signals, SIGINT);
	sigaddset(&signals, SIGQUIT);
	sigaddset(&signals, SIGPIPE);
	sigaddset(&signals, SIGTERM);
	sigaddset(&signals, SIGUSR1);
	sigaddset(&signals, SIGUSR2);


	/* install a handler because otherwise
	   pthreads behaviour is undefined when we enter
	   sigwait.
	*/
	
	action.sa_handler = handler;
	action.sa_mask = signals;
	action.sa_flags = SA_RESTART|SA_RESETHAND;

	for (int i = 1; i < 32; i++) {
		if (sigismember (&signals, i)) {
			if (sigaction (i, &action, 0)) {
				cerr << string_compose (_("cannot setup signal handling for %1"), i) << endl;
				return -1;
			}
		}
	} 

	/* this sets the signal mask for this and all 
	   subsequent threads that do not reset it.
	*/
	
	if (pthread_sigmask (SIG_SETMASK, &signals, 0)) {
		cerr << string_compose (_("cannot set default signal mask (%1)"), strerror (errno)) << endl;
		return -1;
	}

	/* start a thread to wait for signals */
	if (pthread_create_and_store ("signal", &signal_thread_id, signal_thread, 0)) {
		cerr << "cannot create signal catching thread" << endl;
		return -1;
	}

	pthread_detach (signal_thread_id);
	return 0;
}
#endif

/** @param dir Session directory.
 *  @param state Session state file, without .ardour suffix.
 */
Session *
load_session (string dir, string state)
{
	dbg(0, "...");

	SessionEvent::create_per_thread_pool ("test", 512);

	AudioEngine* engine = AudioEngine::create ();

					Ayi::core->set_engine (*engine);
					Ayi::core->plugins_init ();

	if (!engine->set_default_backend ()) {
		std::cerr << "Cannot create Audio/MIDI engine\n";
		::exit (1);
	}

	init_post_engine ();

#if 0
	if (engine->start () != 0) {
		p(0, "Error: cannot start Audio/MIDI engine");
		::exit (1);
	}
#endif

	BusProfile bus_profile;
	bus_profile.master_out_channels = 2;
	bus_profile.input_ac = AutoConnectPhysical;
	bus_profile.output_ac = AutoConnectMaster;
	bus_profile.requested_physical_in = 0; // use all available
	bus_profile.requested_physical_out = 0; // use all available

	Session* session = new Session (*engine, dir, state, &bus_profile);
	Ayi::core->set_session(session);
	Ayi::core->on_session_load(); // too late to catch the SessionLoaded signal, so the handler is called manually.
	engine->set_session (session);

	return session;
}

#ifdef VST_SUPPORT
extern int gui_init (int* argc, char** argv[]);

/* this is called from the entry point of a wine-compiled
   executable that is linked against ardourd built
   as a shared library.
*/
extern "C" {
int ardour_main (int argc, char *argv[])
#else
int main (int argc, char *argv[])
#endif

{
	vector<Glib::ustring> null_file_list;

	pthread_setcanceltype (PTHREAD_CANCEL_ASYNCHRONOUS, 0);

	//catch_signals ();

	receiver.listen_to (error);
	receiver.listen_to (info);
	receiver.listen_to (fatal);
	receiver.listen_to (warning);

	ARDOUR::Session::AudioEngineSetupRequired.connect_same_thread (engine_connections, &do_audio_midi_setup);

	if (parse_opts (argc, argv)) {
		exit (1);
	}

	p(0, "\n\n RESTART");
	//p(0, "%sArdourd " VERSIONSTRING "%s with libardour %s and GCC version %s", yellow, white, svn_revision,
	p(0, "%sArdourd%s with libardour %s and GCC version ", yellow, white,
#ifdef __GNUC__
		__VERSION__
#else
		""
#endif
	);
	
	if (just_version) {
		exit (0);
	}

	if (sizeof(off_t) != 8) printf("warning: sizeof(off_t)=%zu (expected 8)\n", sizeof(off_t));

	Glib::thread_init();
	PBD::ID::init();

#ifdef VST_SUPPORT
	/* this does some magic that is needed to make GTK and Wine's own
	   X11 client interact properly.
	*/
	gui_init (&argc, &argv);
#endif

#ifdef USE_GTK
	Gtk::Main* ui = new Main(argc, argv); //note: yes this "daemon" actually has a gui - needed for vst plugins.
#else
	Glib::RefPtr<Glib::MainLoop> ui = Glib::MainLoop::create(false);
#endif

	if(!Ayi::AD::init()){ cout << "Ayyi init failed. Aborting..." << endl ; return -1; }

#ifdef ENABLE_DBUS
	if (!ad_dbus_register_service (ADDBUS_APP)) {
		return -1;
	}
	p(0, "dbus registration\n%s", print_ok());
#endif

#if 0
	PBD::debug_bits = PBD::debug_bits | DEBUG::AudioEngine/* | DEBUG::SessionEvents*/;
#endif

#if 0
	ARDOUR::AudioEngine *engine;

	try { 
		engine = new ARDOUR::AudioEngine (jack_client_name, "");
	} catch (AudioEngine::NoBackendAvailable& err) {
		error << string_compose (_("Could not connect to JACK server as  \"%1\""), jack_client_name) <<  endmsg;
		return -1;
	}

	AudioFileSource::set_build_peakfiles (true);
	AudioFileSource::set_build_missing_peakfiles (true);

	dbg(1, "doing ARDOUR::init()...");
	try {
		ARDOUR::init (use_vst, try_hw_optimization);
		ARDOUR::init_post_engine ();
		//setup_profile (); //TODO do we need this? what does it do?
		Ayi::core->set_engine (*engine);
		Ayi::core->plugins_init ();
		SessionEvent::create_per_thread_pool ("GUI", 512);
	} catch (failed_constructor& err) {
		error << _("could not initialize Ardour.") << endmsg;
		return -1;
	}

	dbg(1, "Starting engine...");
	try {
		if(!engine->start()) p(0, "AudioEngine::start\n%s", print_ok());
	}
	catch (...) {
		engine->stop ();
		error << _("Unable to start the session running") << endmsg;
		return -2;
	}
#else
	//ARDOUR::BootMessage.connect (msg_connection, invalidator (*this), boost::bind (&Splash::boot_message, this, _1), gui_context());

	//if (!ARDOUR::init (ARDOUR_COMMAND_LINE::use_vst, ARDOUR_COMMAND_LINE::try_hw_optimization, localedir.c_str())) {
	if (!ARDOUR::init (FALSE, TRUE, NULL)) {
		p(0, "could not initialize %s", PROGRAM_NAME);
		exit (1);
	}
#endif

	string snapshot_name, path;
	bool isnew;

	// if a session name is specified, check it exists
	if (session_name.length()){
		//directory?
		if (g_file_test(session_name.c_str(), G_FILE_TEST_IS_DIR)){
			p(0, "directory found: %s",  session_name.c_str());
		} else if (g_file_test(session_name.c_str(), G_FILE_TEST_EXISTS)){
			p(0, "file found: %s", session_name.c_str());
		} else {
			cout << "requested session directory not found! [" << red << " FAIL "<< white << "]" << endl;
			return -3;
		}
	}

	if (session_name.length()) {
		dbg(0, "session_name=", session_name.c_str());

		if (ARDOUR::find_session (session_name, path, snapshot_name, isnew)) {
			error << string_compose(_("could not load command line session \"%1\""), session_name) << endmsg;
			return -1;
		}
		if(isnew) cout << "specified session doesnt exist - making new session..." << endl;
		try {
			cout << "trying to open session: snapshot_name=" << snapshot_name << endl;
																								#if 0
			new_session = new Session(*engine, path, snapshot_name);
																								#endif
		}
		catch (...) {
			error << string_compose(_("Session \"%1 (snapshot %2)\" did not load successfully"), path, snapshot_name) << endmsg;
			return -1;
		}
	} else {
		// no session name was specified. Either load the previous, or make a temporary one.

		//store_current();

		GFile* f = g_file_new_for_path("/tmp/ardour_current"); //TODO use ~/.config/ayyi/ardourd/
		gchar* previous_path = NULL;
		gsize contents_length = 0;
		GError* ferr = NULL;
		if(g_file_load_contents(f, NULL, &previous_path, &contents_length, NULL, &ferr)){
			// loaded ok
			if(previous_path[contents_length - 1] == '\n'){
				previous_path[contents_length - 1] = '\0'; // remove trailing newline
			}
			dbg(0, "using previous session: %s", previous_path);
			path = previous_path;
		}else{
			path = "/tmp/ardour_tmp_session";
		}
		g_object_unref(f);

		snapshot_name = "1";
		rmdir(path.c_str()); // otherwise ardour will crash if dir exists but is empty
		try {
			dbg(0, "trying tmp session: name=%s", snapshot_name.c_str());
#ifdef DONT_INIT_FROM_TEMPLATE
			new_session = new Session (
				*engine,
				path,
				snapshot_name,
				ARDOUR::AutoConnectPhysical, ARDOUR::AutoConnectMaster,
				2, 2, 0, 2,
				44100 * 180
			);
#else
			string template_path;
			Ayi::core->find_template_path("2 Track", &template_path);

			try {
				load_session (path, snapshot_name);
			} catch (failed_constructor& e) {
				cerr << "failed_constructor: " << e.what() << "\n";
				exit (EXIT_FAILURE);
			} catch (AudioEngine::PortRegistrationFailure& e) {
				cerr << "PortRegistrationFailure: " << e.what() << "\n";
				exit (EXIT_FAILURE);
			} catch (exception& e) {
				cerr << "exception: " << e.what() << "\n";
				exit (EXIT_FAILURE);
			} catch (...) {
				cerr << "unknown exception.\n";
				exit (EXIT_FAILURE);
			}
#endif
		}
		catch (...) {
			dbg(0, "Session \"%s (snapshot %s)\" did not load successfully", path.c_str(), snapshot_name.c_str());
			return -1;
		}
	}

	dbg(2, "going into main loop...");
	ui->run();
#ifdef USE_GTK
	ui = 0;
#endif
	dbg(0, "loop finished.");

	ARDOUR::cleanup ();
	shutdown (0);

	return 0;
}
#ifdef VST_SUPPORT
} // end of extern C block
#endif


static void
shutdown (int status)
{
	dbg(0, "...");

	if(Ayi::core->engine->stop(true)) p(0, "engine wont stop!"); //testing...

	if (status) {

		dbg(0, "status=%i pid=%i thread=%i", status, getpid(), (int)pthread_self());
		Ayi::core->shutdown();
		/* drastic, but perhaps necessary */
		/*
		printf("%s(): killing... (disabled)\n", __func__); fflush(stdout);
		kill (-getpgrp(), SIGKILL);
		*/
		/*NOTREACHED*/

	} else {
		p(1, "status=0");
		Ayi::core->shutdown();

		pthread_cancel_all ();
	}

	exit (status);
}

