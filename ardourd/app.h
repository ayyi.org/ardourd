/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifdef VST_SUPPORT
  #include "fst.h"
#endif
#include <ardour/session.h>
#include <ardour/import_status.h>
#include <ardour/plugin.h>
#include "ayyi/ayyi_time.h"
#include "ayyi/inc/song.h"
#include "ayyi/inc/mixer.h"
#include "ayyi/ayyi++.h"
#include "ayyi-ardour/shm.h"
#include "ayyi-ardour/shared.h"
#include "ayyi-ardour/region.h"
#include "ayyi-ardour/filesource.h"
#include "ayyi-ardour/route.h"
#include "ayyi-ardour/midi_track.h"
#include "ayyi-ardour/plugin.h"
#include "ardourd/song.h"
#include "ardourd/mixer.h"
#include <evoral/Control.hpp>
#include <evoral/ControlList.hpp>
using namespace std;
namespace ARDOUR {
	class IO;
	class Region;
	class MidiRegion;
	class Processor;
	class Send;
}
typedef struct _ArdourDbus ArdourDbus;

namespace gnash {
	class Shm;
	class MixerShm;
}

class PluginUIWindow;

namespace Ayi {
	class AyyiConnection;
	class AyyiMidiRegion;

class AD : public AyyiServer, public PBD::ScopedConnectionList
{
  public:
	AD();
	static bool                            init                     ();
	bool                                   start                    ();
	void                                   shutdown                 ();

	ADSong                                 song;
	ADMixer                                mixer;

	ArdourDbus*                            dbus;

	GError*                                gerror;
	bool                                   set_error                (const char* caller, const char*, ...);
	void                                   get_error                (GError**);
	void                                   free_error               ();

	ARDOUR::AudioEngine*                   engine;
	bool                                   session_loaded;

	void                                   find_template_path       (const char* mix_template, string* path);
	bool                                   load_session             (const char* path, const char* snap_name, const char* mix_template);
	void                                   unload_session           ();
	int                                    connect_to_session       (ARDOUR::Session *s);
	void                                   set_engine               (ARDOUR::AudioEngine& e);
	void                                   notify_session_unloaded  ();
	bool                                   default_new_session_path (char* default_path);
	void                                   on_session_load          ();
	void                                   set_session              (ARDOUR::Session *t);
	ARDOUR::Session*                       get_session              ();
	bool                                   save                     ();
	bool                                   undo                     ();
	void                                   store_current_session_name(const char* path);

	void                                   playlist_added           (boost::shared_ptr<ARDOUR::Playlist> playlist, bool);
	bool                                   verify_playlists         ();
	void                                   print_playlists          ();

	void                                   source_removed           (ARDOUR::Source* source);
	void                                   print_files              ();

	void                                   engine_running           ();
	void                                   engine_stopped           ();
	void                                   connections_complete     ();
	void                                   add_bundle(boost::shared_ptr<ARDOUR::Bundle> b, std::vector<boost::shared_ptr<ARDOUR::Bundle> >* bundles);
	static void                            print_bundle             (ARDOUR::Bundle*);

	void                                   plugins_init();
	boost::shared_ptr<ARDOUR::PluginInfo>  plugin_info_lookup_by_name(char* plugin_name);
	void                                   print_plugins            ();
	void                                   print_plugin_parameters  (boost::shared_ptr<ARDOUR::Plugin>);
#ifdef VST_SUPPORT
	PluginUIWindow*                        find_plugin_window       (FST*);
	//list<PluginUIWindow *>               plugin_windows;
#endif

	void                                   position_change          (ARDOUR::framecnt_t);
	void                                   transport_change         ();
	void                                   loop_change              (ARDOUR::Location*);
	void                                   rec_change               ();
	void                                   print_locations          ();

	ARDOUR::IO*                            get_io                   (unsigned shm_index);
	ARDOUR::Route*                         get_route                (unsigned shm_index);
	ARDOUR::MidiTrack*                     get_midi_track           (unsigned shm_index);
	ARDOUR::Bundle*                        get_connection           (unsigned shm_index);
	boost::shared_ptr<ARDOUR::Bundle>*     get_connection_boost     (unsigned shm_index);
	boost::shared_ptr<ARDOUR::Track>       get_track_boost          (unsigned shm_index, Ayi::ObjType);
	boost::shared_ptr<ARDOUR::AudioTrack>  get_audio_track          (ObjIdx);
	boost::shared_ptr<ARDOUR::MidiTrack>   get_midi_track_boost     (unsigned shm_index);
	boost::shared_ptr<ARDOUR::Source>      source_find_by_region_idx(unsigned shm_index);

	std::string                            get_history              ();

	// import
	bool                                   file_import              (string path);
	ARDOUR::ImportStatus                   import_status;
	int                                    import_progress_timeout  ();
	static void*                           _import_thread           (void*);
	void*                                  import_thread            ();
	void                                   catch_new_audio_region   (ARDOUR::AudioRegion*);
	ARDOUR::AudioRegion*                   last_audio_region;

	sigc::connection                       interthread_progress_connection;

	void                                   set_tempo                (double tempo);
	bool                                   set_locator              (int locator_num, uint32_t pos);
	void                                   set_cycle_onoff          (bool on_off);
	bool                                   play                     ();
	bool                                   stop                     ();
	bool                                   rew                      ();
	bool                                   ff                       (int val);
	bool                                   rec                      (int val);

	// Time conversion methods that need access to the tempo map
	uint32_t                               pos2samples              (Ayi::SongPos*);
	uint32_t                               mu2samples               (long long mu);
	uint32_t                               beats2samples_float      (float beats);
	uint32_t                               posbuffer2samples        (char* buffer);

	bool                                   del_playlist             (int pod_idx);
	bool                                   set_input                (ObjIdx track, unsigned connection_idx);
	bool                                   set_output               (ObjIdx track, unsigned connection_idx);

	void                                   automation_on_list_added (ARDOUR::AutomationList* al);
	bool                                   auto_del_point           (guint obj_idx, guint auto_type, guint idx3);
	bool                                   auto_set_point           (guint obj_idx, guint auto_type, guint idx3, double val1, double val2);
	static void                            auto_lists_print         (ARDOUR::Route*);

	void                                   tempo_map_changed        (const PBD::PropertyChange&);
	void                                   location_changed         (ARDOUR::Location*);

  private:

	bool                                   every_point_one_seconds  ();

	bool                                   check_shm                ();

	PBD::ScopedConnectionList              signal_connections;
};


#ifdef VST_SUPPORT
void     fst_show_menu(FST*);
#endif

}; // namespace Ayi


extern "C" {
namespace Ayi {
	int  app_recv_shm_notify (short int _notify_level,int _seg_id,char *_name,int _pages,int _rw,int _status);
}

}

#ifdef APP_CC
char ok[32];
char fail[32];
#else
extern char ok[];
extern char fail[];
#endif

