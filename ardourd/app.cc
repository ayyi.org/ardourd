/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2006-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#include <glibmm.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtkmm.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include <sigc++/bind.h>

#include <pbd/pthread_utils.h>
#include <pbd/memento_command.h>
extern "C" {
#include "tlsf/src/tlsf.h"
}

#ifdef VST_SUPPORT
  #include "fst/fst.h"
#endif

#include <ardour/ardour.h>

#include <evoral/Sequence.hpp>
#include <ardour/tempo.h>
#include <ardour/port.h>
#include <ardour/audio_track.h>
#include <ardour/midi_track.h>
#include <ardour/session_playlist.h>
#include <ardour/diskstream.h>
#include <ardour/midi_diskstream.h>
#include <ardour/playlist.h>
#include <ardour/playlist_factory.h>
#include <ardour/audioplaylist.h>
#include <ardour/audioregion.h>
#include <ardour/midi_region.h>
#include <ardour/region.h>
#include <ardour/region_factory.h>
#include <ardour/audiofilesource.h>
#include <ardour/midi_source.h>
#include <ardour/midi_model.h>
#include <ardour/session.h>
#include <ardour/bundle.h>
#include <ardour/auto_bundle.h>
#include <ardour/audioengine.h>
#include <ardour/panner.h>
#include "ardour/pannable.h"
#include "ardour/panner_shell.h"
#include <ardour/location.h>
#include <ardour/automation_list.h>
#include <ardour/meter.h>
#include "ardour/amp.h"
#include <ardour/send.h>
#include <ardour/plugin.h>
#include <ardour/plugin_manager.h>
#include <ardour/plugin_insert.h>
#include <ardour/ladspa_plugin.h>
#include "ardour/template_utils.h"
#ifdef VST_SUPPORT
#include <ardour/vst_plugin.h>
#endif

#define APP_CC
#define CPLUSPLUS
#define dbg(N, A, ...) { if (N <= debug) dbgprintf("AD", __func__, (char*)A, ##__VA_ARGS__); }
#define warn(A, ...) warnprintf2("AD", __func__, (char*)A, ##__VA_ARGS__)
#define err(A, ...) errprintf2("AD", __func__, (char*)A, ##__VA_ARGS__)
#define AD_ERR(A, ...) errprintf2("AD", __func__, (char*)A, ##__VA_ARGS__)
#define P_GERR if(error){ err("%s\n", error->message); g_error_free(error); error = NULL; }
#define set_error_(A, ...) set_error(__func__, A, ##__VA_ARGS__)
//int zero=0;

extern int debug_general;
extern int debug_shm;
extern int debug_source;
extern int debug_bundle;
extern int debug_route;
extern int debug_playlist;
extern int debug_plugins;

extern int debug;

#include <ayyi/ayyi_types.h>
#include <ayyi/utils.h>
#include <ayyi/list.h>
#include "ayyi-ardour/connection.h"
#include "ayyi-ardour/midi_region.h"
#include "ayyi-ardour/playlist.h"
#include "ayyi-ardour/midi_playlist.h"
#include "ayyi-ardour/mixer_track.h"
#include "dbus_object.h"
#include "plugin_ui_simple.h"
#include "test.h"
#include "plugin.h"
#include "app.h"

struct BootMessageReceiver : sigc::trackable {
	void method2 (std::string);
};

void BootMessageReceiver::method2(std::string msg) {
	p(0, "%s%s%s", green, msg.c_str(), white);
}


namespace Ayi {

AD* core = 0;
ARDOUR::Session* session;

using namespace std;
using namespace ARDOUR;
using boost::shared_ptr;
using namespace Ayi;

extern struct _song_shm* shm_index;


bool
AD::init()
{
	sprintf(smerr,  "%serror!%s",      red, white);
	sprintf(smwarn, "%swarning:%s", yellow, white);
	sprintf(ok,     " [ %sok%s ]",   green, white);
	sprintf(fail,   " [ %sfail%s ]",   red, white);

	dbg (2, "init messaging...");

	try { core = new AD; } catch (...) { return false; }

	char* status = core->start() ? print_ok() : print_fail();

	p(0, "ayyi connections\n%s", status);
	return true;
}


AD::AD()
{
	gerror = NULL;

#ifdef USE_DBUS
	dbus = ad_dbus_get_instance();

	dbg (2, "attaching...");

	if(song.osong->attach_simple("song2", SONG_SEGMENT_SIZE)){
		dbg (2, "shm_address=%p fd=%i", song.osong->get_addr(), song.osong->get_fd());
	} else {
		err ("shm failed.\n");
		throw string("shm init failed.");
	}

	mixer.omixer->attach_simple("mixer2", MIXER_SEGMENT_SIZE);

	dbg(1, "region struct size: %i", sizeof(region_shared));
	dbg(1, "mixer struct size: %i", sizeof(_shm_seg_mixer));
#else
	shm_segment_add("song", SONG_SEGMENT_SIZE);
	shm_segment_add("amixer", MIXER_SEGMENT_SIZE);
#endif

	/*point_one_second_connection = */Glib::signal_timeout().connect (sigc::mem_fun(*this, &AD::every_point_one_seconds), 100);

	//BootMessageReceiver* receiver = new BootMessageReceiver();

	ARDOUR::BootMessage.connect (mem_fun (new BootMessageReceiver(), &BootMessageReceiver::method2));
}


bool
AD::start ()
{
#ifdef USE_DBUS
	got_shm = true;
	song.shm_init();
	mixer.shm_init();
#else
	dae_init_shm();
	shm_set_callback1(&app_recv_shm_notify);
	shm_set_debug(0);

	dbg(1, "registering with ayyi....\n");
	int err =
		dae_register_as(SONG) + //send a fifo message to Core to request reception of Song messages.
	    dae_register_as(REC) +
	    dae_register_as(TRAN) +
	    dae_register_as(AMIXER);
	if(err) err("client registration error!\n");

	dae_init_messaging("SongCore", dae_session);

	dbg(1, "start messaging...");
	start_messaging();

	unsigned long sleep_cnt = 0;

	dbg(1, "waiting for shm...");

	while (true) {
		printf("."); fflush(stdout);
		int connected = dae_get_connected(); // returns fifo_connected.

		//request shm segments - they will be created if they dont exist.
		if ((connected>0) && (sleep_cnt)) core->shm_connect();

		if (core->shm_is_complete()) {

			song.shm_init();
			mixer.shm_init();

			break;
		}

		usleep(40000);
		sleep_cnt++;
	}
#endif

	return true;
}


void
AD::shutdown ()
{
	//we currently leave the shm segments up, so that other clients dont crash.
}


bool
AD::check_shm ()
{
	bool ok = true;

	if(mixer.shm->tracks.block[0] && mixer.shm->tracks.block[0]->last != song.shm->routes.block[0]->last){
		err ("routes and tracks out of sync.\n");
		dbg (0, "routes.last=%i tracks.last=%i", mixer.shm->tracks.block[0]->last, song.shm->routes.block[0]->last);
		ok = false;
	}

	return ok;
}


bool
AD::set_error (const char* caller, const char* format, ...)
{
	char str[256];
	char* s = str;

	if(caller){
		snprintf(str, 32, "%s: ", caller);
		str[32] = '\0';
		s = str + strlen(str);
	}

	va_list args;
	va_start(args, format);
	vsprintf(s, format, args);
	va_end(args);

	errprintf(str);

	if (gerror) free_error();
	g_set_error (&gerror, ad_dbus_error_quark(), 34072, (const gchar*)str, ""); //empty string literal added to stop gcc warning.

	return false;
}


void
AD::get_error (GError** error)
{
	if (!gerror) return;
	if (error) dbg(1, "%s", gerror->message);
	if (error) *error = g_error_copy(core->gerror);
	g_error_free (gerror);
	gerror = NULL;
}


void
AD::free_error ()
{
	if (!gerror) return;
	g_error_free (gerror);
	gerror = NULL;
}


void
AD::find_template_path (const char* mix_template, string* path)
{
	// look for a local path
	// DATA_DIR is relative - in dev (build/default) location is ../../

	*path = DATA_DIR "/ardourd/templates/" + string(mix_template) + ".template";
	if (g_file_test(path->c_str(), G_FILE_TEST_EXISTS)){
		dbg(0, "using local template");
		return;
	}

	vector<TemplateInfo> templates;
	find_session_templates (templates);
	//dbg(0, "n_templates=%i", templates.size());

	for (vector<TemplateInfo>::iterator x = templates.begin(); x != templates.end(); ++x) {
		cout << "template: " << (*x).name << " " << (*x).path << endl;

		if((*x).name == mix_template){
			*path = (*x).path;
			break;
		}
	}
	dbg(0, "path=%s", path->c_str());
}


gboolean
delayed_quit (gpointer user_data)
{
	Ayi::core->unload_session();
	raise(SIGINT);

	return G_SOURCE_REMOVE;
}


bool
AD::load_session (const char* path, const char* snap_name, const char* mix_template)
{
	// @param path         - if this is empty, we create a new session with a default name and path.
	// @param mix_template - should be simple name, not filepath

	dbg(0, "...");
#define STOP_ON_UNLOAD
#ifdef STOP_ON_UNLOAD
	dbg(0, "STOP_ON_UNLOAD is enabled. restarting... path=%s", path);
	store_current_session_name(path);
	g_timeout_add(500, delayed_quit, NULL);
	return true;
#else

	unload_session();

	dbg(0, "template=%s", mix_template);

	string path_;
	if(strlen(path)) path_ = path;
	else {
		char default_path[256];
		default_new_session_path(default_path);
		path_ = default_path;
	}

	string template_path;
	find_template_path(mix_template, &template_path);

	dbg(0, "trying new... ", path_.c_str());
	Session* new_session;
	string snap_name_ = snap_name;
	try {
		new_session = new Session(*engine, path_, snap_name_, 0, template_path);
	}
	catch (...) {
		cout << "**** Session \""<< path << " (snapshot " << snap_name << ")\" did not load successfully" << endmsg;
		return set_error_("unable to load session: %s -  snapshot=%s", path, snap_name);
	}

	connect_to_session(new_session);

	dbg(0, "%s %s", session->path().c_str(), ok);

	return (/*session_loaded = */true); // **** testing setting session_loaded later, when all objects are present
#endif
}


void
AD::unload_session ()
{
	AyyiServer::enable_announcements = false;
	session_loaded = false;

	notify_session_unloaded(); // notify early so clients can ignore subsequent events.

	delete session;
	session = 0;

	if(song.osong->filesources.count_items()){
		warn ("filesources not unloaded! last=%i\n", song.shm->filesources.last);
		_filesource_shared* shared = NULL;
		while((shared = core->song.osong->filesources.next(shared))){
			dbg(0, "  %i...", shared->shm_idx);
			song.osong->filesources.remove_item(shared->shm_idx);
		}

		if(song.shm->filesources.last > -1) warn("filesources kill failed.\n");
	}
	if(song.shm->routes.last > -1){
		warn("routes not unloaded! last=%i\n", song.shm->routes.last);
		_route_shared* shared = NULL;
		while((shared = core->song.osong->routes.next(shared))){
			dbg(0, "  killing route: %i ...", shared->shm_idx);
			ARDOUR::Route* ardour_object = (ARDOUR::Route*)shared->object;
			if(ardour_object) delete (AyyidRoute*)ardour_object->ayyi_track;
			else warn("route kill: server_object already deleted?\n");

			song.osong->routes.remove_item(shared->shm_idx);
		}

		if(song.shm->routes.last > -1){
			warn("routes kill failed.\n");
		}
	}
	if(mixer.tracks.count_items()){
		warn("mixer tracks not unloaded! last=%i. killing...\n", mixer.shm->tracks.last);

		track_shared* shared = NULL;
		while((shared = mixer.tracks.next(shared))){
			dbg(0, "  killing track: name=%s ...", shared->name);
			//MixerTrack<ARDOUR::AudioTrack, struct _ayyi_channel>::kill(0, i); //we dont need to call kill()

			ARDOUR::AudioTrack* ardour_object = (ARDOUR::AudioTrack*)shared->object;
			if(ardour_object) delete (MixerTrack<ARDOUR::Route>*)ardour_object->ayyi_track;
			else warn("server_object already deleted?\n");

			mixer.tracks.remove_item(shared->shm_idx);
		}

		if(mixer.shm->tracks.last > -1){
			warn("mixer tracks kill failed.\n");
		}
	}

	//regions:
	if(song.osong->regions.count_items()){
		warn("regions not unloaded! last=%i\n", song.shm->regions.last);
		//print_regions();  //err... seems to be a bad idea to print regions when sessionn is unloaded.

		//brute force clearing of objects:
		dbg(0, "killing regions...");
		region_shared* region = NULL;
		while((region = song.osong->regions.next(region))){
			dbg(0, "region->name=%s", region->name);
			AyyiAudioRegion::kill(region->shm_idx);

			//delete shared;
		}

		if(song.shm->regions.last > -1) warn("regions kill failed.\n");
	}

	if(song.osong->playlists.count_items()){
		warn("playlists not unloaded! last=%i killing...\n", song.shm->playlists.last);
		_ayyi_shm_playlist* playlist = NULL;
		while((playlist = song.osong->playlists.next(playlist))){
			dbg(0, "region->name=%s", playlist->name);
			//AyyiPlaylist::kill(playlist->shm_idx);

			/*
			ARDOUR::Playlist* ardour_object = (ARDOUR::Playlist*)playlist->object;
			if(ardour_object) delete (AyyidRoute*)ardour_object->ayyi_track;
			else warn("route kill: server_object already deleted?\n");
			*/

			song.osong->playlists.remove_item(playlist->shm_idx);
		}

		if(song.shm->playlists.last > -1) warn("playlists kill failed.\n");
	}
	else dbg(0, "no playlists to clear");

	song.shm_init();
	mixer.shm_init();
	dbg(0, "done\n-------------------------------------------------------------");
}


int
AD::connect_to_session (Session* s)
{
	cout << "AD::connect_to_session()... #############           #############          ##############" << endl;
	//connect signals here (see gtkardour/ardour_ui_dialogs.cc)

	return TRUE;
}


bool
AD::default_new_session_path (char* default_path)
{
	//find a name for a new session without user intervention.

	char basename[] = "new_session";

	strcpy(default_path, "new_session");
	bool exists = g_file_test(default_path, G_FILE_TEST_EXISTS);
	for(int i=0; exists; i++){
		sprintf(default_path, "%s-%02i", basename, i);
		exists = g_file_test(default_path, G_FILE_TEST_EXISTS);
	}
	return !exists;
}


void
AD::set_engine (AudioEngine& e)
{
	engine = &e;

	engine->Running.connect_same_thread (*this, boost::bind (&AD::engine_running, this));
	engine->Stopped.connect_same_thread (*this, boost::bind (&AD::engine_stopped, this));
	/*
	engine->Halted.connect (mem_fun(*this, &ARDOUR_UI::engine_halted));
	engine->SampleRateChanged.connect (mem_fun(*this, &ARDOUR_UI::update_sample_rate));

	FileSource::set_build_peakfiles (true);
	FileSource::set_build_missing_peakfiles (true);

	update_sample_rate (engine->frame_rate());

	starting.connect (mem_fun(*this, &ARDOUR_UI::startup));
	stopping.connect (mem_fun(*this, &ARDOUR_UI::shutdown));
	*/
}


gboolean
onload_delayed0 (gpointer user_data)
{
	dbg(0, "...");
	DBusClient::msg_out_obj_added(AYYI_OBJECT_SONG, 0);
	return false;
}


gboolean
onload_delayed (gpointer user_data)
{
	UNDERLINE;
	dbg(0, "...");
	_route_shared* shared = NULL;
	while((shared = core->song.osong->routes.next(shared))){
		ARDOUR::Route* r = (ARDOUR::Route*)shared->object;
		if(r){
			Ayi::AyyidRoute* rr = r->ayyi_track;
			if(rr){
				ARDOUR::IOChange change;
				rr->output_changed(change, NULL);
				//rr->print_routing();
			}
		}
	}

#if 0
	connection_shared* connection = NULL;
	while((connection = core->song.osong->connections.next(connection))){
		ARDOUR::Bundle* dest = core->get_connection(connection->shm_idx);
		if(!dest){
			dbg(0, "no-bundle connection: %i: '%s'", connection->shm_idx, connection->name);
			std::list<string> l = AyyiConnection::get_port_names(connection);
			for(std::list<string>::iterator i = l.begin(); i != l.end(); ++i){
				dbg(0, "   =%s", (*i).c_str());
			}
		}
	}
#endif

	//check for unused playlists:
	playlist_shared* playlist = NULL;
	while((playlist = core->song.osong->playlists.next(playlist))){
		if(playlist->track == -1){
			warn("unused playlist: %i: %s", playlist->shm_idx, playlist->name);
			core->del_playlist(playlist->shm_idx);
		}
	}

	//core->song.osong->filesources.print();
	//core->print_routes();
	//core->print_regions();

	UNDERLINE;
	return false;
}


bool
port_has_pair (int n, std::string const port, vector<string>* ports, int* pair)
{
	string last = port.substr(port.size()-1, 1);

	stringstream ss(last);
	int nn;
	ss >> nn;

	if(nn == 1){
		stringstream ss;
		ss << (nn + 1);

		string target = port.substr(0, port.size()-1) + ss.str();

		int p = 0;
		for(vector<string>::iterator it = ports->begin(); it<ports->end(); it++, p++){
			string const pp = (*it);
			if(pp == target){
				*pair = p;
				return true;
			}
		}
	}

	return false;
}


void
AD::on_session_load ()
{
	//this is called after a session has loaded. Note: if the engine is not yet running, connections wont be built yet.

	if (!session) { err ("session not set!\n"); return; }

	dbg(0, "path=%s snapshot='%s'",  session->path().c_str(), session->snap_name().c_str());
	g_strlcpy(song.shm->path, session->path().c_str(), 256);
	g_strlcpy(song.shm->snapshot, session->snap_name().c_str(), 256);

	song.shm->sample_rate = session->frame_rate();

	Location::start_changed.connect_same_thread(signal_connections, boost::bind (&AD::location_changed, this, _1));
	Location::end_changed.connect_same_thread(signal_connections, boost::bind (&AD::location_changed, this, _1));

	if(!session->current_end_frame()){
		session->set_session_extents (0, 44100 * 32);
	}
	song.samples2pos((SongPos*)&song.shm->end, session->current_end_frame());

	//ensure that a loop is always set:
	if (!session->locations()->auto_loop_location()) {
		dbg (2, "no autoloop. setting...");
		ARDOUR::Location* loop = new Location(*session, 44100, 88200, "autoloop");
		session->locations()->add(loop, false);
		session->set_auto_loop_location(loop);
	}
	else dbg (2, "autoloop already found in session.");
	//share the autoloop as locators 1 and 2:
	ARDOUR::Location* loop = session->locations()->auto_loop_location();
	set_locator(1, loop->start());
	set_locator(2, loop->end());
	song.samples2pos(&song.shm->locators[9], session->current_end_frame());

	if (false) { // the connections have not yet neccesarily been added to Session.
		if (debug_bundle) dbg(0, "finding output connections...");

		/* session->foreach_bundle has been removed. use boost::shared_ptr<BundleList> session->bundles() instead.

		std::vector<boost::shared_ptr<Bundle> > bundles;
		session->foreach_bundle (sigc::bind (sigc::mem_fun (this, &AD::add_bundle), &bundles));
		*/
	}

	//create AyyiConnections for all jack audio ports.
	//FIXME some of these will be dupes!! how to check? get port_name from each bundle?
	PortFlags port_types[2] = {PortFlags::IsOutput, PortFlags::IsInput};
	bool is_input = false;
	for (int t=0;t<2;t++){
		vector<string> ports;
		engine->get_ports ("", DataType::AUDIO, port_types[t], ports);
		int n = 0;
		for(vector<string>::iterator it = ports.begin(); it<ports.end(); it++, n++){
			string const p = (*it);
			dbg(1, "port=%s", (*it).c_str());
			if(p.find("ardour:") != 0 && p.find(":LTC") != 0){
				AyyiConnection::connections_without_bundles.push_back(new AyyiConnection(p, is_input));
				int pair;
				if(port_has_pair(n, p, &ports, &pair)){
					std::string const p2 = ports[pair];
					AyyiConnection::connections_without_bundles.push_back(new AyyiConnection(p, p2, is_input));
				}
			}
		}
		is_input = true;
	}

	if(!session->ntracks()){
		dbg(0, "session has no tracks. adding one...");
		std::list<boost::shared_ptr<AudioTrack> > route = session->new_audio_track(1, 2, Normal);
	}

	session_loaded = true;
	enable_announcements = true;
	//msg_out_obj_added(AYYI_OBJECT_SONG, 0);

	{
		PBD::PropertyChange c;
		tempo_map_changed(c); //TODO call this instead
		ARDOUR::TempoMap& tempo_map = session->tempo_map();
		const ARDOUR::Tempo& tempo = tempo_map.tempo_at(0);
		song.shm->bpm = tempo.beats_per_minute();

		session->tempo_map().PropertyChanged.connect_same_thread (*this, boost::bind (&AD::tempo_map_changed, this, _1));
	}

	check_shm();

#ifdef NOT_USED
	_ayyi_shm_playlist* playlist = NULL;
	while((playlist = core->song.osong->playlists.next(playlist))){
		ARDOUR::Playlist* _pl = (ARDOUR::Playlist*)playlist->object;
		AyiMidiPlaylist* pl = dynamic_cast<AyiMidiPlaylist*> (_pl);
		dbg(1, "       playlist track=", pl->shm->track);
	}
#endif
	AyiMidiPlaylist::remove_unused();

	store_current_session_name(session->path().c_str());

	boost::shared_ptr<Route> master = session->master_out();
	if(!master){
		warn ("*************** session has no master out ****************\n");

		// perhaps ensure it is done in Session::create instead

#if 0
		RouteList rl;

		boost::shared_ptr<Route> r (new Route (*session, "Master", Route::MasterOut, DataType::AUDIO));
		if (r->init ()) {
//			return -1;
		}

		/*
		{
			Glib::Threads::Mutex::Lock lm (AudioEngine::instance()->process_lock ());
			r->input()->ensure_io (count, false, *session);
			r->output()->ensure_io (count, false, *session);
		}
		*/

		rl.push_back (r);
		session->add_routes (rl, false, false, false);
#endif
	}

	g_timeout_add(3000, onload_delayed0, NULL);
	g_timeout_add(2500, onload_delayed, NULL);
}


void
AD::notify_session_unloaded ()
{
	dbg (0, "...");

	DBusClient::msg_out_obj_deleted(AYYI_OBJECT_SONG, 0);
}


void
AD::playlist_added (boost::shared_ptr<ARDOUR::Playlist> playlist, bool unused)
{
}


bool
AD::verify_playlists ()
{
	return true;
}


void
AD::print_playlists ()
{
	dbg(0, "...");

	p(0, "  ardour playlists:");
	vector<boost::shared_ptr<ARDOUR::Playlist> > playlists;
	session->playlists->get(playlists);
	for (vector<boost::shared_ptr<ARDOUR::Playlist> >::iterator i = playlists.begin(); i != playlists.end(); ++i) {
		if (!(*i)->hidden()) {
			//PBD::ID id = (*i)->id();
			boost::shared_ptr<AyiPlaylist> ap = boost::dynamic_pointer_cast<AyiPlaylist> ((*i));
			if(ap){
				p(0, "    %p %2i %s", ap->shared_, ap->get_index(), (*i)->name().c_str());
			}else{
				boost::shared_ptr<AyiMidiPlaylist> ap = boost::dynamic_pointer_cast<AyiMidiPlaylist> ((*i));
				if(ap){
					p(0, "    %p %2i %12s", ap->shared_, ap->get_index(), (*i)->name().c_str());
				}else{
					p(0, "    %12s", (*i)->name().c_str());
				}
			}
		}
	}

	//shared playlists:
	p(0, "  shared playlists:\n  %3s  %10s %20s %10s %2s", "", "id", "", "obj", "tr");
	int total = 0;
	int n_errors = 0;
	struct _ayyi_shm_playlist* shared = NULL;
	while((shared = song.osong->playlists.next(shared))){
		p(0, "  %3i: %10Lu %20s %10p %2i", shared->shm_idx, shared->id, strlen(shared->name) ? shared->name : "<unnamed>", shared->object, shared->track);
		if(!shared->shm_idx && total){ err("index not set. playlist=%p", shared); n_errors++; }
		total++;
		if(total>255 || n_errors > 5) break;
	}
	p(0, "  total=%i last=%i", total, song.shm->playlists.last);
}


void
AD::connections_complete ()
{
	dbg(0, "...");

	/*
	//this doesnt work cos route->output_connection() is always empty - see AyyidRoute::output_changed() instead.
 
	route_shared* shared_pod = NULL;
	while((shared_pod = song.osong->routes.next(shared_pod))){
		ARDOUR::Route* route = (ARDOUR::Route*)shared_pod->object;
		if(route){
			Connection* input = route->input_connection();
			Connection* output = route->output_connection();
			if(debug_route>-1) dbg(0, "track=%s input=%p output=%p", route->name().c_str(), input, output);
			if(output){
				int type = 0;
				ad_debug(__func__, type, 0, "output=%s", output->name().c_str());
				dbg(0, "output=%s", output->name().c_str());
			}
		}
	}
	*/
}


void
AD::engine_running ()
{
	// too early for connections...
}


void
AD::engine_stopped ()
{
}


void
AD::add_bundle (boost::shared_ptr<ARDOUR::Bundle> b, std::vector<boost::shared_ptr<Bundle> >* bundles)
{
	if(debug_bundle) cout << "AD::add_bundle()... (empty fn)" << endl;
}

void
AD::print_bundle (ARDOUR::Bundle* bundle)
{
}


/*

	ardour plugins:
	---------------

	PluginManager class holds the list of plugins

*/
void
AD::plugins_init ()
{
	if(debug_plugins) printf("AD::%s()...\n", __func__);
	Ayi::ShmSegDefn* segment = (Ayi::ShmSegDefn*)mixer.omixer;

	//PluginManager& mgr (PluginManager::instance());
	ARDOUR::PluginInfoList& pluginlist = PluginManager::instance().windows_vst_plugin_info();
	int i=0;
	for(ARDOUR::PluginInfoList::iterator x = pluginlist.begin(); x != pluginlist.end(); x++){
		if (i++ > 20) break;
		ARDOUR::PluginInfo* pi = (*x).get();
		new AyyiPlugin<ARDOUR::PluginInfo, _ayyi_plugin_nfo>(pi, segment);
		if(debug_plugins) dbg(0, " shared=%p '%s'", pi->ad1_plugin, pi->name.c_str());
	}
}


ARDOUR::PluginInfoPtr
AD::plugin_info_lookup_by_name (char* plugin_name)
{
	//this somewhat duplicates ARDOUR::find_plugin() in ardour/plugin.cc 

	PluginManager& mgr (PluginManager::instance());
	ARDOUR::PluginInfoList& pluginlist = mgr.windows_vst_plugin_info();
	for(ARDOUR::PluginInfoList::iterator x = pluginlist.begin(); x != pluginlist.end(); x++){
		//printf("%s(): name=%s\n", __func__, (*x)->name.c_str());
		if(!strcmp((*x)->name.c_str(), plugin_name)) return *x;
	}
	return ARDOUR::PluginInfoPtr ((PluginInfo*) 0);
}


void
AD::print_plugins ()
{
	//typedef std::list<PluginInfoPtr> PluginInfoList;
	ARDOUR::PluginInfoList& pluginlist = PluginManager::instance().windows_vst_plugin_info();

	if(strcmp(mixer.shm->plugins.signature, "Ayi plugins")){ err("incorrect signature!\n"); return; }

	dbg(0, "found %i plugins", pluginlist.size());

	int i=0;
	for(ARDOUR::PluginInfoList::iterator x = pluginlist.begin(); x != pluginlist.end(); x++){
		if (i++>10) break;
		p(0, "  %s", (*x)->name.c_str());

		//ARDOUR::PluginInfo* pi = (*x).get();
		//pi->load(*session);
		//printf("%s(): name=%s category=%s\n", __func__, pi->name.c_str(), pi->category.c_str());
	}

	struct _container* kontainer = &mixer.shm->plugins;
	struct block* block = mixer.shm->plugins.block[0];
	dbg(0, "k_rel=%x block_rel=%x slots_rel=%x &last=%x last=%i k[0]=%x %p\n", (char*)kontainer-(char*)mixer.shm, (char*)block-(char*)mixer.shm, (char*)block->slot-(char*)mixer.shm, (char*)&block->last-(char*)mixer.shm, (unsigned)block->last, (char*)&(block->slot[0])-(char*)kontainer, (char*)(block->slot[0]));
}


void
AD::print_plugin_parameters (boost::shared_ptr<ARDOUR::Plugin> plugin)
{
	dbg(0, "...");
	set<Evoral::Parameter> params = plugin->automatable();
	for(set<Evoral::Parameter>::iterator p = params.begin(); p != params.end(); p++){
		dbg(0, "type=%i id=%i", (*p).type(), (*p).id());
	}
}


#ifdef VST_SUPPORT
PluginUIWindow*
AD::find_plugin_window (FST* fst)
{
	if (!fst) { err("fst NULL"); return (PluginUIWindow*)0; }

	err("FIXME session->_plugin_inserts no longer exists");
#if 0
	list<PluginInsert *> inserts = core->session->_plugin_inserts;
	for (list<PluginInsert *>::iterator i = inserts.begin(); i != inserts.end(); i++) {
		PluginInsert* insert = *i;
		boost::shared_ptr<Plugin> plugin = insert->plugin();
		boost::shared_ptr<VSTPlugin> vstplugin = boost::dynamic_pointer_cast<VSTPlugin>(plugin);
		if (vstplugin) {
			if (vstplugin->fst() == fst) {
				void* gui = insert->get_gui();
				dbg(0, "found! plugin=%s", insert->plugin()->name());
				return reinterpret_cast<PluginUIWindow *>(gui);
			}
		}
	}
#endif
	warn("plugin window not found.");
	return (PluginUIWindow*)0;
}
#endif


void
AD::set_session (Session* t)
{
	//TODO this is being called twice at startup - is that ok?

	if(debug_general>1) dbg(0, "session=%p", t);
	// deprecated - use ARDOUR::Session::instance
	session = t;

	session->PositionChanged.connect_same_thread (signal_connections, boost::bind(&AD::position_change, this, _1));

#if 0
	session->TransportStateChange.connect(signal_connections, 0/*invalidator(*this)*/, boost::bind(&AD::transport_change, this), 0);
#else
	session->TransportStateChange.connect_same_thread (signal_connections, boost::bind (&AD::transport_change, this));
#endif

	//session->SourceRemoved.connect  (sigc::mem_fun(*this, &AD::source_removed)); - is this causing a crash?
	//session->auto_loop_location_changed.connect(signal_connections, MISSING_INVALIDATOR, boost::bind(&AD::loop_change, this), 0);
	session->auto_loop_location_changed.connect_same_thread(*this, boost::bind(&AD::loop_change, this, _1));
#if 0
	session->RecordStateChanged.connect(signal_connections, MISSING_INVALIDATOR, boost::bind(&AD::rec_change, this), 0);
#else
	session->RecordStateChanged.connect_same_thread(*this, boost::bind(&AD::rec_change, this));
#endif

	//PlaylistFactory::PlaylistCreated.connect(signal_connections, MISSING_INVALIDATOR, boost::bind(&AD::playlist_added, this), 0);
	PlaylistFactory::PlaylistCreated.connect_same_thread (*this, boost::bind (&AD::playlist_added, this, _1, _2));

	session->IOConnectionsComplete.connect_same_thread (*this, boost::bind(&AD::connections_complete, this));

	/*
	ARDOUR::AutomationList::AutomationListCreated.connect (signal_connections, MISSING_INVALIDATOR, boost::bind(&AD::automation_on_list_added, this), 0);
	*/

	//session->RegionRemoved.connect (mem_fun(*this, &AD::on_region_removed));

	//session->SessionLoaded.connect_same_thread(*this, boost::bind(&AD::on_session_load, this));
}


Session*
AD::get_session ()
{
	return session;
}


bool
AD::save ()
{
	session->save_state("");
	return true;
}


bool
AD::undo ()
{
	if (session) session->undo (1);
	return true;
}


#if 0
static void
on_stored (GObject* source, GAsyncResult *res, gpointer user_data)
{
	dbg(0, "stored!");
	GFile* f = (GFile*)user_data;
	GError* error = NULL;
	if(!g_file_replace_contents_finish(f, res, NULL, &error)){
		dbg(0, "store failed");
	}
	g_object_unref(f); //TODO
}
#endif


void
AD::store_current_session_name (const char* path)
{
	// This path determines which session will be loaded next time the service is started.

	dbg(0, "setting current file... %s", path);
	GFile* f = g_file_new_for_path("/tmp/ardour_current"); // TODO use ~/.config/ayyi/ardourd/
#if 1
	//as we are possible about to quit, we need to do the save synchronously.
	GError* error = NULL;
	if(!g_file_replace_contents(f, path, strlen(path), 0, false, G_FILE_CREATE_NONE, NULL, NULL, &error)){
		dbg(0, "store failed");
	}
#else
	g_file_replace_contents_async(f, path, strlen(path), 0, false, G_FILE_CREATE_NONE, NULL, on_stored, f);
	//g_object_unref(f); //TODO
#endif
}


/*
 *  Update shm with automation changes
 */
bool
AD::every_point_one_seconds ()
{
	if(!session_loaded) return true;

	track_shared* track = NULL;
	while((track = mixer.tracks.next(track))){

		ARDOUR::AudioTrack* ardour_track = (ARDOUR::AudioTrack*)track->object;
		if(ardour_track){
			boost::shared_ptr<Amp> amp = ardour_track->amp();
			if(amp){
				track->level = amp->gain_control()->get_double();

				boost::shared_ptr<ARDOUR::Panner> panner = ardour_track->panner();
				if(panner){
//TODO mapping
					track->pan = MixerTrack<ARDOUR::Route>::pan_map_out(panner->position());
				}
			}

			boost::shared_ptr<PeakMeter> meter = ardour_track->shared_peak_meter();
			ChanCount ch = meter->output_streams();
			for(int c=0; c<(int)ch.n_total() && c<2; c++){
				track->visible_peak_power[c] = song.osong->routes.get_item(track->shm_idx)->visible_peak_power[c] = meter->meter_level(c, MeterPeak);
			}
		}

		for(int i=0;i<AYYI_AUX_PER_CHANNEL;i++){
			_ayyi_aux* aux = track->aux[i];
			boost::shared_ptr<Send> send = mixer.get_send_for_route((Route*)track->object, i);
			if(aux && send.get()){
				boost::shared_ptr<Amp> amp = send->amp();
				if(!amp.get()) return set_error_("failed to get io for send.");
				aux->level = amp->gain_control()->get_value();
			}
		}
	}

	mixer.meter_update();

	return true;
}


void
AD::position_change (ARDOUR::framecnt_t frame)
{
	//this is a Transport thing - not session objects.

	dbg(0, "...");

	song.osong->set_transport_frame(frame);
}


void
AD::transport_change ()
{
	dbg(0, "... %.2f", session->_transport_speed);

	song.shm->transport_speed = session->_transport_speed;
	song.shm->play_loop = session->play_loop;

	ad_dbus_emit_transport(dbus, NULL);
}


void
AD::loop_change (ARDOUR::Location* location)
{
	dbg(1, "...");

	//ad_dbus_emit_transport(dbus, NULL);
	//print_locations();
}


void
AD::rec_change ()
{
	song.shm->rec_enabled = session->get_record_enabled();

	dbg(0, "rec_enabled=%i", song.shm->rec_enabled);
	dbg(0, "auto_input=%i", session->config.get_auto_input());

	ad_dbus_emit_transport(dbus, NULL);
}


void
AD::print_locations ()
{
	// session->locations is a class with a private list of locations!!

	ARDOUR::Locations* locs = session->locations();

	uint32_t x = 0;
	framepos_t fr;
	while((fr = locs->first_mark_after(x))){
		dbg(0, "loc_start=%Li", fr);
		x = fr;
	}

	Location* loc = locs->auto_loop_location();

	dbg(0, "loop: start=%Li end=%Li name=%s", loc->start(), loc->end(), loc->name().c_str());
}


void
AD::source_removed (Source* source)
{
	dbg(0, "...");
}


void
AD::print_files ()
{

	p(0, "-------------");

	p(0, "%2s  %10s %20s  %10s %10s", "", "id", "", "obj", "shm");
	int total = 0;
	filesource_shared* file = 0;
	while((file = song.osong->filesources.next(file))){
		p(0, "%2i: %10Lu %20s %p %p", file->shm_idx, file->id, file->name, file->object, file);
		total++;
		if(total > 512){ err("iterator killed\n"); break; }
	}
	p(0, "  total=%i lastblock=%i", total, song.shm->filesources.last);
}


//do we still need this?
ARDOUR::IO*
AD::get_io (unsigned shm_index)
{
	block* block = song.shm->routes.block[0];
	route_shared* shared = (struct _route_shared*)block->slot[shm_index];
	if (!shared) { dbg(0, "failed to get AyyidRoute. trk_idx=%u", shm_index); return NULL; }
	ARDOUR::IO* io = (ARDOUR::IO*)shared->object;
	if (!io) { dbg(0, "failed to get io object. trk_idx=%u", shm_index); return NULL; }
	if(debug) dbg(0, "trk_num=%u track=%s", shm_index, shared->name);
	return io;
}


ARDOUR::Route*
AD::get_route (unsigned slot)
{
	route_shared* shared = song.osong->routes.get_item(slot);

	{
		if(slot > (BLOCK_SIZE * BLOCK_SIZE)) return (ARDOUR::Route*)0;

		block* block = song.shm->routes.block[slot / BLOCK_SIZE];
		int slot_ = slot % BLOCK_SIZE;

		//wtf?
		if(slot_ == block->last + 1){ warn("master\n"); return NULL/*dynamic_cast<Route*>((session->master_out().get()))*/; }
	}

	if (!shared) { err("failed to get AyyidRoute. route_num=%u\n", slot); return NULL; }
	ARDOUR::Route* route = (ARDOUR::Route*)shared->object;
	if (!route) { dbg(0, "failed to get route object. route_num=%u", slot); return NULL; }

	dbg(2, "route_num=%u name=%s", slot, shared->name);

	return route;
}


ARDOUR::MidiTrack*
AD::get_midi_track (unsigned shm_index)
{
	midi_track_shared* track = song.osong->midi_tracks.get_item(shm_index);
	if (!track) { dbg(0, "failed to get shared_track. trk_idx=%u", shm_index); return NULL; }
	dbg(1, "track=%p", track->object);
	return (ARDOUR::MidiTrack*)track->object;
}


void
temp_ptr_deleter (void* p)
{
	// fn does nothing. we don't want the object deleted.
}


ARDOUR::Bundle*
AD::get_connection (unsigned pod_index)
{
	//deprecated! use shared_ptr version below.

	connection_shared* shared = song.osong->connections.get_item(pod_index);
	if (!shared) { dbg(0, "failed to get shared_connection. connection_num=%u", pod_index); return NULL; }
	ARDOUR::Bundle* connection = (ARDOUR::Bundle*)shared->object;
	if (!connection) { dbg(0, "failed to get object. connection_num=%u", pod_index); return NULL; }
	dbg(1, "bus_num=%u output=%s", pod_index, shared->name);
	return connection;
}


boost::shared_ptr<ARDOUR::Bundle>*
AD::get_connection_boost (unsigned pod_index)
{

	//TODO store shared_ptr in shm.

	//boost::shared_ptr<ARDOUR::Bundle> b;

	connection_shared* shared = song.osong->connections.get_item(pod_index);
	if (!shared) { dbg(0, "failed to get shared_connection. connection_num=%u", pod_index); return (boost::shared_ptr<ARDOUR::Bundle>*)0; }
	boost::shared_ptr<ARDOUR::Bundle>* connection = (boost::shared_ptr<ARDOUR::Bundle>*)shared->object;
	if (!connection->get()) { dbg(0, "failed to get connection object. connection_num=%u", pod_index); return (boost::shared_ptr<ARDOUR::Bundle>*)0; }
	dbg(0, "bus_num=%u output=%s", pod_index, shared->name);
	dbg(0, "p=%p", connection->get());

	return connection;
}


boost::shared_ptr<ARDOUR::Track>
AD::get_track_boost (unsigned slot, Ayi::ObjType type)
{
	route_shared* pod = (type == AYYI_OBJECT_AUDIO_TRACK || type == AYYI_OBJECT_TRACK)
		? song.osong->routes.get_item(slot)
		: (route_shared*)song.osong->midi_tracks.get_item(slot);

	if (!pod) { dbg(0, "failed to get AyyidRoute. route_num=%u", slot); return boost::shared_ptr<Track>(); }
	if (!pod->object) { dbg(0, "failed to get route object. route_num=%u", slot); return boost::shared_ptr<Track>(); }
	uint64_t id = pod->id;
	if(!id){ dbg(0, "failed to get track id. route_num=%u", slot); return boost::shared_ptr<Track>(); }

	boost::shared_ptr<ARDOUR::RouteList> routelist = session->get_routes();
	for(ARDOUR::RouteList::iterator i = routelist->begin(); i != routelist->end(); ++i){
		PBD::ID test_id = (*i)->id();
		//dbg(0, "   testing: route_num=%u id=%Lu test_id=%Lu %s", pod_index, id, test_id.get_id(), (*i)->name().c_str());
		if(test_id.get_id() == id){
			if(debug_route) dbg(0, "route_num=%u name=%s", slot, pod->name);
			return boost::dynamic_pointer_cast<Track> ((*i));
		}
	}

	dbg(0, "failed to get route shared pointer. route_num=%u id=%Lu", slot, id);

	return boost::shared_ptr<Track>();
}


boost::shared_ptr<AudioTrack>
AD::get_audio_track (ObjIdx pod_index)
{
	boost::shared_ptr<AudioTrack> r;

	route_shared* shared = song.osong->routes.get_item(pod_index);
	if (!shared) { dbg(0, "failed to get AyyidRoute. route_num=%u", pod_index); return r; }
	if (!shared->object) { dbg(0, "failed to get route object. route_num=%u", pod_index); return r; }
	uint64_t id = shared->id;
	if(!id){ dbg(0, "failed to get track id. route_num=%u", pod_index); return r; }

	//typedef std::list<boost::shared_ptr<Route> >      RouteList;
	boost::shared_ptr<ARDOUR::RouteList> routelist = session->get_routes();
	for(ARDOUR::RouteList::iterator i = routelist->begin(); i != routelist->end(); ++i){
		PBD::ID test_id = (*i)->id();
		//dbg(0, "   testing: route_num=%u id=%Lu test_id=%Lu %s", pod_index, id, test_id.get_id(), (*i)->name().c_str());
		if(test_id.get_id() == id){
			if(debug_route) dbg(0, "route_num=%u name=%s", pod_index, shared->name);
			return boost::dynamic_pointer_cast<AudioTrack> ((*i));
		}
	}
	dbg(0, "failed to get route shared pointer. route_num=%u id=%Lu", pod_index, id);
	return r;
}


boost::shared_ptr<MidiTrack>
AD::get_midi_track_boost (unsigned pod_index)
{
	boost::shared_ptr<MidiTrack> r;

	midi_track_shared* shared = (midi_track_shared*)song.osong->midi_tracks.get_item(pod_index);
	if (!shared) { warn("failed to get AyyidRoute. route_num=%u\n", pod_index); return r; }
	if (!shared->object) { warn("failed to get route object. route_num=%u\n", pod_index); return r; }
	uint64_t id = shared->id;
	if(!id){ warn("failed to get track id. route_num=%u\n", pod_index); return r; }

	boost::shared_ptr<ARDOUR::RouteList> routelist = session->get_routes();
	for(ARDOUR::RouteList::iterator i = routelist->begin(); i != routelist->end(); ++i){
		//dbg(0, "  route=%s", (*i)->name().c_str());
		PBD::ID test_id = (*i)->id();
		if(test_id.get_id() == id){
			dbg(0, "found route_num=%u name=%s", pod_index, shared->name);
			return boost::dynamic_pointer_cast<MidiTrack> ((*i));
		}
	}

	warn("failed to get ARDOUR::MidiTrack. route_num=%u\n", pod_index);

	return r;
}


boost::shared_ptr<ARDOUR::Source>
AD::source_find_by_region_idx (unsigned pod_index)
{
	boost::shared_ptr<Source> ret;

	_region_shared* region = song.osong->regions.get_item(pod_index);
	if(!region){ err ("failed to get default region. i=%i\n", pod_index); return ret; }
	uint64_t source_id = region->source0;
	char s[256]; sprintf(s, "%" PRIu64, source_id);
	string id_string = s;
	const PBD::ID* id = new PBD::ID(id_string);
	ret = session->source_by_id(*id);

	return ret;
}


std::string
AD::get_history ()
{
	// Return a multiline string for now - maybe change to something more advanced as needed.

	std::string h;
	UndoHistory& history = session->history();
	std::list<UndoTransaction*> undolist = history.UndoList;
	if(!undolist.size()) dbg(2, "undolist is empty");
	for(std::list<UndoTransaction*>::iterator i = undolist.begin(); i != undolist.end(); i++){
		//dbg(0, "undoitem=%s", (*i)->name().c_str());
		h = h + (*i)->name() + '\n';
	}

	return h;
}


bool
AD::file_import (string path)
{
	// Start a new thread which imports the named file.
	// See gtk2_ardour/editor_ops.cc or editor_audio_import.cc

	dbg(0, "'%s'", path.c_str());

	bool ok = true;

	import_status.paths.push_back(path);
	import_status.done      = false;
	import_status.cancel    = false;
	import_status.freeze    = false;
	import_status.done      = 0.0;
	import_status.quality   = SrcBest;
	import_status.replace_existing_source = false; //import will fail if we set this to True.

	interthread_progress_connection = Glib::signal_timeout().connect(sigc::mem_fun(*this, &AD::import_progress_timeout), 100);

	pthread_create_and_store("import", &import_status.thread, _import_thread, this);
	pthread_detach(import_status.thread);

	while (!(import_status.done || import_status.cancel)) {
		usleep(100000);
		import_progress_timeout();
	}
	dbg(1, "import done");

	import_status.done = true;
	if (import_status.cancel) ok = false;

	// find our path in the list, and remove it.
	// -assume that the paths list has changed.
	bool found = false; 
	for(std::vector<std::string>::iterator i = import_status.paths.begin(); i != import_status.paths.end(); i++){
		if(*(i) == path){
			found = true;
			import_status.paths.erase (i);
			break;
		}
	}
	if(!found) err("unable to remove path from pathlist. Not found.\n");

	interthread_progress_connection.disconnect();

#if 0
	dbg(0, "sources:");
	ARDOUR::Session::SourceMap sources = session->get_sources();
	for (Session::SourceMap::const_iterator i = sources.begin(); i != sources.end(); ++i){
		dbg(0, "  %s", i->second->path().c_str());
	}
	dbg(0, "-----------------------------------");
#endif

	return ok;
}


void*
AD::_import_thread (void* arg)
{
	SessionEvent::create_per_thread_pool ("import events", 64);

	return core->import_thread();
}


void*
AD::import_thread ()
{
	dbg(0, "...");

	session->import_files(import_status);
	if(import_status.cancel) dbg(0, "import cancelled");

	return 0;
}


int
AD::import_progress_timeout ()
{
	dbg(1, "...");
                                                                                                                                                                                                                                 
	if (import_status.doing_what == "building peak files") {
		return FALSE;
	} else {
		dbg(1, "%.3f", import_status.progress * 100.0);
		ad_dbus_emit_progress_bar(core->dbus, import_status.progress, NULL);
	}
                                                                                                                                                                                                                                 
	return !(import_status.done || import_status.cancel);
}


void
AD::set_tempo (double bpm)
{
	dbg(0, "%.2f", bpm);

	ARDOUR::TempoMap& tempo_map = session->tempo_map();
	//tempo_map.clear(); //fn doesnt exist?!

	ARDOUR::Tempo*      tempo = new Tempo(bpm);
	Timecode::BBT_Time* where = new Timecode::BBT_Time;

	tempo_map.add_tempo(*tempo, *where);

	//FIXME reposition all regions!!

	song.shm->bpm = bpm;
}


bool
AD::set_locator (int locator_num, uint32_t pos)
{
	dbg(1, "loc_num=%i pos=%u", locator_num, pos);

	//update the engine:
	Location* loop = session->locations()->auto_loop_location();
	switch(locator_num){
		case 1:
			loop->set_start(pos);
			session->set_auto_loop_location(loop); // is this needed? it sends out a signal...
			break;
		case 2:
			loop->set_end(pos);
			session->set_auto_loop_location(loop); // is this needed? it sends out a signal...
			break;
		case 9:
			break;
		default:
			return set_error_("only locators 1,2,9 are supported.");
	}

	//check:
	/*
	loop = session->locations()->auto_loop_location();
	//dbg(0, "loop end is now: %u", loop->end());
	*/

	//share:
	song.samples2pos(&song.shm->locators[locator_num], pos);
	ad_dbus_emit_locators(dbus, NULL);

	return true;
}


void
AD::set_cycle_onoff (bool on_off)
{
	//note: cycling is also a diskstream property.

	dbg(0, " on_off=%i", on_off);

	//libardour does not have a method to turn looping on/off without going into PLAY.
	//Code below is taken from session->set_play_loop(on_off).
	//See session_transport.cc
	//FIXME what does merge_event() do?
	//FIXME "Called from event-handling context"
	{
		/* Called from event-handling context */
		
		if ((session->actively_recording() && on_off) || session->locations()->auto_loop_location() == 0) {
			dbg(0, " **** cant loop!");
			return;
		}
		
		session->set_dirty();

																			#if 0 // FIXME broken by upstream change
		if (on_off && Config->get_seamless_loop() && session->synced_to_jack()) {
			warn("Seamless looping cannot be supported while Ardour is using JACK transport. Recommend changing the configured options");
			return;
		}
																			#endif

		if ((session->play_loop = on_off)) {
			dbg(2, "play_loop!");

			Location *loc;
			if ((loc = session->_locations->auto_loop_location()) != 0) {

				//libardour change: diskstreams are now only acessible via their Route's ?

				if (1/*Config->get_seamless_loop()*/) {
					dbg(1, "seamless_loop!");
					// set all diskstreams to use internal looping
					boost::shared_ptr<RouteList> rl = session->routes.reader ();
					for (RouteList::iterator i = rl->begin(); i != rl->end(); ++i) {
						boost::shared_ptr<Track> tr = boost::dynamic_pointer_cast<Track> (*i);
						if (tr) {
							//if (!(*i)->hidden()) {
								tr->set_loop (loc);
							//}
						}
					}
				}
				else {
					dbg(1, "no seamless_looping");
					// set all diskstreams to NOT use internal looping
					boost::shared_ptr<RouteList> rl = session->routes.reader ();
					for (RouteList::iterator i = rl->begin(); i != rl->end(); ++i) {
						boost::shared_ptr<Track> tr = boost::dynamic_pointer_cast<Track> (*i);
						if (tr) {
						//if (!(*i)->hidden()) {
							tr->set_loop (0);
						}
					}
				}
				
				/* stick in the loop event */
				
				SessionEvent* event = new SessionEvent (SessionEvent::AutoLoop, SessionEvent::Replace, loc->end(), loc->start(), 0.0f);
				session->merge_event (event);

				/* locate to start of loop and roll if current pos is outside of the loop range */
				if (session->_transport_frame < loc->start() || session->_transport_frame > loc->end()) {
					                                                                                                                 // FIXME
					event = new SessionEvent (SessionEvent::LocateRoll, SessionEvent::Add, SessionEvent::Immediate, loc->start(), 0, true/*!session->synced_to_jack()*/);
					session->merge_event (event);
				}
				else {
					// locate to current position (+ 1 to force reload)
					                                                                                                                                  // FIXME
					event = new SessionEvent (SessionEvent::LocateRoll, SessionEvent::Add, SessionEvent::Immediate, session->_transport_frame + 1, 0, true/*!session->synced_to_jack()*/);
					session->merge_event (event);
				}
			}

		} else {
			dbg(0, "clearing event...");
			session->clear_events (SessionEvent::AutoLoop);

			// set all diskstreams to NOT use internal looping
			boost::shared_ptr<RouteList> rl = session->routes.reader ();
			for (RouteList::iterator i = rl->begin(); i != rl->end(); ++i) {
				boost::shared_ptr<Track> tr = boost::dynamic_pointer_cast<Track> (*i);
				if (tr) {
				//if (!(*i)->hidden()) {
					tr->set_loop (0);
				}
			}
			
		}
		session->play_loop  = on_off;
	}
	song.shm->play_loop = on_off;

	dbg(0, "play_loop=%i", session->play_loop);
}


bool
AD::play ()
{
	dbg(0, "...");

	if (session->play_loop) {
		dbg(0, "requesting play_loop...");
		session->request_play_loop (true); //what does this arg do? not clear! something to do with seamless looping
	} else {
		// ??? cannot locate while in record mode !!! are we using the wrong rec ready mode?

		//session->request_locate(0, false); //testing! attempt to refresh diskstreams.
		#define TEST_OFFSET 0
		session->request_locate(session->transport_frame() - TEST_OFFSET, true);
	}

	return true;
}


bool
AD::stop ()
{
	session->request_stop();

	dbg(0, "play_loop=%i frame=%u", session->play_loop, session->transport_frame());

	return true;
}


bool
AD::rew ()
{
	dbg(0, "...");

	session->request_transport_speed (session->_transport_speed -1.0f);

	return true;
}


bool
AD::ff(int val)
{
	dbg(0, "...");

	float speed = session->_transport_speed == 0.0 ? 2.0f : session->_transport_speed + 1.0f;
	session->request_transport_speed (speed);

	return true;
}


bool
AD::rec (int val)
{
	dbg(0, "...");

	if(val) session->enable_record();
	else    session->disable_record(false, false);

	return true;
}


unsigned
AD::pos2samples (SongPos *pos)
{
	float bpm = shm_index->bpm;

	float length_of_1_beat_in_secs = 60.0/bpm;
	float samples = length_of_1_beat_in_secs * shm_index->sample_rate * (
		pos->beat + pos->sub/3840.0 +
		pos->mu/(3840.0*11025.0)
	);

	return (uint32_t)(samples + 0.5);
}


uint32_t
AD::mu2samples (long long mu)
{
	return beats2samples_float(((double)mu) / (3840.0 * 11025.0));
}


uint32_t
AD::beats2samples_float (float beats)
{
  //returns the number of audio samples equivalent to the given number of beats.

  float bpm = shm_index->bpm;

  float length_of_1_beat_in_secs = 60.0/bpm;
  float samples = length_of_1_beat_in_secs * shm_index->sample_rate * beats;

  return (uint32_t)(samples + 0.5);
}


uint32_t
AD::posbuffer2samples (char* buffer)
{
	Ayi::SongPos pos;
	memcpy(&pos, buffer, 8);
	return Ayi::core->pos2samples(&pos);
}


bool
AD::del_playlist (int pod_idx)
{
	//AyiPlaylist* playlist = song.osong->playlists.get_item(pod_idx);
	playlist_shared* playlist = song.osong->playlists.get_item(pod_idx);
	ARDOUR::Playlist* _pl = (ARDOUR::Playlist*)playlist->object;
	if(_pl){
		boost::shared_ptr<ARDOUR::Playlist> pl = _pl->shared_from_this();

		dbg(0, "removing playlist...");
		session->remove_playlist (boost::weak_ptr<ARDOUR::Playlist>(pl));
	}
	return true;
}


#define get_track_or_fail(A) ARDOUR::AudioTrack* track; {track = AyyidRoute::get_track(A); if(!track) return set_error_("no track for %i", A); }
#define get_boost_track_or_fail_(A, MEDIA_TYPE) boost::shared_ptr<ARDOUR::Track> track; { \
	track = (MEDIA_TYPE == AYYI_AUDIO) \
		? boost::dynamic_pointer_cast<Track>(core->get_audio_track(A)) \
		: boost::dynamic_pointer_cast<Track>(core->get_midi_track_boost(A)); \
	if(!track) return set_error_("track not found: %i %i", A, MEDIA_TYPE); \
}


bool
AD::set_input (ObjIdx tnum, unsigned connection_idx)
{
	get_track_or_fail(tnum);

	dbg(0, "connection_idx=%i", connection_idx);
	ARDOUR::Bundle* src = get_connection(connection_idx);
	if(!src) return set_error_("failed to get bundle. connection_idx=%i", connection_idx);
	boost::shared_ptr<ARDOUR::Bundle> bundle(src, temp_ptr_deleter);
	if(track->input()->bundle()->nchannels() == bundle->nchannels()){
		if(!track->input()->connect_ports_to_bundle(bundle, true, this)){
			dbg(0, "connecting done ok."); 
			return true;
		}
	}
	else return set_error_("cannot connect. source has wrong channel count for track input: %i --> %i", bundle->nchannels(), track->input()->bundle()->nchannels());

	return set_error_("failed to connect!");
}


bool
AD::set_output (ObjIdx tnum, unsigned connection_idx)
{
	// this replaces the current output with the new one.
	// ideally we need to the option to _add_ to the existing output.

	get_track_or_fail(tnum);

	if(!connection_idx){
		dbg(0, "setting output to OFF...");
		track->output()->disconnect(NULL);
		return true;
	}

	ARDOUR::Bundle* dest = get_connection(connection_idx);
	boost::shared_ptr<ARDOUR::Bundle> dest_bundle(dest, temp_ptr_deleter);
	if(!dest){
		dbg(0, "track has no bundle. connecting jack ports directly...");
		dbg(0, "warning: mono only. TODO stereo");
		connection_shared* shared = song.osong->connections.get_item(connection_idx);
		if (!shared) return set_error_("failed to get shared_connection. connection_num=%u", connection_idx);
		dbg(0, "port=%s device=%s", shared->name, shared->device);

		track->output()->disconnect(NULL);

		/*
		char name[64];
		strcpy(name, shared->name);
		char* end = g_strstr_len(name, 64, "{");
		if(end) *end = '\0';
		else dbg(0, "{ not found in: %s", name);
		dbg(0, "name=%s", name);
		string port_name = name;
		port_name += "/";

		char* b = end + 1;
		dbg(0, "b=%s", b);
		end = g_strstr_len(name, 64, "/");
		char* end2 = g_strstr_len(b, 64, "}");
		if(end2){
			char c[64];
			dbg(0, "len=%i", end2 - end);
			strncpy(c, b, end2 - end);
			port_name += c;
		}
		*/

		std::list<string> l = AyyiConnection::get_port_names(shared);
		for(std::list<string>::iterator i = l.begin(); i != l.end(); ++i){
			string port_name = (*i);
			dbg(0, "   port_name=%s", (*i).c_str());
			if(!strcmp(shared->device, "system")) port_name = "system:" + (*i);

			if(track->output()->connect(track->output()->nth(0), port_name, NULL)){
				return set_error_("error occurred connecting jack port. connection: %i: %s", connection_idx, shared->name);
			}
		}
		return true;
	}

	dbg(0, "output=%s (%u)", song.osong->connections.get_item(connection_idx)->name, connection_idx);

	//check that the requested connection is an input
	if(!dest->ports_are_inputs()) return set_error_("cannot connect output to output");

	//how many channels does the track have?
	// we can either use track->n_outputs().n_total(), or track->output()->bundle()->nchannels(). They seem to give the same result.
	//dbg(0, "bundle n_chans: %i (track has %i channels)", track->output()->bundle()->nchannels(), track->n_outputs().n_total());
	unsigned n_outputs = track->n_outputs().n_total();

	dbg(0, "n_outputs: %i %i dest=%i", track->n_outputs().n_total(), track->output()->bundle()->nchannels().n_total(), dest_bundle->nchannels().n_total());
	//libardour will quit if channel count doesnt match
	if(n_outputs != dest_bundle->nchannels().n_total()){

		if(n_outputs == dest_bundle->nchannels().n_total() - 1){
			dbg(0, "attempting to add port...");
			if(track->output()->add_port("", NULL, DataType::AUDIO) < 0) return set_error_("add_port() failed");
			unsigned new_trackwidth = track->n_outputs().n_total();
			dbg(0, "new_trackwidth=%i %i", new_trackwidth, track->output()->bundle()->nchannels().n_total());
			if(new_trackwidth != n_outputs + 1) return set_error_("add port failed. output_width=%i", new_trackwidth);
		}

		else return set_error_("cannot connect output. destination channel count mismatch (%i-->%i)", n_outputs, dest_bundle->nchannels().n_total());
	}

	//check we are not already connected to the requested ports:
	std::vector<boost::shared_ptr<Bundle> > connected_to = track->output()->bundles_connected();
	//dbg(0, "connected_to.size()=%i", connected_to.size());
	for (std::vector<boost::shared_ptr<Bundle> >::iterator i = connected_to.begin(); i != connected_to.end(); i++) {
		dbg(1, "  connectedto: %s", (*i)->name().c_str());
		if ((*i).get() == dest_bundle.get()){
			return set_error_("already connected - nothing to do..");
		}
	}

	if(connected_to.size()){
		boost::shared_ptr<BundleList> bundles = session->bundles();
		for(BundleList::iterator x = bundles->begin(); x != bundles->end(); x++){
			std::string name1 = (*x)->name();
			std::string name2 = connected_to[0]->name(); //0, cough!
			if(name1 == name2){
				//session->bundles()->erase(x); //testing
				break;
			}
		}

		dbg(1, "disconnecting existing connections...");
		track->output()->disconnect(NULL);
		dbg(1, "existing connections disconnected.");
	}
	else dbg(0, "not disconnecting.");

	//-it calls _session.engine().connect()
	//if(io->use_output_connection(*connection, NULL)) err("****\n");

	//io->connect_output (Port* our_port, string other_port, void* src); //what does this do exactly? calls _session.engine().connect(), just like io->use_output_connection does...
	//printf("%s(): doing outputs...\n", __func__);
	//for(uint32_t x = 0; x < io->n_outputs().n_audio(); ++x){
	//}

	dbg(0, "connecting...");
	dbg(0, "dest=%s", dest_bundle->name().c_str());
	try {
		if(!track->output()->connect_ports_to_bundle(dest_bundle, false, NULL)){
			dbg(0, "connecting done."); 
		}
	}
	catch (.../*ARDOUR::AudioEngine::PortRegistrationFailure& err*/) {
		return set_error_("could not register new ports required for bundle");
	}
	return true;
}


void
AD::automation_on_list_added (ARDOUR::AutomationList* al)
{
	if (al->size()) dbg(2, "listsize=%i", al->size());
}


void
AD::auto_lists_print (ARDOUR::Route* route)
{
}


bool
AD::auto_del_point (guint route_idx, guint auto_type, guint pt_idx)
{
	dbg(0, "...");

	ARDOUR::Route* route = get_route(route_idx);
	if(route){
		switch(auto_type){
			case VOL:
				{
					boost::shared_ptr<ARDOUR::AutomationList> list = route->gain_control()->alist();
					guint len = list->size();
					Evoral::ControlList::iterator i = list->begin();
					bool found = false;
					while(i != list->end()){
						if(std::distance(list->begin(), i) == pt_idx){
							list->erase(i);
							found = true;
							break;
						}
						i++;
					}
					if(!found) return set_error_("point not found: %i len=%i", pt_idx, list->size());

					if(list->size() == len - 1){
						return true;
					}else{
						return set_error_("no points removed: len=%i", list->size());
					}
				}
				break;
			case PAN:
				if(route->panner()->out().n_total()){
					boost::shared_ptr<ARDOUR::Panner> panner = route->panner();
					boost::shared_ptr<ARDOUR::AutomationList> list = panner->pannable()->pan_azimuth_control->alist();

					list->erase_range (pt_idx, pt_idx);

					return true;
				}
				break;
			default:
				return set_error_("unknown automation type: %u", auto_type);
				break;
		}
	}
	return FALSE;
}


bool
AD::auto_set_point (guint route_idx, guint control_idx, guint pt_num, double when, double val)
{
	//update an automation curve point with new values.

	if(val < 0.0 || val > 10.0){ warn ("y out of range: %.2f\n", val); return false; }

	dbg(0, "x=%.2f y=%.2f", when, val);

	boost::shared_ptr<ARDOUR::AutomationList> list;

	ARDOUR::Route* route = get_route(route_idx);
	if(route){
		switch(control_idx){
			case VOL:
				{
					list = route->gain_control()->alist();
				}
				break;
			case PAN:
				if(route->panner()->out().n_total()){
					boost::shared_ptr<ARDOUR::Panner> panner = route->panner();
					list = panner->pannable()->pan_azimuth_control->alist();
				}
				break;
			default:
				int plugin_slot = 0; //FIXME
				auto_lists_print(route);
				boost::shared_ptr<Processor> p = route->nth_processor(plugin_slot);
				if(p){
					dbg(0, "have processor... control_idx=%i  - FIXME broken following libardour api change.", control_idx);
#if 0
					list = processor_get_automationlist(p, control_idx);
#endif
					if(!list) dbg(0, "*** automationlist not found. control_idx=%i", control_idx);
				}
				else dbg(0, "cannot find processor for control_idx=%i", control_idx);
				break;
		}

		if (list){
			dbg(0, "list.length=%i", list->size());
			uint32_t n = 0;
			for(std::list<Evoral::ControlEvent*>::iterator i=list->begin(); i!=list->end(); i++){
				dbg(0, "  n=%i pt_num=%i", n, pt_num);
				if(n == pt_num){
					list->modify (i, when, val);
					break;
				}
				n++;
			}

			route->ayyi_track->set_plugin_automation();

			dbg(0, "TODO connect to signals.");
			return TRUE;
		}
	}
	return FALSE;
}


void
AD::tempo_map_changed (const PBD::PropertyChange&)
{
	dbg(0, "...");
	ARDOUR::TempoMap& tempo_map = session->tempo_map();
	const ARDOUR::Tempo& tempo = tempo_map.tempo_at(0);
	song.shm->bpm = tempo.beats_per_minute();

	ad_dbus_emit_property(core->dbus, AYYI_OBJECT_SONG, 0, AYYI_TEMPO, NULL);
}


void
AD::location_changed (ARDOUR::Location* location)
{
	// note that there is also a Session::EndTimeChanged signal

	dbg(0, "name=%s is_session_range=%i", location->name().c_str(), location->is_session_range());
#if 0
					dbg(0, "------------------- is_session_range=%i is_auto_loop=%i", location->is_session_range(), location->is_auto_loop());
					dbg(0, "------------------- location=%Li-->%Li", location->start(), location->end());
					dbg(0, "------------------- session=%Li->%Li", session->current_start_frame(), session->current_end_frame());

					Locations::LocationList loc = session->locations()->list();
					dbg(0, "------------------- n=%i", loc.size());
					for(Locations::LocationList::iterator i = loc.begin (); i != loc.end(); i++){
						dbg(0, "        name=%s", (*i)->name().c_str());
					}

					Location* range = session->locations()->session_range_location();
					if(range){
						dbg(0, "have session_range");
					}else{
						dbg(0, "NO session_range");
					}
#endif

	song.samples2pos(&song.shm->start, session->current_start_frame());
	song.samples2pos(&song.shm->end, session->current_end_frame());
}


void
ad_debug (const char* func, int type, int level, const char *format, ...)
{
	va_list args;

	va_start(args, format);
	if (level <= debug) {
		fprintf(stderr, "%s(): ", func);
		vfprintf(stderr, format, args);
		fprintf(stderr, "\n");
	}
	va_end(args);
}


#ifdef VST_SUPPORT
void
fst_show_menu (FST* fst)
{
	if (!fst) return;

	PluginUIWindow* window = core->find_plugin_window(fst);
	if (window) {
		PlugUIBase& ui = window->pluginui();
		Gtk::Menu* menu = ui.menu;
		if (menu) {
			dbg(0, "showing menu...");
			menu->popup(0, gtk_get_current_event_time());
		}
		else dbg(0, "no menu");
	}
	else warn("failed to get window.");
}
#endif

} // end namespace Ayi
