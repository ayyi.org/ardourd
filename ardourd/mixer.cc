/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2006-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#include <cstdlib>
#include <glibmm.h>

#include <ayyi/ayyi_time.h>
#include <ayyi/utils.h>
#include <ayyi/ayyi_types.h>
#include <pbd/pthread_utils.h>
#include <ardour/panner.h>
#include "ardour/pannable.h"
#include "ardour/panner_shell.h"
#include "ardour/audio_track.h"
#include "ardour/midi_track.h"
#include "ardour/send.h"
#include "ardour/amp.h"
#include "ardour/meter.h"
#include "ayyi-ardour/connection.h"
#include "ayyi-ardour/mixer_track.h"
#include "ardourd/app.h"
#include "ardourd/mixer.h"

extern int debug;

#define dbg(N, A, ...) { if (N <= debug) dbgprintf("ADMixer", __func__, (char*)A, ##__VA_ARGS__); }
#define warn(A, ...) warnprintf2("Mixer", __func__, (char*)A, ##__VA_ARGS__)
#define err(A, ...) errprintf2("ADMixer", __func__, (char*)A, ##__VA_ARGS__)
#define set_error(A, ...) core->set_error(__func__, A, ##__VA_ARGS__)

#define get_track_or_fail(A) ARDOUR::AudioTrack* track; {track = AyyidRoute::get_track(A); if(!track) return set_error("no track for %i", A); }
#define get_boost_track_or_fail_(A, MEDIA_TYPE) boost::shared_ptr<ARDOUR::Track> track; { \
	track = (MEDIA_TYPE == AYYI_AUDIO) \
		? boost::dynamic_pointer_cast<Track>(core->get_audio_track(A)) \
		: boost::dynamic_pointer_cast<Track>(core->get_midi_track_boost(A)); \
	if(!track) return set_error("track not found: %i %i", A, MEDIA_TYPE); \
}

#include "ayyi/container-impl.cc"
namespace Ayi {

using namespace ARDOUR;
extern ARDOUR::Session* session;
extern AD* core;


ADMixer::ADMixer ()
	: omixer(new MixerShm),
	  tracks(omixer),
	  shm(0)
{
	dbg(0, "...");
}


void
ADMixer::shm_init ()
{
	dbg(0, "...");

#ifdef USE_DBUS
	ShmSegDefn* seg = (ShmSegDefn*)omixer;
	shm = omixer->get_addr();
#else
	ShmSegDefn* seg = shm_segment_find_by_name("amixer");
	this->mixer.shm = (struct _shm_seg_mixer*)seg->get_addr;
#endif
	if(!shm){ err("failed to get segment address. Not attached?\n"); return; }

	dbg(0, "mixer.shm=%p", shm);

	// fill segment header
	strcpy(shm->service_name, "Ayyi mixer");
	strcpy(shm->service_type, "Ayyi mixer");
	shm->owner_shm = shm;
	shm->num_pages = seg->size;
	dbg(3, "headersize=%i segsize=%i percentage used: %i%%", sizeof(*shm), seg->size*SHM_BLOCK_SIZE, (sizeof(*shm)*100)/(seg->size*SHM_BLOCK_SIZE));

	AyyidRoute::shm.mixer = (gnash::Shm*)omixer;
	MixerTrack<ARDOUR::Route>::tracks = &tracks;

#if 0
#ifdef USE_DBUS
	gnash::MixerShm* seg2 = mixer.omixer;
	mixer.shm = mixer.omixer->get_addr();
#else
	shm_seg_defn* seg = shm_segment_find_by_name("amixer");
	this->mixer.shm = (struct _shm_seg_mixer*)seg->get_addr;
#endif
	if(!mixer.shm){ err("failed to get segment address. Not attached?\n"); return; }
#endif

	#define TLSF_MIXER_POOL_SIZE 64 * 1024 //first 3k is used for book-keeping.
	shm->tlsf_pool = seg->malloc(TLSF_MIXER_POOL_SIZE, NULL);
	int free = 0;
	if((free = init_memory_pool(TLSF_MIXER_POOL_SIZE, shm->tlsf_pool))){
		dbg(1, "mixer_tlsf_free=%ikB", free / 1024);
	}
	else { err ("tlsf init failed.\n"); throw string("tlsf init failed."); }

	tracks.init ("Ayyi mix tracks");
	omixer->plugins.init ("Ayyi plugins");
	dbg(2, "pluginblock=%p", &shm->plugins);
}


bool
ADMixer::set_level (int ch_num, float level)
{
	dbg(0, "ch=%i level=%.3f", ch_num, level);

	get_track_or_fail(ch_num);

	track->set_gain(level, PBD::Controllable::GroupControlDisposition::NoGroup);

	return true;
}


bool
ADMixer::set_pan (ObjIdx channel, float pan_val)
{
	// ayyi pan_val range is -1.0 to +1.0
	// ardour pan range is 0.0 to 1.0

	// ardour stores "pan" values as a series of gains, one for each output.

	float ardour_pan = MixerTrack<ARDOUR::Route>::pan_map_in(pan_val);

	dbg(0, "ch=%i mapping: %.2f --> %.2f", channel, pan_val, ardour_pan);

	if(pan_val > 1.0) warn("pan_val too high!\n");

	ARDOUR::Route* route = core->get_route(channel);
	if(route){
		int n_in = route->n_inputs().n_total();
		int n_out = route->n_outputs().n_total();
		if(n_in == 2 && n_out == 2) return set_error("cannot pan stereo tracks");

		boost::shared_ptr<ARDOUR::Panner> panner = route->panner();
		int size = panner->out().n_total();
		if(size){
			if(panner){
				dbg(0, "current: panner size=%i bypassed=%i xpos=%.2f width=%.2f", size, route->panner_shell()->bypassed(), panner->position(), panner->pannable()->pan_width_control->get_value());
				dbg(0, "%s", panner->value_as_string(panner->pannable()->pan_azimuth_control).c_str());

//panner->pannable()->pan_width_control->set_value(0.0);
				panner->set_position(ardour_pan);

				float xpos = panner->position();
				double pos = panner->pannable()->pan_azimuth_control->get_value();
				dbg(0, "after: xpos=%.2f pannable_pos=%.2f", xpos, pos);
				if(ABS(xpos - ardour_pan) > 0.001) return set_error("failed: %.2f != %.2f", xpos, ardour_pan);

				/*
				if (panner->pan_control(0)) {
					panner->pan_control(0)->set_value(ardour_pan);
					if(size > 1){
						if(panner->pan_control(0, 1)){
							dbg(0, "setting rhs: %.2f", (1.0 - ardour_pan));
							panner->pan_control(0, 1)->set_value(1.0 - ardour_pan);
						}
						else warn("size > 1 but no panner(1)!\n");
					}
					dbg(0, "done");
				}
				else warn("panner has no pan_control.\n");
				*/
			} else return set_error("failed to get panner.");
		}
		else return set_error("empty panner. (%s)\n", route->name().c_str());
	}
	return true;
}


bool
ADMixer::set_enable_pan (ObjIdx ch_num, bool enable)
{
	get_track_or_fail(ch_num);

	dbg(0, "pannable=%p panshell=%p panner=%p", track->pannable().get(), track->panner_shell().get(), track->panner().get());

	boost::shared_ptr<ARDOUR::Panner> panner = track->panner();
	if(panner){
		dbg(0, "already have panner");
		return true;
	}else{
		int n_in = track->n_inputs().n_total();
		int n_out = track->n_outputs().n_total();

		dbg(0, "n_in=%i n_out=%i", n_in, n_out);

		if(n_out == 1){
			//there must be 2 outputs for panning
			if(track->output()->add_port("", NULL, DataType::AUDIO) < 0) return set_error("add_port() failed");
			if(track->panner()) return true;
		}
	}

	return set_error("no panner");
}


bool
ADMixer::set_plugin (int ch_num, unsigned plugin_slot, char* plugin_name)
{
	// @arg    plugin_slot.  the Insert number.
	// @arg    plugin_name.  if empty, we remove the plugin for this slot.
	// @return true if successful.

	dbg(0, "ch=%i insert_slot=%i plugin=%s", ch_num, plugin_slot, plugin_name);

	if(plugin_slot >= AYYI_PLUGINS_PER_CHANNEL) return set_error("plugin_slot out of range: %i", plugin_slot);

	AyyiChannel* ac = core->mixer.tracks.get_item(ch_num);
	if(!ac) return set_error("failed to get shared route struct");

	get_track_or_fail(ch_num);

	MixerTrack<ARDOUR::Route>* ayyi_route = track->ayyi_mixer_track;
	if(!ayyi_route) return set_error("failed to get shared route object");

#ifdef VST_SUPPORT
	int p0 = ac->plugin[plugin_slot];
	if(p0){
		//if the plugin_name is the same as the current one, just open the editor
		_ayyi_plugin_nfo* ap = mixer.omixer->plugins.get_item(p0);
		if(!strcmp(plugin_name, ap->name)){
			dbg(0, "opening editor for %s...", plugin_name);
			boost::shared_ptr<Processor> p = track->nth_plugin (plugin_slot);
			boost::shared_ptr<PluginInsert> insert;
			if((insert = boost::dynamic_pointer_cast<PluginInsert>(p))){
				if(!ayyi_route->open_plugin_window(plugin_slot, insert)) return set_error("failed to open plugin window");
			}
			return true;
		}
		else dbg(0, "not current: '%s' '%s'", plugin_name, ap->name);

		dbg(0, "slot in use: removing old plugin...");
		boost::shared_ptr<PluginInsert> ins;
		if(!ayyi_route->set_insert(plugin_slot, ins)) return set_error("failed to clear previous plugin");
	}
	else dbg(0, "slot not in use. nothing to clear.");

	/*
		required steps to instantiate a plugin:
		1- initialise fst.
		2- make a VSTPlugin object.
		3- add PluginInfo to the object: plugin->set_info(info);
		4- add the plugin to an Insert

		5- pluginInfo->load(*session); - this actually loads the dll.
			it calls fst_load() then plugin.reset(new VSTPlugin) !! - so if we do load() we dont have to pre make a Plugin object ??

		gui does: PluginPtr->load(session), then emits a PluginCreated signal ? (this signal is for gui only)

		ui classes:
			-PluginUIWindow (from ArdourDialog) - has PlugUIBase as a property.
			-VSTPluginUI (from PluginUIBase)
			-PlugUIBase

		threads:
			ENSURE_GUI_THREAD ?
			see: gtkmm2ext/gui_thread.h
			there is a singleton Gtkmm2ext::UI
			it creates a new glib mainloop.
			the idea is that gui signals run in this other mainloop
	*/
	if(strlen(plugin_name) && strcmp(plugin_name, "none")){
		try {
			ARDOUR::PluginInfoPtr info = plugin_info_lookup_by_name(plugin_name);
			if(info){
				ARDOUR::PluginPtr plugin = info->load(*core->session);

				//PluginInfoPtr info = plugin->get_info();
				//printf("%s(): pluginInfo: name=%s category=%s\n", __func__, info->name.c_str(), info->category.c_str());

				dbg(0, "n_processor_streams=%i", track->max_processor_streams().n_total());
				if(track->max_processor_streams().n_total() > 0){
					int n_inserts = 0;
					for(guint i=0;i<track->max_processor_streams().n_audio();i++){
						boost::shared_ptr<Processor> p = track->nth_processor (i);
						PluginInsert* ins = dynamic_cast<PluginInsert*>(p.get());
						if(ins) n_inserts++;
						dbg(0, "  name%i=%s", i, p->name().c_str());
					}
					if(n_inserts) warn("track already has inserts! turn off old plugin first? n_processors=%i n_inserts=%i (check which slot!)", track->max_processor_streams().n_audio(), n_inserts);
				}

				boost::shared_ptr<PluginInsert> insert (new PluginInsert (*core->session, plugin));
				if(!insert) return set_error("failed to create PluginInsert!");

				ayyi_route->set_insert(plugin_slot, insert);
			}
			else return set_error("failed to get PluginInfo! '%s'", plugin_name);
		}
		catch (const PBD::sys::filesystem_error& ex) {
			dbg(0, "[error] filesytem error");
		}
	}
#if 0
	else{
		dbg(0, "plugin removed requested... (path deprecated)");
		boost::shared_ptr<Processor> processor = track->nth_processor(plugin_slot); //no!
		boost::shared_ptr<PluginInsert> insert;
		if(insert = boost::dynamic_pointer_cast<PluginInsert>(processor)){

			//note: plugin windows are destroyed by the plugin destructor.

			track->remove_processor(processor, NULL);
			//insert->deactivate();
			boost::shared_ptr<VSTPlugin> plugin = boost::dynamic_pointer_cast<VSTPlugin>(insert->plugin(plugin_slot));
			//int use_count = plugin.use_count();

			ac->plugin[plugin_slot] = 0;
			ac->insert_active[plugin_slot] = false;
			dbg(0, "plugin removal done. usecount=%li", plugin.use_count());
		}
		else return set_error("processor is not a plugin insert.");
	}
#endif

	return true;
#else
	return set_error("plugins not enabled.");
#endif
}


bool
ADMixer::set_plugin_bypass (int ch_num, unsigned plugin_slot, bool bypass)
{
	get_track_or_fail(ch_num);

	dbg(0, "TESTME");

	boost::shared_ptr<Processor> proc = track->nth_plugin (plugin_slot);
	if(proc){
		if(bypass) proc->deactivate(); else proc->activate();
	}
	else warn("plugin not found for slot %i.\n", plugin_slot);

	return true;
}


boost::shared_ptr<Send>
ADMixer::get_send_for_route (ARDOUR::Route* route, unsigned aux_num)
{
	boost::shared_ptr<Send> send;
	if(aux_num >= AYYI_AUX_PER_CHANNEL) return send;

	Ayi::MixerTrack<ARDOUR::Route>* ayyi_mixer_track = route->ayyi_mixer_track;

	return ayyi_mixer_track->get_send(aux_num);
}


bool
ADMixer::del_aux (ObjIdx channel_idx, unsigned aux_idx)
{
	dbg(0, "ch=%i aux=%i", channel_idx, aux_idx);

	get_boost_track_or_fail_(channel_idx, AYYI_AUDIO);

	MixerTrack<ARDOUR::Route>* ayyi_track = track->ayyi_mixer_track;
	if(!ayyi_track) return set_error("ayyi track not found");

	if(!ayyi_track->track->aux[aux_idx]){
		return set_error("no such aux");
	}

	return ayyi_track->remove_send(aux_idx);
}


bool
ADMixer::set_aux_output (unsigned tnum, unsigned aux_num, unsigned connection_idx)
{
	dbg(0, "...");

	if(aux_num >= AYYI_AUX_PER_CHANNEL) return set_error("bad aux number: %u", aux_num);

	get_boost_track_or_fail_(tnum, AYYI_AUDIO);
	MixerTrack<ARDOUR::Route>* ayyi_route = track->ayyi_mixer_track;
	if(!ayyi_route) return set_error("ayyi_route not found");

	boost::shared_ptr<Send> send = get_send_for_route((ARDOUR::Route*)track.get(), aux_num);

	if(!connection_idx){

		{ // test status of the bundle and connection:
			boost::shared_ptr<IO> output = send->output();
			boost::shared_ptr<Bundle> bundle = output->bundle();
			if(bundle){
				AyyiConnection* sc = bundle->shared;
				if(sc){
					dbg(0, "connection_idx=%i", sc->get_index());

					connection_shared* c = sc->connection;
					if(c) dbg(0, "connection='%s'", c->name);
				}
			}
		}

		// set to OFF
		dbg(0, "removing aux send...");
		track->remove_processor(send);
		ayyi_route->track->aux[aux_num] = NULL;
		return true;
	}

	// if the given aux send doesnt exist, we have to create it.
	if(!send.get()){
		dbg(0, "adding new send...");
		boost::shared_ptr<Send> send1 (new Send(*session, track->pannable(), track->mute_master()));
		track->add_processor(send1, PostFader);

		// this has now also created new jack ports, so maybe there is nothing more to do?
		_ayyi_aux* aux = ayyi_route->track->aux[aux_num];
		if(aux) warn("aux slot is supposed to be empty?");
		ayyi_route->track->aux[aux_num] = send1->shm_pod;

		// by default, send gain is set to 1.0. We want it to be 0.0:
		send1->amp()->gain_control()->set_value(0.0, PBD::Controllable::GroupControlDisposition::NoGroup);
		dbg(0, "gain=%.4f", send1->amp()->gain_control()->get_value());

		return true;

		send.swap(send1);
	}

	return set_error("track has send. setting output not implemented");
}


bool
ADMixer::set_aux_level (unsigned channel, unsigned aux_num, float level)
{
	dbg(0, "level=%.4f", level);

	if(aux_num >= AYYI_AUX_PER_CHANNEL) return set_error("bad aux num: %i", aux_num);

	ARDOUR::Route* route = core->get_route(channel);

	boost::shared_ptr<Send> send = get_send_for_route(route, aux_num);
	if(!send.get()) return set_error("couldnt find send %i", aux_num);

	send->amp()->gain_control()->set_value(level, PBD::Controllable::GroupControlDisposition::NoGroup);

	return true;
}


void
ADMixer::meter_update ()
{
	if(!shm || !session || !core->session_loaded) return;

	boost::shared_ptr<Route> master = session->master_out();
	g_return_if_fail(master);

	for(int n=0;n<2;n++)
		shm->master.visible_peak_power[n] = master->peak_meter().meter_level(n, MeterType::MeterPeak);
}


template<>
void
ShmContainer<_ayyi_channel>::set_container ()
{
#if 0
	container = &core->mixer.omixer->get_addr()->tracks;
#else
	//container = MixerTrack<ARDOUR::Route>::tracks;
	container = &((MixerShm*)AyyidRoute::shm.mixer)->get_addr()->tracks;
#endif
	container->obj_type = AYYI_OBJECT_CHAN;
}


template<>
void
ShmContainer<struct _ayyi_plugin_nfo>::set_container ()
{
	container = &core->mixer.omixer->get_addr()->plugins;
}


};
