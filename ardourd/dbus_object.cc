/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 }
 */

#include <sys/types.h>
#include <cstdlib>
#include <unistd.h>
#include <string.h>

#include <glib.h>
#include <glib/gi18n.h>
#include <glib-object.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtkmain.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"

#include <ardour/types.h>
#include <ardour/ardour.h>
#include <ardour/audioengine.h>
#include <ardour/audiofilesource.h>
#include <ardour/audioregion.h>
#include <ardour/bundle.h>
#include <ardour/audio_track.h>

#ifdef VST_SUPPORT
  #include "fst.h"
#endif

#include "dbus_object.h"
#include "dbus_marshallers.c"

extern "C"{
	void errprintf(char *format, ...);
	void warnprintf(char *format, ...);
}

#include "dbus-service.h"       //the file generated from xml.
#include "dbus-glib-bindings.h" //our local copy with explicit casting.

#include "app.h"
#include <ayyi/ayyi_types.h>
#include "ayyi/utils.h"
#include "ayyi/ayyi_error.h"

namespace Ayi {
	extern Ayi::AD* core;
	extern ARDOUR::Session* session;
}
extern int debug;

#define P_IN p(0, "%s--->%s ", green, white)
#define M_OUT p("%s--->%s ", yellow, white)
#define P_OUT p(0, "%s(): %s--->%s", __func__, yellow, white)
#define dbg(A, ...) dbgprintf("ADdbus", __func__, (char*)A, ##__VA_ARGS__)
#define dbgN(N, A, ...){ if(N <= debug) dbgprintf("ADdbus", __func__, (char*)A, ##__VA_ARGS__); }
#define warn(A, ...) warnprintf2("ADdbus", __func__, (char*)A, ##__VA_ARGS__)
#define err(A, ...) errprintf2("ADdbus", __func__, (char*)A, ##__VA_ARGS__)
#define NOT_OK FALSE
#define _unexpected_property(A, B) unexpected_property(__func__, A, B, error)

G_DEFINE_TYPE (ArdourDbus, ad_dbus, G_TYPE_OBJECT);

#define AD_DBUS_ERROR ad_dbus_error_quark()
#define AD_DBUS_UNEXPECTED_PROPERTY ad_dbus_quark(0)
#define AD_DBUS_UNEXPECTED_OBJECT_TYPE ad_dbus_quark(1)
static const char* ad_dbus_quark(int n);

using namespace Ayi;

enum
{
  OBJNEW_SIGNAL,
  HELLO_SIGNAL,
  DELETED_SIGNAL,
  TRANSPORT_SIGNAL,
  LOCATORS_SIGNAL,
  PROPERTY_SIGNAL,
  PROGRESSBAR_SIGNAL,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0, 0, 0, 0, 0, 0 };

static DBusGConnection *connection = NULL;
static DBusGProxy      *proxy      = NULL;

static gboolean unexpected_object_type(const char* func, ObjType object_type, GError**);
static gboolean unexpected_property   (const char* func, gint32 object_type, gint32 prop, GError**);


static void
ad_dbus_class_init (ArdourDbusClass *klass)
{
	signals[OBJNEW_SIGNAL] = g_signal_new ("objnew_signal",
                  G_OBJECT_CLASS_TYPE (klass),
                  (GSignalFlags)(G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED),
                  0,
                  NULL, NULL,
                  _ad_dbus_marshal_VOID__INT_INT,
                  G_TYPE_NONE, 2, G_TYPE_INT, G_TYPE_INT);

	signals[HELLO_SIGNAL] = g_signal_new ("hello_signal",
                  G_OBJECT_CLASS_TYPE (klass),
                  (GSignalFlags)(G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED),
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__STRING,
                  G_TYPE_NONE, 1, G_TYPE_STRING);

	signals[DELETED_SIGNAL] = g_signal_new ("deleted_signal",
                  G_OBJECT_CLASS_TYPE (klass),
                  (GSignalFlags)(G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED),
                  0,
                  NULL, NULL,
                  _ad_dbus_marshal_VOID__INT_INT,
                  G_TYPE_NONE, 2, G_TYPE_INT, G_TYPE_INT);

	signals[TRANSPORT_SIGNAL] = g_signal_new ("transport_signal",
                  G_OBJECT_CLASS_TYPE (klass),
                  (GSignalFlags)(G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED),
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__STRING,
                  G_TYPE_NONE, 1, G_TYPE_STRING);
	signals[LOCATORS_SIGNAL] = g_signal_new ("locators_signal",
                  G_OBJECT_CLASS_TYPE (klass),
                  (GSignalFlags)(G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED),
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__STRING,
                  G_TYPE_NONE, 1, G_TYPE_STRING);
	signals[PROPERTY_SIGNAL] = g_signal_new ("property_signal",
                  G_OBJECT_CLASS_TYPE (klass),
                  (GSignalFlags)(G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED),
                  0,
                  NULL, NULL,
                  _ad_dbus_marshal_VOID__INT_INT_INT,
                  G_TYPE_NONE, 3, G_TYPE_INT, G_TYPE_INT, G_TYPE_INT);
	signals[PROGRESSBAR_SIGNAL] = g_signal_new ("progressbar_signal",
                  G_OBJECT_CLASS_TYPE (klass),
                  (GSignalFlags)(G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED),
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__DOUBLE,
                  G_TYPE_NONE, 1, G_TYPE_DOUBLE);
}

static void
ad_dbus_init (ArdourDbus *application)
{
}

gboolean
ad_dbus_register_service (ArdourDbus *application)
{
	//for successful registration, check security settings in /etc/dbus/.
	//-server must already be running.

	dbgN(1, "...");

	GError *err = NULL;
	guint request_name_result;

	if (connection) {
		g_warning ("Service already registered.");
		return FALSE;
	}

	#define BUS_COUNT 2
	struct _busses {
		DBusBusType type;
		char        name[64];
	} busses[BUS_COUNT] = {
		{DBUS_BUS_SESSION, "session"},
		{DBUS_BUS_SYSTEM, "system"}
	};

	int i; for(i=0;i<BUS_COUNT;i++){
		if (!(connection = dbus_g_bus_get (busses[i].type, &err))){
			p(0, "dbus bus connect (%i: %s) failed", i, busses[i].name);
			if(err->code != 0) printf(": %i: %s", err->code, err->message);
			p(0, "");
			g_error_free (err);
			err = NULL;
		}
		else break;
	}
	if(!connection){ errprintf((char*)"dbus service registration %s\n", fail); return FALSE; }
	application->connection = connection;

	proxy = dbus_g_proxy_new_for_name (connection, DBUS_SERVICE_DBUS, DBUS_PATH_DBUS, DBUS_INTERFACE_DBUS);

	// testing.
	/*
	if (!dbus_g_proxy_call (proxy, "RequestName", &err,
              G_TYPE_STRING, APPLICATION_SERVICE_NAME,
              G_TYPE_UINT, 0,
              G_TYPE_INVALID,
              G_TYPE_UINT, &request_name_result,
              G_TYPE_INVALID)){
		errprintf("Failed to acquire %s: %s", APPLICATION_SERVICE_NAME, err->message);
		g_clear_error (&err);
	}
	*/

	if (!org_freedesktop_DBus_request_name (proxy, APPLICATION_SERVICE_NAME, DBUS_NAME_FLAG_DO_NOT_QUEUE, &request_name_result, &err)) {
		errprintf((char*)"dbus service registration failed. %s\n", err->message);
		g_clear_error (&err);
		return FALSE;
	}

	if (request_name_result == DBUS_REQUEST_NAME_REPLY_EXISTS) return FALSE;

	/*
		Install introspection information about the given object GType sufficient to allow methods on 
		the object to be invoked by name.

		The introspection information is normally generated by dbus-glib-tool, then this function is 
		called in the class_init() for the object class.

		Once introspection information has been installed, instances of the object registered with 
		dbus_g_connection_register_g_object() can have their methods invoked remotely.
	*/
	dbus_g_object_type_install_info (AD_DBUS_TYPE_APPLICATION, &dbus_glib_ad_dbus_object_info);

	dbus_g_connection_register_g_object (connection, DBUS_APP_PATH, G_OBJECT (application));

	return TRUE;
}

ArdourDbus*
ad_dbus_get_instance (void)
{
	static ArdourDbus *instance;

	if (!instance) {
		instance = ADDBUS_APPLICATION (g_object_new (AD_DBUS_TYPE_APPLICATION, NULL));
	}

	return instance;
}

#if 0
gboolean
ad_dbus_open_uri(ArdourDbus *application, const char *uri, GHashTable *args, guint timestamp, GError **error)
{
	//rpc call. For testing only.

	P_IN;
	printf("%s(): :-)\n", __func__);

	EvLinkDest      *dest = NULL;
	EvWindowRunMode  mode = EV_WINDOW_MODE_NORMAL;

	if (args) {
		GValue *value = NULL;
		
		value = g_hash_table_lookup (args, "page-label");
		if (value) {
			const gchar *page_label;
			
			page_label = g_value_get_string (value);
			dest = ev_link_dest_new_page_label (page_label);
		}
		
		value = g_hash_table_lookup (args, "mode");
		if (value) {
			mode = g_value_get_uint (value);
		}
	}

	return TRUE;
}
#endif

/*
void
ad_dbus_open_uri_list (ArdourDbus *application, GSList *uri_list, guint          timestamp)
{
	GSList *l;

	for (l = uri_list; l != NULL; l = l->next) {
		ad_dbus_open_uri (application, (char *)l->data, NULL, timestamp, NULL);
	}
}
*/

gboolean
ad_dbus_ping (ArdourDbus *app, const gchar **pong, GError **error)
{
	*pong = g_strdup("hello");
	return TRUE;
}


gboolean
ad_dbus_get_shm (ArdourDbus *app, char **strings, GError **error)
{
	//return the shm segment addresses.

	//FIXME crashes!
	P_IN;

	dbgN(1, "strings=%p", strings);
	if(!strings) return FALSE;
	//printf("%s()! strings[0]=%p\n", __func__, strings[0]);
	//if(!strings[0]) return;
	//strings[0] = malloc(64);   //FIXME
	strcpy(strings[0], "666");
	//strings[1] = NULL;
	return TRUE;
}


gboolean
ad_dbus_get_shm_single (ArdourDbus *app, const char *name, guint *seg_id, GError **error)
{
	P_IN; p(0, "client connect");

	if(!strcmp(name, "mixer")) *seg_id = Ayi::core->mixer.omixer->get_fd();
	else                       *seg_id = Ayi::core->song.osong->get_fd();
	dbgN(1, "seg_id=%i", *seg_id);
	return TRUE;
}


gboolean
ad_dbus_create_object (ArdourDbus *obj, ObjType type, const char *_name, bool from_src, guint32 src_idx, guint32 parent_idx, guint64 stime, guint64 len_mu, guint32 inset, guint32 *new_obj_idx, GError **error)
{
	P_IN;
	if(Ayi::core->gerror) Ayi::core->free_error();
	*ok = TRUE;
	dbgN(2, "type=%i name=%s", type, _name);
	if(new_obj_idx) *new_obj_idx = 666;

	uint32_t new_obj_id = 0;

	string name(_name);

	uint32_t start = Ayi::core->posbuffer2samples((char*)&stime);
	uint32_t length = Ayi::core->mu2samples(len_mu);

	switch(type){
		case AYYI_OBJECT_AUDIO_PART:
		case AYYI_OBJECT_MIDI_PART:
			{
				dbgN (0, "OBJECT_PART ---- USE create_part instead");
				/*
				uint32_t trk_pod_idx = parent_idx;
				bool from_region = !from_src;
				*ok = (type == AYYI_OBJECT_AUDIO_PART) ?
						Ayi::core->add_part (name, from_region, src_idx, trk_pod_idx, start, inset, length, new_obj_idx, &new_obj_id) :
						Ayi::core->add_midi_part (name, trk_pod_idx, start, inset, length, new_obj_idx, &new_obj_id);
				*/
				*ok = false;
			}
			break;
		case AYYI_OBJECT_TRACK:
			dbgN (0, "OBJECT_TRACK n_tracks=%i %s%s%s", src_idx, bold, _name, white);
			//src_idx is the number of channels (1 or 2)
			*ok = Ayi::core->song.add_track(name, src_idx, new_obj_idx, &new_obj_id);
			break;
		case AYYI_OBJECT_MIDI_TRACK:
			dbgN (0, "OBJECT_MIDI_TRACK");
			*ok = Ayi::core->song.add_midi_track(name, new_obj_idx, &new_obj_id);
			break;
		case AYYI_OBJECT_MIDI_NOTE:
			dbgN (0, "OBJECT_MIDI_NOTE");
			*ok = Ayi::core->song.add_midi_note(parent_idx, src_idx, start, length, inset, new_obj_idx, &new_obj_id, error);
			break;
		case AYYI_OBJECT_FILE:
			dbgN (0, "OBJECT_FILE");
			*ok = Ayi::core->song.add_file(name, new_obj_idx);
			break;
		default:
			return unexpected_object_type(__func__, type, error);
			break;
	}

	Ayi::core->get_error(error);
	return *ok;
}

gboolean
ad_dbus_create_part (ArdourDbus *obj, ObjType type, const char *_name, uint64_t id, bool from_src, guint32 src_idx, guint32 parent_idx, guint64 stime, guint64 len_mu, guint32 inset, guint32 *new_obj_idx, GError **error)
{
	P_IN;
	if(Ayi::core->gerror) Ayi::core->free_error();
	*ok = TRUE;
	dbgN(0, "type=%i name=%s id=%Lu", type, _name, id);
	if(new_obj_idx) *new_obj_idx = 666;

	uint32_t new_obj_id = 0;

	string name(_name);

	uint32_t start = Ayi::core->posbuffer2samples((char*)&stime);
	uint32_t length = Ayi::core->mu2samples(len_mu);

	switch(type){
		case AYYI_OBJECT_AUDIO_PART:
		case AYYI_OBJECT_MIDI_PART:
			{
				dbgN (0, "OBJECT_PART");
				uint32_t trk_pod_idx = parent_idx;
				bool from_region = !from_src;

				*ok = (type == AYYI_OBJECT_AUDIO_PART) ?
						Ayi::core->song.add_part      (name, id, from_region, src_idx, trk_pod_idx, start, inset, length, new_obj_idx, &new_obj_id) :
						Ayi::core->song.add_midi_part (name, id, trk_pod_idx, start, inset, length, new_obj_idx, &new_obj_id);
			}
			break;
		default:
			return unexpected_object_type(__func__, type, error);
			break;
	}

	Ayi::core->get_error(error);
	return *ok;
}

#define DBG_OBJ(A) dbgN(0, "%s", Ayi::core->get_object_string(A)->c_str());
gboolean
ad_dbus_delete_object (ArdourDbus *obj, ObjType object_type, guint32 obj_idx, GError **error)
{
	*ok = FALSE;
	P_IN;
	if(Ayi::core->gerror) Ayi::core->free_error();

	switch(object_type){
		case AYYI_OBJECT_PART:
		case AYYI_OBJECT_AUDIO_PART:
			DBG_OBJ(object_type);
			*ok = Ayi::core->song.del_part(obj_idx);
			break;
		case AYYI_OBJECT_MIDI_PART:
			dbgN(0, "OBJECT_MIDI_PART");
			*ok = Ayi::core->song.del_midi_part(obj_idx);
			break;
		case AYYI_OBJECT_AUDIO_TRACK:
			dbgN(0, "OBJECT_AUDIO_TRACK");
			*ok = Ayi::core->song.del_track(obj_idx);
			break;
		case AYYI_OBJECT_MIDI_TRACK:
			dbgN(0, "OBJECT_MIDI_TRACK");
			*ok = Ayi::core->song.del_midi_track(obj_idx);
			break;
		case AYYI_OBJECT_FILE:
			dbgN(0, "OBJECT_FILE");
			*ok = Ayi::core->song.del_file(obj_idx);
			break;
		case AYYI_OBJECT_AUX:
			DBG_OBJ(object_type);
			dbgN(0, "obj_idx=%i", obj_idx);
			*ok = Ayi::core->mixer.del_aux(obj_idx >> 4, obj_idx & 0xf);
			break;
		case AYYI_OBJECT_TRACK:
		default:
			return unexpected_object_type(__func__, object_type, error);
			break;
	}

	Ayi::core->get_error(error);
	return *ok;
}


gboolean
ad_dbus_get_prop_string (ArdourDbus*, ObjType type, int prop, const gchar **val, GError** error)
{
	P_IN;
	*ok = FALSE;

	switch(prop){
		case AYYI_HISTORY:
			dbgN(0, "AYYI_HISTORY");
			{
				std::string h = Ayi::core->get_history();
				*val = g_strdup(h.c_str());
				*ok = TRUE;
			}
			break;
		default:
			dbgN(0, "%s: %i", AD_DBUS_UNEXPECTED_PROPERTY, prop);
			g_set_error(error, AD_DBUS_ERROR, 503420, "%s: %i", AD_DBUS_UNEXPECTED_PROPERTY, prop);
			break;
	}
	return *ok;
}


gboolean
ad_dbus_set_prop_int (ArdourDbus *obj, ObjType object_type, int prop, guint32 obj_idx, int val, gboolean *ok, GError **error)
{
	P_IN;
	dbgN(1, "...");
	*ok = FALSE;
	if(Ayi::core->gerror){ dbg("freeing core->gerror..."); Ayi::core->free_error(); }

	switch(object_type){
		//case AYYI_OBJECT_PART:
		case AYYI_OBJECT_AUDIO_PART:
		case AYYI_OBJECT_MIDI_PART:
			{
			dbg("OBJECT_PART");

			if(!Ayi::core->song.region_id_ok(obj_idx)) return FALSE;

			int media_type = (object_type == AYYI_OBJECT_AUDIO_PART) ? AYYI_AUDIO : AYYI_MIDI;

			switch (prop){
				case AYYI_STIME:
					*ok = Ayi::core->song.region_set_start((uint32_t)obj_idx, (uint32_t)val, media_type);
					if(Ayi::core->gerror) *error = g_error_copy(Ayi::core->gerror);
					break;
				case AYYI_LENGTH:
					//length must be in samples. For mu, use ad_dbus_set_prop_int64().
					*ok = Ayi::core->song.region_set_length((uint32_t)obj_idx, media_type, (uint32_t)val);
					break;
				case AYYI_TRACK:
					*ok = (object_type == AYYI_OBJECT_AUDIO_PART)
						? Ayi::core->song.audio_region_set_track((uint32_t)obj_idx, (uint32_t)val)
						: Ayi::core->song.midi_region_set_track((uint32_t)obj_idx, (uint32_t)val);
					break;
				case AYYI_INSET:
					warn("TRIM_LEFT is normally a 64 bit function.\n");
					Ayi::core->song.region_trim_left(obj_idx, (uint32_t)val, media_type);
					*ok = TRUE;
					break;
				case AYYI_SPLIT:
					warn("split is a 64 bit function.\n");
					break;
				case AYYI_PB_LEVEL:
					warn("pb_level is a float function. Ignoring...\n");
					break;
				case AYYI_MUTE:
					g_set_error(error, AD_DBUS_ERROR, 28764, "OBJECT_PART: MUTE currently not supported.");
					break;
				default:
					return _unexpected_property(object_type, prop);
					break;
			}
			} break;
		case AYYI_OBJECT_TRACK:
			dbg ("OBJECT_TRACK");
			switch (prop){
				case AYYI_MUTE:
					*ok = Ayi::core->song.route_set_mute(obj_idx, val);
					break;
				case AYYI_ARM:
					*ok = Ayi::core->song.route_set_arm(obj_idx, val);
					break;
				case AYYI_INPUT:
					*ok = Ayi::core->set_input(obj_idx, val);
					break;
				case AYYI_OUTPUT:
					*ok = Ayi::core->set_output(obj_idx, val);
					break;
				case AYYI_COLOUR:
					*ok = Ayi::core->song.route_set_colour(obj_idx, val);
					break;
				case AYYI_HEIGHT:
					g_set_error (error, AD_DBUS_ERROR, 78903, "%s: %s: object=%i prop=%i height is not a model property", __func__, AD_DBUS_UNEXPECTED_PROPERTY, object_type, prop);
					*ok = false;
					break;
				default:
					return _unexpected_property(object_type, prop);
			}
			break;
		case AYYI_OBJECT_CHAN:
			switch (prop){
				case AYYI_PAN_ENABLE:
					*ok = Ayi::core->mixer.set_enable_pan(obj_idx, val);
					break;
				default:
					return _unexpected_property(object_type, prop);
			}
			break;
		case AYYI_OBJECT_AUX:
			switch (prop){
				case AYYI_OUTPUT:
					*ok = Ayi::core->mixer.set_aux_output(obj_idx & 0xffff, obj_idx >> 16, val);
					break;
				default:
					return _unexpected_property(object_type, prop);
			}
			break;
		case AYYI_OBJECT_TRANSPORT:
			switch(prop){
				case AYYI_TRANSPORT_LOCATE:
					dbgN(1, "OBJECT_TRANSPORT: TRANSPORT_LOCATE");
					//bool playing = (Ayi::core->session->_transport_speed > 0.1);
					Ayi::session->request_locate(val, Ayi::session->transport_rolling());
					//if(playing) Ayi::core->session->request_play(); //attempt to recover from an unrequested stop caused by the locate.
					*ok = TRUE;
					break;
				default:
					return _unexpected_property(object_type, prop);
			}
			break;
		case AYYI_OBJECT_SONG:
			switch(prop){
				case AYYI_SAVE:
					*ok = Ayi::core->save();
					break;
				case AYYI_LOAD_SONG:
					*ok = Ayi::core->load_session(Ayi::session->path().c_str(), Ayi::session->snap_name().c_str(), (char*)"");
					break;
				default:
					return _unexpected_property(object_type, prop);
			}
			break;
		default:
			return unexpected_object_type(__func__, object_type, error);
			break;
	}
	Ayi::core->get_error(error);
	return *ok;
}

gboolean
ad_dbus_set_prop_int64 (ArdourDbus *obj, ObjType object_type, gint32 prop, guint32 obj_idx, gint64 val, gboolean *ok, GError **error)
{
	P_IN;
	*ok = FALSE;

	switch(object_type){
		case AYYI_OBJECT_AUDIO_PART:
		case AYYI_OBJECT_MIDI_PART:
		case AYYI_OBJECT_PART:{
			if(!Ayi::core->song.region_id_ok(obj_idx)) break;

			int media_type = (object_type == AYYI_OBJECT_AUDIO_PART) ? AYYI_AUDIO : AYYI_MIDI;

			switch (prop){
				case AYYI_LENGTH:
					{
						dbg ("OBJECT_PART LEN");
						uint32_t _len = Ayi::core->mu2samples(val);
						*ok = Ayi::core->song.region_set_length(obj_idx, media_type, _len);
					}
					break;
				case AYYI_INSET:
					dbg ("OBJECT_PART PROP_TRIM_LEFT");
					Ayi::core->song.region_trim_left(obj_idx, Ayi::core->mu2samples(val), media_type);
					*ok = TRUE;
					break;
				case AYYI_SPLIT:
					dbg ("OBJECT_PART PROP_SPLIT mu=%Li", val);
					if(val > 0){
						*ok = Ayi::core->song.region_split(obj_idx, media_type, Ayi::core->mu2samples(val));
					}else{
						g_set_error(error, AD_DBUS_ERROR, 23421, "PART SPLIT: val out of range: %" PRIi64, val);
						*ok = FALSE;
					}
					break;
				default:
					dbg ("OBJECT_PART: %s: %i", AD_DBUS_UNEXPECTED_PROPERTY, prop);
					g_set_error(error, AD_DBUS_ERROR, 23420, "OBJECT_PART: %s: %i", AD_DBUS_UNEXPECTED_PROPERTY, prop);
			}
			} break;
		case AYYI_OBJECT_SONG:{
			switch (prop){
				case AYYI_END:
					dbg("OBJECT_SONG END");
					*ok = Ayi::core->set_locator(9, Ayi::core->posbuffer2samples((char*)&val));
					break;
				default:
					dbg("OBJECT_SONG: %s: %i", AD_DBUS_UNEXPECTED_PROPERTY, prop);
			}
			} break;
		case AYYI_OBJECT_TRANSPORT: {
			switch (prop){
				case AYYI_TRANSPORT_LOCATOR: {
					dbg("OBJECT_TRANSPORT TRANSPORT_LOCATOR");
					*ok = Ayi::core->set_locator(obj_idx, Ayi::core->posbuffer2samples((char*)&val));
					} break;
				default:
					dbg("OBJECT_TRANSPORT: %s: %i", AD_DBUS_UNEXPECTED_PROPERTY, prop);
			}
			} break;
		default:
			return unexpected_object_type(__func__, object_type, error);
	}

	Ayi::core->get_error(error);
	return *ok;
}

gboolean
ad_dbus_set_prop_float (ArdourDbus *obj, ObjType object_type, gint32 prop, guint32 obj_idx, double val, gboolean *ok, GError **error)
{
	P_IN;
	dbg("...");
	*ok = FALSE;

	//properties that are object independent:
	switch(prop){
		case AYYI_TEMPO:
			Ayi::core->set_tempo(val);
			*ok = TRUE;
			goto out;
			break;
	}

	switch(object_type){
		case AYYI_OBJECT_PART: {
			dbg("OBJECT_PART");

			if(!Ayi::core->song.region_id_ok(obj_idx)) break;

			switch (prop){
				case AYYI_PB_LEVEL:
					;dbg("OBJECT_PART PB_LEVEL");
					Ayi::core->song.region_set_pblevel(obj_idx, (uint32_t)val);
					*ok = TRUE;
					break;
				default:
					_unexpected_property(object_type, prop);
					break;
			}
			} break;
		case AYYI_OBJECT_CHAN:
			switch(prop){
				case AYYI_LEVEL:
					dbg("OBJECT_CHAN LEVEL");
					*ok = Ayi::core->mixer.set_level(obj_idx, val);
					break;
				case AYYI_PAN:
					dbg("OBJECT_CHAN PAN");
					*ok = Ayi::core->mixer.set_pan(obj_idx, val);
					break;
				default:
					return _unexpected_property(object_type, prop);
					break;
			}
			break;
		case AYYI_OBJECT_AUX:
			switch(prop){
				case AYYI_LEVEL:
					dbg("OBJECT_AUX CHAN_LEVEL");
					*ok = Ayi::core->mixer.set_aux_level(obj_idx && 0xffff, obj_idx >> 16, val);
					break;
				default:
					return _unexpected_property(object_type, prop);
					break;
			}
			break;
		default:
			return unexpected_object_type(__func__, object_type, error);
			break;
	}

out:
	if(!*ok) Ayi::core->get_error(error);
	return *ok;
}

gboolean
ad_dbus_set_prop_bool (ArdourDbus *obj, ObjType object_type, gint32 prop, guint32 obj_idx, gboolean val, gboolean *ok, GError **error)
{
	P_IN;
	dbg("...");
	*ok = FALSE;

	switch(object_type){
		case AYYI_OBJECT_CHAN:
			switch(prop){
				case AYYI_ARM:
					dbg("OBJECT_CHAN ARM");
					*ok = Ayi::core->song.route_set_arm(obj_idx, val);
					break;
				case AYYI_MUTE:
					dbg("OBJECT_CHAN MUTE");
					*ok = Ayi::core->song.route_set_mute(obj_idx, val);
					break;
				case AYYI_SOLO:
					dbg("OBJECT_CHAN SOLO");
					*ok = Ayi::core->song.route_set_solo(obj_idx, val);
					break;
				case AYYI_SDEF:
					dbg("OBJECT_CHAN SDEF - FIXME");
					*ok = TRUE;
					break;
				case AYYI_PLUGIN_BYPASS:
					{
					dbg("OBJECT_CHAN PLUGIN_BYPASS - FIXME");
					guint plugin_slot = obj_idx >> 8; //TODO pass as separate parameter.
					*ok = Ayi::core->mixer.set_plugin_bypass(obj_idx & 0xff, plugin_slot, val);
					}
					break;
				default:
					dbg("OBJECT_CHAN: unexpected property type. %i", prop);
					break;
			}
			break;
		case AYYI_OBJECT_TRANSPORT:
			switch(prop){
				case AYYI_TRANSPORT_STOP:
					dbg("TRANSPORT STOP");
					Ayi::core->stop();
					*ok = TRUE;
					break;
				case AYYI_TRANSPORT_PLAY:
					dbg("TRANSPORT PLAY");
					Ayi::core->play();
					*ok = TRUE;
					break;
				case AYYI_TRANSPORT_REW:
					dbg("TRANSPORT REW");
					Ayi::core->rew();
					*ok = TRUE;
					break;
				case AYYI_TRANSPORT_FF:
					dbg("TRANSPORT FF");
					Ayi::core->ff(0);
					*ok = TRUE;
					break;
				case AYYI_TRANSPORT_REC:
					dbg("%sTRANSPORT REC%s", red, white);
					Ayi::core->rec(val);
					*ok = TRUE;
					break;
				case AYYI_TRANSPORT_CYCLE:
					dbg("OBJECT_TRANSPORT SM_TRANSPORT_CYCLE");
					Ayi::core->set_cycle_onoff(val);
					*ok = TRUE;
					break;
				default:
					dbg("OBJECT_TRANSPORT: unexpected property type. %i", prop);
					break;
			}
			break;
		default:
			return unexpected_object_type(__func__, object_type, error);
			break;
	}

	Ayi::core->get_error(error);
	return *ok;
}

gboolean
ad_dbus_set_prop_string (ArdourDbus *obj, ObjType object_type, gint32 prop, guint32 obj_idx, const char* val, gboolean *ok, GError **error)
{
	P_IN;
	dbg("obj_idx=%i", obj_idx);
	*ok = FALSE;

	switch(object_type){
		case AYYI_OBJECT_AUDIO_PART:
		case AYYI_OBJECT_PART: {
			if(!Ayi::core->song.region_id_ok(obj_idx)) break;

			switch (prop){
				case AYYI_NAME: {
					dbg("OBJECT_AUDIO_PART STRING");
					*ok = Ayi::core->song.region_set_name(obj_idx, AYYI_AUDIO, val);
					} break;
				default:
					warn("OBJECT_MIDI_PART: %s: %i - error=%p\n", AD_DBUS_UNEXPECTED_PROPERTY, prop, error);
			}
			} break;
		case AYYI_OBJECT_MIDI_PART:
			switch (prop){
				case AYYI_NAME:
					dbg("OBJECT_MIDI_PART STRING");
					*ok = Ayi::core->song.region_set_name(obj_idx, AYYI_MIDI, val);
					break;
				default:
					warn("OBJECT_PART: %s: %i - error=%p\n", AD_DBUS_UNEXPECTED_PROPERTY, prop, error);
			}
			break;
		case AYYI_OBJECT_TRACK:
			switch (prop){
				case AYYI_NAME:
					dbg("OBJECT_TRACK STRING");
					*ok = Ayi::core->song.route_set_name(obj_idx, val);
					break;
				default:
					warn("OBJECT_TRACK: %s: %i\n", AD_DBUS_UNEXPECTED_PROPERTY, prop);
					break;
			}
			break;
		case AYYI_OBJECT_CHAN:
			switch (prop){
				case AYYI_PLUGIN_SEL:
					{
					dbgN(1, "OBJECT_CHAN PLUGIN_SEL");
					guint plugin_slot = obj_idx >> 8; //TODO pass as separate parameter.
					*ok = Ayi::core->mixer.set_plugin(obj_idx & 0xff, plugin_slot, (char*)val);
					}
					break;
				default:
					warn("OBJECT_CHAN: unexpected property. %i\n", prop);
					break;
			}
			break;
		case AYYI_OBJECT_SONG: {
			dbg("OBJECT_SONG");

			char snapshot[] = "1";
			//char template_path[256] = "2 Track";

			*ok = Ayi::core->load_session(val, snapshot, (char*)"2 Track");
			} break;
		default:
			return unexpected_object_type(__func__, object_type, error);
			break;

	}
	/*
	if(!*ok){
		gint error_code = 666;
		g_set_error(error, AD_DBUS_ERROR, error_code, "object=%i", object_type);
	}
	*/
	Ayi::core->get_error(error);
	return *ok;
}


gboolean
ad_dbus_set_prop_float_pair (ArdourDbus*, ObjType object_type, gint32 prop, GArray* obj_idx, double val1, double val2, gboolean *ok, GError** error)
{
	P_IN;
	p(1, "%s()...", __func__);

	guint32 idx1 = g_array_index(obj_idx, guint, 0);
	guint32 idx2 = g_array_index(obj_idx, guint, 1);
	guint32 idx3 = g_array_index(obj_idx, guint, 2);

	*ok = false;
	switch(object_type){
		case AYYI_OBJECT_AUTO:
			switch(prop){
				case AYYI_ADD_POINT:
					*ok = Ayi::core->song.auto_add_point(idx1, idx2, idx3, val1, val2);
					break;
				case AYYI_DEL_POINT:
					*ok = Ayi::core->auto_del_point(idx1, idx2, idx3);
					break;
				case AYYI_AUTO_PT:
					*ok = Ayi::core->auto_set_point(idx1, idx2, idx3, val1, val2);
					break;
				default:
					return unexpected_property(__func__, object_type, prop, error);
					break;
			}
			break;
		case AYYI_OBJECT_MIDI_NOTE:
			*ok = Ayi::core->song.del_midi_note(idx1, idx3);
			break;
		default:
			return unexpected_object_type(__func__, object_type, error);
			break;
	}

	if(!*ok){
		gint error_code = 666;
		if(Ayi::core->gerror){ *error = g_error_copy(Ayi::core->gerror); g_error_free(Ayi::core->gerror); Ayi::core->gerror = 0; }
		else g_set_error (error, AD_DBUS_ERROR, error_code, "ad_dbus_set_prop_3index_2float(): object=%i", object_type);
	}
	return *ok;
}

gboolean
ad_dbus_automation_point_add (ArdourDbus*, guint32 track, guint32 plugin, guint32 auto_type, double pos, double val, gboolean *ok, GError**)
{
	P_IN;
	p(0, "%s()...", __func__);
	*ok = Ayi::core->song.auto_add_point(track, plugin, auto_type, pos, val);
	return *ok;
}

#define DBUS_STRUCT_UINT_BYTE_BYTE_UINT_UINT (dbus_g_type_get_struct ("GValueArray", G_TYPE_UINT, G_TYPE_UCHAR, G_TYPE_UCHAR, G_TYPE_UINT, G_TYPE_UINT, G_TYPE_INVALID))
gboolean
ad_dbus_set_notelist (ArdourDbus *obj, guint32 part_idx, GPtrArray* notes, gboolean *ok, GError** error)
{
	P_IN;
	p(0, "%s()...\n", __func__);
	*ok = FALSE;
	if(part_idx < BLOCK_SIZE * CONTAINER_SIZE){
		std::list<struct Ayi::_shm_midi_note*> notelist;// = new std::list<struct _shm_midi_note*>;
		if(notes){
			for (guint i=0; i<notes->len; i++) {
				GValue value= {0, };
				g_value_init (&value, DBUS_STRUCT_UINT_BYTE_BYTE_UINT_UINT);
				g_value_set_boxed(&value, g_ptr_array_index(notes, i));

				if(!G_IS_VALUE(&value)) p(0, "%s(): not GValue!\n", __func__);
				//GValue* noteval = g_value_array_get_nth(val_array, 0);
				Ayi::_shm_midi_note* note = new Ayi::_shm_midi_note;
				dbus_g_type_struct_get(&value, 0, &note->shm_idx, 1, &note->note, 2, &note->velocity, 3, &note->start, 4, &note->length, G_MAXUINT);
				if(note->note > 128){ warn("bad note value! %i\n", note->note); continue; }
				notelist.push_back(note);
				dbg("%iof%i: i=%i note=%i v=%i.", i+1, notes->len, note->shm_idx, note->note, note->velocity);
			}

			*ok = Ayi::core->song.update_note_list(part_idx, &notelist);

			for (guint i=0; i<notes->len; i++) {
				//free the _shm_midi_note's
			}
		}
	}
	else g_set_error (error, AD_DBUS_ERROR, AYYI_ERR_IDX_RANGE, " ");
	return *ok;
}

gboolean
ad_dbus_undo (ArdourDbus *obj, guint32 n, gboolean *ok, GError** error)
{
	P_IN;
	p(0, "%s()...\n", __func__);
	*ok = Ayi::core->undo();
	return *ok;
}

gboolean
ad_dbus_get_props_test (ArdourDbus *obj, const char *uri, GHashTable **properties, GError **error)
{
	//demonstrates how to return a hashtable.

	P_IN;
	p(0, "%s()...\n", __func__);
	*ok = FALSE;

	GValue* value = g_new0 (GValue, 1);
	g_value_init (value, G_TYPE_STRING);
	g_value_set_string(value, g_strdup("the one"));

	GHashTable* hashtable = g_hash_table_new(g_str_hash, g_str_equal);
	g_hash_table_insert(hashtable, g_strdup("saffirea"), value);

	*properties = hashtable;
	*ok = TRUE;

	if (0) {
		gint error_code = 908622;
		g_set_error (error,	AD_DBUS_ERROR,	error_code, _("Unknown Error: %s"), uri);

		return FALSE;
	}
	return *ok;
}

gboolean
ad_dbus_emit_hello (ArdourDbus *obj, GError **error)
{
	P_OUT;
	g_signal_emit (obj, signals[HELLO_SIGNAL], 0, "Hello");
	return TRUE;
}

gboolean
ad_dbus_emit_objnew (ArdourDbus *obj, ObjType object_type, int object_idx, GError **error)
{
	if(object_type >= AYYI_OBJECT_UNSUPPORTED){ err("object_type=%i\n", object_type); return FALSE; }

	dbg ("%s %i %s--->%s", AD::get_object_string((ObjType)object_type)->c_str(), object_idx, yellow, white);
	g_signal_emit (obj, signals[OBJNEW_SIGNAL], 0, object_type, object_idx);
	return TRUE;
}

gboolean
ad_dbus_emit_deleted (ArdourDbus *obj, ObjType object_type, int object_idx, GError **error)
{
	g_return_val_if_fail(object_type <= AYYI_OBJECT_ALL, false);

	dbg("%s %i %s--->%s", AD::get_object_string((ObjType)object_type)->c_str(), object_idx, yellow, white);
	g_signal_emit (obj, signals[DELETED_SIGNAL], 0, object_type, object_idx);
	return true;
}

gboolean
ad_dbus_emit_property (ArdourDbus *obj, ObjType object_type, int object_idx, int prop_type, GError **error)
{
	p(0, "%s(): %s %s %s--->%s", __func__, AD::get_object_string((ObjType)object_type)->c_str(), AD::get_prop_type_string(prop_type), yellow, white);
	g_signal_emit (obj, signals[PROPERTY_SIGNAL], 0, object_type, object_idx, prop_type);
	return TRUE;
}

gboolean
ad_dbus_emit_transport (ArdourDbus *obj, GError **error)
{
	if(debug) P_OUT;
	g_signal_emit (obj, signals[TRANSPORT_SIGNAL], 0, "");
	return TRUE;
}

gboolean
ad_dbus_emit_locators (ArdourDbus *obj, GError **error)
{
	if(debug) P_OUT;
	g_signal_emit (obj, signals[LOCATORS_SIGNAL], 0, "");
	return TRUE;
}

gboolean
ad_dbus_emit_progress_bar (ArdourDbus *obj, double value, GError **error)
{
	if(debug) P_OUT;
	g_signal_emit (obj, signals[PROGRESSBAR_SIGNAL], 0, value);
	return TRUE;
}

void
ad_dbus_send_signal (ArdourDbus *obj)
{
	//this is for testing only! crashes !

	DBusConnection* dbusconnection = dbus_g_connection_get_connection(connection);
	if(!dbusconnection){ err("dbus_g_connection_get_connection() failed.\n"); return; }

	//FIXME we dont want to create new each time?
	//message = dbus_message_new_signal ("/org/gnome/dbus/ping", "org.gnome.ardourd.Signal", "obj_new");
	DBusMessage *message = dbus_message_new_signal (DBUS_APP_PATH, APPLICATION_SERVICE_NAME, "Dummy");
	if(message){
		//append the string "Ping!" to the signal:
		p(0, "%s(): appending args...\n", __func__);
		dbus_message_append_args (message, DBUS_TYPE_STRING, "Ping", DBUS_TYPE_INVALID);  //segfault!
		p(0, "%s(): sending dbus signal...\n", __func__);
		dbus_connection_send (dbusconnection, message, NULL);
		dbus_message_unref (message);
	}

	/*
	static gboolean signal_added = FALSE;

	if(!signal_added && FALSE){
		printf("%s(): adding...\n", __func__);
		dbus_g_proxy_add_signal(proxy, "objNew", G_TYPE_STRING, G_TYPE_INVALID); //this is for incoming!  outgoing also?
		signal_added = TRUE;
	}
	warnprintf("%s(): calling...\n", __func__);
	//dbus_g_proxy_begin_call(proxy, "objNew", NULL, NULL, NULL, G_TYPE_STRING, "Ping", G_TYPE_INVALID);
	//dbus_g_proxy_call_no_reply(proxy, "objNew", G_TYPE_STRING, "Ping", G_TYPE_INVALID);
	//dbus_g_connection_flush (connection);

	GError *error = NULL;
	if(!dbus_g_proxy_call(proxy, "objNew", &error, G_TYPE_STRING, "Ping", G_TYPE_INVALID)){
		errprintf("%s(): %s\n", __func__, error->message);
		g_error_free(error);

		// we get: org.freedesktop.DBus does not understand message objNew
	}
	printf("%s(): done. no errors\n", __func__);
	*/
}

void
ad_dbus_req_signal (ArdourDbus *obj)
{
	dbg("making dbus request for DeletedSignal...");
	dbus_g_proxy_call_no_reply (proxy, "emitDeletedSignal", G_TYPE_INVALID);
}

GQuark
ad_dbus_error_quark ()
{
	static GQuark quark = 0;
	if (!quark) quark = g_quark_from_static_string ("ad_dbus_error");
	return quark;
}

const char* error_string[2] = {"unexpected property type", "unexpected object type"};

static const char*
ad_dbus_quark (int n)
{
	static GQuark quark[2] = {0,0};
	if (!quark[n]) quark[n] = g_quark_from_static_string (error_string[n]);
	return g_quark_to_string(quark[n]);
}

static gboolean
unexpected_object_type (const char* func, ObjType object_type, GError **error)
{
	warn ("%s: %i %s\n", AD_DBUS_UNEXPECTED_OBJECT_TYPE, object_type, core->get_object_string((ObjType)object_type)->c_str());
	g_set_error (error, AD_DBUS_ERROR, 78903, "%s: %s: type=%i %s", func, AD_DBUS_UNEXPECTED_OBJECT_TYPE, object_type, core->get_object_string((ObjType)object_type)->c_str());
	return NOT_OK;
}

static gboolean
unexpected_property (const char* func, gint32 object_type, gint32 prop, GError **error)
{
	warn ("OBJECT_AUTO: unexpected property. %i\n", prop);
	warn ("%s: %i\n", AD_DBUS_UNEXPECTED_PROPERTY, object_type);
	g_set_error (error, AD_DBUS_ERROR, 78903, "%s: %s: object=%i prop=%i", func, AD_DBUS_UNEXPECTED_PROPERTY, object_type, prop);
	return NOT_OK;
}


