#include <sys/types.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <cerrno>
#include <cstdlib>
#include <signal.h>
#include <unistd.h>

#include <ardour/ardour.h>

#include <ardour/tempo.h>
#include <ardour/audio_track.h>
#include <ardour/session_playlist.h>
#include <ardour/diskstream.h>
#include <ardour/playlist.h>
#include <ardour/audioplaylist.h>
#include <ardour/audioregion.h>
#include <ardour/region.h>
#include <ardour/region_factory.h>
#include <ardour/audiofilesource.h>
#include <ardour/silentfilesource.h>
#include <ardour/session.h>
#include <ardour/bundle.h>
#include <ardour/plugin.h>
#include <ardour/panner.h>

#include <sigc++/bind.h>

#include "ayyi/ayyi.h"
#include "ayyi/ayyi++.h"
#ifdef USE_DBUS
  #include <ayyi/ayyi_types.h>
#else
  #include "ayyi/dae_types.h"
  #include <dae/dae_msg_types.h>
  #include <dae/dae_song_props.h>
  #include "dae/msgs.h"
#endif

#include "ayyi-ardour/shared.h"
#include "test.h"
#include "shm.h"
#include "ayyi-ardour/filesource.h"

namespace Ayi {

using namespace Ayi;
using namespace std;
using namespace ARDOUR;
using boost::shared_ptr;

extern AD* core;

sm_test::sm_test()
{
	cout << "sm_test::sm_test()..." << endl;

	for(int i=0;i<100;i++){
		try { new AyyiFilesource; }
		catch (...) { return; }
	}
}

} //end namespace


