#ifndef __ardourd_version_h__
#define __ardourd_version_h__

extern const char* ardourd_revision;
extern int ardourd_major_version;
extern int ardourd_minor_version;
extern int ardourd_micro_version;

#endif
