/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef AD_DBUS_APPLICATION_H
#define AD_DBUS_APPLICATION_H

#define DBUS_API_SUBJECT_TO_CHANGE
#include <glib-object.h>
#include <dbus/dbus.h>
#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-lowlevel.h>
#include "ayyi/ayyi_types.h"

#ifdef __cplusplus
extern "C" {
#endif

#define APPLICATION_SERVICE_NAME "org.ayyi.ardourd.ApplicationService"
#define DBUS_APP_PATH            "/org/ayyi/ardourd/Ardourd"

G_BEGIN_DECLS

typedef struct _ArdourDbus ArdourDbus;
typedef struct _ArdourDbusClass ArdourDbusClass;

#define AD_DBUS_TYPE_APPLICATION             (ad_dbus_get_type ())
#define ADDBUS_APPLICATION(object)           (G_TYPE_CHECK_INSTANCE_CAST((object), AD_DBUS_TYPE_APPLICATION, ArdourDbus))
#define ADDBUS_APPLICATION_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), AD_DBUS_TYPE_APPLICATION, ArdourDbusClass))
#define ADDBUS_IS_APPLICATION(object)        (G_TYPE_CHECK_INSTANCE_TYPE((object), AD_DBUS_TYPE_APPLICATION))
#define ADDBUS_IS_APPLICATION_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), AD_DBUS_TYPE_APPLICATION))
#define ADDBUS_APPLICATION_GET_CLASS(object) (G_TYPE_INSTANCE_GET_CLASS((object), AD_DBUS_TYPE_APPLICATION, ArdourDbusClass))

#define ADDBUS_APP                           (ad_dbus_get_instance ())

struct _ArdourDbus {
	GObject base_instance;
	
	DBusGConnection *connection;
	gpointer         local_shm; //temporary. c file cannot get properties of c++ class?
};

struct _ArdourDbusClass {
	GObjectClass base_class;
};

GType	        ad_dbus_get_type           (void);
ArdourDbus     *ad_dbus_get_instance       (void);
gboolean        ad_dbus_register_service   (ArdourDbus*);


//the dbus service:
gboolean        ad_dbus_ping                (ArdourDbus*, const gchar**, GError**);
gboolean        ad_dbus_get_shm             (ArdourDbus*, char **strings, GError**);
gboolean        ad_dbus_get_shm_single      (ArdourDbus*, const char *_name, guint *first_address, GError**);
gboolean        ad_dbus_create_object       (ArdourDbus*, Ayi::ObjType, const char *name, bool from_src, guint32 src_idx, guint32 parent_idx, guint64 stime, guint64 len, guint32 inset, guint32 *idx, GError**);
gboolean        ad_dbus_create_part         (ArdourDbus*, Ayi::ObjType, const char *name, uint64_t id, bool from_src, guint32 src_idx, guint32 parent_idx, guint64 stime, guint64 len, guint32 inset, guint32 *idx, GError**);
gboolean        ad_dbus_delete_object       (ArdourDbus*, Ayi::ObjType, guint32 obj_idx, GError**);
gboolean        ad_dbus_get_prop_string     (ArdourDbus*, Ayi::ObjType, int prop, const gchar **val, GError**);
gboolean        ad_dbus_set_prop_int        (ArdourDbus*, Ayi::ObjType, int prop, uint32_t obj_idx, int val, gboolean *ok, GError**);
gboolean        ad_dbus_set_prop_int64      (ArdourDbus*, Ayi::ObjType, gint32 prop, guint32 obj_idx, gint64 val, gboolean *ok, GError**);
gboolean        ad_dbus_set_prop_float      (ArdourDbus*, Ayi::ObjType, gint32 prop, guint32 obj_idx, double val, gboolean *ok, GError**);
gboolean        ad_dbus_set_prop_bool       (ArdourDbus*, Ayi::ObjType, gint32 prop, guint32 obj_idx, gboolean val, gboolean *ok, GError**);
gboolean        ad_dbus_set_prop_string     (ArdourDbus*, Ayi::ObjType, gint32 prop, guint32 obj_idx, const char* val, gboolean *ok, GError**);
gboolean        ad_dbus_set_prop_float_pair (ArdourDbus*, Ayi::ObjType, gint32 prop, GArray* obj_idx, double val1, double val2, gboolean *ok, GError**);
gboolean        ad_dbus_automation_point_add(ArdourDbus*, guint32 track, guint32 plugin, guint32 auto_type, double pos, double val, gboolean *ok, GError**);
gboolean        ad_dbus_set_notelist        (ArdourDbus*, guint32 part_idx, GPtrArray*, gboolean *ok, GError**);
gboolean        ad_dbus_undo                (ArdourDbus*, guint32 n, gboolean *ok, GError**);
gboolean        ad_dbus_get_props_test      (ArdourDbus*, const char *uri, GHashTable **properties, GError**);
gboolean        ad_dbus_emit_objnew         (ArdourDbus*, Ayi::ObjType, int object_idx, GError**);
//gboolean        ad_dbus_emit_objchanged    (ArdourDbus*, Ayi::ObjType, int object_idx, GError**);
gboolean        ad_dbus_emit_hello          (ArdourDbus*, GError**);
gboolean        ad_dbus_emit_deleted        (ArdourDbus*, Ayi::ObjType, int object_idx, GError**);
gboolean        ad_dbus_emit_transport      (ArdourDbus*, GError**);
gboolean        ad_dbus_emit_locators       (ArdourDbus*, GError**);
gboolean        ad_dbus_emit_property       (ArdourDbus*, Ayi::ObjType, int object_idx, int prop_type, GError**);
gboolean        ad_dbus_emit_progress_bar   (ArdourDbus*, double value, GError**);

void            ad_dbus_send_signal         (ArdourDbus*);
void            ad_dbus_req_signal          (ArdourDbus*);

GQuark          ad_dbus_error_quark         ();

G_END_DECLS

#endif /* !AD_DBUS_APPLICATION_H */

#ifdef __cplusplus
} //end extern C
#endif
