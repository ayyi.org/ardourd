/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*/

#include <glibmm.h>
#include <boost/lexical_cast.hpp>

#include <pbd/memento_command.h>
#include <pbd/controllable.h>
#include <evoral/Sequence.hpp>
#include <ardour/region_factory.h>
#include <ardour/audiofilesource.h>
#include <ardour/midi_source.h>
#include <ardour/playlist.h>
#include <ardour/audio_track.h>
#include <ardour/midi_track.h>
#include <ardour/midi_region.h>
#include <ardour/panner.h>
#include "ardour/pannable.h"
#include "ardour/panner_shell.h"
#include <ayyi/ayyi_time.h>
#include <ayyi/utils.h>
#include <ayyi/ayyi_types.h>
#include <ayyi/list.h>
#include <ayyi-ardour/connection.h>
#include <ayyi-ardour/midi_region.h>
#include <ayyi-ardour/playlist.h>
#include <ayyi-ardour/midi_playlist.h>
#include <ardour/amp.h>
#include "ardourd/app.h"
#include "ayyi-ardour/mixer_track.h" // for plugin automation
#include "ardourd/song.h"

extern int debug;
extern int debug_shm;
#define dbg(N, A, ...) { if (N <= debug) dbgprintf("ADSong", __func__, (char*)A, ##__VA_ARGS__); }
#define warn(A, ...) warnprintf2("Song", __func__, (char*)A, ##__VA_ARGS__)
#define err(A, ...) errprintf2("Song", __func__, (char*)A, ##__VA_ARGS__)
#define set_error(A, ...) core->set_error(__func__, A, ##__VA_ARGS__)

#define get_region_pod_or_fail(I, T) \
	({_region_base_shared* pod = (T == AYYI_AUDIO) \
		? (_region_base_shared*)osong->regions.get_item(I) \
		: (_region_base_shared*)osong->midi_regions.get_item(I); \
	if(!pod) return set_error("bad shm_idx: %i type=%i", I, T); \
	pod; \
	});

#include "ayyi/container-impl.cc"
namespace Ayi {

using namespace ARDOUR;
extern AD* core;
extern ARDOUR::Session* session;


//TODO should inherit from SongShm ?
//     or should have base Ayyi::Service class ? currently, Ardourd only offers a single service, but has 2 shm segs...
ADSong::ADSong ()
	: osong(new SongShm)
{
	dbg(1, "...");
}


void
ADSong::shm_init ()
{
#ifdef USE_DBUS
	ShmSegDefn* seg = (ShmSegDefn*)osong;
	shm = shm_index = osong->get_table();
#else
	ShmSegDefn* seg = shm_segment_find_by_name("song");
	song = seg->address;
	song.shm = shm_index = (struct _shm_song*)song;
#endif

	strcpy(shm->service_name, "Ayyi Song");
	strcpy(shm->service_type, "Ayyi Song");
	strcpy(shm->peaks_dir, "peaks");

	shm->version         = AYYI_SHM_VERSION;
	shm->owner_shm       = shm;
	shm->num_pages       = seg->size;
	shm->path[0]         = '\0';
	shm->snapshot[0]     = '\0';
	shm->sample_rate     = 44100;
	shm->bpm             = 120.0;
	shm->transport_frame = 0;
	shm->transport_speed = 0.0;
	shm->play_loop       = false;
	shm->rec_enabled     = false;
	//shm->start           = { 0,}; // is set later...
	//shm->end             = {32,};
													// TODO is not being set on song load
													shm->end             = {32,};
	for(int i=0;i<MAX_LOC;i++) song_pos_set((SongPos*)&shm->locators[i], -1, 0, 0);

	if(debug_shm>1) dbg(0, "segment_size=%i space for %i regions\n", osong->get_size(), osong->get_size() / sizeof(*shm->regions.block[0]));

	osong->clear();
	//reserve memory for tlsf allocator (first 3k is used by tlsf for book-keeping):
	size_t pool_size = seg->unallocated_space();
	shm->tlsf_pool = seg->malloc(pool_size, NULL);
	int free = 0;
	if(!(free = init_memory_pool(pool_size, shm->tlsf_pool))){
		err ("tlsf init failed. size=%u\n", pool_size);
		throw string("tlsf init failed.");
	}
	dbg(1, "tlsf/song: size=%i(kB) free=%i(kB) (%i(kB))", pool_size/1024, free/1024, (osong->get_size() - sizeof(*shm))/1024);

										// TODO Shared::base isnt used much if at all
	AyyiConnection::base = seg;
	//AyyiConnection::shm = (gnash::Shm*)osong;
	AyyiConnection::connections = &osong->connections;
	AyyidRoute::routes = &osong->routes;
	AyyiRegion::shm = (gnash::Shm*)osong;
	AyyiRegion::regions = &osong->regions;
	AyyiRegion::dbus = core->dbus;
	AyyiMidiRegion::midi_regions = &osong->midi_regions;
	AyiPlaylist::shm = (gnash::Shm*)osong;
	AyiPlaylist::playlists = &osong->playlists;
	AyyiFilesource::shm = (gnash::Shm*)osong;
	AyyiFilesource::filesources = &osong->filesources;
	AyyidMidiTrack::shm = (gnash::Shm*)osong;
	AyyidMidiTrack::midi_tracks = &osong->midi_tracks;

	//initialise containers, and allocate a block for each:
	osong->regions.init         ("Ayyi Regions");
	osong->midi_regions.init    ("Ayyi Midi Rgns");
#ifdef VST_SUPPORT
	osong->vst_info.init        ("Ayyi VST Info");
#endif
	osong->filesources.init     ("Ayyi Filesrcs");
	osong->connections.init     ("Ayyi Conections");
	osong->connections.add_item_with_idx(); // dummy entry for "no connection"
	osong->routes.init          ("Ayyi Routes");
	osong->midi_tracks.init     ("Ayyi Midi Trks");
	osong->playlists.init       ("Ayyi Playlists");
}


void
ADSong::set_end (uint32_t end)
{
	dbg(0, "end=%i", end);
	session->maybe_update_session_range(0, end);
	samples2pos(&shm->end, end);
}


void
ADSong::samples2pos (SongPos* pos, uint32_t samples)
{
	int64_t mu = samples2mu(samples);
	mu2pos(mu, (SongPos*)pos);
}


int64_t
ADSong::samples2mu (uint32_t samples)
{
	//returns the length in high resolution tempo-related units, of the given number of audio samples.

	//normally, the ratio is of the order of 1924

	int sample_rate = shm_index->sample_rate;
	if(!sample_rate){ err ("samplerate zero!\n"); sample_rate = 44100; }

	float secs = (float)samples / (float)sample_rate; //length in seconds.

	float bpm = shm_index->bpm;
	float length_of_1_beat_in_secs = 60.0/bpm;

	float beats = secs / length_of_1_beat_in_secs;

	int64_t mu = beats2mu(beats);

	//FIXME this check below indicates there are some largish rounding errors:
	//dbg(0, "samples=%Li mu=%Li check=%Li.", samples, mu, mu2samples(mu));
	return mu;
}


bool
ADSong::add_file (string& path_str, uint32_t* new_obj_idx)
{
	if(!core->file_import(path_str)){
		return set_error("file import failed.");
	}

	ARDOUR::SourceList new_regions = core->import_status.sources;
	for(ARDOUR::SourceList::iterator i=new_regions.begin(); i!=new_regions.end();i++){
		boost::shared_ptr<AudioFileSource> source = boost::dynamic_pointer_cast<AudioFileSource>((*i));
		dbg(0, "source=%s", source->name().c_str());
		Ayi::AyyiFilesource* s = (AyyiFilesource*)source->shared_obj;
		if(s){
			s->announce();
			dbg(0, "returning idx=%u", s->get_index());
			*new_obj_idx = s->get_index();
			break; // we only return the first file index. Could be more for multichannel files.
		}
		else return set_error("source has no shared object");
	}
	//TODO: clear the list?

	return true;
}


bool
ADSong::del_file (ObjIdx pod_idx)
{
	dbg(0, "deleting file %i...", pod_idx);
	if(!osong->filesources.idx_is_valid(pod_idx)) return set_error("bad idx: %i", pod_idx);

	boost::weak_ptr<ARDOUR::Source> file = AyyiFilesource::get_filesource_boost(pod_idx);
	if(!file.use_count()) return set_error("cannot find filesource. idx=%i", pod_idx);

	{
		boost::shared_ptr<ARDOUR::Source> source = file.lock();
		boost::shared_ptr<ARDOUR::AudioFileSource> audio_file = boost::dynamic_pointer_cast<AudioFileSource> (source);
		if(!audio_file->removable()) warn("file is not removable: %i", pod_idx);
	}

	session->remove_source (file);

	if(file.use_count()){
		//2011.06: use counts are correct now? maybe this path can be removed?
		dbg(0, "use_count=%i", file.use_count());
		dbg(0, "killing...");
		AyyiFilesource::kill(pod_idx);
	}

	dbg(0, "done.");

	return true;
}


bool
ADSong::add_part (string& name, uint64_t id, bool from_region, uint32_t src_idx, uint32_t trk_pod_idx, nframes_t start, ARDOUR::framecnt_t inset, nframes_t length, uint32_t* new_obj_idx, uint32_t* new_obj_id)
{
	// @name        - requested part name. The actual name used may be different.
	// @src_idx     - can either be a region, or a file index, depending on whether from_region is set.
	// @start       - start position of the region
	// @inset       - the sample within the filesource at which the region starts.
	// @length      - must be set. no defaults are implemented. *** TESTING default length
	// @new_obj_idx - return the pod index of the new part to the client.
	// @return      - true=ok, false=fail

	dbg (0, "...");
	if(inset != 0) dbg (0, "inset=%u", inset);

	*new_obj_idx = 0;
	*new_obj_id  = 0;

	//if part already exist with this name, find a new name:
	boost::shared_ptr<ARDOUR::Region> existing = region_find(name);
	if(existing.get()){
		string new_name;
		RegionFactory::region_name (new_name, name, false);
		dbg(0, "name changed: '%s'", new_name.c_str());
		name = new_name;
	}

	uint32_t src_region_idx = src_idx;
	//nframes_t copy_offset = 0;

	boost::shared_ptr<ARDOUR::Region> src_region;
	boost::shared_ptr<ARDOUR::Source> src_file;
	if(from_region){
		if(!osong->regions.slot_is_used(src_region_idx)) return set_error("bad src region_idx. (%u)\n", src_region_idx);

		src_region = get_region_boost(src_region_idx);
		if(!src_region) return set_error("from region: failed to get src region. (src_region_idx=%u)\n", src_region_idx);
		dbg(0, "from_region: src_region_idx=%i src_region=%s.", src_region_idx, src_region->name().c_str());
		if(!length){
			length = src_region->length();
		}
		//copy_offset = start - src_region->position();
	}else{
		//FIXME get file source from msg_string->source

		if(1 /*|| src_region_idx == REGION_FULL*/){
			dbg(0, "not from_region - using file_idx=%i", src_idx);
			if(!osong->filesources.idx_is_valid(src_idx)) return set_error("bad filesource idx: %i", src_idx);
			src_file = AyyiFilesource::get_filesource_boost(src_idx);
			if(!src_file) return set_error("failed to get src file. (idx=%u)", src_idx);

			if(length < 1){
				boost::shared_ptr<ARDOUR::AudioFileSource> audio_file = boost::dynamic_pointer_cast<AudioFileSource> (src_file);
				if(audio_file){
					framecnt_t file_length = audio_file->readable_length();
					dbg(0, "file length=%Li", file_length); //TODO get the file length so we can implement default part length.
					if(file_length) length = file_length;
				}
				//else return set_error("failed to cast to AudioFileSource. (idx=%u)", src_idx);
			}
			else dbg(0, "length set explicitly: %u", length);
		}else{
			//we have a (hopefully valid) region index that we can use to find which source file to use.

			//our dbus command doesnt include the source_idx, so lets attempt to get the source from the default region.
			src_file = core->source_find_by_region_idx(src_region_idx);
#ifdef sdfss
			region_shared* default_region = (region_shared*)block->slot[src_region_idx];
			if(!default_region) return set_error("failed to get default region. i=%i\n", src_region_idx);
			uint64_t source_id = default_region->source0;
			//boost::shared_ptr<Source> source_by_id (const PBD::ID&);
			char s[256]; sprintf(s, "%Lu", source_id);
			string id_string = s;
			const PBD::ID* id = new PBD::ID(id_string);
			src_file = session->source_by_id(*id);

#endif
			dbg(0, "FIXME from_source not working.");
		}
	}

	if(!length) return set_error("length not set.\n");

	boost::weak_ptr<ARDOUR::Playlist> playlist;
	boost::shared_ptr<ARDOUR::Playlist> pl;
	{
		//find the playlist for our track...

		route_shared* route = osong->routes.get_item(trk_pod_idx);
		if(!route) return set_error("failed to get shared route struct!\n");

		if(route->flags & master){
			return set_error("cannot add part to master track\n");
		}

		ARDOUR::Track* r = (ARDOUR::Track*)route->object;
		if(!r) return set_error("failed to get Route.\n");

#if 0
		playlist = AyiPlaylist::lookup_by_trackname(route->name);

		if(playlist.expired()){
			print_playlists();
			return set_error("failed to get playlist!\n");
		}

 		pl = playlist.lock();
#endif
		pl = r->playlist();
		//dbg(0, "playlist compare: %s %s", pl->name().c_str(), pl2->name().c_str());

		boost::shared_ptr<AyiPlaylist> apl = boost::dynamic_pointer_cast<AyiPlaylist> (pl);
		if(apl->has_region_starting_at(start)){
			return set_error("region already at this position! %i\n", start);
		}

	}

	PBD::ID Id = PBD::ID(boost::lexical_cast<std::string>(id));

	PBD::PropertyList props; 
	props.add (Properties::position, start);
	props.add (Properties::length, length);
	props.add (Properties::start, inset);
	props.add (Properties::name, name);

	boost::shared_ptr<ARDOUR::AudioRegion> region =
		from_region ? boost::dynamic_pointer_cast<AudioRegion> (RegionFactory::RegionFactory::create(src_region, props, true, &Id))
		            : boost::dynamic_pointer_cast<AudioRegion> (RegionFactory::RegionFactory::create(src_file, props, true, &Id));

	if(region){

		dbg(0, "new region created! %s position=%u", region->name().c_str(), region->position());
		AyyiRegion* object = (AyyiRegion*)region->shared_obj;
		if(!object) return set_error("no shared data for new region!\n");
		*new_obj_idx = object->get_index();

		{
			//region->set_id(boost::lexical_cast<std::string>(id));
			if(((PBD::ID)region->id()).get_id() != id) return set_error("failed to set id");

#if 0
			//PBD::ID old_id = PBD::ID(region->id());
			//RegionFactory::RegionMap::iterator i = RegionFactory::all_regions().find(old_id);
			//m.remove(old_id);
			//RegionFactory::map_add(region);

			dbg(0, "changing id from %Lu to %Lu", object->shm_pod->id, id);
			object->shm_pod->id = id;
#endif
		}

		//find the route for target track:
		ARDOUR::IO* io = core->get_io(trk_pod_idx);
		if(!io) return set_error("failed to get io object!\n");

		region->set_playlist(playlist);
		region->set_position((framepos_t)start);
		pl->add_region(region, start);

		//it is expected that announcement has already happened
		//so this call will be ignored.
		//however it is _essential_ that the announcement happens before this fn returns.
		object->announce();
	}
	else return set_error("region create failed!\n");

	return true;
}


bool
ADSong::add_midi_part (string& name, uint64_t id, uint32_t trk_pod_idx, nframes_t start, ARDOUR::framecnt_t inset, nframes_t length, uint32_t* new_obj_idx, uint32_t* new_obj_id)
{
	dbg (0, "...");

	string source_name("new part"); //FIXME

	//find the route for target track:
	ARDOUR::MidiTrack* track = core->get_midi_track(trk_pod_idx);
	if(!track) return set_error("failed to get track object!\n");

	//boost::shared_ptr<Source> src = new MidiSource(core, source_name);
	//const boost::shared_ptr<MidiDiskstream> diskstream = boost::dynamic_pointer_cast<MidiDiskstream>(track->diskstream());
	//boost::shared_ptr<Source> src = core->session->create_midi_source_for_session(*diskstream.get());
	boost::shared_ptr<MidiSource> src = session->create_midi_source_for_session (name);

	PBD::ID Id = PBD::ID(boost::lexical_cast<std::string>(id));

	PBD::PropertyList plist; 
	plist.add (Properties::start, start);
	plist.add (Properties::length, length);
	plist.add (Properties::name, name);
	boost::shared_ptr<ARDOUR::MidiRegion> region = boost::dynamic_pointer_cast<MidiRegion> (RegionFactory::create (src, plist, true, &Id));

	if(region.get()){

		midi_track_shared* route = osong->midi_tracks.get_item(trk_pod_idx);
		if(!route) return set_error("failed to get shared route struct!\n");

		boost::shared_ptr<ARDOUR::Playlist> playlist = track->playlist();
		playlist->add_region(boost::dynamic_pointer_cast<ARDOUR::Region>(region), start);

		*new_obj_idx = region->shared_obj->get_index();

		region->shared_obj->announce();
	}
	else return set_error("region create failed.");

	return 1;
}


bool
ADSong::del_part (ObjIdx pod_idx)
{
	//if the region is in a playlist, it will just be removed from the playlist.
	//if the region is not in a playlist, it will be deleted.
	//so to delete a Part and its pool region, call this fn twice.
	//-this is done to avoid having additional function args.
#define KEEP_REGIONS_WITH_NO_PLAYLIST_ENTRIES 1
#undef KEEP_REGIONS_WITH_NO_PLAYLIST_ENTRIES //temp.

	//question: can a region occur multiple times in a playlist ?

	dbg (0, "deleting part %i...", pod_idx);

	struct _region_shared* shared = osong->regions.get_item(pod_idx);
	if(shared && (shared->flags & deleted)){
		dbg(0, "is already deleted");
		return true;
	}

	boost::shared_ptr<Region> region = get_region_boost(pod_idx);
	if(!region) return set_error("failed to find region.");

	dbg(0, "use_count=%i", region.use_count());

	AyiPlaylist* playlist = (AyiPlaylist*)(region->playlist().get());
	if(playlist){
		//remove from playlist.
		//(deleting the ARDOUR region will fail if it belongs a playlist, it is not done automatically.)
		dbg(0, "playlist_name=%s", playlist->name().c_str());
		playlist->remove_region(region);

		//has it actually been removed?
		boost::shared_ptr<list<boost::shared_ptr<Region> > > regionlist = playlist->regions_touched(0, 10000000);
		dbg (0, "new playlist length: %i", regionlist->size());
	}

#ifdef KEEP_REGIONS_WITH_NO_PLAYLIST_ENTRIES
	printf("%s(): KEEP_REGIONS_WITH_NO_PLAYLIST_ENTRIES - deleting from playlist only...\n", __func__);
#endif
	if(!playlist){
		dbg (0, "region not in playlist. deleting...");

		RegionFactory::map_remove(region);

		//temporary, pending resolution of use_count issues:
		((AyyiAudioRegion*)region->shared_obj)->del();

		//dbg(0, "why ARDOUR::AudioRegion destructor not run? use_count=%i", region.use_count());
	}
#ifdef KEEP_REGIONS_WITH_NO_PLAYLIST_ENTRIES
#endif

	return true;
}


bool
ADSong::del_midi_part (ObjIdx pod_idx)
{
	if(pod_idx < 0) return set_error("index out of range: %i", pod_idx);

	dbg(0, "... idx=%i", pod_idx);
	boost::shared_ptr<Region> region = AyyiMidiRegion::get(pod_idx);
	if(!region) return set_error("failed to find region.");

	AyiPlaylist* playlist = (AyiPlaylist*)(region->playlist().get());
	if(playlist){
		//remove from playlist.
		//(deleting the ARDOUR region will fail if it belongs a playlist, it is not done automatically.)
		dbg(0, "playlist_name=%s", playlist->name().c_str());
		playlist->remove_region(region);

		//has it actually been removed?
		boost::shared_ptr<list<boost::shared_ptr<Region> > > regionlist = playlist->regions_touched(0, 10000000);
		dbg (0, "new playlist length: %i", regionlist->size());

#if 0
		//TODO should be done from a libardour signal?
		//     The meaning of this signal is ambiguous. The Part has been deleted, but the _Region_ still exists.
		//     ....isnt this done too early? so far we have only removed it from the playlist!
		msg_out_obj_deleted(AYYI_OBJECT_MIDI_PART, pod_idx); //notify clients
#endif
	}else{
		dbg (0, "region not in playlist. deleting...");

		RegionFactory::map_remove(region);

		//temporary, pending resolution of use_count issues:
		((AyyiMidiRegion*)region->shared_obj)->del();

		//dbg(0, "why ARDOUR::AudioRegion destructor not run? use_count=%i", region.use_count());
	}

	return true;
}


boost::shared_ptr<Region>
ADSong::get_region_boost (ObjIdx pod_index)
{
	boost::shared_ptr<Region> r;

	if(!session){ err("session not loaded!\n"); return r; }

	struct _region_shared* shared = osong->regions.get_item(pod_index);

	if(!shared) { dbg (0, "failed to get AyyiRegion. region_num=%u", pod_index); return r; }

	bool is_deleted = shared->flags & deleted;
	if(is_deleted){ dbg (0, "is deleted"); return r; }

	if(!shared->id) { dbg (0, "failed to get region id. region_num=%u", pod_index); return r; }
	uint64_t id = shared->id;

	dbg (2, "object=%p id=%Lu", shared->server_object, id);

	/*
		for some reason, regions that are "similar", are not stored in the session regionlist!! so the lookup below doesnt always work.

		?? what does "similar" mean? can we make them disimilar? hopefully, making the name and position distinct is enough.
	*/

	//TODO looks like the loop below can be replaced with:
	//boost::shared_ptr<Region> RegionFactory::region_by_id (const PBD::ID& id)

	const RegionFactory::RegionMap& region_map (RegionFactory::all_regions());
	for (RegionFactory::RegionMap::const_iterator i = region_map.begin(); i != region_map.end(); ++i) {
		PBD::ID test_id = i->first;
		if(test_id.get_id() == id){
			dbg (2, "found! region_num=%u name=%s", pod_index, shared->name);
			return i->second;
		}
	}

	/*
		search playlists:
		!!! we cant search playlists because the region we want may not yet be in any playlist. !!!!
	*/
#if 0
	char* playlist_name = shared->playlist_name;
	cout << "AD::get_region_boost(): looking for playlist: " << playlist_name << endl;
	AyiPlaylist* playlist = (AyiPlaylist*)core->session->playlist_by_name(playlist_name);
	if(!playlist){ printf("AD::%s() failed to get playlist. region_num=%u\n", __func__, pod_index);
		print_playlists();
		return r;
	}

	if(r = playlist->find_region_by_id(pod_index)){
	} else {
		printf("AD::%s(): failed to get region shared pointer. region_num=%u id=%Lu\n", __func__, pod_index, id);
		print_regions();
	}
#endif

	warn ("FIXME fn fails sometimes for 'similar' regions. Make sure regions have unique name.\n");
	return r;
}


boost::shared_ptr<Region>
ADSong::region_find (uint64_t id)
{
	/*
	-get a region object for the region with the given guid.

	*/

	dbg(0, "...");
	dbg(0, "looking for region with id %Lu", id);

	// convert id to a string - crazy oo!
	char string_char[256];
	sprintf(string_char, "%" PRIu64, id);
	dbg(0, "looking for region with id %s", string_char);
	string id_string = string(string_char);

	const RegionFactory::RegionMap& region_map (RegionFactory::all_regions());

	dbg (0, "iterating over audioregion list...");
	for (RegionFactory::RegionMap::const_iterator i = region_map.begin(); i != region_map.end(); ++i) {

		//boost::shared_ptr<Region> region = (*x).second;
		boost::shared_ptr<Region> region = i->second;
                        /* only use regions not attached to playlists */
		/*
                        if (r->playlist() == 0) {
                                child->add_child_nocopy (r->state (true));
                        }
		*/
		string test_id_string = region->id().to_s();
		dbg(0, "in loop: id=%s test=%s", id_string.c_str(), test_id_string.c_str());
		//if(test_id == id) return region;
		if(test_id_string == id_string) return region;
	}

	dbg (0, "failed to get playlist!");

	return boost::shared_ptr<Region>();
}


boost::shared_ptr<ARDOUR::Region>
ADSong::region_find (string& name)
{
	const RegionFactory::RegionMap& region_map (RegionFactory::all_regions());
	for (RegionFactory::RegionMap::const_iterator i = region_map.begin(); i != region_map.end(); ++i) {
		boost::shared_ptr<Region> region = i->second;
		if(region->name() == name) return region;
	}
	return boost::shared_ptr<Region>();
}


boost::shared_ptr<Region>
ADSong::region_find_by_source_id (uint64_t id)
{
	/*
	-get a region object for the region with the given source id. We just return the first match.

	*/
	boost::shared_ptr<Region> ret;

	const RegionFactory::RegionMap& region_map (RegionFactory::all_regions());

	if(!region_map.size()){ dbg(0, "region_map is empty."); return ret; }

	dbg (0, "looking for region with source id %Lu", id);

	for (RegionFactory::RegionMap::const_iterator i = region_map.begin(); i != region_map.end(); ++i) {

		boost::shared_ptr<Region> region = i->second;
		boost::shared_ptr<Source> source = region->source(0);
		PBD::ID test_id = source->id();
		dbg (0, "%Lu==%Lu?\n", id, test_id.get_id());
		if(test_id.get_id() == id) return region;
	}

	dbg (0, "failed to find region.");

	return ret;
}


void
ADSong::region_foreach (AudioRegion* region)
{
	dbg(0, "...");

	//tmp_audio_region_list.push_front (region);
}


bool
ADSong::region_id_ok (unsigned idx)
{
	//check idx in incoming message
	if(idx >= CONTAINER_SIZE * BLOCK_SIZE) err ("not ok! idx=0x%x\n", idx);
	return (idx < CONTAINER_SIZE * BLOCK_SIZE);
}


bool
ADSong::region_valid (region_shared* region)
{
	//check that the shared region pointer points to a shared region.
	if((uintptr_t)region<1024) return false;

	region_shared* test = NULL;
	while((test = osong->regions.next(test))){
		if(test == region) return true;
	}
	warn("not found. region=%p\n", region);

	/* this block can probably be deleted. Replaced by new iterator.

	int block_num, m;
	for(int r=0; r<=shm->regions.last; r++){
		block* block = msg_idx_get_block(r, &block_num, &m);

		region_shared* pod = (struct _region_shared*)block->slot[r];
		dbg(0, "    %i pod=%p", r, pod);
		if(!pod) continue;

		//Ayi::AyyiRegion* obj = (AyyiRegion*)pod->object;
		//AudioRegion* ardour_region = (AudioRegion*)pod->object;
		//if(!obj){ errprintf("%s\n", __func__); continue; }
		//if(!obj->region){ errprintf("%s() obj=%p obj->region=%p\n", __func__, obj, obj->region); continue; }

		//printf("  %s(): %p==%p\n", __func__, obj->region, shm->regions.slot[r]);
		//if(obj->region == shm->regions.slot[r]) return true;
		if(region == pod) return true;
	}
	*/
	return false;
}


bool
ADSong::region_set_start (Region* region, uint32_t pos)
{
	dbg(0, "region.start=%u old_position=%u new_position=%u", region->start(), region->position(), pos);

	session->begin_reversible_command ("part move");
	boost::shared_ptr<ARDOUR::Playlist> pl = region->playlist();
	if (pl) {
		XMLNode &before = pl->get_state();

		//session->add_undo (r.playlist()->get_memento());
		region->set_position((framepos_t)pos/* - region->start()*/);
		//session->add_redo_no_execute (r.playlist()->get_memento());

		XMLNode &after = pl->get_state();
		session->add_command(new MementoCommand<ARDOUR::Playlist>(*pl, &before, &after));
	}
	else return set_error("couldnt get playlist for region: %s", region->name().c_str());

	session->commit_reversible_command ();
	return true;
}


bool
ADSong::region_set_start (uint32_t shm_idx, uint32_t pos, int region_type)
{
	_region_base_shared* pod = get_region_pod_or_fail(shm_idx, region_type);

	//AudioRegion* region = core->playlist_region_find(idx);     //this function is unfortunately not yet working...

	Region* region = (Region*)(pod->server_object);
	dbg(1, "region_obj=%p name='%s'", region, region->name().c_str());

	if (region) {
		return region_set_start(region, pos);
	}
	return set_error("failed to find ARDOUR::Region");
}


bool
ADSong::region_set_length (uint32_t shm_idx, int media_type, uint32_t length)
{
	dbg(0, "idx=%u requested_length=%u media_type=%s", shm_idx, length, (media_type == AYYI_AUDIO) ? "AUDIO" : "MIDI");

	if(!length) return set_error("cannot set length to zero");

	_region_base_shared* pod = get_region_pod_or_fail(shm_idx, media_type);

	Region* region = (Region*)pod->server_object;
	if(region){
		region->set_length((framepos_t)length);
		return true;
	}
	return set_error("failed to get Ardour region. idx=%i\n", shm_idx);
}


bool
ADSong::audio_region_set_track (ObjIdx shm_idx, ObjIdx track_num)
{
	if(!osong->routes.idx_is_valid(track_num)) return false;

	region_shared* pod = osong->regions.get_item(shm_idx);
	AudioRegion* _region = (AudioRegion*)pod->server_object;
	if(boost::shared_ptr<ARDOUR::Region> region = _region->shared_from_this()){
		dbg(0, "part=%i new_track=%i", shm_idx, track_num);

		//note: it looks like its possible that empty tracks/routes dont have any playlists.

		if(boost::shared_ptr<AudioTrack> track = core->get_audio_track(track_num)){

			//remove the region from the old track:
			boost::shared_ptr<ARDOUR::Playlist> old_playlist = region->playlist();
			if(old_playlist) old_playlist->remove_region(region);

#if 0
			boost::weak_ptr<Playlist> playlist = AyiPlaylist::lookup_by_trackname(track->name());
#else
			boost::weak_ptr<ARDOUR::Playlist> playlist = track->playlist();
#endif
			if(playlist.use_count()){
				boost::shared_ptr<ARDOUR::Playlist> p = playlist.lock();
				dbg(0, "  setting playlist '%s' = %p", p->name().c_str(), p.get());
				p->add_region(region, region->position(), 1.0f);

				//check that the part now has the requested playlist
				dbg(0, "  done. checking...");
				if(_ayyi_shm_playlist* sp = osong->playlists.get_item(pod->playlist)){
					AyiPlaylist* playlist = (AyiPlaylist*)sp->object;
					if(playlist){
						PBD::ID id1 = playlist->id();
						PBD::ID id2 = p->id();
						if (id1.get_id() == id2.get_id()) {
							return true;
						}
						else return set_error("playlist id mismatch: %Lu %lu", id1.get_id(), id2.get_id());
					}
				}

				return set_error("unable to verify track change");
			} else {
				dbg(0, "playlist not found. track='%s'", track->name().c_str());
			}
		}
		else return set_error("track not found: %i", track_num);
	}
	return false;
}


bool
ADSong::midi_region_set_track (ObjIdx shm_idx, ObjIdx track_num)
{
	if(!osong->midi_tracks.idx_is_valid(track_num)) return set_error("invalid idx: track=%i", track_num);

	_midi_region_shared* pod = osong->midi_regions.get_item(shm_idx);
	MidiRegion* _region = (MidiRegion*)pod->server_object;
	if(!_region) return set_error("cannot find region. part=%i track=%i", shm_idx, track_num);
	if(boost::shared_ptr<ARDOUR::Region> region = _region->shared_from_this()){
		dbg(0, "part=%i new_track=%i", shm_idx, track_num);

		boost::shared_ptr<ARDOUR::Playlist> old_playlist = region->playlist();
		if(old_playlist) old_playlist->remove_region(region);

		if(boost::shared_ptr<MidiTrack> track = core->get_midi_track_boost(track_num)){
			boost::weak_ptr<ARDOUR::Playlist> playlist = track->playlist();
			if(!playlist.expired()){
				boost::shared_ptr<ARDOUR::Playlist> pl = playlist.lock();
				dbg(0, "  setting playlist '%s' = %p", pl->name().c_str(), pl.get());
				pl->add_region(region, region->position(), 1.0f);
				return true;
			} else {
				return set_error("playlist not found. track=", track->name().c_str());
			}

		}
		return true;
	}

	return set_error("failed");
}


bool
ADSong::region_trim_left (uint32_t region_idx, uint32_t left, int region_type)
{
	_region_base_shared* pod = get_region_pod_or_fail(region_idx, region_type);
	AudioRegion* region = (AudioRegion*)((struct _region_shared*)pod)->server_object;
	if(region){
		dbg(0, "pre-trim:  part=%i old_position=%u new_left=%u old_left=%i len=%i", region_idx, region->position(), left, region->position() + region->start(), region->length());
		region->trim_front((framepos_t)left);
		//region->trim_start(left, NULL); //doesnt do much.
		//region->set_start(left - region->position(), NULL);  //verification errors - dont understand... but i guess this is not an appropriate function to use.
		dbg(0, "post-trim: part=%i new_position=%u new_left=%u             len=%i", region_idx, region->position(), region->position() + region->start(), region->length());
		return true;
	}
	return false;
}


bool
ADSong::region_split (uint32_t region_idx, int media_type, uint32_t split_point)
{
	_region_base_shared* pod = get_region_pod_or_fail(region_idx, media_type);

	if(media_type == AYYI_MIDI) dbg(0, "FIXME midi");
	boost::shared_ptr<ARDOUR::Region> region = get_region_boost(region_idx);

	if(region){

		// check positions
		if(split_point <= region->position()/* + region->start()*/) return set_error("split point before part start");
		uint32_t end = region->position() + region->length();
		if(split_point >= end) return set_error("split point after part end. split=%u end=%u", split_point, end);

		_ayyi_shm_playlist* p = osong->playlists.get_item(pod->playlist);
		if(p){
			AyiPlaylist* playlist = (AyiPlaylist*)p->object;
			if(playlist){
				playlist->split_region(region, split_point); //this creates 2 new regions.

				// boost pointer for old region isnt being released. Lets try and clear the shm early.
				if(media_type == AYYI_AUDIO) delete (Ayi::AyyiAudioRegion*)region->shared_obj; 
				if(media_type == AYYI_MIDI)  delete (Ayi::AyyiMidiRegion*)region->shared_obj; 
				region->shared_obj = 0;

				return true;
			}
			return set_error("can't get playlist.");
		}
		return set_error("no playlist name");

#ifdef OLD_SPLIT_METHOD
		uint32_t start1  = region->position() + region->start();
		uint32_t start2  = split_point;
		uint32_t length1 = split_point - start1;
		uint32_t length2 = region->length() - length1;
		uint32_t end     = start1 + region->length();
		printf("  part=%i split_point=%Lumu (%usamples) current_start=%i\n", idx, val_u64, pos, region->position());

		if((split_point > start1) && (split_point < end)){

			//duplicate the region
			string name = region->name() + ".split";
			boost::shared_ptr<AudioRegion> new_region = boost::dynamic_pointer_cast<AudioRegion> (RegionFactory::RegionFactory::create(region, start2 - start1, length2, name));
			if(new_region){
				new_region->set_position(start1, NULL);
				//new_region->set_start(region->start() + length1, NULL);

				//new_region->trim_front(start2, NULL);

				dbg(0, "region created! shared=%p position=%u", region->shared_obj, new_region->position());
				AyyiRegion* new_object = (AyyiRegion*)new_region->shared_obj;
				if(!new_object){ printf("***%s(): no shared data for new region!\n", __func__); return; }
				int idx2 = new_object->get_index();

				dbg(0, "adding '%s' to playlist... pod_idx=%i", new_region->name().c_str(), idx2);
				char* playlist_name = pod->playlist_name;
				if(playlist_name){
					if(ARDOUR::Playlist* playlist = core->playlist_lookup_by_name(playlist_name)){
						//new_region->set_playlist(playlist);

						//note: when you set the playlist position, it also modifies the region position itself!?
						float times = 1;
						bool with_save = true;
						playlist->add_region(new_region, start1, times, with_save);
					}
				}else printf("***%s(): failed to get playlist name!\n", __func__);

				//truncate the 1st region
				printf("  regionA: setting length=%i\n", length1);
				region->set_length(length1, NULL);

				return;
			}

		}else{
			printf("  split point out of range! split_point=%u start=%u end=%u\n", split_point, start1, end);
			err=1; //this is probably the wrong error
		}
#endif
	}
	return set_error("region not found: %i", region_idx);
}


bool
ADSong::region_set_name (uint32_t region_idx, MediaType media_type, const char* _name)
{
	_region_base_shared* pod = get_region_pod_or_fail(region_idx, media_type);

	Region* region = (Region*)pod->server_object;
	if(region){
		string new_name = _name;
		region->set_name(new_name);
		return true;
	}
	return false;
}


#if 0
bool
ADSong::midi_region_set_name(uint32_t region_idx, const char* _name)
{
	_region_base_shared* pod = get_region_pod_or_fail(region_idx, AYYI_MIDI);

	Region* region = (Region*)pod->server_object;
	if(region){
		string new_name = _name;
		region->set_name(new_name);
		return true;
	}
	return false;
}
#endif


bool
ADSong::region_set_pblevel (uint32_t region_idx, double level)
{
	_region_shared* r = osong->regions.get_item(region_idx);

	AudioRegion* region = (AudioRegion*)r->server_object;
	if(region){
		region->set_scale_amplitude(level);
		return true;
	}
	return false;
}


#if 0
void
AD::on_region_removed(boost::weak_ptr<ARDOUR::Region> weak_region)
{
	dbg(0, "...");

	//attached to session->RegionRemoved, it will be called for *each* existing region.
	//-it is called just _before_ the objects are deleted.

	if (weak_region.expired()) { err("expired!"); return; }

	boost::shared_ptr<Region> region = weak_region.lock();

	//we get here because the RegionRemoved signal is emitted. However, _sometimes_ the ARDOUR region is not actually deleted.

	//as the region is not actually deleted (because of use_count issues?), we set the deleted flag:
#ifdef LATER //(currently done in Region)
	AudioRegion* ar = dynamic_cast<AudioRegion*>(region.get());
	if (ar) {
		dbg(0, "@@@ region=%s use_count=%i pod=%p flags=%i %s", region->name().c_str(), region.use_count(), shm_pod, ((_region_shared*)shm_pod)->flags, shm_pod->name);
		PBD::ID id = ar->id();
		if (id.get_id() == shm_pod->id) {
			dbg(0, "found. setting timeout...");
			g_timeout_add(1000, check_is_deleted_timeout, this);
		}
	}
#endif
	MidiRegion* mr = dynamic_cast<MidiRegion*>(region.get());
	if (mr) {
		dbg(0, "midi!");
/* TODO have no instance here - can we find the ayyi object from the region?         yes......
		dbg(0, "@@@ region=%s use_count=%i pod=%p flags=%i %s", region->name().c_str(), region.use_count(), shm_pod, ((_region_shared*)shm_pod)->flags, shm_pod->name);
		PBD::ID id = mr->id();
		if (id.get_id() == shm_pod->id) {
			dbg(0, "found. setting timeout...");
			//g_timeout_add(1000, check_is_deleted_timeout, this);
		}
*/
	}
}
#endif


void
ADSong::print_regions ()
{
	UNDERLINE;
	dbg(0, "...");

	osong->regions.print();

	const RegionFactory::RegionMap& region_map (RegionFactory::all_regions());
	if(region_map.size()){
		printf("%20s %10s %4s %10s\n", "", "obj", "idx", "id");
	}
	for(RegionFactory::RegionMap::const_iterator i = region_map.begin(); i != region_map.end(); ++i) {
		PBD::ID id = i->first;
		boost::shared_ptr<Region> region = i->second;

		boost::shared_ptr<AudioRegion> audio_region = boost::dynamic_pointer_cast<AudioRegion>(region);
		if(audio_region){
			AyyiRegion* ar = region->shared_obj;
			if(ar){
				region_shared* shared = (region_shared*)ar->shm_pod;
				if(!region_valid(shared)){ err ("shared not valid. shared=%p\n", shared); continue; }

				AudioRegion* ardour_region = (AudioRegion*)shared->server_object;
				if(ardour_region != region.get()) err("not same");
				Ayi::AyyiRegion* obj = (AyyiRegion*)ardour_region->shared_obj;
				if(!obj){ err ("shared->object.\n"); continue; }

				printf("%20s %10p %4i %8" PRIu64 "\n", region->name().c_str(), obj, obj->get_index(), id.get_id());
			}
			else printf("** missing **\n");
		}
		else{
			boost::shared_ptr<MidiRegion> midi_region = boost::dynamic_pointer_cast<MidiRegion>(region);
			if(midi_region){
				/*
				_midi_region_shared* shared = (_midi_region_shared*)region.get()->shared_obj->shm_pod;
				if(!region_valid(shared)){ err ("shared not valid. shared=%p\n", shared); continue; }
				*/

				Ayi::AyyiMidiRegion* obj = (AyyiMidiRegion*)midi_region->shared_obj;
				printf("%s%20s%s %p %4i %8" PRIu64 "\n", bold, region->name().c_str(), white, obj, obj ? obj->get_index() : -1, id.get_id());
				if(obj) obj->print();
			}
		}
	}

	//shared regions:
	printf("shared audio regions:\n");
	if(osong->regions.count_items()){
		printf("%2s  %10s %20s %10s %10s %10s\n", "", "id", "", "obj", "region", "playlist");
		int total = 0;
		region_shared* region = 0;
		//Shm_container<region_shared> c;
		while((region = osong->regions.next(region))){
			_ayyi_shm_playlist* p = osong->playlists.get_item(region->playlist);
			printf("%2i: %10" PRIu64 " %20s %10p %10p %10s\n", region->shm_idx, region->id, region->name, region->server_object, region, p ? p->name : "");
			total++;
			if(total > 512){ err("iterator killed\n"); break; }
		}
		printf("  total=%i last=%i\n", total, shm->regions.last);
	}
	else printf("    no regions\n");

	printf("shared midi regions:\n");
	_midi_region_shared* r = 0;
	while((r = osong->midi_regions.next(r))){
		printf("  %s\n", r->name);
	}
	UNDERLINE;
}


#define get_track_or_fail(A) ARDOUR::AudioTrack* track; {track = AyyidRoute::get_track(A); if(!track) return set_error("no track for %i", A); }

#define get_boost_track_or_fail(A, MEDIA_TYPE) boost::shared_ptr<ARDOUR::AudioTrack> track; {track = core->get_audio_track(A); if(!track){ print_routes(); return set_error("no track for %i", A); }}

#define get_boost_track_or_fail_(A, MEDIA_TYPE) boost::shared_ptr<ARDOUR::Track> track; { \
	track = (MEDIA_TYPE == AYYI_AUDIO) \
		? boost::dynamic_pointer_cast<Track>(core->get_audio_track(A)) \
		: boost::dynamic_pointer_cast<Track>(core->get_midi_track_boost(A)); \
	if(!track) return set_error("track not found: %i %i", A, MEDIA_TYPE); \
}


bool
ADSong::route_set_name (uint32_t route_num, const char* name)
{
	dbg(0, "...");

	get_track_or_fail(route_num);

	if(!track->set_name(string(name))){
		return set_error("IO::set_name() failed! name=%s\n", name);
	}

	return true;
}


bool
ADSong::route_set_mute (uint32_t route_num, int val)
{
	dbg(0, "...");

	get_track_or_fail(route_num);

	if(val > 1) printf("  unexpected MUTE value (%u)\n", val);
	track->set_mute(val, PBD::Controllable::GroupControlDisposition::NoGroup);

	return true;
}


bool
ADSong::route_set_arm (uint32_t route_num, int val)
{
	dbg(0, "val=%i", val);

	get_track_or_fail(route_num);

	track->set_record_enabled(val, PBD::Controllable::GroupControlDisposition::NoGroup);

	return true;
}


bool
ADSong::route_set_solo (uint32_t route_num, int val)
{
	dbg(0, "val=%i", val);

	get_track_or_fail(route_num);

	track->set_solo(val, PBD::Controllable::GroupControlDisposition::NoGroup);

	return true;
}


bool
ADSong::route_set_colour (uint32_t route_num, int val)
{
	dbg(0, "val=%i", val);
	if(val < -1 || val > 255) return set_error("colour out of range: %i", val);

	get_track_or_fail(route_num);

	route_shared* pod = osong->routes.get_item(route_num);
	if(!pod) return false;
	pod->colour = val;
	AyyidRoute* o = track->ayyi_track;
	if(o) o->emit_changed();
	return true;
}


bool
ADSong::add_track (string& name, int n_channels, uint32_t* new_obj_idx, uint32_t* new_obj_id)
{
	// note: the pod_index of the new track is returned to the client.

	dbg (0, "...");

	if(!(n_channels == 1 || n_channels == 2)) return set_error("n_channels %i - must be 1 or 2", n_channels);

	int input_channels = 1;
	int output_channels = n_channels;
	TrackMode mode = Normal;
	std::list<boost::shared_ptr<AudioTrack> > route = session->new_audio_track(input_channels, output_channels, mode);
	if (route.size()) {
		//TODO should we connect outputs, add panner?

		std::list<boost::shared_ptr<AudioTrack> >::iterator i = route.begin();

		AyyidRoute* object = (*i)->ayyi_track;
		if(!object) return set_error("shared obj not set.");

		dbg(0, "new object index: %i", object->get_index());
		*new_obj_idx = object->get_index();

	} else {
		return set_error("route.size()");
	}

	return true;
}


bool
ADSong::add_midi_track (string& name, uint32_t* new_obj_idx, uint32_t* new_obj_id)
{
core->print_playlists();

	std::list<boost::shared_ptr<MidiTrack> > track = session->new_midi_track(ChanCount(DataType::MIDI, 1), ChanCount(DataType::MIDI, 1));
	if(track.size()){
		AyyidMidiTrack* ayyi_object = (AyyidMidiTrack*)(*track.begin())->ayyi_track;
		dbg(0, "setting new object index: idx=%i", ayyi_object->get_index());
		*new_obj_idx = ayyi_object->get_index();

		dbg(0, "playlist=%i" , ((AyiPlaylist*)(*track.begin())->playlist().get())->get_index());

		boost::shared_ptr<Bundle> b = session->bundle_by_name ("hello");

	} else {
		return set_error("track not created");
	}
	return true;
}


bool
ADSong::del_track (ObjIdx pod_idx)
{
	// remove a track and any associated playlists. Regions used do not appear to be deleted.

	get_boost_track_or_fail(pod_idx, AYYI_AUDIO);

	//temp - check track is in the session routelist
	{
		bool found = false;
		boost::shared_ptr<ARDOUR::RouteList> routelist = session->get_routes();
		for(ARDOUR::RouteList::iterator i = routelist->begin(); i != routelist->end(); ++i){
			Route* r = (*i).get();
			if(r->id() == track->id()) found = true;
		}
		if(!found) warn("not found in RouteList\n");
	}

	if(track->ayyi_track) track->ayyi_track->unbind();
	string name = track->name();
	dbg(0, "deleting track idx=%i %s...", pod_idx, name.c_str());

	Session::instance->remove_route(track);
	if(track.use_count() != 1) warn("use_count=%li (should be 1?)\n", track.use_count());

	//temporary hack? does something with use_counts, and forces the old ARDOUR::AudioTrack to be deleted.
#if 0 //testing
	session->resort_routes();
#endif

	//temp - kill the shared object imediately to make debugging easier
	if(track->ayyi_track){
		osong->routes.remove_item(pod_idx);
//		track->ayyi_track->route = NULL;
	}

	print_routes();
	core->print_playlists();

	boost::weak_ptr<ARDOUR::Playlist> playlist = track->playlist();
	if(!playlist.expired()){
		{
			boost::shared_ptr<ARDOUR::Playlist> pl = playlist.lock();
			dbg(0, "removing playlist... %s", pl->name().c_str());
		}
		session->remove_playlist (playlist);

		if(!playlist.expired()){
			dbg(0, "playlist not expired! use_count=%li. killing...", playlist.use_count());
			boost::shared_ptr<ARDOUR::Playlist> _pl = playlist.lock();
			boost::shared_ptr<AyiPlaylist> pl = boost::dynamic_pointer_cast<AyiPlaylist> (_pl); //TODO playlist_lookup_by_trackname might as well return type AyiPlaylist?
			if(pl) pl->remove_pod();
		}
	}
	else warn("returned playlist has expired (not found): track='%s'", name.c_str());

	// try again in case have more than one playlist or playlist changed in the meantime.
	playlist = AyiPlaylist::lookup_by_trackname(name);
	if(!playlist.expired()){
		{
			boost::shared_ptr<ARDOUR::Playlist> pl = playlist.lock();
			dbg(0, "%sfound additional playlist%s %s", green, white, pl->name().c_str());
		}
		session->remove_playlist (playlist);

		if(!playlist.expired()){
			boost::shared_ptr<ARDOUR::Playlist> _pl = playlist.lock();
			boost::shared_ptr<AyiPlaylist> pl = boost::dynamic_pointer_cast<AyiPlaylist> (_pl);
			if(pl) pl->remove_pod();
		}
	}

	playlist = AyiPlaylist::lookup_by_trackname(name);
	if(!playlist.expired()){
		return set_error("failed to removed track playlist");
	}
	/*
		playlist = AyiPlaylist::lookup_by_trackname(name);
		if(!playlist.expired()){
			dbg(0, "%sfound additional playlist%s", green, white);
			session->remove_playlist (playlist);
		}
	*/

	core->verify_playlists();
	return true;
}


bool
ADSong::del_midi_track (ObjIdx pod_idx)
{
	get_boost_track_or_fail_(pod_idx, AYYI_MIDI);

	dbg(0, "deleting track %i %s ...", pod_idx, osong->midi_tracks.get_item(pod_idx)->name);

	string name = track->name();
	session->remove_route(track);

	session->resort_routes();

	print_routes();

	boost::weak_ptr<ARDOUR::Playlist> playlist = track->playlist();
	PBD::ID id1;
	if(!playlist.expired()){
		{
			boost::shared_ptr<ARDOUR::Playlist> pl = playlist.lock();
			id1 = pl->id();
			dbg(0, "removing playlist... %s", pl->name().c_str());
		}
		Session::instance->remove_playlist (playlist);

		if(!playlist.expired()){
			warn("playlist not expired! use_count=%li. killing...", playlist.use_count());
			boost::shared_ptr<ARDOUR::Playlist> _pl = playlist.lock();
			boost::shared_ptr<AyiMidiPlaylist> pl = boost::dynamic_pointer_cast<AyiMidiPlaylist> (_pl); //TODO AyiPlaylist::lookup_by_trackname might as well return type AyiPlaylist?
			if(pl) pl->remove_pod();
			else return set_error("failed to cast to AyiPlaylist");
		}
	}
	else warn("playlist has expired");

	//try again in case have more than one playlist or playlist changed in the meantime.
	//-if the first playlist has not expired, its quite likely we will just get the same playlist again.
	boost::weak_ptr<ARDOUR::Playlist> playlist2 = AyiPlaylist::lookup_by_trackname(name);
	if(playlist2.use_count() && !playlist2.expired()){
		PBD::ID id2;
		const char* name = NULL;
		{
			boost::shared_ptr<ARDOUR::Playlist> pl = playlist2.lock();
			id2 = pl->id();
			name = pl->name().c_str();
		}

		if(id1 != id2){
			dbg(0, "%sfound additional playlist%s %s", green, white, name);
			session->remove_playlist (playlist2);
		}

		if(!playlist2.expired()){
			boost::shared_ptr<ARDOUR::Playlist> _pl = playlist2.lock();
			boost::shared_ptr<AyiMidiPlaylist> pl = boost::dynamic_pointer_cast<AyiMidiPlaylist> (_pl);
			if(pl) pl->remove_pod();
		}
	}

	return true;
}


typedef Evoral::Note<Evoral::Beats> NoteType;

bool
ADSong::add_midi_note (ObjIdx region_idx, int note, nframes_t start, nframes_t length, uint32_t velocity, uint32_t* new_obj_idx, uint32_t* new_obj_id, GError **error)
{
	if(note < 1 || note > 127) return set_error("note out of range: %i\n", note);
	if(length < 1 || length > 44100*60) return set_error("note length out of range: %i len=%u\n", note, length);
	if(velocity > 127) return set_error("velocity out of range");

	boost::shared_ptr<ARDOUR::MidiRegion> region = AyyiMidiRegion::get(region_idx);
	if(region){
		dbg(0, "region=%s", region->name().c_str());
		boost::shared_ptr<ARDOUR::MidiSource> midi_src = region->midi_source();
		if(midi_src.get()){

			uint8_t chan = 0;
			BeatsFramesConverter converter (Session::instance->tempo_map(), 0);
			Evoral::Beats time = converter.from(start);
			BeatsFramesConverter converter2 (Session::instance->tempo_map(), start);
			Evoral::Beats len = converter2.from(length);
			uint8_t vel = velocity;
			dbg(0, "note=%i time=%u length=%u", note, start, length);

			boost::shared_ptr<ARDOUR::MidiModel> model = midi_src->model();

			// there is a bug in Evoral such that no two notes can have the same start time,
			// so ensure that the start is not the same as an existing note:

			MidiModel::Notes notes = model->notes();
			for (MidiModel::Notes::iterator n = notes.begin(); n != notes.end(); ++n) {
				//dbg(0, "   note! %i %f", (*n)->note(), (*n)->time());
				if((*n)->time() == time) time += Evoral::Beats(1.0 / 384.0);
			}

			const boost::shared_ptr<NoteType> new_note(new NoteType(chan, time, len, (uint8_t)note, vel));

			MidiModel::NoteDiffCommand* cmd = model->new_note_diff_command("add note");
			cmd->add(new_note);
			model->apply_command(*session, cmd);

			dbg(0, "new size: %i", model->notes().size());

			region->shared_obj->emit_changed();
			dbg(0, "note added.");
			return true;
		}
		warn("failed to get MidiSource.\n");
	}
	return set_error("failed to get MidiRegion.");
}


bool
ADSong::update_note_list (ObjIdx part_idx, std::list<struct _shm_midi_note*>* notes)
{
	_region_base_shared* region_pod = get_region_pod_or_fail(part_idx, AYYI_MIDI);

	ARDOUR::Region* region = (Region*)region_pod->server_object;
	AyyiMidiRegion* oshared = region ? (AyyiMidiRegion*)region->shared_obj : NULL;
	if (!oshared) return set_error("cannot find part.");

	dbg(1, "region_pod=%p oshared=%p part=%i array_size=%i.", oshared, region_pod, part_idx, notes->size());

	oshared->modify_events(notes);

	return true;
}


bool
ADSong::del_midi_note (ObjIdx region_idx, int note_idx)
{
	boost::shared_ptr<ARDOUR::MidiRegion> region = AyyiMidiRegion::get(region_idx);
	if(region){
		dbg(0, "region=%s", region->name().c_str());
		boost::shared_ptr<ARDOUR::MidiSource> midi_src = region->midi_source();
		if(midi_src.get()){
			boost::shared_ptr<ARDOUR::MidiModel> model = midi_src->model();
			MidiModel::NoteDiffCommand* cmd = model->new_note_diff_command("delete note");

			MidiModel::Notes notes = model->notes();
			const Evoral::Sequence<Evoral::Beats>::NotePtr note;
			int i = 0;
			for (MidiModel::Notes::iterator n = notes.begin(); n != notes.end(); ++n) {
				if(i++ < note_idx) continue;

				cmd->remove(*n);
				model->apply_command(*session, cmd);
				dbg(0, "new size: %i", model->notes().size());

				region->shared_obj->emit_changed();
				return true;
			}

			return set_error("note not found %i", note_idx);
		}
		return set_error("failed to get Midi source");
	}
	return set_error("failed to get MidiRegion");
}


/*
 *  Insert a new automation curve point into a list.
 */
bool
ADSong::auto_add_point (ObjIdx route_idx, guint control_idx, guint plugin_slot, double when, double val)
{
	dbg(2, "...");
	if(plugin_slot != 0) dbg(0, "plugin_slot=%i not tested with plugin_slot!=0", plugin_slot);

	dbg(1, "route_idx=%i route_count=%i", route_idx, osong->routes.count_items());
	get_boost_track_or_fail(route_idx, AYYI_AUDIO);

	switch(control_idx){
		case VOL:
			{
				boost::shared_ptr<ARDOUR::GainControl> gain_control = track->amp()->gain_control();
				boost::shared_ptr<ARDOUR::AutomationList> list = gain_control->alist();
				auto_list_print(list);
				unsigned old_size = list->size();
				dbg(1, "VOL: old_size=%i when=%.2f new_val=%.2f", old_size, when, val);

				list->editor_add(when, val, false); // Triggers AutomationList.StateChanged()

				list = track->gain_control()->alist();
				dbg(1, "VOL: new_size=%i", list->size());

				bool found = false;
				for(Evoral::ControlList::iterator i = list->begin(); i != list->end(); ++i){
					if((*i)->when == when){ found = true; break; }
				}
				if(!found) err ("insert failed? not found. VOL: size=%i\n", list->size());

#if 0
				if(list->automation_state() != Play){
					list->set_automation_state(Play);
				}
#endif
				return true;
			}
			break;
		case PAN:
			dbg(1, "PAN...");
			{
				boost::shared_ptr<ARDOUR::Panner> panner = track->panner();
				if(panner){
					if(panner->out().n_total()){
						boost::shared_ptr<AutomationControl> control = panner->pannable()->pan_azimuth_control;
						boost::shared_ptr<ARDOUR::AutomationList> list = control->alist();
						list->editor_add(when, val, false);
						return true;
					}
					else return set_error("n_total");
				}
				else return set_error("route has no panner");
			}
			break;
		default:
			dbg(1, "control_idx=%i", control_idx);

			control_idx -= 2;
			if(control_idx) dbg(1, "control_idx=%i !! - only works for 1st plugin control.", control_idx);

			boost::shared_ptr<Processor> p = track->nth_processor(plugin_slot);
			if(p){
				//boost::shared_ptr<ARDOUR::AutomationList> list = processor_get_automationlist(p, control_idx);
				boost::shared_ptr<Evoral::ControlList> list = AyyidRoute::processor_get_automationlist(p, control_idx);
				list->add(when, val);

				//shm automation lists are only created when the controls has events, so we need to check if it already exists or not:
				AyyidRoute* ayyi_route = track->ayyi_track;
				MixerTrack<ARDOUR::Route>* ayyi_channel = track->ayyi_mixer_track;
				if(!ayyi_channel->plugin0_automation){
					ayyi_route->automation_slot_init(plugin_slot);
				}
				AyyiList* shmlist = ayyi_channel->plugin0_automation;
				if(!shmlist) return set_error("track has no plugin controls setup.");

				track_shared* mixer_track = core->mixer.tracks.get_item(ayyi_route->get_index());
				if(!mixer_track) err ("failed to get mixer_track. idx=%i", ayyi_route->get_index());

				const Evoral::Parameter& param = list->parameter();
				dbg(1, "param=%i", param.id());
				//int plugin_idx = mixer_track->plugin[plugin_slot];
				struct _ayyi_list* existing  = ayyi_channel->plugin0_automation->find(param.id()/*, plugin_idx*/);
				if(!existing) dbg(0, "control param not found in shm - need to add it...");
				if(!existing) ayyi_route->add_automation_type(plugin_slot, control_idx);

				track->ayyi_track->set_plugin_automation();
				return true;
			}

			warn ("cannot find processor for control_idx=%i", control_idx);
			break;
	}
	return false;
}


void
ADSong::auto_list_print (boost::shared_ptr<const ARDOUR::AutomationList> list)
{
	// the libardour list is now a private property of the curve.

	ARDOUR::AutomationList list_copy = *list; //just here temporarily to get round a compilation problem

	//AutomationList* list = (AutomationList*)&curve._list;
	dbg(0, "size=%i", list->size());
	if(list->size()){
		double old_when = 0;
		for(ARDOUR::AutomationList::const_iterator i = list_copy.begin(); i != list_copy.end(); i++){
			p(0, "       %10.2f %4.2f", (*i)->when, (*i)->value);

			if((*i)->when < old_when) err ("automation list not sorted.\n");
			old_when = (*i)->when;
		}
		p(0, "---------------\n");
	}
}


void
ADSong::print_routes ()
{
	dbg(0, "...");
	int total = 0;

	p(0, "----------------------------------------------------------");

	//typedef std::list<boost::shared_ptr<Route> > RouteList;
	boost::shared_ptr<ARDOUR::RouteList> routelist = session->get_routes();
	if(routelist->size() > 0){
		p(0, "%20s %10s %4s %16s %3s panners", "", "obj", "idx", "id", "use");

		for(ARDOUR::RouteList::iterator i = routelist->begin(); i != routelist->end(); ++i){
			Route* r = (*i).get();
			if(dynamic_cast<AudioTrack*>((*i).get()) == 0) { p(0, "%20s [not a track]                             %i", r->name().c_str(), 0/*(*i)->panner().outputs.size()*/); continue; }

			Ayi::AyyidRoute* ayyi_obj = r->ayyi_track;
			if(!ayyi_obj){ err ("shared->object.\n", ""); continue; }

			if (!osong->routes.item_is_valid(ayyi_obj->route)) { err ("shared not valid. shared=%p\n", ayyi_obj->route); continue; }

			PBD::ID id = r->id();

			p(0, "%20s %10p %4i %16Lu %3li %6i", r->name().c_str(), ayyi_obj, ayyi_obj->get_index(), id.get_id(), (*i).use_count(), 0/*(*i)->panner().outputs.size()*/);
		}
	}
	else p(0, " <ardour routelist empty>");

	//shared routes:
	p(0, "\n%2s  %16s %20s  %8s", "", "id", "", "obj");
	block* block = shm->routes.block[0];
	for(int i=0;i<BLOCK_SIZE;i++){
		struct _route_shared* shared_pod = (struct _route_shared*)block->slot[i];
		if(!shared_pod) continue;
		p(0, "%2i: %16Lu %20s %p", i, shared_pod->id, shared_pod->name, shared_pod->object);
		total++;
	}

	total = routelist->size();
	p(0, "  routelist_total=%i shm_last=%i", total, shm->routes.last);
	p(0, "----------------------------------------------------------");
}


template<>
void
ShmContainer<struct _filesource_shared>::set_container ()
{
	container = &core->song.osong->get_table()->filesources;
	container->obj_type = AYYI_OBJECT_FILE;
}


template<>
void
ShmContainer<connection_shared>::set_container ()
{
	container = &core->song.osong->get_table()->connections;
	container->obj_type = AYYI_OBJECT_ROUTE;
}


template<>
void
ShmContainer<route_shared>::set_container ()
{
	container = &core->song.osong->get_table()->routes;
	container->obj_type = AYYI_OBJECT_AUDIO_TRACK;
}


template<>
void
ShmContainer<region_shared>::set_container ()
{
	container = &core->song.osong->get_table()->regions;
	container->obj_type = AYYI_OBJECT_AUDIO_PART;
}


template<>
void
ShmContainer<playlist_shared>::set_container ()
{
	container = &core->song.osong->get_table()->playlists;
}

template<>
void
ShmContainer<midi_track_shared>::set_container ()
{
	container = &core->song.osong->get_table()->midi_tracks;
	container->obj_type = AYYI_OBJECT_MIDI_TRACK;
}


template<>
void
ShmContainer<struct _shm_vst_info>::set_container ()
{
	container = &core->song.osong->get_table()->vst_plugin_info;
}


};
