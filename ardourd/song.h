/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#pragma once

#include "ayyi/ayyi_time.h"
#include "ayyi/interface.h"
#include "ayyi/ayyi++.h"
#include "song_shm.h"
#include "ayyi-ardour/shared.h"

using namespace std;

namespace ARDOUR {
	class MidiRegion;
}

namespace Ayi {

class ADSong
{
  public:
	                                  ADSong();

	SongShm*                          osong;
	struct _song_shm*                 shm;
	void                              shm_init                   ();

	void                              set_end                    (uint32_t);

	void                              samples2pos                (Ayi::SongPos*, uint32_t);
	int64_t                           samples2mu                 (uint32_t);

	bool                              add_file                   (string& name, uint32_t* new_obj_idx);
	bool                              del_file                   (ObjIdx);
	bool                              add_part                   (std::string& name, uint64_t id, bool from_region, uint32_t src_region_idx, uint32_t trk_pod_idx, nframes_t start, ARDOUR::framecnt_t inset, nframes_t length, uint32_t* new_obj_idx, uint32_t* new_obj_id);
	bool                              add_midi_part              (std::string& name, uint64_t id, uint32_t trk_pod_idx, nframes_t start, ARDOUR::framecnt_t inset, nframes_t length, uint32_t* new_obj_idx, uint32_t* new_obj_id);
	bool                              del_part                   (ObjIdx);
	bool                              del_midi_part              (ObjIdx);

	boost::shared_ptr<ARDOUR::Region> get_region_boost           (ObjIdx);
	boost::shared_ptr<ARDOUR::Region> region_find                (uint64_t id);
	boost::shared_ptr<ARDOUR::Region> region_find                (std::string&);
	boost::shared_ptr<ARDOUR::Region> region_find_by_source_id   (uint64_t id);
	void                              region_foreach             (ARDOUR::AudioRegion* region);
	bool                              region_id_ok               (unsigned id);
	bool                              region_valid               (struct _region_shared*);
	bool                              region_set_start           (ARDOUR::Region*, uint32_t pos);
	bool                              region_set_start           (uint32_t region_idx, uint32_t pos, int region_type);
	bool                              region_set_length          (uint32_t shm_idx, int media_type, uint32_t length);
	bool                              audio_region_set_track     (ObjIdx, ObjIdx track);
	bool                              midi_region_set_track      (ObjIdx, ObjIdx track);
	bool                              region_trim_left           (uint32_t region_idx, uint32_t left, int region_type);
	bool                              region_split               (uint32_t region_idx, int media_type, uint32_t pos);
	bool                              region_set_name            (uint32_t region_idx, MediaType media_type, const char* name);
	bool                              region_set_pblevel         (uint32_t region_idx, double level);
	void                              print_regions              ();

	bool                              route_set_name             (uint32_t route_num, const char* name);
	bool                              route_set_mute             (uint32_t route_num, int val);
	bool                              route_set_arm              (uint32_t route_num, int val);
	bool                              route_set_solo             (uint32_t route_num, int val);
	bool                              route_set_colour           (uint32_t route_num, int val);

	bool                              add_track                  (std::string& name, int n_channels, uint32_t* new_obj_idx, uint32_t* new_obj_id);
	bool                              add_midi_track             (std::string& name, uint32_t* new_obj_idx, uint32_t* new_obj_id);
	bool                              del_track                  (ObjIdx);
	bool                              del_midi_track             (ObjIdx);

	bool                              add_midi_note              (ObjIdx part, int note, nframes_t start, nframes_t length, uint32_t velocity, uint32_t* new_obj_idx, uint32_t* new_obj_id, GError**);
	bool                              update_note_list           (ObjIdx part, std::list<struct _shm_midi_note*>*);
	bool                              del_midi_note              (ObjIdx part, int note_idx);

	bool                              auto_add_point             (ObjIdx route, guint auto_type, guint idx3, double val1, double val2);
	static void                       auto_list_print            (boost::shared_ptr<const ARDOUR::AutomationList> list);

	void                              print_routes               ();

};

};     // namespace Ayi
