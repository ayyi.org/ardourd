/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <sys/types.h>
#include <string>
#include <list>
#include <glib-2.0/glib.h>

#include "pbd/signals.h"

#include "ayyi/utils.h"
#include "ayyi/inc/song.h"
#include "song_shm.h"

#define shm_err(A, ...) errprintf2("SongShm", __func__, (char*)A, ##__VA_ARGS__)


namespace Ayi {

SongShm::SongShm()
	: regions(this),
	  filesources(this),
	  connections(this),
	  routes(this),
	  midi_tracks(this),
	  midi_regions(this),
	  playlists(this)
#ifdef VST_SUPPORT
	,  vst_info(this)
#endif
{
	_content_type = SEG_TYPE_SONG;
	size          = SONG_SEGMENT_SIZE; //FIXME overridden by attach_simple()
	table_size    = sizeof(Ayi::SongHeader);
}


void
SongShm::set_transport_frame(uint32_t frame)
{
	if(frame > 44100 * 4 * 1000){ shm_err("frame out of range! %u\n", frame); return; }

	((Ayi::SongHeader*)address)->transport_frame = frame;
}

}
