/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <cerrno>
#include <cstdlib>
#include <signal.h>
#include <unistd.h>

#include <pbd/xml++.h>
#include <pbd/pthread_utils.h>
extern "C" {
#include "tlsf/src/tlsf.h"
}

#include <ardour/ardour.h>
#include <ardour/tempo.h>
#include <ardour/audio_track.h>
#include <ardour/audioregion.h>
#include <ardour/region.h>
#include <ardour/midi_region.h>
#include <ardour/midi_source.h>
#include <ardour/midi_model.h>
#include <ardour/audiofilesource.h>
#include <ardour/bundle.h>
#include <ardour/session.h>

#include <sigc++/bind.h>

#include <ayyi/ayyi_time.h>
#include <ayyi/ayyi_types.h>
#include "dbus_object.h"

#ifdef VST_SUPPORT
  #include "fst.h"
#endif

extern int debug_source;
extern int debug_region;
extern int debug;
extern int debug_general;

#include "ayyi/utils.h"
#include "app.h"
#include "midi.h"

#include "ayyi/container-impl.cc"

#define dbg(N, A, ...){ if(N <= debug) dbgprintf("AyyiMidiNote", __func__, (char*)A, ##__VA_ARGS__); }
#define warn(A, ...) warnprintf2("AyyiMidiNote", __func__, (char*)A, ##__VA_ARGS__)
#define err(A, ...) errprintf2("AyyiMidiNote", __func__, (char*)A, ##__VA_ARGS__)
#define region_dbg(L, A, ...) { if(L <= debug_region) dbgprintf("AyyiMidiRegion", __func__, (char*)A, ##__VA_ARGS__); }

