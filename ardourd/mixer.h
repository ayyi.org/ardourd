/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __ad_mixer_h__
#define __ad_mixer_h__
#include "ayyi/ayyi_time.h"
#include "ayyi/interface.h"
#include "ayyi/ayyi++.h"
#include "ayyi-ardour/shared.h"
#include "mixer_shm.h"

namespace Ayi {

class ADMixer
{
	friend class AD;
  public:
	ADMixer();
	bool                            set_level          (ObjIdx channel, float level);
	bool                            set_pan            (ObjIdx channel, float level);
	bool                            set_enable_pan     (ObjIdx channel, bool enable);
	bool                            set_plugin         (int channel, unsigned plugin_slot, char* plugin_name);
	bool                            set_plugin_bypass  (int channel, unsigned plugin_slot, bool bypass);

	bool                            del_aux            (ObjIdx channel, unsigned aux_num);
	bool                            set_aux_output     (unsigned tnum, unsigned aux_num, unsigned connection_idx);
	bool                            set_aux_level      (unsigned channel, unsigned aux_num, float level);

	void                            shm_init           ();

	MixerShm*                       omixer;

	Ayi::ShmContainer<Ayi::_ayyi_channel>
	                                tracks;

  private:
	struct _shm_seg_mixer*          shm;

	boost::shared_ptr<ARDOUR::Send> get_send_for_route (ARDOUR::Route*, unsigned aux_num);

	void                            meter_update       ();
};

};     // namespace Ayi

#endif //__ad_mixer_h__
