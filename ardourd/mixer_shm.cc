/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "ayyi/inc/mixer.h"
#include "mixer_shm.h"

namespace Ayi {

MixerShm::MixerShm()
	//: tracks(this)
	: plugins(this)
{
	_content_type = SEG_TYPE_MIXER;
	size          = MIXER_SEGMENT_SIZE;
	table_size    = sizeof(Ayi::_shm_seg_mixer);
}

}
