Paul Davis (Philadelphia, PA) was and is the primary author of Ardour.

Taybin Rutkin (New York, NY) has contributed lots of code, and was
particularly responsible for the use of XML in the state persistence
aspect of the program. He also (re)wrote the soundfile library code to
use LRDF. In addition he was responsible for the integration of the
gettext system and the compose() templates that make Ardour's
internationalization possible. He has consistently made suggestions
that resulted in significantly more elegant code and design. Taybin
also set up and oversees the Mantis bug reporting system used by all
of us, and tends to take on "infrastructure" issues such as the switch
to SCons as the build system.

Jesse Chappell (Washington, DC) keeps on making major contributions to
Ardour. It almost seems pointless to list the things he has worked on
because there is so much of it. They include being able to handle
multichannel tracks, a major change in the program's design and
capabilities, and many additions/improvements to the GTK GUI,
including mouse zoom mode and the route params editor. Jesse was the
first person to just "walk in" and understand the Ardour codebase.

Marcus Andersson (Karlstad, Sweden) contributed a number of useful
patches and worked on the dB-related issues in the gain stages and
metering, other numeric computations, and much useful debugging, bug
reporting and analysis.

Jeremy Hall (Sterling, VA) contributed several patches and worked
intensively on ksi_ardour, the keystroke-based-interface to libardour
designed for sight-impaired and GUI-averse users.

Steve Harris (Southampton, UK) contributed code to handle speed-based
interpolation, an area I did not want to get my head around, as well
as dithering, panning, metering and other DSP-centric issues.  He also
wrote the LRDF library used by Ardour's soundfile library code, not to
mention dozens of LADSPA plugins that make Ardour a genuinely useful
tool.

Tim Mayberry (Brisbane, Australia) did lots and lots and lots of work
on mouse-driven editing.

Nick Mainsbridge <beatroot@optushome.com.au> is responsible for many
improvements to the rulers, and several other tweaks. 

Colin Law wrote the code that supports Ardour's integration with
the CMT Animatics engine. He was also very involved in refactoring the
GUI code design to support different kinds of tracks, thus laying the
groundwork for extending ardour's domain to include MIDI and video.

Gerard van Dongen (Rotterdam, Netherlands) has done a set of scattered
but critical work with a vague focus on the mouse. He has made some
particularly important fixes to the incredibly hairy code that draws
automation curves. Gerard also helped out with a workshop on Ardour
held at the Dutch Electronic Arts Festival, Rotterdam, in November
2004.

Sampo Savolainen became a major contributor of minor patches as Paul
began working full time for a while. He fixed numerous bugs, some on
mantis and some not, fairly continuously for several months. He then
moved on to write SSE assembler routines to handle the CPU-hungry
metering and mixing routines.

Brian Ahr contributed many small fixes for ardour 2.0.

Smaller (but not necessarily minor) patches were received from the
following people:

  Mark Stewart
  Sam Chessman (Reston, VA)
  Jack O'Quin (Austin, TX)
  Matt Krai
  Ben Bell
  Thomas Charbonnel (Lyon, France)
  Robert Jordens
  Christopher George
  Rob Holland
  Joshua Leachman
  Per Sigmond
  Nimal Ratnayake