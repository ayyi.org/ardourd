Ademas de aquellos que aparecen mencionados en el archivo AUTHORS.es,
las siguientes personas estan entre aquellas que ofrecieron soporte
financiero, observaciones de disenio e ideas, animo, aportes, reportes
de bugs (errores en codigo) y mucho mas durante el desarrollo del
pre lanzamiento de Ardour. Generalmente, ellos sufrieron de dias de 
frustracion y soportaron cientas de revisiones de CVS sin quejas.

Ninguna plegaria o agradecimiento posible es suficiente por sus
contribuciones a Ardour.

(en ningun orden en particular)

Joe Hartley
Marek Peteraj
Ryan Gallagher
Rob Holland
Jan Depner
Bryan Koschmann
Patrick Shirkey
Rob Fell
Ant <avan@uwm.edu>
Chris Ross
Joshua Pritikin
Rohan Drape
Johan De Groote
Bob Ham

Quisiera destacar particularmente a:

Frank Carmickle
       - primer brinadador de soporte financiero a Ardour, 
         instigador de ardour/ksi.

Ron Parker (de Minneapolis, MN)
       - primer usuario de Ardour en un estudio comercial,
         contribuidor financiero, iniciador mayor de
         funcionalidad MTC and MMC.

DuWayne Holsbeck
       - dominio de disenio de sponsors linuxaudiosystems.com ,
         configuro la base de datos bugzilla y mas.

Havoc Pennington & Owen Taylor 
       - brindaron mucha asistencia en cuanto a comprension, 
         diagnostico y simplificacion de GTK+.

Tom Pincince (de Shasta, CA)
       - contribuyo continuas y exceles observaciones de disenio y
         racionalizaciones, asi como informacion acerca de
	 estaciones de trabajo de audio digital existentes.

Tambien quisiera agradecer a Jim Hamilton de Rittenhouse Recording,
Philadelphia, for el companierismo, amistad y vision que demostro
al permitirme usar el estudio como base de desarrollo para Ardour.
Conoci a Jim tocando percusion jazz en un evento de recaudacion de
fondos para celebrar el 30imo aniversario de la guarderia escolar de
nuestros hijos. Desde entonces me ha abierto los ojos a ambos, la musica
misma, el proceso de hacer musica y la vida de un musico trabajando.
Jim es el mejor y mas inventivo percusionista que haya visto, y uno
de los mejores que he oido. El siempre ha creido en las implicaciones
filosoficas y sociales de Ardoury su soporte e interes han sido vitales
en el desarrollo de Ardour. Es solo cuestion de tiempo hasta que 
Rittenhouse Recording use Ardour.








Nota de Traduccion (Spanish Translation Note)
---------------------------------------------
#Nota del tipeo:la letra pronunciada ENIE aparece en este archivo 
#como ni (letra "n" y letra "i") para mayor compatibilidad con todos 
#los visores de texto.
#Asi mismo no se han aplicado las tildes(acentos).
#Estos no son errores de tipeo. Si llegara a encontrar algun otro error
#en cualquiera de los archivos con extension ".es" por favor 
#hagamelo saber a alexkrohn@fastmail.fm
#  Muchas gracias
#      Alex