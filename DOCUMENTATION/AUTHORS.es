Paul Davis (de Philadelphia, PA) fue y es el principal autor de Ardour.

Taybin Rutkin (de New York, NY) ha contribuido grandes cantidades de 
codigo y fue particularmente responsable por el uso de XML en el 
aspecto de persistencia de estado del programa. Tambien escribio y 
reescribio el codigo relativo a la libreria soundfile (archivo de 
sonido) para que use LRDF. Ademas fue responsable por la integracion
del sistema gettext y los esquemas compose() que hacen posible la 
internacionalizacion de Ardour. Ha hecho constantes sugerencias las
cuales resultaron en codigo y disenio significativamente mas elegantes.
Taybin tambien configuro y administra el sistema Mantis de reporte de
bugs (errores de codigo) usado por todos nosotros.

Jesse Chappell (de Washington, DC) continua haciendo grandes 
contribuciones a Ardour, particularmente en el hecho de que Ardour sea
capaz de manejar pistas multicanal, un cambio mayor en el disenio del
programa y capacidades. Tambien hizo muchos agregados y mejoras a la
interfaz visual de GTK, incluyendo el modo de zoom mediante mouse y el
editor de parametros de ruteo. Jessie fue la primera persona en "pasar"
y entender las bases del codigo de Ardour.

Marcus Andersson (de Karlstad, Sweden) contribuyo un numero de patches
(actualizaciones) utiles y trabajo con los problemas relacionados a "dB"
en las etapas de gain (volumen) y vumetros, otras computaciones numericas
y muy util debugging, reporte de bugs y analisis.

Jeremy Hall (de Sterling, VA) contribuyo varios patches y trabajo 
intensamente con ksi_ardour, que es la interfase basada en teclas de la
libreria libardour diseniada para no videntes y amantes de lineas de 
comando (consolas).

Steve Harris (de Southampton, UK) contribuyo codigo para manejar
interpolacion basada en velocidad, un area en la cual no me queria meter,
asi como dithering, balance, vumetros y otros temas relacionados con 
procesamiento de senial. Tambien escribio la libreria LRDF usada por 
el codigo de Ardour de la libreria soundfile, sin mencionar docenas 
de plugins LADSPA que hacen a Ardour una herramienta genuinamente util.

Tim Mayberry (de Brisbane, Australia) hizo mucho, mucho, mucho trabajo
en cuanto a edicion basada en mouse.

Nick Mainsbridge <beatroot@optushome.com.au> es responsable de muchas
mejoras a los medidores/lineas/referencias de tiempo.

Colin Law <> escribio el codigo que soporta la integracion de Ardour
al sistema de Animatics CMT. Tambien se ocupo en el redisenio del codigo
de la interfase visual para soportar diferentes tipos de pistas.

Mas pequenios (preo no necesariamente menores) patches fueron recividos
de las siguientes personas:

  Mark Stewart
  Sam Chessman (Reston, VA)
  Jack O'Quin (Austin, TX)
  Matt Krai
  Ben Bell
  Gerard vanDongen (Rotterdam, Netherlands)
  Thomas Charbonnel (Lyon, France)
  Robert Jordens








Nota de Traduccion (Spanish Translation Note)
---------------------------------------------
#Nota del tipeo:la letra pronunciada ENIE aparece en este archivo 
#como ni (letra "n" y letra "i") para mayor compatibilidad con todos 
#los visores de texto.
#Asi mismo no se han aplicado las tildes(acentos).
#Estos no son errores de tipeo. Si llegara a encontrar algun otro error
#en cualquiera de los archivos con extension ".es" por favor 
#hagamelo saber a alexkrohn@fastmail.fm
#  Muchas gracias
#      Alex