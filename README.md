Ardourd
-------

Ardourd provides composition and mixing services for the Ayyi audio production system.


Building
--------

The procedure for building this program is exactly the same as building Ardour

For details please see:

 * http://ardour.org/
 * http://ardour.org/building

The essential steps are:
```
	./waf configure
	./waf
```


Installation
------------

Note that, if you prefer, this program can be run without installation.

Files are installed into the following directories.
```
	PREFIX/bin
	PREFIX/lib/ardourd
	PREFIX/share/ardourd
	PREFIX/etc/ardourd
```
where PREFIX is normally either /usr/ or /usr/local/

It is installed completely independently of the regular Ardour application
and will not overwrite any other files.


Running
-------

The program is designed to be run as a service and is normally started on demand by client
applications. Service start up can be configured in the file:
```
/usr/share/dbus-1/services/ardourd.service
```

It can also be started manually using the ardourd/start_ardourd script.
To run from this directory without installation, run:
```
ardourd/start_ardourd
```

Logging information is sent to /tmp/ardourd.log

The main client UI can be found [here](https://gitlab.com/ayyi.org/canvas)
