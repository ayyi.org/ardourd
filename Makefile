# see ardourd/version.cc, lib/ardour/revision.cc
version=0.2.3
name=ardourd-$(version).0
name2=ardourd-$(version)
tarball=$(name).tar.bz2
tarball2=$(name2).tar.bz2
tmpdir=dist_tmp

all: waf

install:
	./waf install

dist:
	@#APPNAME=ardourd ./waf dist --vst
	APPNAME=ardourd ./waf dist
	@##cp libs/fst/vstsdk2.3.zip ardourd-$(version)/libs/fst/
	@#
	if [ -e $(tmpdir) ]; then rm -r $(tmpdir); fi
	mkdir -p $(tmpdir)
	cp $(tarball) $(tmpdir)/
	cd $(tmpdir) && tar xvf $(tarball)
	if [ -e $(tmpdir)/$(name)/vendor ]; then rm -r $(tmpdir)/$(name)/vendor; fi
	rm $(tmpdir)/$(name)/DOCUMENTATION/TRANSLATORS
	cd $(tmpdir)/$(name) && if [ ! -z "`ls -A *bz2`" ]; then rm *bz2; fi
	cd $(tmpdir)/$(name) && if [ ! -z "`ls -A __*`" ]; then rm __*; fi
	cd $(tmpdir)/$(name) && if [ ! -z "`ls -A TODO*`" ]; then rm TODO*; fi
	cd $(tmpdir) && \
		tar cjf $(tarball) $(name)
	mv $(tarball) $(name2).orig.tar.bz2
	mv $(tmpdir)/$(tarball) $(tarball2)
	rm -r $(tmpdir)

clean:
	./waf clean

waf:
	./waf

cscope: cscope.out

cscope.out: cscope.files
	cscope -b

cscope.files:
	find . -name '*.[ch]' -o -name '*.cc' > $@

.PHONY: all cscope.files cscope waf
